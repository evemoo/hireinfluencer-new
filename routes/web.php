<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Authentication Routes...
$router->get('login', 'Auth\LoginController@showLoginForm')->name('login')->middleware('geo');
$router->post('login', 'Auth\LoginController@login')->middleware('geo');
$router->post('logout', 'Auth\LoginController@logout')->name('logout');
$router->get('blocked', 'Auth\LoginController@blocked')->name('blocked');

$router->get('user/login/{service}', ['as' => 'social.login', 'uses' => 'Auth\LoginController@redirectToProvider']);
$router->get('user/login/{service}/callback', 'Auth\LoginController@handleProviderCallback');

// Registration Routes...
$router->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware('geo');
$router->post('register', 'Auth\RegisterController@register')->middleware('geo');

// Password Reset Routes...
$router->get('password/reset',         'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$router->post('password/reset',        'Auth\ResetPasswordController@reset')->name('password.reset');

$router->get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/error', 'HomeController@index')->name('error');


/*$router->get('customer/dashboard', function () {
    return view('backend.customer.dashboard.index');
})->middleware(['auth','auth.customer'])->name('customer.dashboard');*/


$router->get('admin/error/{code}', 'Backend\ErrorController@index')->name('admin.error');


$router->get('stripe/card', 'StripeController@payWithStripe')->name('addmoney.stripe');
$router->post('stripe/card', 'StripeController@stripe')->name('stripe');

Route::get('paywithpaypal', array('as' => 'addmoney.paywithpaypal','uses' => 'AddMoneyController@payWithPaypal',));
Route::post('paypal', array('as' => 'addmoney.paypal','uses' => 'AddMoneyController@postPaymentWithpaypal',));
Route::get('paypal', array('as' => 'payment.status','uses' => 'AddMoneyController@getPaymentStatus',));

Route::get('paypal/fund', array('as' => 'payment.fund.status',
    'uses' => 'Backend\Customer\FundController@getPaymentStatus',));


$router->get('markAsRead', function(){
	auth()->user()->unreadNotifications->markAsRead();
});

$router->get('verify', ['as' => 'verify', 'uses' => 'Auth\VerificationController@verify']);
$router->get('verify/{token}', ['as' => 'user.verification', 'uses' => 'Auth\VerificationController@verify']);

