<?php
/**
 * Customer Router
 */
$router->group([
    'prefix'     => 'customer/',
    'as'         => 'customer.',
    'namespace'  => 'Backend\Customer\\',
    'middleware' => [ 'auth' ]
] , function () use ( $router) {

    //dashboard route
    $router->get('dashboard', [
        'as'   => 'dashboard',
        'uses' => 'DashboardController@index'
    ]);

    //fund
    $router->get('fund', [
        'as'   => 'fund.index',
        'uses' => 'FundController@index'
    ]);

    $router->post('fund/store', [
        'as'   => 'fund.store',
        'uses' => 'FundController@store'
    ]);

    $router->post('fund/stripe/store', [
        'as'   => 'fund.stripe.store',
        'uses' => 'FundController@stripStore'
    ]);

    //profile
    $router->get('profile', [
        'as'   => 'profile.index',
        'uses' => 'ProfileController@index'
    ]);

    $router->post('updateAvatar', [
        'as'   => 'profile.updateAvatar',
        'uses' => 'ProfileController@updateAvatar'
    ]);

    $router->post('update', [
        'as'   => 'profile.update',
        'uses' => 'ProfileController@update'
    ]);

    //Slots
    $router->get('slot', [
        'as'   => 'slot.index',
        'uses' => 'SlotController@index'
    ]);

    $router->get('slot/new', [
        'as'   => 'slot.create',
        'uses' => 'SlotController@create'
    ]);

    $router->post('slot/new', [
        'as'   => 'slot.store',
        'uses' => 'SlotController@store'
    ]);

    $router->get('slot/edit', [
        'as'   => 'slot.edit',
        'uses' => 'SlotController@edit'
    ]);

    $router->post('slot/update', [
        'as'   => 'slot.update',
        'uses' => 'SlotController@update'
    ]);

    $router->post('slot/delete', [
        'as'   => 'slot.delete',
        'uses' => 'SlotController@delete'
    ]);

    //Reviews
    $router->get('review', [
        'as'   => 'review.index',
        'uses' => 'ReviewController@index'
    ]);

    $router->get('review/new', [
        'as'   => 'review.create',
        'uses' => 'ReviewController@create'
    ]);

    $router->post('review/new', [
        'as'   => 'review.store',
        'uses' => 'ReviewController@store'
    ]);

    $router->get('review/edit', [
        'as'   => 'review.edit',
        'uses' => 'ReviewController@edit'
    ]);

    $router->post('review/update', [
        'as'   => 'review.update',
        'uses' => 'ReviewController@update'
    ]);

    $router->post('review/delete', [
        'as'   => 'review.delete',
        'uses' => 'ReviewController@delete'
    ]);

    //Projects
    $router->get('project', [
        'as'   => 'project.index',
        'uses' => 'ProjectController@index'
    ]);

    $router->get('project/new', [
        'as'   => 'project.create',
        'uses' => 'ProjectController@create'
    ]);

    $router->post('project/new', [
        'as'   => 'project.store',
        'uses' => 'ProjectController@store'
    ]);

    $router->get('project/edit', [
        'as'   => 'project.edit',
        'uses' => 'ProjectController@edit'
    ]);

    $router->post('project/update', [
        'as'   => 'project.update',
        'uses' => 'ProjectController@update'
    ]);

    $router->post('project/delete', [
        'as'   => 'project.delete',
        'uses' => 'ProjectController@delete'
    ]);

    //Conversations
    $router->get('convo', [
        'as'   => 'convo.index',
        'uses' => 'ConversationController@index'
    ]);

    $router->get('convo/new', [
        'as'   => 'convo.create',
        'uses' => 'ConversationController@create'
    ]);

    $router->post('convo/new', [
        'as'   => 'convo.store',
        'uses' => 'ConversationController@store'
    ]);

    $router->get('convo/edit', [
        'as'   => 'convo.edit',
        'uses' => 'ConversationController@edit'
    ]);

    $router->post('convo/update', [
        'as'   => 'convo.update',
        'uses' => 'ConversationController@update'
    ]);

    $router->post('convo/delete', [
        'as'   => 'convo.delete',
        'uses' => 'ConversationController@delete'
    ]);

} );