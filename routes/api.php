<?php

$router->group([
    'prefix'     => 'api/',
    'as'         => 'api.',
    'namespace'  => 'Api\\',
    'middleware' => [ 'auth', 'auth.admin']
], function () use ( $router) {

    //package api

    $router->get('/admin/client/{query?}', 'UserController@getClient')
        ->name('admin.client');



});

// Social route

$router->group([
    'prefix'     => 'api/',
    'as'         => 'api.',
    'namespace'  => 'Social\\',
//    'middleware' => [ 'auth', 'auth.customer' ]
], function () use ( $router) {

    // Youtube routes
    $router->get('youtube/user/{profile}', 'YoutubeController@getStatistics');
    $router->get('youtube/video/{video_id}', 'YoutubeController@getVideoStatistics');

    // Instagram routes
    $router->get('instagram/user/{profile}', 'InstagramController@getStatistics');
    $router->get('instagram/post/{post_id}', 'InstagramController@getPostStatistics');

    // Twitter routes
    $router->get('twitter/user/{profile}', 'TwitterController@getStatistics');
    $router->get('twitter/search/{query}', 'TwitterController@searchTweet');
    $router->get('twitter/tweet/{tweet_id}', 'TwitterController@getTweet');

    // Facebook routes
    $router->get('facebook/search/{query}', 'FacebookController@search');
    $router->get('facebook/page/{profile}', 'FacebookController@getStatistics');
    $router->get('facebook/rating/{page_id}', 'FacebookController@getPageRating');
    $router->get('facebook/post/{post_id}', 'FacebookController@getPostStatistics');

});

Route::group([
    'prefix'     => 'api/influencer',
    'as'         => 'api.influencer',
    'namespace'  => 'Api\Influencer\Controllers'
], function () {

    Route::post('/auth/refresh_token',          [ 'uses' => 'Auth\LoginController@login' ]);
    // Auth
    Route::post('/auth/login',          [ 'uses' => 'Auth\LoginController@login' ]);
    Route::post('/auth/register',       [ 'uses' => 'Auth\RegisterController@register' ]);

//    Route::group([ 'middleware' => [ 'auth:api' ] ], function () {
//
//    });

});

Route::group([
    'prefix'     => 'api/influencer',
    'as'         => 'api.influencer.',
    'namespace'  => 'Api\Influencer\Controllers\Profile'
], function () {

    /*Route::get('/authUser', function (Request $request) {
        return auth()->user();
    });*/
    Route::get('/authUser', ['as' => 'authUser', 'uses' => 'ProfileController@getAuthenticatedUser']);

    //Influencer Profile Routes
    Route::post('/signup', ['as' => 'signup', 'uses' => 'ProfileController@create']);
    Route::post('/verify', ['as' => 'verify', 'uses' => 'ProfileController@verifyUser']);
    Route::post('/{username}/update', ['as' => 'update', 'uses' => 'ProfileController@update']);
    Route::post('/{username}/delete', ['as' => 'delete', 'uses' => 'ProfileController@delete']);
    Route::get('/{username}', ['as' => 'influencer', 'uses' => 'ProfileController@index']);

    //Slots Route
    Route::get('/{username}/slot', ['as' => 'slot', 'uses' => 'SlotController@getInfluencerSlots']);
    Route::get('/{username}/slot/{id}', ['as' => 'slot.detail', 'uses' => 'SlotController@getSlotDetails']);

    //Routes Related to Slot Gallery
    Route::get('/{username}/slot/{id}/gallery', ['as' => 'slot.gallery', 'uses' => 'SlotController@getSlotGallery']);

    //Routes Related to Slot Package
    Route::get('/{username}/slot/{id}/packages', ['as' => 'slot.packages', 'uses' => 'SlotController@getSlotPackages']);
    Route::get('/{username}/slot/{id}/packages/{package_id}', ['as' => 'slot.packages.details', 'uses' => 'SlotController@getSlotPackageDetails']);
    Route::get('/{username}/slot/{id}/packages/{package_id}/projects', ['as' => 'slot.packages.projects', 'uses' => 'SlotController@getSlotPackageProjects']);

    //Routes Related to Slot Project
    Route::get('/{username}/slot/{id}/projects', ['as' => 'slot.projects', 'uses' => 'SlotController@getSlotProjects']);
    Route::get('/{username}/slot/{id}/projects/{project_id}', ['as' => 'slot.projects.details', 'uses' => 'SlotController@getProjectDetails']);
    Route::get('/{username}/slot/{id}/reviews', ['as' => 'slot.reviews', 'uses' => 'SlotController@getSlotReviews']);
    Route::get('/{username}/slot/{id}/project/{project_id}/reviews', ['as' => 'slot.projects.reviews', 'uses' => 'SlotController@getProjectReviews']);
    Route::get('/{username}/slot/{id}/project/{project_id}/chat', ['as' => 'slot.projects.chat', 'uses' => 'SlotController@getProjectChat']);

    //Social Media Routes
    Route::get('/{username}/{socialmedia}', ['as' => 'socialmedias', 'uses' => 'SocialMediaController@getInfluencerSocialMediaDatas']);
    Route::post('/{username}/{socialmedia}/create', ['as' => 'socialmedias.add', 'uses' => 'SocialMediaController@addInfluencerSocialMediaDatas']);
    Route::post('/{username}/{socialmedia}/update', ['as' => 'socialmedias.update', 'uses' => 'SocialMediaController@editInfluencerSocialMediaDatas']);
    Route::post('/{username}/{socialmedia}/delete', ['as' => 'socialmedias.delete', 'uses' => 'SocialMediaController@deleteInfluencerSocialMediaDatas']);

});