<?php
/**
 * Admin Router
 */
$router->group([
    'prefix'     => 'admin/',
    'as'         => 'admin.',
    'namespace'  => 'Backend\Admin\\',
    'middleware' => [ 'auth', 'auth.admin', 'authorize' ]
] , function () use ( $router) {

    //user route

    $router->get('user/quick-details/{id}', [
        'as'   => 'user.quickdetails',
        'uses' => 'UserController@getUserQuickDetails'
    ]);

    $router->get('user/create/{role}', [
        'as'   => 'user.create',
        'uses' => 'UserController@create'
    ]);

    $router->post('user/store/{role}', [
        'as'   => 'user.store',
        'uses' => 'UserController@store'
    ]);

    $router->get('user/{role}', [
        'as'   => 'user.index',
        'uses' => 'UserController@index'
    ]);

    $router->get('user/{user}/message', [
        'as'   => 'user.msg',
        'uses' => 'UserController@getMessage'
    ]);

    $router->post('user/{user}/message/send', [
        'as'   => 'user.msg.send',
        'uses' => 'UserController@sendMessage'
    ]);

    $router->get('user/{user}/view/overview', [
        'as'   => 'user.view.overview',
        'uses' => 'UserController@view'
    ]);

    $router->get('user/{user}/view/setting', [
        'as'   => 'user.view.setting',
        'uses' => 'UserController@view'
    ]);

    $router->get('user/{user}/view/setting/edit', [
        'as'   => 'user.view.setting.edit',
        'uses' => 'UserController@view'
    ]);

    $router->get('user/{user}/view/slot', [
        'as'   => 'user.view.slot',
        'uses' => 'UserController@view'
    ]);

    $router->get('user/{user}/view/slot/{id}', [
        'as'   => 'user.view.slot.detail',
        'uses' => 'UserController@slotDetail'
    ]);

    $router->get('user/{user}/view/slot/project/{slug}', [
        'as'   => 'user.view.slot.project',
        'uses' => 'UserController@projectDetail'
    ]);

    $router->get('user/{user}/view/social-media', [
        'as'   => 'user.view.socialmedia',
        'uses' => 'UserController@socialMediaDetail'
    ]);

    $router->get('user/{user}/view/review', [
        'as'   => 'user.view.review',
        'uses' => 'UserController@view'
    ]);

    $router->get('user/{user}/view/review/{id}/edit', [
        'as'   => 'user.view.review.edit',
        'uses' => 'UserController@reviewEdit'
    ]);

    $router->post('user/{user}/view/review/{id}/delete', [
        'as'   => 'user.view.review.delete',
        'uses' => 'UserController@reviewDelete'
    ]);

    $router->post('user/{user}/view/review/{id}/update', [
        'as'   => 'user.view.review.update',
        'uses' => 'UserController@reviewUpdate'
    ]);

    $router->get('user/{user}/view/help', [
        'as'   => 'user.view.help',
        'uses' => 'UserController@view'
    ]);

    $router->get('user/{user}/edit', [
        'as'   => 'user.edit',
        'uses' => 'UserController@edit'
    ]);

    $router->post('user/{user}/update', [
        'as'   => 'user.update',
        'uses' => 'UserController@update'
    ]);

    $router->post('updateAvatar', [
        'as'   => 'user.updateAvatar',
        'uses' => 'UserController@updateAvatar'
    ]);

    $router->post('user/{user}/delete', [
        'as'   => 'user.delete',
        'uses' => 'UserController@delete'
    ]);

    $router->post('user/{user}/ban', [
        'as'   => 'user.ban',
        'uses' => 'UserController@banUser'
    ]);

    $router->get('reset/password', [
        'as'   => 'user.reset.password',
        'uses' => 'PasswordResetController@index'
    ]);

    $router->post('reset/password', [
        'as'   => 'user.reset.password',
        'uses' => 'PasswordResetController@reset'
    ]);

    $router->get('slots/getSlotReview', [
        'as' => 'user.getSlotReview',
        'uses' => 'UserController@getSlotReview'
    ]);

    //category controller routes
    $router->get('category', [
        'as'    => 'category.index',
        'uses'  => 'CategoryController@index'
    ]);

    $router->get('category/create', [
        'as'    => 'category.create',
        'uses'  => 'CategoryController@create'
    ]);

    $router->post('category/store', [
        'as'    => 'category.store',
        'uses'  => 'CategoryController@store'
    ]);

    $router->get('category/{id}/edit', [
        'as'    => 'category.edit',
        'uses'  => 'CategoryController@edit'
    ]);

    $router->post('category/{id}/update', [
        'as'    => 'category.update',
        'uses'  => 'CategoryController@update'
    ]);

    $router->post('category/{id}/delete', [
        'as'    => 'category.delete',
        'uses'  => 'CategoryController@destroy'
    ]);

    //category subcategory level2

    $router->get('category/level2/{cat_id}/{type}', [
        'as'    => 'category.level2.index',
        'uses'  => 'CategoryController@index'
    ]);

    $router->get('category/level2/{cat_id}/{type}/create', [
        'as'    => 'category.level2.create',
        'uses'  => 'CategoryController@create'
    ]);

    $router->get('category/level2/{id}/{cat_id}/{type}/edit', [
        'as'    => 'category.level2.edit',
        'uses'  => 'CategoryController@edit'
    ]);

    //category subcategory level3

    $router->get('category/level3/{cat_id}/{type}', [
        'as'    => 'category.level3.index',
        'uses'  => 'CategoryController@index'
    ]);

    $router->get('category/level3/{cat_id}/{type}/create', [
        'as'    => 'category.level3.create',
        'uses'  => 'CategoryController@create'
    ]);

    $router->get('category/level3/{id}/{cat_id}/{type}/edit', [
        'as'    => 'category.level3.edit',
        'uses'  => 'CategoryController@edit'
    ]);

    //category subcategory level4

    $router->get('category/level4/{cat_id}/{type}', [
        'as'    => 'category.level4.index',
        'uses'  => 'CategoryController@index'
    ]);

    $router->get('category/level4/{cat_id}/{type}/create', [
        'as'    => 'category.level4.create',
        'uses'  => 'CategoryController@create'
    ]);

    $router->get('category/level4/{id}/{cat_id}/{type}/edit', [
        'as'    => 'category.level4.edit',
        'uses'  => 'CategoryController@edit'
    ]);

    //Social Media
    $router->get('social-media', [
        'as'    => 'socialmedia.index',
        'uses'  => 'SocialMediaController@index'
    ]);

    $router->get('social-media/create', [
        'as'    => 'socialmedia.create',
        'uses'  => 'SocialMediaController@create'
    ]);

    $router->post('social-media/store', [
        'as'    => 'socialmedia.store',
        'uses'  => 'SocialMediaController@store'
    ]);

    $router->post('social-media/{id}/delete', [
        'as'    => 'socialmedia.delete',
        'uses'  => 'SocialMediaController@delete'
    ]);

    //Social Media Attributes
    $router->get('social-media/attributes', [
        'as'    => 'socialmedia.attributes.index',
        'uses'  => 'SocialMediaController@indexAttributes'
    ]);

    $router->get('social-media/attributes/create', [
        'as'    => 'socialmedia.attributes.create',
        'uses'  => 'SocialMediaController@createAttribute'
    ]);

    $router->post('social-media/attributes/store', [
        'as'    => 'socialmedia.attributes.store',
        'uses'  => 'SocialMediaController@storeAttribute'
    ]);

    $router->post('social-media/attributes/{id}/delete', [
        'as'    => 'socialmedia.attributes.delete',
        'uses'  => 'SocialMediaController@deleteAttribute'
    ]);

    //RoleController routes
    $router->get('roles',                     [ 'as' => 'roles.index',    'uses' => 'Acl\RolesController@index']);
    $router->get('roles/create',              [ 'as' => 'roles.create',   'uses' => 'Acl\RolesController@create']);
    $router->post('roles/store',              [ 'as' => 'roles.store',    'uses' => 'Acl\RolesController@store']);
    $router->get('roles/show/{roleId}',       [ 'as' => 'roles.show',     'uses' => 'Acl\RolesController@show']);
    $router->get('roles/{roleId}/edit',       [ 'as' => 'roles.edit',     'uses' => 'Acl\RolesController@edit']);
    $router->post('roles/{roleId}/update',    [ 'as' => 'roles.update',   'uses' => 'Acl\RolesController@update']);
    $router->post('roles/getInfo',            [ 'as' => 'roles.get-info', 'uses' => 'Acl\RolesController@getInfo']);
    $router->get('roles/{roleId}/confirm-delete', [
        'as'   => 'roles.confirm-delete',
        'uses' => 'Acl\RolesController@getModalDelete'
    ]);
    $router->post('roles/{roleId}/delete', [
        'as'   => 'roles.delete',
        'uses' => 'Acl\RolesController@destroy'
    ]);

    //PermissionController routes
    $router->get('permissions',        [
        'as'   => 'permissions.index',
        'uses' => 'Acl\PermissionsController@index'
    ]);
    $router->get('permissions/create', [
        'as'   => 'permissions.create',
        'uses' => 'Acl\PermissionsController@create'
    ]);
    $router->post('permissions/store', [
        'as'   => 'permissions.store',
        'uses' => 'Acl\PermissionsController@store'
    ]);
    $router->get('permissions/show/{permissionId}', [
        'as' => 'permissions.show',
        'uses' => 'Acl\PermissionsController@show'
    ]);
    $router->get('permissions/{permissionId}/edit', [
        'as'   => 'permissions.edit',
        'uses' => 'Acl\PermissionsController@edit'
    ]);
    $router->post('permissions/{permissionId}/update', [
        'as'   => 'permissions.update',
        'uses' => 'Acl\PermissionsController@update'
    ]);
    $router->get('permissions/generate', [
        'as'   => 'permissions.generate',
        'uses' => 'Acl\PermissionsController@generate'
    ]);
    $router->post('routes/savePerms', [
        'as'   => 'routes.save-perms',
        'uses' => 'Acl\RoutesController@savePerms'
    ]);
    $router->get('routes/search', [
        'as'   => 'routes.search',
        'uses' => 'Acl\RoutesController@searchByName'
    ]);
    $router->post('routes/getInfo', [
        'as'   => 'routes.get-info',
        'uses' => 'Acl\RoutesController@getInfo'
    ]);
    $router->get('permissions/{permissionId}/confirm-delete', [
        'as'   => 'permissions.confirm-delete',
        'uses' => 'Acl\PermissionsController@getModalDelete'
    ]);
    $router->get('permissions/{permissionId}/delete', [
        'as'   => 'permissions.delete',
        'uses' => 'Acl\PermissionsController@destroy'
    ]);

    //RouteController routes
    $router->get('routes', [
        'as'   => 'routes.index',
        'uses' => 'Acl\RoutesController@index'
    ]);

    $router->get('routes/create', [
        'as'   => 'routes.create',
        'uses' => 'Acl\RoutesController@create'
    ]);

    $router->post('routes/store', [
        'as'   => 'routes.store',
        'uses' => 'Acl\RoutesController@store'
    ]);

    $router->get('routes/{routeId}/edit', [
        'as'   => 'routes.edit',
        'uses' => 'Acl\RoutesController@edit'
    ]);

    $router->post('routes/{routeId}/update', [
        'as'   => 'routes.update',
        'uses' => 'Acl\RoutesController@update'
    ]);

    $router->get('routes/{routeId}/confirm-delete', [
        'as'   => 'routes.confirm-delete',
        'uses' => 'Acl\RoutesController@getModalDelete'
    ]);
    $router->get('routes/{routeId}/delete', [
        'as'   => 'routes.delete',
        'uses' => 'Acl\RoutesController@destroy'
    ]);

    $router->get('routes/load', [
        'as'   => 'routes.load',
        'uses' => 'Acl\RoutesController@load'
    ]);

    //dashboard route
    $router->get('dashboard', [
        'as'   => 'dashboard',
        'uses' => 'DashboardController@index'
    ]);

    //log route
    $router->get('log', [
        'as'   => 'log.index',
        'uses' => 'LogController@index'
    ]);

    $router->get('log/{log}/view', [
        'as'   => 'log.view',
        'uses' => 'LogController@view'
    ]);

    $router->post('log/{log}/update', [
        'as'   => 'log.update',
        'uses' => 'LogController@update'
    ]);

    //setting route
    $router->get('setting', [
        'as'   => 'setting.index',
        'uses' => 'SystemSettingController@index'
    ]);

    $router->post('setting/update', [
        'as'   => 'setting.update',
        'uses' => 'SystemSettingController@update'
    ]);

    //Email Template route
    $router->get('email_template', [
        'as'   => 'email_template.index',
        'uses' => 'EmailTemplateController@index'
    ]);

    $router->get('email_template/{id}/edit', [
        'as'   => 'email_template.edit',
        'uses' => 'EmailTemplateController@edit'
    ]);

    $router->post('email_template/{id}/update', [
        'as'   => 'email_template.update',
        'uses' => 'EmailTemplateController@update'
    ]);
    
} );


