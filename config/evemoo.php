<?php

return [

    'assets_path' => [
        'bower_components' => 'bower_components/'
    ],

    'social_medias' => [
        'facebook' => [
            'title' => 'Facebook',
            'icon' => 'fa fa-facebook',
        ],
        'instagram' => [
            'title' => 'Instagram',
            'icon' => 'fa fa-instagram',
        ],
        'twitter' => [
            'title' => 'Twitter',
            'icon' => 'fa fa-twitter',
        ],
        'youtube' => [
            'title' => 'Youtube',
            'icon' => 'fa fa-youtube',
        ],
    ],

    'social_media_attributes' => [
        'likes'         => 'Likes',
        'followers'     => 'Followers',
        'subscribers'   => 'Subscribers'
    ],

    'route' => [
        'admin-login' => 'login',
        'admin-logout' => 'logout',
        'admin-dashboard' => 'admin/dashboard',
        'front-login' => 'user/login',
        'front-logout' => 'user/logout',
        'front-login-reset' => 'user/login-reset',
        'front-dashboard' => 'panel/my-account',
    ],

    // Error Codes
    'error-codes' => [
        'system-error'            => 'system-error',
        'no-error-code-passed'    => 'no-error-code-passed',
        'invalid-request'         => 'invalid-request',
        'un-authorized-request'   => 'un-authorized-request',
        'not-found'               => 'not-found',
        'no-role-assigned'        => 'no-role-assigned',
        'system-config-not-found' => 'system-config-not-found',
        'no-page-type'            => 'no-page-type',
        'model-not-saved'         => 'model-not-saved',
        'model-not-updated'       => 'model-not-updated',
        'cart-not-added'          => 'cart-not-added',
    ],

    'user' => [
        'roles' => [
            'admin' => [
                'title' => 'Admin',
                'slug'  => 'admin'
            ],
            'customer' => [
                'title' => 'Customer',
                'slug'  => 'customer'
            ]
        ]
    ],

    'log' => [
        'error_process_status' => [
            'not_seen' => [
                'title' => 'Not Seen',
                'key'   => 'not_seen'
            ],
            'seen' => [
                'title' => 'Seen',
                'key'   => 'seen'
            ],
            'processing' => [
                'title' => 'Processing',
                'key'   => 'processing'
            ],
            'closed' => [
                'title' => 'Closed',
                'key'   => 'closed'
            ]
        ],
        'error_level' => [
            'warning' => [
                'title' => 'Warning',
                'key'   => 'warning',
            ],
            'urgent' => [
                'title' => 'Urgent',
                'key'   => 'urgent',
            ],
        ]
    ],

    'lang' => [
        'default' => 'en',
        'options' => [
            'en' => [
                'code'  => 'en',
                'title' => 'English',
            ],
            'np' => [
                'code'  => 'np',
                'title' => 'Nepali'
            ],
            'fra' => [
                'code'  => 'fra',
                'title' => 'France'
            ]
        ],

    ],

    'package' => [
        'package' => [
            'title' => 'Package',
            'key'   => 'package'
        ],
        'custom_package' => [
            'title' => 'Custom Package',
            'key'   => 'custom_package'
        ],
        'enquiry_package' => [
            'title' => 'Enquiry Package',
            'key'   => 'enquiry_package'
        ]
    ],

    'currency' => [
        'default' => 'usd',
        'options' => [
            'usd' => [
                'symbol'    => '$',
                'code'      => 'USD',
                'key'       => 'usd'
            ],
            'npr' => [
                'symbol'  => 'Rs.',
                'code'    => 'NPR',
                'key'     => 'npr'
            ]
        ],
    ],

    'order' => [
        'order_status' => [
            'order_confirmation' =>[
                'title' => 'Order Confirmation',
                'key'   => 'order_confirmation'
            ],
            'order_confirmed' => [
              'title' => 'Order Confirmed',
                'key' => 'order_confirmed'
            ],
            'order_complete' => [
                'title' => 'Order Complete',
                'key'   => 'order_complete'
            ]
        ]
    ],

    //services
    'services' => [
       'facebook'  => [
           'title' => 'Facebook',
           'key'  => 'facebook',
           'package_types' => [
               'likes' => [
                   'title' => 'Likes',
                   'key'   => 'likes',
                   'link_placeholder' => 'Enter facebook fan page url',
                   'auto'  => 1
               ],
               'post-likes' => [
                   'title' => 'Post Likes',
                   'key'   => 'post-likes',
                   'link_placeholder' => 'Enter facebook post url',
                   'auto'  => 1
               ],
               'share' => [
                   'title' => 'Share',
                   'key'   => 'share',
                   'link_placeholder' => 'Enter facebook post url',
                   'auto'  => 1
               ],
               '5-star-rating' => [
                   'title' => '5 Star Rating',
                   'key'   => '5-star-rating',
                   'link_placeholder' => 'Enter facebook fan page url',
                   'auto'  => 0
               ],
               'comment' => [
                   'title' => 'Comment',
                   'key'   => 'comment',
                   'link_placeholder' => 'Enter facebook post url',
                   'auto'  => 1
               ],
           ]
       ],
       'youtube'   => [
            'title' => 'Youtube',
            'key'  => 'youtube',
            'package_types' => [
                'views' => [
                    'title' => 'Views',
                    'key'   => 'views',
                    'link_placeholder' => 'Enter youtube video url',
                    'auto'  => 1
                ],
                'likes' => [
                    'title' => 'Likes',
                    'key'   => 'likes',
                    'link_placeholder' => 'Enter youtube video url',
                    'auto'  => 1
                ],
                'comment' => [
                    'title' => 'Comment',
                    'key'   => 'comment',
                    'link_placeholder' => 'Enter youtube video url',
                    'auto'  => 1
                ],
                'share' => [
                    'title' => 'Share',
                    'key'   => 'share',
                    'link_placeholder' => 'Enter youtube video url',
                    'auto'  => 0
                ],
                'subscriber' => [
                    'title' => 'Subscriber',
                    'key'   => 'subscriber',
                    'link_placeholder' => 'Enter youtube channel url',
                    'auto'  => 1
                ]
            ]
       ],
       'instagram' => [
            'title' => 'Instagram',
            'key'  => 'instagram',
            'package_types' => [
                'likes' => [
                    'title' => 'Likes',
                    'key'   => 'likes',
                    'link_placeholder' => 'Enter instagram photo url',
                    'auto'  => 1
                ],
                'share' => [
                    'title' => 'Share',
                    'key'   => 'share',
                    'link_placeholder' => 'Enter instagram photo url',
                    'auto'  => 0
                ],
                'comment' => [
                    'title' => 'Comment',
                    'key'   => 'comment',
                    'link_placeholder' => 'Enter instagram photo url',
                    'auto'  => 1
                ],
                'followers' => [
                    'title' => 'Followers',
                    'key'   => 'followers',
                    'link_placeholder' => 'Enter instagram username',
                    'auto'  => 1
                ]
            ]
        ],
       'twitter'   => [
            'title' => 'Twitter',
            'key'  => 'twitter',
            'package_types' => [
                'likes' => [
                    'title' => 'Likes',
                    'key'   => 'likes',
                    'link_placeholder' => 'Enter tweet url',
                    'auto'  => 1
                ],
                'followers' => [
                    'title' => 'Followers',
                    'key'   => 'followers',
                    'link_placeholder' => 'Enter twitter username',
                    'auto'  => 1
                ],
                'retweets' => [
                    'title' => 'Retweets',
                    'key'   => 'retweets',
                    'link_placeholder' => 'Enter tweet url',
                    'auto'  => 1
                ],
            ]
        ]
    ],

    // TODO: Remove Un-wanted configurations & seeds
    'email_patterns' => [
        '{APP_LOGO}'                => 'Our application logo',
        '{APP_NAME}'                => 'Our application name',
        '{USER_FULL_NAME}'          => 'User full name',
        '{USER_NAME}'               => 'User name',
        '{USER_EMAIL}'              => 'User email address',
        '{USER_PHONE}'              => 'User Phone address',
        '{USER_ADDRESS}'            => 'User Address',
        '{ITEM_TITLE}'              => 'Item title name',
        '{ADS_LIST}'                => 'List of listing\'s, used when sending advertisement list.',
        '{UNSUB_LINK}'              => 'Un-subscribe link',
        '{OFFERED_PRICE}'           => 'Price offered to advertisement(product)',
        '{MESSAGE}'                 => 'Messages to User',
        '{EMAIL_SUBJECT}'           => 'Subject for email',
        '{VERIFICATION_TOKEN}'      => 'Password Verification token to reset the user password.',
        '{USER_ACTIVATION_LINK}'    => 'Verification link to activate user account',
        '{WEB_LINK}'                => 'Url to visit or our website link.',
        '{HOTEL_NAME}'              => 'Hotel name',
        '{NO_OF_ROOMS}'             => 'Total no of room\'s user wants',
        '{NO_OF_PEOPLES}'           => 'Total no of people\'s with user',
        '{CHECK_IN_DATE}'           => 'Date user want\'s to check in hotel',
        '{CHECK_OUT_DATE}'          => 'ate user want\'s to check out from hotel'
    ],

    'email_templates' => [
        [
            'title' => 'Welcome Template',
            'slug'  => 'email_welcome_template',
            'content' => '<p>Hi</p><p>{USER_FULL_NAME} ,Welcome to the {APP_NAME} .</p><p>Regards,</p><p>{WEB_LINK}</p>'
        ],
        [
            'title' => 'Advertisement List',
            'slug'  => 'email_advertise_template',
            'content' => '<p>Hi</p><p>{USER_NAME} ,Everyday 100s of ads is posted :</p><p>{ADS_LIST}</p><p>Regards,</p><p>{WEB_LINK}</p>'
        ],
        [
            'title' => 'Send Message To User',
            'slug'  => 'email_send_message',
            'content' => '<p>Hi</p><p>{USER_NAME} ,<p>{MESSAGE}</p><p>Regards,</p><p>{WEB_LINK}</p>'
        ],
        [
            'title'  => 'User sending price offers to ads author.',
            'slug'   => 'email_send_offer',
            'content'=> '<p>Hi</p><p>{USER_NAME} ,<p>This is {OFFERED_PRICE} rs offered by me.</p><p>Regards,</p><p>{WEB_LINK}</p>'
        ],
        [
            'title'     => 'Password Verification token to reset the user password.',
            'slug'      => 'reset_user_password',
            'content'   => '<p>Hi</p><p>{USER_NAME} ,<p>Please, click the link below to reset your password. <br>{VERIFICATION_TOKEN}</p><p>Regards,</p><p>{WEB_LINK}</p>'
        ],
        [
            'title'     => 'Verification token to activate user account.',
            'slug'      => 'send_user_activation_code',
            'content'   => '<p>Hi</p><p>{USER_FULL_NAME} ,<p>Please, click the link below to activate your account. <br>{USER_ACTIVATION_LINK}</p><p>Regards,</p><p>{WEB_LINK}</p>'
        ],
        [
            'title'     => 'Hotel Booking/Enquiry ',
            'slug'      => 'hotel_booking_enquiry',
            'content'   => '<table cellpadding="0" cellspacing="0"  style="border-collapse: collapse;"><tr><td style="border-bottom: 1px solid #dedede;padding-bottom: 15px;"><p>New Enquiry To {HOTEL_NAME}</p></td></tr><tr><td colspan="7" rowspan="" headers="" height="8"></td></tr><tr><td><p><span style="font-weight: bold;margin-right: 7px">Name :</span> {USER_NAME}</p><p><span style="font-weight: bold;margin-right: 7px">Email :</span> {USER_EMAIL}</p><p><span style="font-weight: bold;margin-right: 7px">Contact :</span> {USER_PHONE}</p><p><span style="font-weight: bold;margin-right: 7px">No. of room\'s :</span> {NO_OF_ROOMS}</p><p><span style="font-weight: bold;margin-right: 7px">No. of people\'s :</span> {NO_OF_PEOPLES}</p><p><span style="font-weight: bold;margin-right: 7px">Check In Date :</span> {CHECK_IN_DATE}</p><p><span style="font-weight: bold;margin-right: 7px">Check out date :</span> {CHECK_OUT_DATE}</p><p><span style="font-weight: bold;margin-right: 7px">Message :</span> {MESSAGE}</p></td></tr><tr><td colspan="7" rowspan="" headers="" height="10"></td></tr></table><p>{WEB_LINK}</p>'
        ],
        [
            'title'     => 'Contact us',
            'slug'      => 'contact_us',
            'content'   => '<img src="{APP_LOGO}" alt="{APP_LOGO}" style="margin: 0 auto;"><table cellpadding="0" cellspacing="0"  style="border-collapse: collapse;"><tr><td style="border-bottom: 1px solid #dedede;padding-bottom: 15px;"><p>New contact made</p></td></tr><tr><td colspan="7" rowspan="" headers="" height="8"></td></tr><tr><td><p><span style="font-weight: bold;margin-right: 7px">Name :</span> {USER_NAME}</p><p><span style="font-weight: bold;margin-right: 7px">Email :</span> {USER_EMAIL}</p><p><span style="font-weight: bold;margin-right: 7px">Contact :</span> {USER_PHONE}</p><p><span style="font-weight: bold;margin-right: 7px">Address :</span> {USER_ADDRESS}</p><p><span style="font-weight: bold;margin-right: 7px">Message :</span> {MESSAGE}</p></td></tr><tr><td colspan="7" rowspan="" headers="" height="10"></td></tr></table><p>{WEB_LINK}</p>'
        ]
    ],

];