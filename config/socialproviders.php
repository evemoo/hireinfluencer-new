<?php

/**
 *
 * Social providers configuration
 *
 */

return [

    'providers' => [

        'twitter'   => [
            'TWITTER_CONSUMER_KEY'          => 'sC65FqWQmeW3ZdjfhVDsgXoDU',
            'TWITTER_CONSUMER_SECRET'       => '9JPLi4FeDx4LF9JDVHbIphJxKosvC6c3j0CyMdYybBzefCmgtk',
            'TWITTER_ACCESS_TOKEN_KEY'      => '3097315814-y6ePstICGXDdlKhsR7oEifGPWyJ3T3EVjnPdbqN',
            'TWITTER_ACCESS_TOKEN_SECRET'   => 'w8lnbcTml0TSbz0uMmrU3A6nzsCKiLIEPYixsCWOuN5DA',
            'TWITTER_SEARCH_ENDPOINT'       => '1.1/users/search',
            'TWITTER_PROFILE_ENDPOINT'      => '1.1/users/show.json',
            'TWITTER_TWEET_SEARCH'          => '1.1/statuses/user_timeline.json',
            'TWITTER_SPECIFIC_TWEET_SEARCH' => '1.1/statuses/show.json',
            'TWITTER_RESULT_LIMIT'          => 2
        ],

        'youtube'   => [
            'YOUTUBE_DEVELOPER_KEY'         => 'AIzaSyCr43gON4z541Mvxvp__-MMKyR72a4Kraw',
            'YOUTUBE_ENDPOINT'              => 'https://www.googleapis.com/youtube/v3/search',
            'YOUTUBE_PART'                  => [
                'SNIPPET'                   => 'snippet',
                'CONTENTDETAIL'             => 'contentDetails',
                'STATISTICS'                => 'statistics',
            ],
            'YOUTUBE_SEARCH_TYPE'           => [
                'CHANNEL'                   => 'channel',
                'PLAYLIST'                  => 'playlist',
                'VIDEO'                     => 'video'
            ],
            'YOUTUBE_PROFILE_ENDPOINT'      => 'https://www.googleapis.com/youtube/v3/channels',
            'YOUTUBE_VIDEO_ENDPOINT'        => 'https://www.googleapis.com/youtube/v3/videos',
            'YOUTUBE_RESULT_LIMIT'          => 2
        ],

        'instagram' => [
            'INSTAGRAM_ACCESS_TOKEN'        => '2234276191.457aa14.3ce97e970e8142729d1c5d19e948bea9',
            'INSTAGRAM_ENDPOINT'            => 'https://api.instagram.com/v1/users/search',
            'INSTAGRAM_PROFILE_ENDPOINT'    => 'https://www.instagram.com/%s/?__a=1',
            'INSTAGRAM_POST_ENDPOINT'       => 'https://api.instagram.com/v1/media/%s',
            'INSTAGRAM_RESULT_LIMIT'        => 2
        ],

        'facebook'  => [
            'FACEBOOK_ACCESS_TOKEN'         => 'EAAbhTRO6MEcBAOhvp2EWCfQ8W72Cfy1zNuh1Y7G68Sm14Ku9UeWcebJ9cepvyftjv4pmDo3FNXsrZAdhJBYP62iRFJ8bdgCZANKtd2xeRKyZAIQZA8UNh67ZCqrWcq8IMcDj3MSnrpxvRL84gQcTSAEsUo0WmhQkZD',
            'FACEBOOK_ENDPOINT'             => 'https://graph.facebook.com/%s',
            'FACEBOOK_PROFILE_ENDPOINT'     => 'https://graph.facebook.com/%s/feed',
            'FACEBOOK_PAGE_RATING'          => 'https://graph.facebook.com/%s/ratings',
            'FACEBOOK_POST_ENDPOINT'        => 'https://graph.facebook.com/%s',
            'FACEBOOK_FIELDS'               => 'from,likes.summary(true),comments.summary(true),shares',
            'FACEBOOK_PRETTY'               => 1,
            'FACEBOOK_SUMMARY'              => 1,
            'FACEBOOK_RESULT_LIMIT'         => 2
        ],

        'default'   => [
            'message'                       => '%s not found & could not return %s .'
        ]
    ]

];
