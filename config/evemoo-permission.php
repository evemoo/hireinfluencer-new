<?php
$permission_tree = [

    /*
    |--------------------------------------------------------------------------
    | Route Group
    |--------------------------------------------------------------------------
    | Group Routes so Permissions can be shown Grouped in Role ADD and UPDATE
    | Form
    | 
    | Every routes MUST be listed here
    |
    */
    'route-groups' => [

        'admin' => [

            'section' => [
                'name' => 'Admin Section',
                'description' => '',
            ],

            'groups' => [

                /* User Manager */
                'users' => [
                    /*
                    |--------------------------------------------------------------------------
                    | Section Name
                    |--------------------------------------------------------------------------
                    | Used to show section name in Role Permission Manager page
                    |
                    */
                    'section' => [
                        'name' => 'User Manager',
                        'description' => '',
                        'routes' => [
                            'list'           => 'List Users',
                            'view'           => 'View Users',
                            'store'          => 'Create Users',
                            'update'         => 'Update Users',
                            'destroy'        => 'Remove Users',
                            'reset_password' => 'Reset Password',
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'admin.user.index:GET',
                        ],
                        'view' => [
                            'admin.user.view.overview:GET',
                            'admin.user.view.setting:GET',
                            'admin.user.view.help:GET',
                        ],
                        'store' => [
                            'admin.user.create:GET',
                            'admin.user.store:POST',
                        ],
                        'update' => [
                            'admin.user.edit:GET',
                            'admin.user.update:POST',
                        ],
                        'destroy' => [
                            'admin.user.delete:POST',
                        ],
                        'reset_password' => [
                            'admin.user.reset.password:GET',
                            'admin.user.reset.password:POST',
                        ],
                    ],
                ],

                /* Role Manager */
                'roles' => [
                    'section' => [
                        'name' => 'Role Manager',
                        'routes' => [
                            'list' => 'List Role',
                            'view' => 'View Role',
                            'store' => 'Create Role',
                            'update' => 'Update Role',
                            'destroy' => 'Remove Role',
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'admin.roles.index:GET',
                        ],
                        'view' => [
                            'admin.roles.show:GET'
                        ],
                        'store' => [
                            'admin.roles.create:GET',
                            'admin.roles.store:POST',
                        ],
                        'update' => [
                            'admin.roles.edit:GET',
                            'admin.roles.update:POST',
                        ],
                        'destroy' => [
                            'admin.roles.confirm-delete:GET',
                            'admin.roles.delete:GET',
                        ],
                    ],
                ],

                /* Permission Manager */
                'permissions' => [
                    'section' => [
                        'name' => 'Permission Manager',
                        'routes' => [
                            'list' => 'List Permission',
                            'view' => 'View Permission',
                            'store' => 'Create Permission',
                            'update' => 'Update Permission',
                            'destroy' => 'Remove Permission',
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'admin.permissions.index:GET',
                            'admin.permissions.generate:GET',
                        ],
                        'view' => [
                            'admin.permissions.show:GET'
                        ],
                        'store' => [
                            'admin.permissions.create:GET',
                            'admin.routes.save-perms:POST',
                            'admin.routes.search:GET',
                            'admin.routes.get-info:POST',
                            'admin.permissions.store:POST',
                        ],
                        'update' => [
                            'admin.permissions.edit:GET',
                            'admin.routes.save-perms:POST',
                            'admin.routes.search:GET',
                            'admin.routes.get-info:POST',
                            'admin.permissions.update:POST',
                        ],
                        'destroy' => [
                            'admin.permissions.confirm-delete:GET',
                            'admin.permissions.delete:GET',
                        ],
                    ],
                ],

                /* Route Manager */
                'routes' => [
                    'section' => [
                        'name' => 'Route Manager',
                        'routes' => [
                            'list' => 'List Route',
                            'view' => 'View Route',
                            'store' => 'Create Route',
                            'update' => 'Update Route',
                            'destroy' => 'Remove Route',
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'admin.routes.index:GET',
                            'admin.routes.load:GET',
                        ],
                        'view' => [
                            'admin.routes.show:GET'
                        ],
                        'store' => [
                            'admin.routes.create:GET',
                            'admin.routes.store:POST',
                        ],
                        'update' => [
                            'admin.routes.edit:GET',
                            'admin.routes.update:POST',
                        ],
                        'destroy' => [
                            'admin.routes.confirm-delete:GET',
                            'admin.routes.delete:GET',
                        ],
                    ],
                ],

                /* Admin Dashboard */
                'admin-dashboard' => [
                    'section' => [
                        'name' => 'Admin Dashboard',
                        'routes' => [
                            'list' => 'Dashboard',
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'admin.dashboard:GET',
                        ],
                    ],
                ],

                /* Category Manager */
                'Category' => [
                    'level-1' => [
                        'section' => [
                            'name' => 'Category Level 1 Manager',
                            'routes' => [
                                'list' => 'List category',
                                'create' => 'Create Category',
                                'update' => 'Update category',
                            ],
                        ],
                        'routes' => [
                            'list' => [
                                'admin.category.index:GET',
                            ],
                            'create' => [
                                'admin.category.create:GET',
                                'admin.category.store:POST',
                            ],
                            'update' => [
                                'admin.category.view:GET',
                                'admin.category.update:POST',
                            ],
                        ],
                    ],
                    'level-2' => [
                        'section' => [
                            'name' => 'category Level 2 Manager',
                            'routes' => [
                                'list' => 'List Level 2 category',
                                'create' => 'Create Level 2 Category',
                                'update' => 'Update Level 2 category',
                            ],
                        ],
                        'routes' => [
                            'list' => [
                                'admin.category.level2.index:GET',
                            ],
                            'create' => [
                                'admin.category.level2.create:GET',
                                'admin.category.level2.store:POST',
                            ],
                            'update' => [
                                'admin.category.level2.view:GET',
                                'admin.category.level2.update:POST',
                            ],
                        ],
                    ],
                    'level-3' => [
                        'section' => [
                            'name' => 'category Level 3 Manager',
                            'routes' => [
                                'list' => 'List category',
                                'create' => 'Create Level 3 Category',
                                'update' => 'Update Level 3 category',
                            ],
                        ],
                        'routes' => [
                            'list' => [
                                'admin.category.level3.index:GET',
                            ],
                            'create' => [
                                'admin.category.level3.create:GET',
                                'admin.category.level3.store:POST',
                            ],
                            'update' => [
                                'admin.category.level3.view:GET',
                                'admin.category.level3.update:POST',
                            ],
                        ],
                    ],
                    'level-4' => [
                        'section' => [
                            'name' => 'category Level 4 Manager',
                            'routes' => [
                                'list' => 'List Level 4 category',
                                'create' => 'Create Level 4 Category',
                                'update' => 'Update Level 4 category',
                            ],
                        ],
                        'routes' => [
                            'list' => [
                                'admin.category.level4.index:GET',
                            ],
                            'create' => [
                                'admin.category.level4.create:GET',
                                'admin.category.level4.store:POST',
                            ],
                            'update' => [
                                'admin.category.level4.view:GET',
                                'admin.category.level4.update:POST',
                            ],
                        ],
                    ],
                ],

                /* Log Manager */
                'log' => [
                    'section' => [
                        'name' => 'Log Manager',
                        'routes' => [
                            'list' => 'List Log',
                            'update' => 'Update Log',
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'admin.log.index:GET',
                        ],
                        'update' => [
                            'admin.log.view:GET',
                            'admin.log.update:POST',
                        ],
                    ],
                ],

                /* Setting Manager */
                'setting' => [
                    'section' => [
                        'name' => 'Setting Manager',
                        'routes' => [
                            'list' => 'List Setting',
                            'update' => 'Update Setting'
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'admin.setting.index:GET',
                        ],
                        'update' => [
                            'admin.setting.update:GET'
                        ]
                    ],
                ],

            ],

        ],

        'customer' => [
            'section' => [
                'name' => 'Customer Section',
                'description' => '',
            ],

            'groups' => [
                /* Customer Dashboard */
                'customer-dashboard' => [
                    'section' => [
                        'name' => 'Customer Dashboard',
                        'routes' => [
                            'list' => 'Dashboard',
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'customer.dashboard:GET',
                        ],
                    ],
                ],

                /* Fund Data */
                'fund' => [
                    /*
                    |--------------------------------------------------------------------------
                    | Section Name
                    |--------------------------------------------------------------------------
                    | Used to show section name in Fund Manager page
                    |
                    */
                    'section' => [
                        'name' => 'Fund Manager',
                        'description' => '',
                        'routes' => [
                            'list' => 'List Fund',
                            'store' => 'List Store',
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'customer.fund.index:GET',
                        ],
                        'store' => [
                            'customer.fund.store:POST',
                            'customer.fund.stripe.store:POST',
                        ]
                    ],
                ],

                /* Profile Data */
                'profile' => [
                    /*
                    |--------------------------------------------------------------------------
                    | Section Name
                    |--------------------------------------------------------------------------
                    | Used to show section name in Profile Manager page
                    |
                    */
                    'section' => [
                        'name' => 'Profile Manager',
                        'description' => '',
                        'routes' => [
                            'list' => 'List Profile',
                            'avatar' => 'Upload Avatar',
                            'update' => 'Update',
                        ],
                    ],
                    'routes' => [
                        'list' => [
                            'customer.profile.index:GET',
                        ],
                        'avatar' => [
                            'customer.profile.updateAvatar:POST',
                        ],
                        'update' => [
                            'customer.profile.update:POST',
                        ],
                    ],
                ],

            ],

        ],

    ],
];

return $permission_tree;