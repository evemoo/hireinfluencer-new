<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '238940599195-tl4r9kc12uf9boeu1rmg9llcanoo9mbl.apps.googleusercontent.com',
        'client_secret' => '-UexYroJAo-srD0oEwiQgnmj',
        'redirect' => 'http://hireinfluencer.localhost.com/user/login/google/callback',
    ],

    'facebook' => [
        'client_id' => '390797151342780',
        'client_secret' => 'e28418da5d5e49eae922cfa36e83f090',
        'redirect' => 'http://hireinfluencer.localhost.com/user/login/facebook/callback',
    ],

    'twitter' => [
        'client_id' => 'mdA8HmLedI7bk02nivDqggkMT',
        'client_secret' => '2AgHlFYVhfEwpKf3b0uvXxBOOn5ChIrz9ZdalkgkKuY19uBBMT',
        'redirect' => 'http://hireinfluencer.localhost.com/user/login/twitter/callback',
    ],

];
