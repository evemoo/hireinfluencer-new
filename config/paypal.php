<?php

return array(
	/** set your paypal credential **/
	'client_id' =>'AQqCpoGW_L1jKKp3NCAQm6IYoP-akyJKn0FINOOvYeE2ZFcnHwBG3f3MkV8k4niyHSlxPDhECQAqj9IF',
	'secret' => 'EOEMLkY_v4vLX5Tg_KzL4ObRgJedWe_Y5Xyyos8mlSIO5mqFsb_pOsaBSvlc8WmwZP_RXbMzztE7NIYq',
	/**
	* SDK configuration 
	*/
	'settings' => array(
	/**
	* Available option 'sandbox' or 'live'
	*/
	'mode' => 'sandbox',
	/**
	* Specify the max request time in seconds
	*/
	'http.ConnectionTimeOut' => 1000,
	/**
	* Whether want to log to a file
	*/
	'log.LogEnabled' => true,
	/**
	* Specify the file that want to write on
	*/
	'log.FileName' => storage_path() . '/logs/paypal.log',
	/**
	* Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
	*
	* Logging is most verbose in the 'FINE' level and decreases as you
	* proceed towards ERROR
	*/
	'log.LogLevel' => 'FINE'
	),
);