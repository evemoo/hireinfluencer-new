
## Setup Steps

Influencer platform customer module

- Add config to webpack : mix.js('Modules/UserPanel/Resources/assets/js/app.js', 'public/vendor/userpanel-module/js')
                              .version();
- Copy assets to public folder: Copy Userpanel/Resources/assets/assets to public/Modules/UserPanel
  - Or, add config to webpack: mix.copyDirectory('Modules/UserPanel/Resources/assets/assets', 'public/vendor/userpanel-modules/assets'); 
  - run: npm run dev
  - comment the config after then or can remove
