import Vue from 'vue'

import VueRouter from 'vue-router'

import UserPanelIndex from '../views/UserPanelIndex.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {
            path: '/index',
            component: UserPanelIndex
        }
    ]
})


export default router