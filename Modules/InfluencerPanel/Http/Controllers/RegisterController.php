<?php

namespace Modules\InfluencerPanel\Http\Controllers;

use App\Evemoo\Models\Role;
use App\Evemoo\Services\EmailTemplate\EmailTemplateServices;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Modules\InfluencerPanel\Entities\Influencer;
use Modules\InfluencerPanel\Events\InfluencerRegistered;
use Modules\InfluencerPanel\Http\Requests\RegistrationRequest;
use AppHelper;
use ConfigHelper;

class RegisterController extends BaseController
{
    use RegistersUsers;

    protected $panel = 'register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    public function register()
    {
        return view(parent::loadDefaultDataToView($this->view_path.'.index'));
    }

    public function registerPost(RegistrationRequest $request)
    {
        event(new InfluencerRegistered($user = $this->create($request->all())));

        $request->session()->flash('success_message', trans($this->module_alias.'.registration_success_message'));
        return redirect()->route($this->module_alias.'.login');
    }

    public function verification(Request $request, $token)
    {
        $this->validateAcVerificationToken($request, $token);

        $user = User::where('verify_token', $token)->first();
        $user->enabled = 1;
        $user->verify_token = AppHelper::verifyToken();
        $user->save();

        $templateService = new EmailTemplateServices();
        $templateService->sendEmail(
            [
                '{APP_NAME}'           => ConfigHelper::getSiteConfigBykey('company'),
                '{USER_FULL_NAME}'     => request()->get('full_name'),
                '{WEB_LINK}'           => url('/')
            ],
            'email_welcome_template',
            [
                'receiver'    => $user->email,
                'sender'      => ConfigHelper::getSiteConfigBykey('email')
            ]
        );

        $request->session()->flash('success_message', __($this->trans_path.'account_verification_success'));
        return redirect()->route($this->module_alias.'.login');
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $name_pcs = \AppHelper::split_name($data['full_name']);
        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => $data['password'],
            'first_name' => $name_pcs['first_name'],
            'middle_name' => $name_pcs['first_name'],
            'last_name' => $name_pcs['last_name'],
            'primary_contact' => $data['contact'],
            'verify_token' => \AppHelper::verifyToken(),
            'enabled' => 0
        ]);

        Influencer::create([
            'users_id' => $user->id,
            'registered_at' => Carbon::now(),
            'message' => $data['message']
        ]);

        $role = Role::where('name', config($this->module_alias.'.role_to_assign_after_registration'))->first();
        $response = $user->roles()->sync([$role->id]);

        return $user;

    }

    protected function validateAcVerificationToken($request, $token)
    {
        if (User::where('verify_token', $token)->count() == 0 || User::where('verify_token', $token)->where('enabled', 1)->count() > 0) {
            $request->session()->flash('error_message', __($this->trans_path.'account_verification_invalid'));
            return redirect()->route($this->module_alias.'.login')->send();
        }

    }



}
