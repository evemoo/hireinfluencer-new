<?php

namespace Modules\InfluencerPanel\Listeners;

use App\Evemoo\Services\EmailTemplate\EmailTemplateServices;
use App\Mail\UserActivation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use AppHelper;
use ConfigHelper;
use Modules\InfluencerPanel\Events\PasswordReset as PasswordResetEvent;
use Modules\InfluencerPanel\Mail\ResetPassword;

class PasswordReset
{
	/*
	 * Template Services instance
	 */
	protected $templateServices;

	/**
	 * SendVerificationEmail constructor.
	 *
	 * @param EmailTemplateServices $email_template_services
	 */
	public function __construct(EmailTemplateServices $email_template_services)
	{
		$this->templateServices = $email_template_services;
	}

    /**
     * Handle the event.
     *
     * @param  CustomerRegistered  $event
     * @return void
     */
    public function handle(PasswordResetEvent $event)
    {
        Mail::to($event->user->email)->send(new ResetPassword($event->user, $event->user->password));
    }
}
