<?php

namespace Modules\InfluencerPanel\Listeners;

use App\Evemoo\Services\EmailTemplate\EmailTemplateServices;
use App\Mail\UserActivation;
use App\Modules\InfluencerPanel\Mail\EmailTemplate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use AppHelper;
use ConfigHelper;
use Modules\InfluencerPanel\Events\InfluencerRegistered;

class SendVerificationEmail
{
	/*
	 * Template Services instance
	 */
	protected $templateServices;

	/**
	 * SendVerificationEmail constructor.
	 *
	 * @param EmailTemplateServices $email_template_services
	 */
	public function __construct(EmailTemplateServices $email_template_services)
	{
		$this->templateServices = $email_template_services;
	}

    /**
     * Handle the event.
     *
     * @param  CustomerRegistered  $event
     * @return void
     */
    public function handle(InfluencerRegistered $event)
    {
        // send email to register user with link to active account
        //Mail::to($event->user->email)->send(new UserActivation($event->user));

        $this->templateServices->sendEmail(
		    [
			    '{APP_NAME}'               => ConfigHelper::getSiteConfigBykey('company'),
			    '{USER_FULL_NAME}'         => request()->get('full_name'),
			    '{USER_ACTIVATION_TOKEN}'  => route('influencerpanel.verification', $event->user->verify_token) ,
			    '{WEB_LINK}'               => route('influencerpanel.verification', $event->user->verify_token),
            ],
		    'send_user_activation_code',
		    [
			    'receiver'    => $event->user->email,
			    'sender'      => ConfigHelper::getSiteConfigBykey('email')
		    ]
	    );
    }
}
