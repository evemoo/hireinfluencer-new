<?php
return [
    'name' => 'InfluencerPanel',
    'asset_path' => 'panel/assets/',
    'role_to_assign_after_registration' => 'influencer',
];
