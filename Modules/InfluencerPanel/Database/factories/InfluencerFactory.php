<?php

use Faker\Generator as Faker;
use \Modules\InfluencerPanel\Entities\Influencer;

$factory->define(Influencer::class, function (Faker $faker) {

    $date = $faker->dateTime(\Carbon\Carbon::now());
    return [
        'registered_at' => $date,
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        }
    ];
});
