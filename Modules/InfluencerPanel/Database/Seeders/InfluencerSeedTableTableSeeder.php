<?php

namespace Modules\InfluencerPanel\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\InfluencerPanel\Entities\Influencer;

class InfluencerSeedTableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(Influencer::class, 3)->create();
    }
}
