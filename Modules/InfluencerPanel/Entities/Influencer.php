<?php

namespace Modules\InfluencerPanel\Entities;

use Illuminate\Database\Eloquent\Model;

class Influencer extends Model
{
    protected $fillable = ['users_id', 'registered_at', 'verified_at', 'message', 'seen_by_admin'];

    public $timestamps = false;
}
