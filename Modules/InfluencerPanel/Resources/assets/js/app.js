window.Vue = require('vue');

Vue.component(
	'app',
	require('./App.vue')
	);

const app =  new Vue({
    el: '#root'
});