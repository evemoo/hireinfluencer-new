// Authorization header

import axios from "axios"

// import Vue from "vue"
//
// Vue.http.interceptors.push((request, next) => {
//     request.headers.set('Authorization', 'Bearer ' + localStorage.getItem('access_token'))
//     // continue to next interceptor
//     next((response) => {
//         // Show toast with message for non OK responses
//         if (response.status !== 200) {
//             store.dispatch('addToastMessage', {
//                 text: response.body.message || 'Request error status: ' + response.status,
//                 type: 'danger'
//             })
//         }
//     })
// })

axios.interceptors.request.use(function(config){
    // config.headers['X-CSRF-TOKEN']  = Laravel.csrfToken
    config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('access_token')

    return config;
}, function (error) {
    console.log('interceptors result --------------------------  '+error)
})