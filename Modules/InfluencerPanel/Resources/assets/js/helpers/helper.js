/**
 * Global helpers for Influencer Panel
 * Helper name : inno
 */

import { API_CONFIG } from "../config/config"

export const evemoo = {

    /**
     * Get Item from local storage if found
     * @param key
     * @returns {string | null}
     */
    getItem(key) {
        if(this.isSet(key)){
            return localStorage.getItem(key)
        }
        return ''
    },

    /**
     * States true/false for the key
     * @param key
     * @returns {boolean}
     */
    isSet(key) {
        return !!localStorage.getItem(key)
    },

    /**
     * Get value from config
     * @param key
     * @returns {*}
     */
    getConfig(key) {
        if (this.configSet(key)) {
            return API_CONFIG.key
        }
        return ''
    },

    /**
     * States true/false for config value
     * @param key
     * @returns {boolean}
     */
    configSet(key) {
        return !!API_CONFIG.key
    }



}