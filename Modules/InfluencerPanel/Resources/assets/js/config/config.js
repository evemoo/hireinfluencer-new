/**
 * API Configuration
 * @type {string}
 */
let base_url = 'http://hireinfluencer.com'

let client_id = 2

let client_secret = 'EXvSGVpil4B6RCJr8113ciy2msfkfWkY1dWt3NnM'

let grant_type = 'password'

let scope = '*'

let response_type = ''

let static_access_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImUyN2ZkYjRkM2Y2OGZlN2FjZjhiMTlmMTU2MGYyYjdkNWQzMGY4YjIyNjc3MTUyODUzMmU2YWI3NTIzOTE5NTU4NTkyNzdmY2MyNDQ0YzFhIn0.eyJhdWQiOiIyIiwianRpIjoiZTI3ZmRiNGQzZjY4ZmU3YWNmOGIxOWYxNTYwZjJiN2Q1ZDMwZjhiMjI2NzcxNTI4NTMyZTZhYjc1MjM5MTk1NTg1OTI3N2ZjYzI0NDRjMWEiLCJpYXQiOjE1MTQzNjU5OTAsIm5iZiI6MTUxNDM2NTk5MCwiZXhwIjoxNTQ1OTAxOTkwLCJzdWIiOiIzMyIsInNjb3BlcyI6W119.t7kK7Xj73dnliA9_ETnhgpVZ87b8nUFfH6TD4mqfW9YxJ6MOuPqSL69cKtiR-r9cKpq_PVIXBsqLVyQ5HJ3xXtHb6shGAjhvs3lF4IlxFV9zV_z5UYAaPeO1ytPQu7w8Mdinw8mfGrsufSmP3hyj6hkLnqeMN-Vr7GAsLyWCjwkj9t6j64eO2jKATGNGTYENJpVQt_-RCrxexrc4-HdYxnBe2A0yb2OCP2t2Ads2QJRVY0902WuK629S9Y2E1pbaEld-WiXjFvHFo11tczVyaSDg2XJWWqYfIOuqxRey7MN4tmdxCLyd63TttbM7F4rvRxRiJabCzaXoGC2oOY_wno_Al_VJ4BrKvI7cHMX8s88Etxn-fbuZAAdUH0w8lyuhtsNvbMSnMubTauzRRh7GbEpKr1Y6y8JvFMey2R1pf4lAxayjcyBOxpjqzUCCqvmScqxuKWYOMIAFq7tkn9sVfHHAmOLl9KKGd4hmcZhXwARXkozwBZjKSMUo04iFF6pHaFtWcyP2UUtlr-Zdt8Zxaoekc1JcA8-9IITburj7icJPt4bYRNSDXGuBgb0z3haBM11sM9pEFPyFk4VILWhcrLIxmHI8KZ9vvDPuvQKXu2JOmVZsVtEftU_kFnKsogJraD_s-U0oPIq1cxxHLyNczC2rz-8arRF3pmZ4ucr31Hg'

export const API_CONFIG = {

    API_URL             : base_url,

    CLIENT_ID           : client_id,

    CLIENT_SECRET       : client_secret,

    GRANT_TYPE          : grant_type,

    SCOPE               : scope,

    RESPONSE_TYPE       : response_type,

    STATIC_ACCESS_TOKEN : static_access_token

}

