/**
 * State management
 * registering all the state modules here
 */

import Vue from 'vue'

import Vuex from 'vuex'

import createLogger from 'vuex/dist/logger'

import auth from './modules/auth'

import register from './modules/register'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    strict: debug,
    plugins: debug ? [createLogger()] : [],
    modules: {
        auth,
        register
    }
})