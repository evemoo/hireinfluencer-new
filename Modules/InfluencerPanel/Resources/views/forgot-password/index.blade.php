@extends($_module.'::layouts.auth')

@section('content')

    <!-- BEGIN : LOGIN PAGE 5-1 -->
    <div class="user-login-5">
        <div class="row bs-reset">
            @include($_module.'::common.auth_left_section')

            <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
                <div class="login-content">

                    @include($_view_path.'.index.forget_password_form')

                </div>

                @include($_module.'::common.auth_footer')
            </div>
        </div>
    </div>
    <!-- END : LOGIN PAGE 5-1 -->

    @endsection

@section('js')

    <script src="{{ asset($_asset_path. 'pages/scripts/influencer-forgot_password.js') }}" type="text/javascript"></script>

@endsection