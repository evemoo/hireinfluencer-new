<!-- BEGIN FORGOT PASSWORD FORM -->
{!! Form::open(['route' => $_base_route.'.post', 'class' => 'forget-form']) !!}
    <h3 class="font-green">{{ __($_trans_path.'Forgot-Password?') }}</h3>

    <div class="alert alert-danger display-hide" style="margin-top: 0px;">
        <button class="close" data-close="alert"></button>
        <span>{{ __($_trans_path.'form-validation-alert-message') }} </span>
    </div>

    <div {!! $errors->any()  || session()->has('success_message') || session()->has('error_message')?'style="margin-top: 65px;"':'' !!}>
        @include($_module.'::common.form_alert_messages')
        @include($_module.'::common.validation_alert_messages')
    </div>

    <p> {{ __($_trans_path.'Forgot-Password?-Text') }} </p>

    <div class="form-group">
        {!! Form::email('email', null, [
           'class' => 'form-control form-control-solid placeholder-no-fix form-group',
           'autocomplete' => 'off',
           'placeholder' => __($_trans_path.'Email'),
           'require'
        ]) !!}
    </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-success uppercase pull-right">{{ __($_trans_path.'Submit') }}</button>
    </div>
{!! Form::close() !!}
<!-- END FORGOT PASSWORD FORM -->