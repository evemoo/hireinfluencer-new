<!-- BEGIN FORGOT PASSWORD FORM -->
{!! Form::open(['route' => $_module.'.password-reset.post', 'class' => 'login-form reset-form']) !!}
<input type="hidden" name="token" value="{{ request()->get('token') }}">


<div class="alert alert-danger display-hide" style="margin-top: 0px;">
    <button class="close" data-close="alert"></button>
    <span>{{ __($_trans_path.'reset-form-validation-alert-message') }} </span>
</div>

<div {!! $errors->any()  || session()->has('success_message') || session()->has('error_message')?'style="margin-top: 65px;"':'' !!}>
    @include($_module.'::common.form_alert_messages')
    @include($_view_path.'.reset.validation_alert_messages')
</div>

<p> {{ __($_trans_path.'Reset-Your-Password-Text') }} </p>

<div class="row">

    <div class="col-xs-12">
        {!! Form::email('email', null, [
          'class' => 'form-control form-control-solid placeholder-no-fix form-group',
          'autocomplete' => 'off',
          'placeholder' => __($_trans_path.'Your-Email'),
          'require'
       ]) !!}
        {!! ViewHelper::getFormError('email') !!}
    </div>
    <div class="col-xs-6">
        <input type="password" name="password" id="password" class="form-control form-control-solid placeholder-no-fix form-group {{ ViewHelper::hasValidationError('password') }}"
               autocomplete="off" placeholder="{{ __($_trans_path.'Password') }}" required>
        {!! ViewHelper::getFormError('password') !!}
    </div>
    <div class="col-xs-6">
        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control form-control-solid placeholder-no-fix form-group"
               autocomplete="off" placeholder="{{ __($_trans_path.'Confirm-Password') }}" required>
    </div>
</div>

<div class="form-actions">
    <button type="submit" class="btn btn-success uppercase pull-right">{{ __($_trans_path.'Reset-Password') }}</button>
</div>
{!! Form::close() !!}
<!-- END FORGOT PASSWORD FORM -->