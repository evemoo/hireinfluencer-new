@if($errors->any())

    <div class="alert alert-danger">
        <button class="close" data-close="alert"></button>
        <span>{{ __($_trans_path.'reset-form-validation-alert-message') }} </span>
    </div>

@endif