@if(session()->has('success_message'))

    <div class="alert alert-success">
        <button class="close" data-close="alert"></button>
        <span>{{ session()->get('success_message') }} </span>
    </div>

@elseif (session()->has('error_message'))

    <div class="alert alert-danger">
        <button class="close" data-close="alert"></button>
        <span>{{ session()->get('error_message') }} </span>
    </div>

@endif