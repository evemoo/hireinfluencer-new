{!! Form::open(['route' => $_base_route.'.post', 'class' => 'login-form', 'id' => 'registration-form']) !!}

    @if ($errors->any())
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span>{{ __($_trans_path.'server-validation-alert-message') }} </span>
        </div>
    @endif

    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span>{{ __($_trans_path.'jquery-validation-alert-message') }} </span>
    </div>
    <div class="row">
        <div class="col-xs-6">
            {!! Form::text('full_name', null, [
                'class' => 'form-control form-control-solid placeholder-no-fix form-group '.ViewHelper::hasValidationError('full_name'),
                'autocomplete' => 'off',
                'placeholder' => __($_trans_path.'Full-Name'),
                'required'
                ]) !!}
            {!! ViewHelper::getFormError('full_name') !!}
        </div>
        <div class="col-xs-6">
            {!! Form::text('username', null, [
                'class' => 'form-control form-control-solid placeholder-no-fix form-group '.ViewHelper::hasValidationError('username'),
                'autocomplete' => 'off',
                'placeholder' => __($_trans_path.'Username'),
                'required'
                ]) !!}
            {!! ViewHelper::getFormError('username') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            {!! Form::email('email', null, [
                'class' => 'form-control form-control-solid placeholder-no-fix form-group '.ViewHelper::hasValidationError('email'),
                'autocomplete' => 'off',
                'placeholder' => __($_trans_path.'Email'),
                'required'
                ]) !!}
            {!! ViewHelper::getFormError('email') !!}
        </div>
        <div class="col-xs-6">
            {!! Form::text('contact', null, [
                'class' => 'form-control form-control-solid placeholder-no-fix form-group '.ViewHelper::hasValidationError('contact'),
                'autocomplete' => 'off',
                'placeholder' => __($_trans_path.'Contact'),
                'required'
                ]) !!}
            {!! ViewHelper::getFormError('contact') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <input type="password" name="password" class="form-control form-control-solid placeholder-no-fix form-group {{ ViewHelper::hasValidationError('password') }}"
                   autocomplete="off" placeholder="{{ __($_trans_path.'Password') }}" required>
            {!! ViewHelper::getFormError('password') !!}
        </div>
        <div class="col-xs-6">
            <input type="password" name="password_confirmation" class="form-control form-control-solid placeholder-no-fix form-group"
                   autocomplete="off" placeholder="{{ __($_trans_path.'Confirm-Password') }}" required>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            {!! Form::textarea('message', null, [
                'class' => 'form-control form-control-solid placeholder-no-fix form-group',
                'autocomplete' => 'off',
                'placeholder' => __($_trans_path.'Message'),
                'rows' => 3
                ]) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">&nbsp; </div>
        <div class="col-sm-8 text-right">
            <div class="forgot-password">
                <a href="{{ route($_module.'.login') }}" class="forget-password">{{ __($_trans_path.'Has-Login?') }}</a>
            </div>
            <button class="btn green" type="submit">{{ __($_trans_path.'Sign-Up') }}</button>
        </div>
    </div>
{!! Form::close() !!}