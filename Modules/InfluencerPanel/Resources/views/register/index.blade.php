@extends($_module.'::layouts.auth')

@section('css')
    <link href="{{ asset(config('evemoo.assets_path.bower_components').'materialize-social/materialize-social.css') }}" rel="stylesheet" type="text/css">
    @endsection

@section('content')

    <!-- BEGIN : LOGIN PAGE 5-1 -->
    <div class="user-login-5">
        <div class="row bs-reset">
            @include($_module.'::common.auth_left_section')

            <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">

                <div class="login-content" style="margin-top: 25%;">

                    <h1>{{ __($_trans_path.'Hireinfluencer-Influencer-Registration') }}</h1>
                    <p> {{ __($_trans_path.'Hireinfluencer-Influencer-Registration-Text') }} </p>

                    <i-register></i-register>

                </div>

                <hr>

                <div style="text-align: center;">
                    <a href="{{ route('social.login', 'facebook') }}" class="waves-effect waves-light btn social facebook"><i class="fa fa-facebook"></i> Sign up with facebook</a>
                    <a href="{{ route('social.login', 'google') }}" class="waves-effect waves-light btn social google"><i class="fa fa-google"></i> Sign up with google</a>
                    <a href="{{ route('social.login', 'twitter') }}" class="waves-effect waves-light btn social twitter"><i class="fa fa-twitter"></i> Sign up with twitter</a>
                </div>

                <hr/>


                @include($_module.'::common.auth_footer')
            </div>
        </div>
    </div>
    <!-- END : LOGIN PAGE 5-1 -->

    @endsection

@section('js')

    <script src="{{ asset($_asset_path. 'pages/scripts/influencer-registration.js') }}" type="text/javascript"></script>

    @endsection