<?php

namespace Modules\InfluencerPanel\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\InfluencerPanel\Events\InfluencerRegistered;
use Modules\InfluencerPanel\Events\PasswordReset;
use Modules\InfluencerPanel\Events\PasswordResetRequest;
use Modules\InfluencerPanel\Listeners\PasswordReset as PasswordResetListener;
use Modules\InfluencerPanel\Listeners\SendPasswordResetLinkEmail;
use Modules\InfluencerPanel\Listeners\SendVerificationEmail;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        InfluencerRegistered::class => [
            SendVerificationEmail::class,
        ],
        PasswordResetRequest::class => [
            SendPasswordResetLinkEmail::class,
        ],
        PasswordReset::class => [
            PasswordResetListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
