<?php

namespace Modules\BrandPanel\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $password;
    /**
     * ResetPassword constructor.
     * @param User $user
     * $password
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->password = $user->password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('brandpanel::mail.reset_password');
    }
}
