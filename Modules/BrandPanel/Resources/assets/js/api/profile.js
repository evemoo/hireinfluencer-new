/*
    Import Configuration API URL
 */

import { API_CONFIG } from "../config/config"

export default {

    /**
     * Get User Information
     */
    getUserInfo: function () {
        return axios.get( API_CONFIG.API_URL + '/user' );
    }

}