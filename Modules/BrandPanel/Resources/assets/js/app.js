import Vue from 'vue'

// import App from './App.vue'
import router from './router/route'

Vue.component('i-app', require('./App.vue'))

/**
 * Component registration Initialized
 */
import global_register from './global-register'

_register_components.map(function (component) {
    Vue.component(component.name,  require('./views/' + component.path + '.vue'))
});

const app =  new Vue({
    el: '#root',
    router,
    global_register
});