/**
 * Include all the global components here
 *
 * @type {Array}
 * @private
 *
 */

window._register_components = [

    { name : 'i-index' ,        path : 'BrandPanel' },

    { name : 'i-header' ,       path : 'common/header/Header' },

    { name : 'i-breadcumb',     path : 'common/breadcrumb/Breadcrumb' },

    { name : 'i-siderbar',      path : 'common/sidebar/Sidebar' },

    { name : 'i-footer',        path : 'common/footer/Footer' },

    // Login

    { name : 'i-login',         path: 'auth/Login' }

];