import Vue from 'vue'

import VueRouter from 'vue-router'

import Home from '../views/home/Home.vue';
import Profile from '../views/profile/Profile.vue';

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {
            path: '/index',
            component: Home
        },
        {
            path: '/profile',
            component: Profile
        }
    ]
})


export default router