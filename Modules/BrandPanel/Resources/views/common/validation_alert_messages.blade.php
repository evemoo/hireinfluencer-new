@if($errors->any())

    <div class="alert alert-danger">
        <button class="close" data-close="alert"></button>
        <span>{{ __($_trans_path.'form-validation-fail-alert-message') }} </span>
    </div>

@endif