<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Module BrandPanel</title>

    <script type='text/javascript'>
        window.Laravel = {{ json_encode([
                'csrfToken' => csrf_token(),
            ]) }}
    </script>

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/backend/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('assets/backend/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('assets/backend/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('assets/backend/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

<div id="root">

    <i-app></i-app>
    {{--<router-view></router-view>--}}

</div>

<script src="{{ mix('vendor/brandpanel-module/js/app.js') }}"></script>

<script src="{{ asset('assets/backend/js/respond.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/excanvas.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/ie8.fix.min.js') }}"></script>

<!-- BEGIN CORE PLUGINS -->
<script src="{{ asset('assets/backend/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/backend/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/backend/js/js.cookie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/backend/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/backend/js/jquery.blockui.min.js') }}" type="text/javascript"></script>


<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('assets/backend/js/app.min.js') }}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{ asset('assets/backend/js/layout.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('panel/assets/pages/scripts/profile.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/backend/js/bootstrap-fileinput.js') }}"></script>


<script src="{{ asset('assets/backend/js/demo.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/backend/js/quick-sidebar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/backend/js/quick-nav.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</body>
</html>
