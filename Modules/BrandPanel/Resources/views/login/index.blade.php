@extends($_module.'::layouts.auth')

@section('css')
    <link href="{{ asset(config('evemoo.assets_path.bower_components').'materialize-social/materialize-social.css') }}" rel="stylesheet" type="text/css">
    @endsection

@section('content')

    <!-- BEGIN : LOGIN PAGE 5-1 -->
    <div class="user-login-5">
        <div class="row bs-reset">
            @include($_module.'::common.auth_left_section')

            <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
                <div class="login-content">
                    <h1>{{ __($_trans_path.'Hireinfluencer-Brand-Login') }}</h1>
                    <p> {{ __($_trans_path.'Hireinfluencer-Brand-Login-Text') }} </p>

                    @include($_view_path.'.index.login_form')

                    <hr/>

                    <div style="text-align: center;">
                        <a href="{{ route('social.login', 'facebook') }}" class="waves-effect waves-light btn social facebook"><i class="fa fa-facebook"></i> Sign in with facebook</a>
                        <a href="{{ route('social.login', 'google') }}" class="waves-effect waves-light btn social google"><i class="fa fa-google"></i> Sign in with google</a>
                        <a href="{{ route('social.login', 'twitter') }}" class="waves-effect waves-light btn social twitter"><i class="fa fa-twitter"></i> Sign in with twitter</a>
                    </div>

                    <hr/>

                    <div style="text-align: center;">
                        <a href="{{ route($_module.'.register') }}" class="btn green mt-clipboard"><i class="icon-login"></i>&nbsp;&nbsp;Sign Up</a>
                    </div>

                </div>

                @include($_module.'::common.auth_footer')
            </div>
        </div>
    </div>
    <!-- END : LOGIN PAGE 5-1 -->

    @endsection

@section('js')

    <script src="{{ asset($_asset_path. 'pages/scripts/brand-auth.js') }}" type="text/javascript"></script>

@endsection