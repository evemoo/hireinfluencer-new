@component('mail::message')
# Password Reset

 You are receiving this because you have successfully reset your password.

Thanks,<br>
{{ config('app.name') }}
@endcomponent