<?php

namespace Modules\BrandPanel\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\BrandPanel\Events\BrandRegistered;
use Modules\BrandPanel\Events\PasswordReset;
use Modules\BrandPanel\Events\PasswordResetRequest;
use Modules\BrandPanel\Listeners\PasswordReset as PasswordResetListener;
use Modules\BrandPanel\Listeners\SendPasswordResetLinkEmail;
use Modules\BrandPanel\Listeners\SendVerificationEmail;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        BrandRegistered::class => [
            SendVerificationEmail::class,
        ],
        PasswordResetRequest::class => [
            SendPasswordResetLinkEmail::class,
        ],
        PasswordReset::class => [
            PasswordResetListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
