<?php
return [
    'name' => 'BrandPanel',
    'asset_path' => 'panel/assets/',
    'role_to_assign_after_registration' => 'brand',
];
