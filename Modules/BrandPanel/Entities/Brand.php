<?php

namespace Modules\BrandPanel\Entities;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['users_id', 'name', 'domain', 'registered_at', 'verified_at', 'message', 'seen_by_admin'];

    public $timestamps = false;
}
