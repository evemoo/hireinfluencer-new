<?php

Route::group([
    'middleware' => 'web', 'prefix' => 'brandpanel', 'as' => 'brandpanel.',
    'namespace' => 'Modules\BrandPanel\Http\Controllers'
], function()
{
    Route::get('register',                              ['as' => 'register',                            'uses' => 'RegisterController@register']);
    Route::post('register',                             ['as' => 'register.post',                       'uses' => 'RegisterController@registerPost']);
    Route::get('verification/{token}',                  ['as' => 'verification',                        'uses' => 'RegisterController@verification']);

    Route::get('login',                                 ['as' => 'login',                               'uses' => 'LoginController@loginForm']);
    Route::post('login',                                ['as' => 'login.post',                          'uses' => 'LoginController@login']);

    Route::get('forgot-password',                       ['as' => 'forgot-password',                     'uses' => 'ForgotPasswordController@showLinkRequestForm']);
    Route::post('forgot-password',                      ['as' => 'forgot-password.post',                'uses' => 'ForgotPasswordController@sendResetLinkEmail']);

    Route::get('password/reset',                       ['as' => 'password-reset',                      'uses' => 'ForgotPasswordController@showPasswordResetForm']);
    Route::post('password-reset',                      ['as' => 'password-reset.post',                'uses' => 'ForgotPasswordController@resetPassword']);

    Route::get('dashboard',                             ['as' => 'dashboard',                           'uses' => 'BrandPanelController@index']);

});
