<?php

namespace Modules\BrandPanel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BrandPanelController extends BaseController
{
    protected $panel = 'dashboard';

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('brandpanel::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('brandpanel::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('brandpanel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('brandpanel::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
