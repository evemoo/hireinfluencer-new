<?php

namespace Modules\BrandPanel\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends BaseController
{
    use AuthenticatesUsers;

    protected $panel = 'login';

    public function loginForm()
    {
        return view(parent::loadDefaultDataToView($this->view_path.'.index'));
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return redirect()->route($this->module_alias.'.dashboard');
    }
}
