<?php

namespace Modules\BrandPanel\Http\Controllers;

use App\User;
use Modules\BrandPanel\Events\PasswordReset;
use Modules\BrandPanel\Http\Requests\PasswordResetForm;
use Modules\BrandPanel\Http\Requests\PasswordResetRequest;
use Modules\BrandPanel\Events\PasswordResetRequest as PasswordResetRequestEvent;
use Auth;

class ForgotPasswordController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    protected $panel = 'forgot-password';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest');

    }

    public function showLinkRequestForm()
    {
        return view(parent::loadDefaultDataToView($this->view_path.'.index'));
    }

    public function sendResetLinkEmail(PasswordResetRequest $request)
    {
        $this->sendResetLink($request);
        return back()->with('success_message', __($this->trans_path.'password-reset-link-sent-message'));
    }

    public function showPasswordResetForm()
    {
        return view(parent::loadDefaultDataToView($this->view_path.'.reset'));
    }

    public function resetPassword(PasswordResetForm $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        $user->password = $request->get('password');
        $user->save();

        event(new PasswordReset($user));

        Auth::login($user);
        return redirect()->route($this->module_alias.'.dashboard');
    }


    protected function sendResetLink($request)
    {
        $this->updatePasswordResetToken($request->get('email'));
        event(new PasswordResetRequestEvent(User::where('email', $request->get('email'))->first()));
    }

    protected function updatePasswordResetToken($email)
    {
        $token_row = \DB::table('password_resets')->where('email', $email)->first();
        if ($token_row) {
            \DB::table('password_resets')->where('email', $email)->update([
                'token' => str_random(60)
            ]);
        } else {
            \DB::table('password_resets')->insert([
                'email' => $email,
                'token' => str_random(60)
            ]);
        }
    }
}
