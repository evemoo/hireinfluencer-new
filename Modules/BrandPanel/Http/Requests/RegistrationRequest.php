<?php

namespace Modules\BrandPanel\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;

class RegistrationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_name' => 'required',
            'brand_domain' => 'required|url',
            'full_name' => 'required | full_name',
            'username' => 'required|unique:users,username|max:45',
            'email' => 'required|unique:users,email|max:45',
            'contact' => 'required',
            'password' => 'required|min:6|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/|confirmed',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'full_name.full_name' => trans('brandpanel::general.register.full_name_validation_message'),
        ];
    }

}
