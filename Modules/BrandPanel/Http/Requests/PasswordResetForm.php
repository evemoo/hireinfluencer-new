<?php

namespace Modules\BrandPanel\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Validator;

class PasswordResetForm extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'email' => 'required|email|valid_token',
            'password' => 'required|max:45|confirmed',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'email.valid_token' => __('brandpanel::general.forgot-password.Invalid-Email')
        ];
    }

    protected function customValidation()
    {
        Validator::extend('valid_token', function($attribute, $value, $params, $validator){

            if (
                \DB::table('password_resets')
                ->where('email', $value)
                ->where('token', $this->request->get('token'))
                ->first()
            ) {

                return true;

            }

            return false;
        });
    }



}
