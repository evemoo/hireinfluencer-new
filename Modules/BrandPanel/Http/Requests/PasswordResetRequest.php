<?php

namespace Modules\BrandPanel\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Validator;

class PasswordResetRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'email' => 'required|max:45|email_exist',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function customValidation()
    {
        Validator::extend('email_exist', function($attribute, $value, $params, $validator){

            if (User::where('email', $value)->first()) {

                return true;

            }

            return false;
        });
    }



}
