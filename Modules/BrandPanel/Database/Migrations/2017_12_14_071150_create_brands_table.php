<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('users_id');
            $table->string('name', 150);
            $table->text('domain')->nullable();
            $table->dateTime('registered_at');
            $table->dateTime('verified_at')->nullable()->comment('DateTime when user is verified as brand');
            $table->text('message')->nullable()->comment('Message sent by brand while registration');
            $table->boolean('seen_by_admin')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}
