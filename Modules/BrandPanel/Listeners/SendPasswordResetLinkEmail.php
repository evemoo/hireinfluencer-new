<?php

namespace Modules\BrandPanel\Listeners;

use App\Evemoo\Services\EmailTemplate\EmailTemplateServices;
use Modules\BrandPanel\Events\PasswordResetRequest;
use ConfigHelper;

class SendPasswordResetLinkEmail
{
	/*
	 * Template Services instance
	 */
	protected $templateServices;

	/**
	 * SendVerificationEmail constructor.
	 *
	 * @param EmailTemplateServices $email_template_services
	 */
	public function __construct(EmailTemplateServices $email_template_services)
	{
		$this->templateServices = $email_template_services;
	}

    /**
     * Handle the event.
     *
     * @param  CustomerRegistered  $event
     * @return void
     */
    public function handle(PasswordResetRequest $event)
    {
        // send email to register user with link to active account
        $verification_token_row = \DB::table('password_resets')->where('email', $event->user->email)->first();

        $this->templateServices->sendEmail(
		    [
			    '{USER_NAME}'              => $event->user->username,
			    '{VERIFICATION_TOKEN}'     => route('brandpanel.password-reset', ['token' => $verification_token_row->token]),
			    '{WEB_LINK}'               => route('home'),
            ],
		    'reset_user_password',
		    [
			    'receiver'    => $event->user->email,
			    'sender'      => ConfigHelper::getSiteConfigBykey('email')
		    ]
	    );
    }
}
