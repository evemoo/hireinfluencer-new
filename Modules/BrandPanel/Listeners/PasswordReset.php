<?php

namespace Modules\BrandPanel\Listeners;

use App\Evemoo\Services\EmailTemplate\EmailTemplateServices;
use Mail;
use Modules\BrandPanel\Events\PasswordReset as PasswordResetEvent;
use Modules\BrandPanel\Mail\ResetPassword;

class PasswordReset
{
	/*
	 * Template Services instance
	 */
	protected $templateServices;

	/**
	 * SendVerificationEmail constructor.
	 *
	 * @param EmailTemplateServices $email_template_services
	 */
	public function __construct(EmailTemplateServices $email_template_services)
	{
		$this->templateServices = $email_template_services;
	}

    /**
     * Handle the event.
     *
     * @param  CustomerRegistered  $event
     * @return void
     */
    public function handle(PasswordResetEvent $event)
    {
        Mail::to($event->user->email)->send(new ResetPassword($event->user, $event->user->password));
    }
}
