<?php

namespace App\Rules;

use App\Evemoo\Models\Package;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class QuantityPackageUniqueValidation implements Rule
{
    /**
     * @var Request
     */
    private $request;

    /**
     * Create a new rule instance.
     *
     * CustomPackageUniqueValidation constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(
            $this->request->get('default')['type'] == \ConfigHelper::evemooConfig('package.package.key')
        ) {
            $package_type_id = $this->request->get('default')['packages_types_id'];

            $package = Package::where('packages_types_id', $package_type_id)
                ->where('qty', $this->request->get('default')['qty'])->first();

            if($package) {
                return false;
            }

        }
        return  true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This Quantity is already added in Packages.';
    }
}
