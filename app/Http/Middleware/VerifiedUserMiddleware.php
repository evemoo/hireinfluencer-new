<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

class VerifiedUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = \App\User::where('email', $request->email)->first();
        if ($user) {
            if ($user->enabled == 0) {
                throw new Exception("Error Processing Request", 1);
            }
        }

        return $next($request);

    }
}
