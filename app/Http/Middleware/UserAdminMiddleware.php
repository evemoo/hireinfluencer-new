<?php

namespace App\Http\Middleware;

use Closure;

class UserAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(
            in_array(\ConfigHelper::getRole('admin')['slug'],
            auth()->user()->roles->pluck('name')->all())
        )
        {
            return $next($request);
        }

        if ( request()->ajax() ) {

            return response()->json([
                'error' => 'Trying to access unauthorized resource.',
            ], 401);

        } else
            return redirect()->route('admin.error', 'un-authorized-request');

    }
}
