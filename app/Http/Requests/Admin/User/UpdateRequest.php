<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('id');
        return [
            'first_name' => 'required|min:2|max:50',
            'middle_name' => 'sometimes|nullable|min:2|max:50',
            'last_name' => 'required|min:2|max:50',
            'username' => 'sometimes|nullable|min:6|max:50|unique:users,username,'.$id,
            'email' => 'required|min:6|max:50|unique:users,email,'.$id,
            'image' =>'sometimes|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ];
    }

}
