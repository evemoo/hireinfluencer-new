<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class CategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->request->all());
        $id = $this->get('id');
        $lang = \ConfigHelper::evemooConfig('lang.default');
        return [
            $lang.'.title' => 'bail|required|min:2|max:100|unique:category_translations,title,'.$id.',entry_id',
            'default.status' => 'required|boolean',
            $lang.'.description' => 'bail|sometimes|max:255|min:10'
        ];
    }
}
