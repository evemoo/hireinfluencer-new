<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $lang = \ConfigHelper::evemooConfig('lang.default');
        return [
            'default.status' => 'required|boolean',
            $lang.'.title' => 'bail|required|min:2|max:100|unique:category_translations,title',
            $lang.'.description' => 'bail|sometimes|max:255|min:10'
        ];
    }

    public function messages() {
        $lang = \ConfigHelper::evemooConfig('lang.default');
        $language = \ConfigHelper::evemooConfig('lang.options.'.$lang.'.title');
        return [
           'default.status.required'        => 'Status field is required.',
           'default.status.boolean'         => 'Please DO NOT mess up with the values',
           $lang.'.title.required'          => $language.' Title is required',
           $lang.'.title.min'               => $language.' Title must be atleast 2 characters.',
           $lang.'.title.max'               => $language.' Title must be atmost 100 characters.',
           $lang.'.title.unique'            => $language.' Title already taken.',
           $lang.'.description.required'    => $language.' Description is required',
           $lang.'.description.min'         => $language.' Description must be atleast :min characters.',
           $lang.'.description.max'         => $language.' Description must be atmost :max characters.',
        ];
    }
}
