<?php

namespace App\Http\Requests\Admin\Service;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'en.*'      => 'required',
            'default.type' => 'required| unique:services,type'
        ];
    }

    public function messages() {
        return [
           'default.type.unique'   => 'This Service has already been taken.',
           'default.type.required' => 'This Service field is required.',
           'en.title.required'     => 'English Title field is required',
           'en.hint.required'      => 'English Hint field is required',
           'en.icon.required'      => 'English Hint field is required',
        ];
    }
}
