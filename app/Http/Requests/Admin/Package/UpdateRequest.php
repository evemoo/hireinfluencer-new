<?php

namespace App\Http\Requests\Admin\Package;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'en.*'     => 'required',
            'default.*'     => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            //english message
            'en.title.required'     => 'English Title field is required',
            'en.details.required'   => 'English Detail field is required',
            // default message
            'default.icon.required' => 'Icon field is required',
        ];
    }
}
