<?php

namespace App\Http\Requests\Admin\Package;

use App\Rules\CustomPackageUniqueValidation;
use App\Rules\QuantityPackageUniqueValidation;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'en.*'                      => 'required',
            'default.*'                 => 'required',
            'default.packages_types_id' => [ 'required', new CustomPackageUniqueValidation($this->request) ],
            'default.qty'               => [ 'required', new QuantityPackageUniqueValidation($this->request) ],
        ];
    }



    public function enMessages() {

        return [
            'en.title.required'     => 'English Title field is required',
            'en.details.required'   => 'English Detail field is required',
        ];

    }

    /**
     * Default Messages
     * @return array
     */
    public function defaultMessages()
    {
        return [
            'default.packages_types_id.required' => 'Package Type field is required',
            'default.qty.required'             => 'Quantity field is required',
            'default.old_price.required'       => 'Old Price field is required',
            'default.new_price.required'       => 'New Price field is required',
            'default.delivery_in_min.required' => 'Delivery in minimum field is required',
            'default.delivery_in_max.required' => 'Delivery in maximum field is required',
            'default.details.required'         => 'Detail field is required',
            'default.rank.required'            => 'Rank is required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return array_merge($this->enMessages(), $this->defaultMessages());
    }
}
