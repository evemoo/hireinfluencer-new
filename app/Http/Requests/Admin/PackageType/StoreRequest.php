<?php

namespace App\Http\Requests\Admin\PackageType;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'en.*'      => 'required',
            'default.*' => 'required'
        ];
    }

    public function enMessages() {

        return [
            'en.title.required'     => 'English Title field is required',
            'en.hint.required'      => 'English Hint field is required'
        ];

    }

    /**
     * Default Messages
     * @return array
     */
    public function defaultMessages()
    {
        return [
            'default.services_id.required' => 'Services field is required',
            'default.rank.required'        => 'Quantity field is required',
            'default.status.required'      => 'Old Price field is required'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return array_merge($this->enMessages(), $this->defaultMessages());
    }
}
