<?php

namespace App\Http\Requests\Customer\Profile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile.birth_date'       => 'date',
            'profile.password'    => 'confirmed',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'profile.birth_date.date'     => 'Use Proper Date',
            'profile.password.confirm'   => 'Password Mismatch',
        ];
    }
}
