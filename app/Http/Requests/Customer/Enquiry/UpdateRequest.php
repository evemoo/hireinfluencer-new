<?php

namespace App\Http\Requests\Customer\Enquiry;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject'       => 'required',
            'service_id'    => 'required|integer',
            'deadline'      => 'required|date',
            'message'       => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'subject.required'     => 'Subject field is required',
            'service_id.required'   => 'Platform field is required',
            'service_id.integer'   => 'Invalid Platform',
            'deadline.required'   => 'Deadline field is required',
            'deadline.date'   => 'Deadline must be date',
            'message.required'   => 'Message field is required',
        ];
    }
}
