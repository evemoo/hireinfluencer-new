<?php

namespace App\Http\Requests\Influencer\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name'  => 'required|max:20',
            'username'   => 'required|max:20|unique:users,username',
            'contact'    => 'required|max:10|min:10',
            'email'      => 'required|email|max:30|unique:users,email',
            'password'   => 'required|max:45|confirmed',
            'message'    => 'required|max:200|min:10'
        ];
    }



}