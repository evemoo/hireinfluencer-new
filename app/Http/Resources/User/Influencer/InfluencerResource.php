<?php

namespace App\Http\Resources\User\Influencer;

use App\Evemoo\Facades\AppHelper;
use Illuminate\Http\Resources\Json\Resource;

class InfluencerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => AppHelper::getFullName($this),
            'username' => $this->influencer_username,
            'email' => $this->email,
            'gender' => $this->gender != null?$this->gender:'Unspecified!',
        ];
    }
}
