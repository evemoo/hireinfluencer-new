<?php

namespace App\Http\Controllers\Social;

use App\Evemoo\Social\Services\Twitter\TwitterService;
use App\Http\Controllers\Controller;

class TwitterController extends Controller
{

    /*
     * @var Service
     */
    private $service;

    /**
     * TwitterController constructor.
     * @param TwitterService $service
     */
    public function __construct( TwitterService $service )
    {
        $this->service = $service;
    }

    /**
     * @param $profile
     * @param string $part
     * @return array|string
     */
    public function getStatistics($profile, $part = '' )
    {
        $data = [];

        $data = $this->service->getStatistics($profile);

        if (!empty($part)) {
            $data = $this->checkStatus($data, $part);
        }

        return $data;
    }

    /**
     * @param $query
     * @param string $part
     * @return array|string
     */
    public function searchTweet($query, $part = '')
    {
        $data = [];

        $data = $this->service->searchTweet($query);

        return $data;
    }

    /**
     * @param $tweet
     * @param string $part
     * @return array|string
     */
    public function getTweet( $tweet, $part = '' )
    {
        $data = [];

        $data = $this->service->getTweetStatistics($tweet);

        return $data;
    }

    /**
     * @param $data
     * @param $part
     * @return int|string
     */
    public function checkStatus ($data, $part)
    {
        if (is_array($data))
        {
            return $data;
        }

        return $this->response(
            json_encode($data), $part);
    }

    public function response($data, $query)
    {
        $service = 'TwitterController';

        switch ($query) {
            case "retweets":
                return $this->getSearchTweet($service, $query);
                break;
            case "likes":
                return $this->getTweetById($service, $query);
                break;
            case "followers":
                return $data->followers_count;
                break;
            default:
                return $this->errorHandler('', $query);
        }
    }

}