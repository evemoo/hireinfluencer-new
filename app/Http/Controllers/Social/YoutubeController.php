<?php

namespace App\Http\Controllers\Social;

use App\Evemoo\Social\Services\Http\HttpInterface;
use App\Evemoo\Social\Services\YouTube\YouTubeInterface;
use App\Evemoo\Social\Traits\SocialTrait;
use App\Http\Controllers\Controller;

class YoutubeController extends Controller
{

    use SocialTrait;

    /*
     * @var Service
     */
    private $service;

    /*
     * @var Http
     */
    protected $http;

    /**
     * YoutubeController constructor.
     * @param YouTubeInterface $youTube
     * @param HttpInterface $http
     */
    public function __construct(
        YouTubeInterface $youTube,
        HttpInterface $http
    )
    {
        $this->service = $youTube;
        $this->http    = $http;
    }


    /**
     * @param $profile
     * @param string $part
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatistics($profile, $part = '')
    {

        $data = [];

        $result = $this->service->profileParams($profile);

        if (empty($part))
        {
            $data = $this->http->send(
                $result['endpoint'], $result['params']
            );
        } else
        {
            $data = $this->http->send(
                $result['endpoint'], $result['params']
            );

            if (count($data->items) > 0)
            {
                $data = $this->response($data, $part);
            } else {
                $data = [
                    'status' => 404
                ];
            }

        }


        return response()
            ->json($data);

    }

    /**
     * Get Video Post Like, Shares & Comment count
     * @param $video_id
     * @param string $part
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVideoStatistics ($video_id, $part = '')
    {
        $data = [];

        $result = $this->service->getVideoStatistics($video_id);

        if (empty($part))
        {
            $data = $this->http->send(
                $result['endpoint'], $result['params']
            );
        } else
        {
            $data = $this->http->send(
                $result['endpoint'], $result['params']
            );

            $data = $this->response($data, $part);
        }

        return response()
            ->json($data);
    }

    /**
     * @param $data
     * @param $query
     * @return int
     */
    public function response($data, $query)
    {
        switch ($query) {
            case "likes":
                return $data->items[0]->statistics->likeCount;
                break;
            case "share":
                return 0;
                break;
            case "comment":
                return $data->items[0]->statistics->commentCount;
                break;
            case "views":
                return $data->items[0]->statistics->viewCount;
                break;
            case "subscriber":
                return $data->items[0]->statistics->subscriberCount;
                break;
            default:
                return $this->errorHandler('', $query);
        }
    }

}