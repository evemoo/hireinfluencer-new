<?php

namespace App\Http\Controllers\Social;

use App\Evemoo\Social\Services\Http\HttpInterface;
use App\Evemoo\Social\Services\Instagram\InstagramInterface;
use App\Evemoo\Social\Traits\SocialTrait;
use App\Http\Controllers\Controller;

class InstagramController extends Controller
{

    use SocialTrait;

    /*
     * @var Service
     */
    private $service;

    /*
     * @var Http
     */
    protected $http;

    /**
     * InstagramController constructor.
     * @param InstagramInterface $instagram
     * @param HttpInterface $http
     */
    public function __construct(
        InstagramInterface $instagram,
        HttpInterface $http
    )
    {
        $this->service = $instagram;
        $this->http    = $http;
    }

    /**
     * @param $profile
     * @param string $part
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatistics($profile, $part = '')
    {

        $data = [];

        $result = $this->service->profileParams($profile);

        $data = $this->http->send(
            $result['endpoint'], $result['params']
        );

        if (!empty($part))
        {
            $data = $this->checkStatus($data, $part);
        }

        return response()
            ->json($data);

    }

    /**
     * Get Post Like, Shares & Comment count
     * @param $post_id
     * @param string $part
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPostStatistics ($post_id, $part = '')
    {
        $data = [];

        $result = $this->service->getPostStatistics($post_id);

        $data = $this->http->send(
            $result['endpoint'], $result['params']
        );

        if (!empty($part))
        {
            $data = $this->checkStatus($data, $part);
        }

        return response()
            ->json($data);
    }

    /**
     * @param $data
     * @param $part
     * @return int|string
     */
    public function checkStatus ($data, $part)
    {
        if (is_object($data))
        {
            $data = $this->response($data, $part);
        }

        return $data;
    }

    /**
     * @param $data
     * @param $query
     * @return int|string
     */
    public function response($data, $query)
    {
        switch ($query) {
            case "likes":
                return $data->data->likes->count;
                break;
            case "share":
                return 0;
                break;
            case "comment":
                return $data->data->comments->count;
                break;
            case "followers":
                return $data->user->followed_by->count;
                break;
            default:
                return $this->errorHandler('', $query);
        }
    }

}