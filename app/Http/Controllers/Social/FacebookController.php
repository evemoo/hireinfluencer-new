<?php

namespace App\Http\Controllers\Social;

use App\Evemoo\Social\Services\Facebook\FacebookInterface;
use App\Evemoo\Social\Services\Http\HttpInterface;
use App\Evemoo\Social\Traits\SocialTrait;
use App\Http\Controllers\Controller;

class FacebookController extends Controller
{

    use SocialTrait;

    /*
     * @var Service
     */
    private $service;

    /*
     * @var Http
     */
    protected $http;

    /**
     * FacebookController constructor.
     * @param FacebookInterface $facebook
     * @param HttpInterface $http
     */
    public function __construct(
        FacebookInterface $facebook,
        HttpInterface $http
    )
    {
        $this->service = $facebook;
        $this->http    = $http;
    }

    /**
     * @param $query
     * @param string $part
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($query, $part = '' )
    {

        $data = [];

        $result = $this->service->searchParams($query);

        if (empty($part))
        {
            $data = $this->http->send(
                $result['endpoint'], $result['params']
            );
        } else
        {
            $data = $this->http->send(
                $result['endpoint'], $result['params']
            );

            if (count($data) > 0)
            {
                $data = $this->response($data, $part);
            } else {
                $data = [
                    'status' => 404
                ];
            }

        }

        return response()
            ->json($data);

    }

    /**
     * Get Facebook Page Rating
     * @param $page_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPageRating ($page_id)
    {
        $data = [];

        $result = $this->service->getPageRating($page_id);

        $data = $this->http->send(
            $result['endpoint'], $result['params']
        );

        return response()
            ->json($data);
    }

    /**
     * Get Facebook Post Like, Shares & Comment count
     * @param $post_id
     * @param string $part
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPostStatistics ( $post_id, $part = '')
    {
        $data = [];

        $result = $this->service->getPostStatistics($post_id);

        if (empty($part))
        {
            $data = $this->http->send(
                $result['endpoint'], $result['params']
            );
        } else
        {
            $data = $this->http->send(
                $result['endpoint'], $result['params']
            );

            if (count($data) > 0)
            {
                $data = $this->response($data, $part);
            } else {
                $data = [
                    'status' => 404
                ];
            }

        }

        return response()
            ->json($data);
    }

    public function response($data, $query)
    {
        switch ($query) {
            case "5-star-rating":
                return 0;
                break;
            case "comment":
                return $data->comments->summary->total_count;
                break;
            case "likes":
                return $data->fan_count;
                break;
            case "post-likes":
                return $data->likes->summary->total_count;
                break;
            case "share":
                return 0;
                break;
            default:
                return $this->errorHandler('', $query);
        }
    }

}