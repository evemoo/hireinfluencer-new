<?php

namespace App\Http\Controllers\Social;

use App\Evemoo\Services\Social\SocialInterface;
use App\Http\Controllers\Controller;
use App\Evemoo\Social\Traits\FuncSocialTrait;

class SocialBaseController extends Controller
{

    use FuncSocialTrait;

    /**
     * @var $api_pagination_limit
     */
    protected $api_pagination_limit = 10;

    /**
     * @var SocialInterface
     */
    protected $social;

    /**
     * @var $default_controller_path
     */
    protected $default_controller_path = 'App\Http\Controllers\Social';

    /**
     * SocialBaseController constructor.
     * @param SocialInterface $social
     */
    function __construct( SocialInterface $social )
    {
        $this->social = $social;
    }

    /**
     * @param $package_id
     * @return mixed
     */
    public function getPackageInfo($package_id)
    {
        return $this->social->getPackageInfo($package_id);
    }

    /**
     * @param $package_id
     * @param $query
     * @return mixed|string
     */
    public function setSocialProviders($package_id, $query)
    {
        return $this->getSocialProviders(
            $this->getPackageInfo($package_id), $query);
    }

    /**
     * @param $package
     * @param $query
     * @return mixed|string
     */
    public function getSocialProviders($package, $query)
    {
        switch ($package->service_type) {
            case "youtube":
                return $this->run($package, 'youtube', $query);
                break;
            case "facebook":
                return $this->run($package, 'facebook', $query);
                break;
            case "twitter":
                return $this->run($package, 'twitter', $query);
                break;
            case "instagram":
                return $this->run($package, 'instagram', $query);
                break;
            default:
                return $this->errorHandler('Service Type', $package->service_type);
        }

    }

    /**
     * @param $package
     * @param $service
     * @param $query
     * @return mixed|string
     */
    public function run ($package, $service, $query)
    {
        if (!in_array($package->package_type,
            array_keys(config('evemoo.services.'.$service.'.package_types'))))
        {
            return $this->errorHandler('Package Type', $package->package_type);
        }

        return call_user_func_array(
            [ $this, 'func_'.$service ],[ $package, $query ]);

    }

    /**
     * @param $type
     * @param $value
     * @return string
     */
    protected function errorHandler($type = '' , $value )
    {
        return sprintf("%s %s Not Matched.",$type ,$value);
    }


}