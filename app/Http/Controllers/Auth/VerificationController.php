<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VerificationController extends Controller
{
    public function verify(Request $request)
    {
    	if($request->has('token'))
    	{
    		$user = User::where('verify_token', $request->token)->first();
    		if(!$user)
    		{
    			session()->flash('warning', 'Invalid Token!');
    		}
    		else
    		{
    			if($user->enabled == 1)
    			{
    				session()->flash('warning', 'Already Verified! You can login now.');
    			}
    			elseif($user->enabled == 0)
    			{
	    			$user->update([
	    				'enabled' => 1,
	    			]);
	    			session()->flash('warning', 'Verification Successfull! You can login now.');
	    		}
    		}
    	}
    	else
    	{
    		session()->flash('warning', 'Invalid Token!');
    	}
    	
    	return redirect()->route('login');
    }
}
