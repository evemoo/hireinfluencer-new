<?php

namespace App\Http\Controllers\Auth;

use App\Evemoo\Services\User\UserInterface;
use App\Http\Controllers\Controller;
use App\Events\ResetPassword;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Mail;
use Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * ResetPasswordController constructor.
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->middleware('guest');
        $this->user = $user;
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules(), $this->validationErrorMessages());

        $user     = $this->user->findAsEmail($request->get('email'));

        $validator->after(function ($validator) use ($user) {
            if (!$user) {
                $validator->errors()->add('email', 'You haven\'t register. please register and login!');
            }
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        event(new ResetPassword($user));
        $request->session()->flash('success', "Password sent to your mail successfully");
        return redirect()->back();
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'email' => 'required|email'
        ];
    }
}
