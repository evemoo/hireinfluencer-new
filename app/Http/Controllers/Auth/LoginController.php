<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Evemoo\AppTrait\SocialLogin;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    use SocialLogin;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {

        if($user->enabled == 1)
        {
            if(
            in_array(\ConfigHelper::getRole('admin')['slug'],
                auth()->user()->roles->pluck('name')->all())
            )
            {
                $this->redirectTo = '/admin/dashboard';
            }

            if(
            in_array(\ConfigHelper::getRole('customer')['slug'],
                auth()->user()->roles->pluck('name')->all())
            )
            {
                $this->redirectTo = '/customer/dashboard';
            }
        }
        else
        {
            auth()->logout();
            session()->flash('warning', 'You are not Verified!');
            return back();
        }


    }

    public function blocked() {
        return view('index');
    }
}
