# About HireInfluencer Api

### OAuth Authentication

This is where we generate our **access_token** with users *email* and *password*.

>NOTE: This step is after a new client is set up where his/her **Client_ID** and **Client_Secret** is generated. Also we are using PostMan App.

#### Add Header Keys
>1. Key:Accept, Value:application/json
>1. Key:Content-Type, Value:application/json
>1. Key:Authorization, Value:Bearer access_token //this one after access_token is generated

Send a **POST** request to `http://hireinfluencer.localhost.com/oauth/token` with additional datas in the format given below:

	{
		"grant_type" : "password",
		"client_id" : {client id},
		"client_secret" : "{client_secret}",
		"username" : "{user email}",
		"password" : "{user password}"
	}

You'll get a response as such:

	{
	    "token_type": "Bearer",
	    "expires_in": 31536000,
	    "access_token": "{access_token:generated}",
	    "refresh_token": "{refresh_token:generated}"
	}

### Sign Up

This **does not** require an **access_token**. Send a **POST** request to `http://hireinfluencer.localhost.com/api/influencer/signup`.

At least these fields are required:

	{
		"name": "{Full Name}",
        "username": "{username}",
        "email": "{email address}",
		"password" : "*******"
	}

#### Response

##### On Success

	{
	    "data": {
	        "name": "{Full Name}",
            "username": "{username}",
            "email": "{email address}",
            "primary_contact": {primary contact info},
            "secondary_contact": {secondary contact info},
            "gender": "{gender}!",
	        "social_media": []
	    }
	}

##### On Validation Failure

	{
		"message": "The given data was invalid.",
	    "errors": {
	        "username": [
	            "The username has already been taken."
	        ],
	        "email": [
	            "The email must be a valid email address."
	        ],
	        "password": [
	            "The password field is required."
	        ]
	    }
	}

### Get Influencer Profile Data

>Note: Available only after user has logged in.

This is where we get the basic info of an influencer filtered by his/her *username*.

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}`.

#### Response

##### On Success

	{
	    "data": {
	        "name": "{Full Name}",
            "username": "{username}",
            "email": "{email address}",
            "primary_contact": {primary contact info},
            "secondary_contact": {secondary contact info},
            "gender": "{gender}!",
	        "social_media": {
	            "{social_media_slug}": "http://hireinfluencer.localhost.com/api/influencer/{username}/{social_media_slug}",
	            "{social_media_slug}": "http://hireinfluencer.localhost.com/api/influencer/{username}/{social_media_slug}"
	            ...
	        }
	    }
	}

##### On Failure

	{
	    "message": "Access Denied!"
	}

### Update Influencer Profile Data

Send a **POST** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/update`.

#### Response

##### On Success

    {
        "data": {
            "name": "{Full Name}",
            "username": "{username}",
            "email": "{email address}",
            "primary_contact": {primary contact info},
            "secondary_contact": {secondary contact info},
            "gender": "{gender}!",
            "social_media": [
                {linked social medias}
            ]
        }
    }

##### On Validation Failure

    {
        "message": "The given data was invalid.",
        "errors": {
            "username": [
                "The username has already been taken."
            ],
            "email": [
                "The email must be a valid email address."
            ],
            "password": [
                "The password field is required."
            ]
        }
    }

### Delete Influencer/User Profile

Send a **POST** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/delete`.

#### Response

##### On Success

    {
        NO CONTENT!
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Get Influencer Social Media Profiles

Send a **POST** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/{social_media_slug}`.

#### Response

##### On Success

    {
        "data": [
            {
                "name": "{Profile Display Name}",
                "followers": {profile followers count},
                "url": "{profile url}"
            },
            {
                "name": "{Profile Display Name}",
                "followers": {profile followers count},
                "url": "{profile url}"
            }
            ...
        ]
    }

##### On Failure

###### Route Not Found

    {
        "message": "Route Not Found!"
    }

###### Un-authorized Access

    {
        "message": "Access Denied!"
    }"message": "Route Not Found!"
    }

###### No Data

    {
        "data": []
    }

### Create Influencer Social Media Profiles

Catch the following informations from the social media login and send a **POST** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/{social_media_slug}/create`

    {
        "attribute_id" : "{attribute ID}",
        "name" : "{Profile Full Name}",
        "followers" : {followers count},
        "url" : "{url to the profile}"
    }

#### Response

##### On Success

    {
        "name" : "{Profile Full Name}",
        "followers" : {followers count},
        "url" : "{url to the profile}"
    }

##### On Validation Failure

    {
        "message": "The given data was invalid.",
        "errors": {
            "url": [
                "social profile already registered!"
            ]
        }
    }

### Update Influencer Social Media Profiles

Send a **POST** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/{social_media_slug}/create`.

Some example datas to send:

    {
        "id" : "{profile id}",
        "attribute_id" : {attribute ID},
        "name" : "{Profile Full Name}",
        "followers" : {followers count},
        "url" : "{url to the profile}"
    }

#### Response

##### On Success

    {
        "name" : "{Profile Full Name}",
        "followers" : {followers count},
        "url" : "{url to the profile}"
    }

##### On Validation Failure

    {
        "message": "The given data was invalid."
    }

### Delete Influencer Social Media Profiles

Send a **POST** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/delete`.

Also send the `id` of the profile.

#### Response

##### On Success

    {
        NO CONTENT!
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot List

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot`.

#### Response

##### On Success

    {
        "data": [
            {
                "slot_id": {slot id},
                "social_media": "{social media slug}",
                "social_media_icon": "{social media icon}",
                "name": "{Slot Name}",
                "thumbnail": "{Slot Thumbnail}",
                "category": "{Slot Category}",
                "deliver_within": {delivery in days},
                "reviews_count": {number of reviews},
                "average_rating": {average rating},
                "active": {status:true/false},
                "href": {
                    "details": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}"
                }
            },
            ...
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Detail

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}`.

#### Response

##### On Success

    {
        "data": {
            "slot_id": {slot ID},
            "social_media": "{social media slug}",
            "social_media_icon": "{social media icon}",
            "social_media_page_name": "{social media page name}",
            "social_media_page_url": "{social media page url}",
            "name": "{Slot Name}",
            "thumbnail": "{Slot Thumbnail}",
            "category": "{Slot Category}",
            "deliver_within": {delivery in days},
            "reviews_count": {number of reviews},
            "average_rating": {average rating},
            "active": {status:true/false},
            "href": {
                "gallery": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/gallery",
                "packages": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/packages",
                "projects": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/projects",
                "reviews": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/reviews"
            }
        }
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Based Reviews

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/reviews`.

#### Response

##### On Success

    {
        "data": [
            {
                "project": "{Project Title}",
                "reviewer": "{reviewer username}",
                "review": {
                    "star": {review rating},
                    "comment": "{review comment}",
                    "author_reply": "{reply by author}"
                }
            },
            ...
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Projects

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/projects`.

#### Response

##### On Success

    {
        "data": [
            {
                "package": "{Package Title}",
                "title": "{Project Title}",
                "slug": "{Project Slug}",
                "budget": {project budget},
                "agreed_on": {project final deal price},
                "href": {
                    "details": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/projects/{project_id}"
                }
            },
            ...
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Gallery List

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/gallery`.

#### Response

##### On Success

    {
        "data": [
            {
                "image": "{image url}",
                "alt_text": "{alternate text}",
                "active": {status:true/false},
            },
            ...
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Based Packages

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/packages`.

#### Response

##### On Success

    {
        "data": [
            {
                "title": "{Package Title}",
                "summary": "{Package Summary}",
                "price": {Package Price},
                "deliver_in_days": {delivery in days},
                "audience_size": {social media followers},
                "default_package": {true/false},
                "href": {
                    "details": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/packages/{package_id}"
                }
            },
            ...
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Package Detail

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/packages/{package_id}`.

#### Response

##### On Success

    {
        "data": [
            {
                "title": "{Package Title}",
                "summary": "{Package Summary}",
                "price": {Package Price},
                "description": "{Package Description}",
                "deliver_in_days": {delivery in days},
                "audience_size": {social media followers},
                "post_count": {number of posts to make},
                "additional_link": {true/false},
                "default_package": {true/false},
                "permanent_post": {true/false},
                "href": {
                    "projects": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/packages/{package_id}/projects"
                }
            }
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Package Projects

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/packages/{package_id}/projects`.

#### Response

##### On Success

    {
        "data": [
            {
                "package": "{Package Title}",
                "title": "{Project Title}",
                "slug": "{Project Slug}",
                "budget": {project budget},
                "agreed_on": {project final deal price},
                "href": {
                    "details": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/projects/{project_id}"
                }
            },
            ...
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Project Detail

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/packages/{package_id}/projects/{project_id}`.

#### Response

##### On Success

    {
        "data": [
            {
                "package": "{Package Title}",
                "title": "{Project Title}",
                "slug": "{Project Slug}",
                "description": "{Project Description}",
                "budget": {project budget},
                "agreed_on": {project final deal price},
                "href": {
                    "reviews": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/project/{project_id}/reviews",
                    "project_chat": "http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/project/{project_id}/chat"
                }
            }
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Project Reviews

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/packages/{package_id}/projects/{project_id}/reviews`.

#### Response

##### On Success

    {
        "data": [
            {
                "project": "{Project Title}",
                "reviewer": "{reviewer username}",
                "review": {
                    "star": {review rating},
                    "comment": "{review comment}",
                    "author_reply": "{reply by author}"
                }
            },
            ...
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

### Influencer Slot Project Chat

Send a **GET** request to `http://hireinfluencer.localhost.com/api/influencer/{username}/slot/{slot_id}/packages/{package_id}/projects/{project_id}/chat`.

#### Response

##### On Success

    {
        "data": [
            {
                "project": "{Project Title}",
                "from": "{sender username}",
                "message": "{message}"
            },
            ...
        ]
    }

##### On Failure

    {
        "message": "Access Denied!"
    }

