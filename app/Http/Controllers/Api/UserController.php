<?php

namespace App\Http\Controllers\Api;

use AppExceptionHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserCollection;
use App\Http\Controllers\Api\User\Services\UserServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserServices
     */
    private $user;

    /**
     * UserController constructor.
     * @param UserServices $user
     */
    public function __construct(UserServices $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        //dd($this->user->getAllUsersWithPagination());
        return UserCollection::collection($this->user->getAllUsersWithPagination());
    }

    /*public function getClient($query =null)
    {
        $clients = $this->user->getAllClientUser($query, 5);

        return new JsonResponse([
            'success' => true,
            'results' => dropDownFormat($clients, 'username', 'id')
        ]);
    }*/


}
