<?php

namespace App\Http\Controllers\Api\User\Services;

use App\Evemoo\AppTrait\LangByRepo;
use DB;
use Exception;
use Carbon\Carbon;

use App\User;
use App\Evemoo\Models\Slot;
use App\Evemoo\Models\Review;
use App\Evemoo\Models\Project;
use App\Evemoo\Models\ProjectChat;
use App\Evemoo\Models\Influencer;
use App\Evemoo\Models\Package;
use App\Evemoo\Models\Category;
use App\Evemoo\Models\InfluencerSocialData;
use App\Evemoo\Models\SocialMediaAttribute;
use App\Evemoo\Models\SocialMediaAttributeTranslation;
use App\Evemoo\Models\ReviewTranslation;
use App\Evemoo\Models\Blacklist;
use App\Evemoo\Models\RoleUser;
use App\Evemoo\Models\Role;
use App\Evemoo\Models\SlotTranslation;
use App\Evemoo\Models\PackageTranslation;
use App\Evemoo\Models\ProjectTranslation;
use App\Evemoo\Models\SocialMedia;
use App\Evemoo\Models\SlotCategory;
use App\Evemoo\Models\SocialMediaTranslation;
use App\Evemoo\Models\CategoryTranslation;
use App\Evemoo\Models\PackageExtraWork;
use App\Evemoo\Models\ProjectStateTracking;

use App\Evemoo\Repositories\Influencer\UserRepository;
use App\Evemoo\Repositories\Influencer\SlotRepository;
use App\Evemoo\Repositories\Influencer\ReviewRepository;
use App\Evemoo\Repositories\Influencer\ProjectRepository;
use App\Evemoo\Repositories\Influencer\PackageRepository;
use App\Evemoo\Repositories\Influencer\CategoryRepository;
use App\Evemoo\Repositories\Influencer\BlacklistRepository;
use App\Evemoo\Repositories\Influencer\InfluencerRepository;
use App\Evemoo\Repositories\Influencer\SocialMediaRepository;
use App\Evemoo\Repositories\Influencer\ProjectChatRepository;
use App\Evemoo\Repositories\Influencer\InfluencerSocialDataRepository;

use Evemoo\Repositories\Criteria\GroupByCriteria;
use Evemoo\Repositories\Criteria\InnerJoinCriteria;
use Evemoo\Repositories\Criteria\LeftJoinCriteria;
use Evemoo\Repositories\Criteria\OrderByCriteria;
use Evemoo\Repositories\Criteria\SelectCriteria;
use Evemoo\Repositories\Criteria\WhereCriteria;
use Evemoo\Repositories\Criteria\OrWhereCriteria;

use App\Evemoo\Repositories\Influencer\Criteria\User\ReviewProjectFilterCriteria;
use App\Evemoo\Repositories\Influencer\Criteria\User\ListFilterCriteria;

class UserServices
{
    use LangByRepo;

    /**
     * @var User Repository
     */
    private $model_repo;

    /**
     * UserService constructor.
     * @param User $user
     */
    public function __construct(
        UserRepository $model_repo,
        SlotRepository $slot,
        ReviewRepository $review,
        ProjectRepository $project,
        PackageRepository $package,
        CategoryRepository $category,
        BlacklistRepository $blacklist,
        InfluencerRepository $influencer,
        SocialMediaRepository $social_media,
        ProjectChatRepository $project_chat,
        InfluencerSocialDataRepository $influencer_social_data
    )
    {
        $this->model_repo = $model_repo;
        $this->slot = $slot;
        $this->review = $review;
        $this->project = $project;
        $this->package = $package;
        $this->category = $category;
        $this->blacklist = $blacklist;
        $this->influencer = $influencer;
        $this->social_media = $social_media;
        $this->project_chat = $project_chat;
        $this->influencer_social_data = $influencer_social_data;
    }

    /**
     * Get all users with pagination.
     * @return mixed
     */
    public function getAllUsersWithPagination()
    {
        $users = $this->model_repo
                    ->pushCriteria(new SelectCriteria([
                        'users.*', 'roles.name'
                    ]))
                    ->pushCriteria(new OrderByCriteria(new User, 'id', 'desc'))
                    ->pushCriteria(new InnerJoinCriteria(new RoleUser, new User, 'user_id', 'id'))
                    ->pushCriteria(new InnerJoinCriteria(new Role, new RoleUser, 'id', 'role_id'))
                    ->pushCriteria(new LeftJoinCriteria(new Influencer, new User, 'user_id', 'id'))
                    ->pushCriteria(new LeftJoinCriteria(new InfluencerSocialData, new Influencer, 'influencer_id', 'id'))
                    ->pushCriteria(new LeftJoinCriteria(new SocialMedia, new InfluencerSocialData, 'id', 'social_media_id'))
                    ->pushCriteria(new LeftJoinCriteria(new Slot, new InfluencerSocialData, 'influencer_social_data_id', 'id'))
                    ->pushCriteria(new LeftJoinCriteria(new Package, new Slot, 'slot_id', 'id'))
                    ->pushCriteria(new LeftJoinCriteria(new Project, new Package, 'package_id', 'id'))
                    ->pushCriteria(new LeftJoinCriteria(new SlotTranslation, new Slot, 'entry_id', 'id'))
                    ->pushCriteria(new LeftJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                    ->pushCriteria(new LeftJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                    ->pushCriteria(new LeftJoinCriteria(new SlotCategory, new Slot, 'slot_id', 'id'))
                    ->pushCriteria(new LeftJoinCriteria(new Category, new SlotCategory, 'id', 'category_id'))
                    ->pushCriteria(new GroupByCriteria(new User, 'id'))
                    ->paginate(10);

                    $this->model_repo->resetModel();

        return $users;
    }

    public function getInfluencerStats()
    {
        $stats = [];

        $stats['influencers_count'] = $this->model_repo
                    ->pushCriteria(new InnerJoinCriteria(new RoleUser, new User, 'user_id', 'id'))
                    ->pushCriteria(new InnerJoinCriteria(new Role, new RoleUser, 'id', 'role_id'))
                    ->pushCriteria(new WhereCriteria(new Role, 'name', 'influencer'))
                    ->count();

        $stats['slots_count'] = $this->slot
                    ->count();

        $stats['total_earnings'] = $this->project
                    ->pushCriteria(new SelectCriteria([DB::raw('SUM(final_deal_price) as total_earnings')]))
                    ->first()
                    ->total_earnings;

        $stats['project_completed_count'] = $this->project
                    ->pushCriteria(new WhereCriteria(new Project, 'status', 1))
                    ->count();

        $stats['project_ongoing_count'] = $this->project
                    ->pushCriteria(new WhereCriteria(new Project, 'status', 0))
                    ->count();

        return $stats;
    }

    public function getUserQuickDetails($id)
    {
        $details = [];

        $details['slots_count'] = $this->slot
                    ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                    ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                    ->count();

        $details['total_earnings'] = $this->project
                    ->pushCriteria(new SelectCriteria([DB::raw('SUM(final_deal_price) as total_earnings')]))
                    ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                    ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                    ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                    ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                    ->first()
                    ->total_earnings;

        $details['project_completed_count'] = $this->project
                    ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                    ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                    ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                    ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                    ->pushCriteria(new WhereCriteria(new Project, 'status', 1))
                    ->count();

        $details['project_ongoing_count'] = $this->project
                    ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                    ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                    ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                    ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                    ->pushCriteria(new WhereCriteria(new Project, 'status', 0))
                    ->count();

        return $details;
    }

    /**
     * Create new user
     *
     * @param $userDetails
     */
    public function create($userDetails)
    {
        try {

            $user = $this->model_repo->create($userDetails);

            return $user;
        } catch (Exception $exception)
        {
            $message  = PHP_EOL.'Error: '.User::class.' : model not saved.';
            $message .= PHP_EOL.'Path: '.get_class($this).'@'.__FUNCTION__;
            $message .= PHP_EOL.'URl: '. request()->fullUrl();

            \AppExceptionHandler::systemError(
                config('evemoo.error-codes.model-not-saved'),
                $message,
                1
            );

            return;
        }

    }

    /**
     * User Update
     *
     * @param $user
     * @param $userDetails
     */
    public function update($user, $userDetails)
    {
        try {

            if(empty($userDetails['password'])) {
                unset($userDetails['password']);
            }
            if(!empty($userDetails['birth_date'])) {
                $userDetails['birth_date'] = \Carbon\Carbon::parse($userDetails['birth_date']);
            }

            $userDetails['social_medias'] = $this->prepareSocialMediaLinks($userDetails);

            $user = $user->update($userDetails);

            return $user;

        } catch (Exception $exception)
        {
            $message  = PHP_EOL.'Error: '.User::class.' : model not updated.';
            $message .= PHP_EOL.'Path: '.get_class($this).'@'.__FUNCTION__;
            $message .= PHP_EOL.'URl: '. request()->fullUrl();

            \AppExceptionHandler::systemError(
                config('evemoo.error-codes.model-not-updated'),
                $message,
                1
            );

            return;
        }
    }

    /**
     * @param $password
     * @return mixed|void
     */
    public function resetPassword($password)
    {
        try {

            $user = $this->find(auth()->user()->id);
            $user->password = $password['password'];

            $user->save();

            return $user;

        } catch (Exception $exception)
        {
            $message  = PHP_EOL.'Error: '.User::class.' : model not updated.';
            $message .= PHP_EOL.'Path: '.get_class($this).'@'.__FUNCTION__;
            $message .= PHP_EOL.'URl: '. request()->fullUrl();

            \AppExceptionHandler::systemError(
                config('evemoo.error-codes.model-not-updated'),
                $message,
                1
            );

            return;
        }
    }

    /**
     * @param $email
     * @return mixed
     */
    public function findAsEmail($email)
    {
        $user = $this->model_repo
            ->pushCriteria(new WhereCriteria(new User, 'email', $email))
            ->first();

        return $user;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $user = $this->model_repo->findOrFail($id);

        $user = $this->changeJsonToArray($user);

        return $user;
    }

    public function uploadImage($request)
    {
        $image = $request->file('image');
        $imageName = asset('img/profile/').time().'.'.$image->getClientOriginalExtension();
        $folderPath = public_path('/img/profile');
        $image->move($folderPath, $imageName);

        return $imageName;
    }

    /**
     * @param null $users
     * @param int $limit
     * @return mixed
     */
    public function getAllClientUser($users =null, $limit= 10)
    {
        $clients = $this->model_repo->select('users.*')
                    ->join('role_user', 'role_user.user_id', '=', 'users.id')
                    ->join('roles', function ($query) {
                        $query->on('roles.id', '=', 'role_user.role_id')
                            ->where('roles.name', \ConfigHelper::getRole('customer')['slug']);
                    })->where(function ($query) use ($users) {
                        if($users) {
                            $query->where('users.username', 'like', '%'.$users.'%');
                        }
                    })
                    ->take($limit)
                    ->get();

        return $clients;
    }

    public function getStatus()
    {
        $status = ['all'=>'Status - All', '1'=>'Active', '0'=>'Inactive'];
        return $status;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $roles = ['all'=>'Roles - All', '1'=>'Admin', '2'=>'Brand', '3'=>'Influencer'];

        return $roles;
    }

    /**
     * @return mixed
     */
    public function getGenders()
    {
        $roles = ['all'=>'Genders - All', 'male'=>'Male', 'female'=>'Female', 'other'=>'Other'];

        return $roles;
    }

    /**
     * @return mixed
     */
    public function getSocialMedias()
    {
        $medias = $this->social_media
                    ->pushCriteria(new InnerJoinCriteria(new SocialMediaTranslation, new SocialMedia, 'entry_id', 'id'))
                    ->pushCriteria(new WhereCriteria(new SocialMediaTranslation, 'code', config('evemoo.lang.default')))
                    ->pluck('social_media_translations.name', 'social_media.id');

        return $medias;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        $categories = $this->category
                    ->pushCriteria(new InnerJoinCriteria(new CategoryTranslation, new Category, 'entry_id', 'id'))
                    ->pushCriteria(new WhereCriteria(new CategoryTranslation, 'code', config('evemoo.lang.default')))
                    ->pluck('category_translations.title', 'categories.id');

        return $categories;
    }

    /**
     * @return mixed
     */
    public function getUserSlots($id, $request)
    {
        $slots = $this->slot
                ->pushCriteria(new SelectCriteria([
                    'slots.*', 'slot_translations.title', 'slot_translations.description'
                ]))
                ->pushCriteria(new InnerJoinCriteria(new SlotTranslation, new Slot, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new SlotTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                ->pushCriteria(new OrderByCriteria(new Slot, 'id', 'desc'))
                ->get();

        return $slots;
    }

    /**
     * @return mixed
     */
    public function getUserSlotsBySocialMedia($id)
    {
        $slots = [];
        $social_medias = $this->getUserSocialMedias($id);

        foreach($social_medias as $media) {
            $slots[$media->slug] = $this->slot
                ->pushCriteria(new SelectCriteria(['slots.*', 'slot_translations.title', 'slot_translations.description', 'influencer_social_datas.name', 'social_media_translations.name as smTitle']))
                ->pushCriteria(new InnerJoinCriteria(new SlotTranslation, new Slot, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new InnerJoinCriteria(new SocialMedia, new InfluencerSocialData, 'id', 'social_media_id'))
                ->pushCriteria(new InnerJoinCriteria(new SocialMediaTranslation, new SocialMedia, 'entry_id', 'id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                ->pushCriteria(new WhereCriteria(new InfluencerSocialData, 'social_media_id', $media->id))
                ->pushCriteria(new WhereCriteria(new SlotTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new SocialMediaTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new OrderByCriteria(new Slot, 'id', 'desc'))
                ->get();
        }

        return $slots;
    }

    public function getSlotDetails($id, $user_id)
    {
        $details = $this->slot
                ->pushCriteria(new SelectCriteria(['slots.*', 'slot_translations.title', 'slot_translations.description', 'packages.price', 'package_extra_works.extra_price', 'social_media_translations.name']))
                ->pushCriteria(new InnerJoinCriteria(new SlotTranslation, new Slot, 'entry_id', 'id'))
                ->pushCriteria(new LeftJoinCriteria(new Package, new Slot, 'slot_id', 'id'))
                ->pushCriteria(new LeftJoinCriteria(new PackageExtraWork, new Package, 'package_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new SocialMedia, new InfluencerSocialData, 'id', 'social_media_id'))
                ->pushCriteria(new InnerJoinCriteria(new SocialMediaTranslation, new SocialMedia, 'entry_id', 'id'))
                ->pushCriteria(new WhereCriteria(new Slot, 'id', $id))
                ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $user_id))
                ->pushCriteria(new WhereCriteria(new SlotTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new SocialMediaTranslation, 'code', config('evemoo.lang.default')))
                ->firstOrFail();

        return $details;
    }

    public function getPackagesBySlot($id, $user_id)
    {
        $details = $this->package
                ->pushCriteria(new SelectCriteria(['packages.*', 'package_translations.title', 'package_translations.summary', 'package_translations.description']))
                ->pushCriteria(new InnerJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $user_id))
                ->pushCriteria(new WhereCriteria(new Package, 'slot_id', $id))
                ->pushCriteria(new WhereCriteria(new PackageTranslation, 'code', config('evemoo.lang.default')))
                ->get();

        return $details;
    }

    public function getProjectDetails($slug, $user)
    {
        $details = $this->project
                ->pushCriteria(new SelectCriteria(['projects.*', 'project_translations.title', 'project_translations.description', 'project_state_trackings.initialized_date', 'project_state_trackings.completed_date']))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new ProjectStateTracking, new Project, 'project_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $user))
                ->pushCriteria(new WhereCriteria(new Project, 'slug', $slug))
                ->firstOrFail();

        return $details;
    }

    public function getProjectChats($slug, $user)
    {
        $project = $this->getProjectDetails($slug, $user);

        $details = $this->project_chat
                ->pushCriteria(new WhereCriteria(new ProjectChat, 'project_id', $project->id))
                ->get();

        return $details;
    }

    public function getSlotfinance($id, $user_id)
    {
        $slot = $this->getSlotDetails($id, $user_id);

        $finance['list'] = $this->project
                ->pushCriteria(new SelectCriteria(['projects.*', 'project_translations.title', 'project_translations.description']))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new WhereCriteria(new Package, 'slot_id', $slot->id))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new OrderByCriteria(new Project, 'id', 'desc'))
                ->get();

        $finance['total'] = $this->project
                ->pushCriteria(new SelectCriteria([DB::raw('SUM(final_deal_price) as total')]))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new WhereCriteria(new Package, 'slot_id', $slot->id))
                ->first();

        return $finance;
    }

    public function getSlotCategory($id)
    {
        $category = $this->category
                    ->pushCriteria(new SelectCriteria(['categories.*', 'category_translations.title', 'category_translations.description']))
                    ->pushCriteria(new InnerJoinCriteria(new CategoryTranslation, new Category, 'entry_id', 'id'))
                    ->pushCriteria(new InnerJoinCriteria(new SlotCategory, new Category, 'category_id', 'id'))
                    ->pushCriteria(new InnerJoinCriteria(new Slot, new SlotCategory, 'id', 'slot_id'))
                    ->pushCriteria(new WhereCriteria(new Slot, 'id', $id))
                    ->first();

        return $category;
    }

    public function getSocialMediaDetails($user_id)
    {
        $details = [];

        $social_medias = $this->getUserSocialMedias($user_id);
        foreach($social_medias as $media) {
            $details[$media->slug] = $this->influencer_social_data
                                    ->pushCriteria(new SelectCriteria(['influencer_social_datas.*']))
                                    ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                                    ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $user_id))
                                    ->pushCriteria(new WhereCriteria(new InfluencerSocialData, 'social_media_id', $media->id))
                                    ->get();
        }

        return $details;
    }

    /**
     * @return mixed
     */
    public function getSlotReview($id)
    {
        $reviews = $this->review
                ->pushCriteria(new SelectCriteria(['reviews.*', 'review_translations.comment', 'influencers.influencer_username', 'project_translations.title', 'package_translations.title as pkg_title']))
                ->pushCriteria(new InnerJoinCriteria(new ReviewTranslation, new Review, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Project, new Review, 'id', 'project_id'))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new ReviewTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new PackageTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new Slot, 'id', $id))
                ->pushCriteria(new OrderByCriteria(new Review, 'id', 'desc'))
                ->get();
        return $reviews;
    }

    /**
     * @return mixed
     * Total Amount Earned by an influencer and the list of project from where he earned
     */
    public function getFinance($id)
    {
        $finance['total'] = $this->project
                ->pushCriteria(new SelectCriteria([DB::raw('SUM(projects.final_deal_price) as total')]))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                ->pushCriteria(new WhereCriteria(new Project, 'status', 1))
                ->pushCriteria(new WhereCriteria(new Project, 'final_deal_price', null, '!='))
                ->first();

        $finance['list'] = $this->project
                ->pushCriteria(new SelectCriteria(['projects.*', 'slots.banner', 'project_translations.title']))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                ->pushCriteria(new WhereCriteria(new Project, 'status', 1))
                ->pushCriteria(new WhereCriteria(new Project, 'final_deal_price', null, '!='))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->get();

        return $finance;
    }

    /**
     * @return mixed
     */
    public function getUserSocialMedias($id)
    {
        $social_medias = $this->influencer_social_data
                        ->pushCriteria(new SelectCriteria(['social_media.*', 'influencer_social_datas.value', 'social_media_translations.name', 'social_media_attribute_translations.name as sma_name', 'influencer_social_datas.profile_link']))
                        ->pushCriteria(new InnerJoinCriteria(new SocialMedia, new InfluencerSocialData, 'id', 'social_media_id'))
                        ->pushCriteria(new InnerJoinCriteria(new SocialMediaTranslation, new SocialMedia, 'entry_id', 'id'))
                        ->pushCriteria(new InnerJoinCriteria(new SocialMediaAttribute, new InfluencerSocialData, 'id', 'social_media_attribute_id'))
                        ->pushCriteria(new InnerJoinCriteria(new SocialMediaAttributeTranslation, new SocialMediaAttribute, 'entry_id', 'id'))
                        ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                        ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                        ->pushCriteria(new WhereCriteria(new SocialMediaTranslation, 'code', config('evemoo.lang.default')))
                        ->pushCriteria(new WhereCriteria(new SocialMediaAttributeTranslation, 'code', config('evemoo.lang.default')))
                        ->pushCriteria(new GroupByCriteria(new SocialMedia, 'id'))
                        ->get();

        return $social_medias;
    }

    /**
     * @return mixed
     */
    public function getUserReviews($id, $project_slug = null)
    {
        $reviews = $this->review
                ->pushCriteria(new SelectCriteria(['reviews.*', 'review_translations.comment', 'project_translations.title', 'package_translations.title as pkg_title']))
                ->pushCriteria(new InnerJoinCriteria(new ReviewTranslation, new Review, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Project, new Review, 'id', 'project_id'))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new ReviewTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new PackageTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new ReviewProjectFilterCriteria($project_slug))
                ->pushCriteria(new OrderByCriteria(new Review, 'id', 'desc'))
                ->get();

        return $reviews;
    }

    /**
     * @return mixed
     */
    public function getReviewsByUser($id)
    {
        $reviews = $this->review
                ->pushCriteria(new SelectCriteria(['reviews.*', 'review_translations.comment', 'project_translations.title', 'slot_translations.title as stitle']))
                ->pushCriteria(new InnerJoinCriteria(new ReviewTranslation, new Review, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Project, new Review, 'id', 'project_id'))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new InnerJoinCriteria(new SlotTranslation, new Slot, 'entry_id', 'id'))
                ->pushCriteria(new WhereCriteria(new Review, 'reviewer_id', $id))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new ReviewTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new SlotTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new OrderByCriteria(new Review, 'id', 'desc'))
                ->get();
                
        return $reviews;
    }

    /**
     * @return mixed
     */
    public function getUserProjects($id)
    {
        $projects = $this->project
                ->pushCriteria(new SelectCriteria(['projects.*', 'project_translations.description', 'project_translations.title', 'slot_translations.title as stitle']))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new SlotTranslation, new Slot, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new Slot, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $id))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new SlotTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new OrderByCriteria(new Project, 'id', 'desc'))
                ->get();

        return $projects;
    }

    public function banUser($id, $dates)
    {
        //dd($dates);
        $from = Carbon::parse($dates['from']);

        if($dates['to'] != null)
            $to = Carbon::parse($dates['to']);
        else
            $to = null;

        if($dates['ban_id'] != null)
        {
            $ban = Blacklist::find($dates['ban_id']);
            $banned = $ban->update([
                'user_id' => $id,
                'start_date' => $from,
                'end_date' => $to,
                'comment' => $dates['comment'],
            ]);
        } else {
            $banned = Blacklist::create([
                'user_id' => $id,
                'start_date' => $from,
                'end_date' => $to,
                'comment' => $dates['comment'],
            ]);
        }

        if($banned)
            return true;
        else
            return false;
    }

    public function isBanned($id)
    {
        $ban = $this->blacklist
            ->pushCriteria(new WhereCriteria(new Blacklist, 'user_id', $id))
            ->pushCriteria(new WhereCriteria(new Blacklist, 'start_date', Carbon::today()->addDay(), '<'))
            ->pushCriteria(new WhereCriteria(new Blacklist, 'end_date', Carbon::today()->addDay(), '>='))
            ->pushCriteria(new OrWhereCriteria(new Blacklist, 'end_date', null))
            ->pushCriteria(new OrderByCriteria(new Blacklist, 'id', 'desc'))
            ->first();

        return $ban;
    }

    protected function prepareSocialMediaLinks($data)
    {
        $user = $this->find($data['id']);

        $social = $user->social_medias;

        foreach ($social as $key => $value) {
            $social[$key]['link'] = $data['social_medias'][$key];
        }

        $data = json_encode($social);
        return $data;
    }

    /** Helper Methods **/

    public function changeJsonToArray($data)
    {
        if ($this->isJson($data->social_medias)) {
            $data->social_medias = json_decode($data->social_medias, 1);
        }
        return $data;
    }

    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}