<?php

namespace App\Http\Controllers\Api\Influencer\Transformers\Auth;

use App\Http\Controllers\Api\Influencer\Transformers\BaseTransformer;

class Register extends BaseTransformer
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

//        return $this->prepareResponse(
//            200,
//            [
//                'email' => $this->email,
//                'fullname' => $this->full_name,
//                'message'   => $this->message
//            ],
//            '',
//            'Successfully registered.'
//        );

        return [
            'email'    => $this->email,
            'fullname' => $this->full_name,
            'message'  => $this->message,
            'status'   => [
                'code'      => 200,
                'message'   => 'Successfully registered.'
            ]
        ];

    }

}