<?php

namespace App\Http\Controllers\Api\Influencer\Transformers\SocialMedia;

use App\Evemoo\Facades\AppHelper;
use Illuminate\Http\Resources\Json\Resource;

class SocialMedia extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'followers' => $this->value,
            'url' => $this->profile_link
        ];
    }

}