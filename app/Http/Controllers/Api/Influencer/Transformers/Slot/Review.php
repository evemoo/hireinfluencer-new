<?php

namespace App\Http\Controllers\Api\Influencer\Transformers\Slot;

use App\Evemoo\Facades\AppHelper;
use App\Http\Controllers\Api\Influencer\Transformers\BaseTransformer;
use App\Http\Controllers\Api\Influencer\Transformers\SocialMedia\SocialMedia;
use Illuminate\Http\Resources\Json\Resource;

class Review extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'project' => $this->project_name,
          'reviewer' => $this->reviewer->username,
          'review' => [
            'star' => $this->rating,
            'comment' => $this->comment,
            'author_reply' => $this->reply_comment?$this->reply_comment:'no-reply'
          ]
        ];
    }

}