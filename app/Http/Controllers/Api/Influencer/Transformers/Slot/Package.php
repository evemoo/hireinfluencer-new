<?php

namespace App\Http\Controllers\Api\Influencer\Transformers\Slot;

use App\Evemoo\Facades\AppHelper;
use App\Http\Controllers\Api\Influencer\Transformers\BaseTransformer;
use App\Http\Controllers\Api\Influencer\Transformers\SocialMedia\SocialMedia;
use Illuminate\Http\Resources\Json\Resource;

class Package extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'title' => $this->title,
          'summary' => $this->summary,
          'price' => $this->price,
          'deliver_in_days' => $this->delivery_in_days,
          'audience_size' => $this->audience_size,
          'default_package' => $this->default_package == 1?true:false,
          'href' => [
            'details' => route('api.influencer.slot.packages.details', [$this->username, $this->slot_id, $this->id])
          ]
        ];
    }

}