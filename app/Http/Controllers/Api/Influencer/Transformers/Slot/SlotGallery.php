<?php

namespace App\Http\Controllers\Api\Influencer\Transformers\Slot;

use App\Evemoo\Facades\AppHelper;
use App\Http\Controllers\Api\Influencer\Transformers\BaseTransformer;
use App\Http\Controllers\Api\Influencer\Transformers\SocialMedia\SocialMedia;
use Illuminate\Http\Resources\Json\Resource;

class SlotGallery extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'image' => asset($this->image),
          'alt_text' => $this->alt_text,
          'active' => $this->status == 1?true:false,
        ];
    }

}