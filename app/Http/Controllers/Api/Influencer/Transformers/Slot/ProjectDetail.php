<?php

namespace App\Http\Controllers\Api\Influencer\Transformers\Slot;

use App\Evemoo\Facades\AppHelper;
use App\Http\Controllers\Api\Influencer\Transformers\BaseTransformer;
use App\Http\Controllers\Api\Influencer\Transformers\SocialMedia\SocialMedia;
use Illuminate\Http\Resources\Json\Resource;

class ProjectDetail extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'package' => $this->package_title,
          'title' => $this->title,
          'slug' => $this->slug,
          'description' => $this->description,
          'budget' => $this->budget,
          'agreed_on' => $this->final_deal_price,
          'href' => [
            'reviews' => route('api.influencer.slot.projects.reviews', [$this->username, $this->slot_id, $this->id]),
            'project_chat' => route('api.influencer.slot.projects.chat', [$this->username, $this->slot_id, $this->id])
          ]
        ];
    }

}