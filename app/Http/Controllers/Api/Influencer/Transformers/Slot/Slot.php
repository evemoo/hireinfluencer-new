<?php

namespace App\Http\Controllers\Api\Influencer\Transformers\Slot;

use App\Evemoo\Facades\AppHelper;
use App\Http\Controllers\Api\Influencer\Transformers\BaseTransformer;
use App\Http\Controllers\Api\Influencer\Transformers\SocialMedia\SocialMedia;
use Illuminate\Http\Resources\Json\Resource;

class Slot extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'slot_id' => $this->id,
          'social_media' => $this->media_slug,
          'social_media_icon' => $this->media_icon,
          'name' => $this->title,
          'thumbnail' => $this->banner == null?'undefined':asset($this->banner),
          'category' => $this->category == null?'undefined':$this->category,
          'deliver_within' => $this->can_deliver_within != 0?$this->can_deliver_within:'undefined',
          'reviews_count' => $this->total_reviews == null?0:$this->total_reviews,
          'average_rating' => $this->average_rating == null?'unrated':$this->average_rating,
          'active' => $this->status == 1?true:false,
          'href' => [
            'details' => route('api.influencer.slot.detail', [$this->username, $this->id])
          ]
        ];
    }

}