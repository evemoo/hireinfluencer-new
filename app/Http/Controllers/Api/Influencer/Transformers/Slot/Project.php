<?php

namespace App\Http\Controllers\Api\Influencer\Transformers\Slot;

use App\Evemoo\Facades\AppHelper;
use App\Http\Controllers\Api\Influencer\Transformers\BaseTransformer;
use App\Http\Controllers\Api\Influencer\Transformers\SocialMedia\SocialMedia;
use Illuminate\Http\Resources\Json\Resource;

class Project extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'package' => $this->package_title,
          'title' => $this->title,
          'slug' => $this->slug,
          'budget' => $this->budget,
          'agreed_on' => $this->final_deal_price,
          'href' => [
            'details' => route('api.influencer.slot.projects.details', [$this->username, $this->slot_id, $this->id])
          ]
        ];
    }

}