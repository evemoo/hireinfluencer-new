<?php

namespace App\Http\Controllers\Api\Influencer\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class BaseTransformer extends Resource
{

    /**
     * Preparing the Response
     * @param $status
     * @param array $data
     * @param $errors
     * @param $message
     * @return array
     */
    protected function prepareResponse ( $status, array $data, $errors, $message )
    {
        return [
            'data'    => $data,
            'status'  => $status,
            'message' => $message,
            'errors'  => $errors
        ];
    }

}