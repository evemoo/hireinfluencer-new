<?php

namespace App\Http\Controllers\Api\Influencer\Transformers\Profile;

use App\Evemoo\Facades\AppHelper;
use App\Http\Controllers\Api\Influencer\Transformers\BaseTransformer;
use App\Http\Controllers\Api\Influencer\Transformers\SocialMedia\SocialMedia;
use Illuminate\Http\Resources\Json\Resource;

class Profile extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'name' => AppHelper::getFullName($this->basic_info),
          'username' => $this->basic_info->influencer_username,
          'email' => $this->basic_info->email,
          'primary_contact' => $this->basic_info->primary_contact,
          'secondary_contact' => $this->basic_info->secondary_contact,
          'gender' => $this->basic_info->gender != null?$this->basic_info->gender:'Unspecified!',
          'about_you' => $this->basic_info->about_you,
          'country' => $this->basic_info->country,
          'city' => $this->basic_info->state,
          'social_media' => $this->social_medias
        ];
    }

}