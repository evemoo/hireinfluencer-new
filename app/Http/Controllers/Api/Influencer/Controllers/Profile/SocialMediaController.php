<?php

namespace App\Http\Controllers\Api\Influencer\Controllers\Profile;

use AppExceptionHandler;
use App\Http\Controllers\Api\Influencer\Services\SocialMedia\InfluencerSocialDataService;
use App\Http\Controllers\Api\Influencer\Transformers\SocialMedia\SocialMedia;
use App\Http\Controllers\Api\Influencer\Validations\InfluencerSocialDataCreateRequest;
use App\Http\Controllers\Api\Influencer\Validations\InfluencerSocialDataUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SocialMediaController extends Controller
{
    /**
     * @var SocialMediaService
     */
    private $media;

    /**
     * profileController constructor.
     * @param ProfileService $profile
     */
    public function __construct(InfluencerSocialDataService $media)
    {
        $this->middleware('auth:api');
        $this->media = $media;
    }

    /**
     * @param  username
     * @param  social_media
     * @return object
     */
    public function getInfluencerSocialMediaDatas($username, $social_media)
    {
        AppExceptionHandler::authorizeOwner($username);
        $data = $this->media->getInfluencerSocialDatasByMedia($username, $social_media);
        return response([
           "data" => SocialMedia::collection($data)
        ], Response::HTTP_OK);
    }

    public function addInfluencerSocialMediaDatas(InfluencerSocialDataCreateRequest $request, $username, $social_media) {
        AppExceptionHandler::authorizeOwner($username);
        $data = $this->media->create($request, $username, $social_media);
        return response([
           "data" => new SocialMedia($data)
        ], Response::HTTP_CREATED);
    }

    public function editInfluencerSocialMediaDatas(InfluencerSocialDataUpdateRequest $request, $username, $social_media) {
        AppExceptionHandler::authorizeOwner($username);
        $data = $this->media->update($request, $username, $social_media);
        return response([
           "data" => new SocialMedia($data)
        ], Response::HTTP_OK);
    }

    public function deleteInfluencerSocialMediaDatas(Request $request, $username, $social_media) {
        AppExceptionHandler::authorizeOwner($username);
        $data = $this->media->delete($request, $username);
        return response([], Response::HTTP_NO_CONTENT);
    }
}
