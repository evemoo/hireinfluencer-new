<?php

namespace App\Http\Controllers\Api\Influencer\Controllers\Profile;

use AppExceptionHandler;
use App\Http\Controllers\Api\Influencer\Services\Slot\SlotService;
use App\Http\Controllers\Api\Influencer\Transformers\Slot\Chat;
use App\Http\Controllers\Api\Influencer\Transformers\Slot\Package;
use App\Http\Controllers\Api\Influencer\Transformers\Slot\PackageDetail;
use App\Http\Controllers\Api\Influencer\Transformers\Slot\Project;
use App\Http\Controllers\Api\Influencer\Transformers\Slot\ProjectDetail;
use App\Http\Controllers\Api\Influencer\Transformers\Slot\Review;
use App\Http\Controllers\Api\Influencer\Transformers\Slot\Slot;
use App\Http\Controllers\Api\Influencer\Transformers\Slot\SlotDetail;
use App\Http\Controllers\Api\Influencer\Transformers\Slot\SlotGallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SlotController extends Controller
{
    /**
     * @var SlotService
     */
    private $slot;

    /**
     * SlotController constructor.
     * @param SlotService $profile
     */
    public function __construct(SlotService $slot)
    {
        $this->middleware('auth:api');
        $this->slot = $slot;
    }

    public function getInfluencerSlots($username)
    {
        AppExceptionHandler::authorizeOwner($username);
        $slots = $this->slot->getInfluencerSlots($username);
        return response([
            'data' => Slot::collection($slots)
        ], Response::HTTP_OK);
    }

    public function getSlotDetails($username, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $details = $this->slot->getSlotDetails($username, $id);
        return response([
            'data' => new SlotDetail($details)
        ], Response::HTTP_OK);
    }

    public function getSlotGallery($username, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $gallery = $this->slot->getSlotGallery($username, $id);
        if(count($gallery) > 0)
        {
            return response([
                'data' => SlotGallery::collection($gallery)
            ], Response::HTTP_OK);
        } else {
            return response([
                'message' => 'no data!'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getSlotProjects($username, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $projects = $this->slot->getSlotProjects($username, $id);
        if(count($projects) > 0)
        {
            return response([
                'data' => Project::collection($projects)
            ], Response::HTTP_OK);
        } else {
            return response([
                'message' => 'no data!'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getSlotPackages($username, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $packages = $this->slot->getSlotPackages($username, $id);
        if(count($packages) > 0)
        {
            return response([
                'data' => Package::collection($packages)
            ], Response::HTTP_OK);
        } else {
            return response([
                'message' => 'no data!'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getSlotPackageProjects($username, $slot_id, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $projects = $this->slot->getSlotPackageProjects($username, $slot_id, $id);
        if(count($projects) > 0)
        {
            return response([
                'data' => Project::collection($projects)
            ], Response::HTTP_OK);
        } else {
            return response([
                'message' => 'no data!'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getSlotPackageDetails($username, $slot_id, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $package = $this->slot->getSlotPackageDetails($username, $slot_id, $id);
        if(count($package) > 0)
        {
            return response([
                'data' => new PackageDetail($package)
            ], Response::HTTP_OK);
        } else {
            return response([
                'message' => 'no data!'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getProjectDetails($username, $slot_id, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $project = $this->slot->getProjectDetails($username, $slot_id, $id);
        if(count($project) > 0)
        {
            return response([
                'data' => new ProjectDetail($project)
            ], Response::HTTP_OK);
        } else {
            return response([
                'message' => 'no data!'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getProjectReviews($username, $slot_id, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $reviews = $this->slot->getProjectReviews($username, $slot_id, $id);
        if(count($reviews) > 0)
        {
            return response([
                'data' => Review::collection($reviews)
            ], Response::HTTP_OK);
        } else {
            return response([
                'message' => 'no data!'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getProjectChat($username, $slot_id, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $chat = $this->slot->getProjectChat($username, $slot_id, $id);
        if(count($chat) > 0)
        {
            return response([
                'data' => Chat::collection($chat)
            ], Response::HTTP_OK);
        } else {
            return response([
                'message' => 'no data!'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getSlotReviews($username, $id)
    {
        AppExceptionHandler::authorizeOwner($username);
        $reviews = $this->slot->getSlotReviews($username, $id);
        if(count($reviews) > 0)
        {
            return response([
                'data' => Review::collection($reviews)
            ], Response::HTTP_OK);
        } else {
            return response([
                'message' => 'no data!'
            ], Response::HTTP_NOT_FOUND);
        }
    }
}
