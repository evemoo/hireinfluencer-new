<?php

namespace App\Http\Controllers\Api\Influencer\Controllers\Profile;

use AppExceptionHandler;
use App\Http\Controllers\Api\Influencer\Services\Profile\ProfileService;
use App\Http\Controllers\Api\Influencer\Services\SocialMedia\InfluencerSocialDataService;
use App\Http\Controllers\Api\Influencer\Transformers\Profile\Profile;
use App\Http\Controllers\Api\Influencer\Validations\InfluencerCreateRequest;
use App\Http\Controllers\Api\Influencer\Validations\InfluencerUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends Controller
{
    /**
     * @var ProfileService
     */
    private $profile;

    /**
     * @var SocialMediaService
     */
    private $media;

    /**
     * profileController constructor.
     * @param ProfileService $profile
     */
    public function __construct(ProfileService $profile, InfluencerSocialDataService $media)
    {
        $this->middleware('auth:api')->except(['create', 'verifyUser']);
        $this->profile = $profile;
        $this->media = $media;
    }

    public function index($username)
    {
        AppExceptionHandler::authorizeOwner($username);
        $profile['basic_info'] = $this->profile->getInfluencer($username);
        $profile['social_medias'] = $this->media->getInfluencerSocialMedias($username);
        return new Profile((object) $profile);
    }

    public function getAuthenticatedUser() {
        $user['basic_info'] = $this->profile->getInfluencer(auth()->user()->username);
        $user['social_medias'] = $this->media->getInfluencerSocialMedias(auth()->user()->username);
        return response([
            'user' => new Profile((object) $user)
        ], Response::HTTP_OK);
    }

    public function create(InfluencerCreateRequest $request)
    {
        $user = $this->profile->create($request);

        Mail::send('mail.template', ['user' => $user], function ($message) use ($user) {
            $message->from('welcome@hireinfluencer.com', 'HireInfluencer');
        
            $message->to($user->email, $user->username);
        
            $message->subject('Verify Email!');
        });
        
        $profile['basic_info'] = $this->profile->getInfluencer($user->username);
        $profile['social_medias'] = $this->media->getInfluencerSocialMedias($user->username);

        return response([
            'data' => new Profile((object) $profile)
        ], Response::HTTP_CREATED);
    }

    public function update(InfluencerUpdateRequest $request, $username)
    {
        AppExceptionHandler::authorizeOwner($username);
        $user = $this->profile->update($request, $username);
        $data['basic_info'] = $this->profile->getInfluencer($user->username);
        $data['social_medias'] = $this->media->getInfluencerSocialMedias($user->username);
        return response([
            'data' => new Profile((object) $data)
        ], Response::HTTP_OK);
    }

    public function verifyUser(Request $request) {
        $verify = $this->profile->verifyToken($request->token);
        return $verify;
    }

    public function delete($username)
    {
        AppExceptionHandler::authorizeOwner($username);
        $user = $this->profile->delete($username);
        return response([], Response::HTTP_NO_CONTENT);
    }
}
