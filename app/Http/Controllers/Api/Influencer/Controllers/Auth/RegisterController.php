<?php

namespace App\Http\Controllers\Api\Influencer\Controllers\Auth;

use App\Http\Controllers\Api\Influencer\Services\Auth\RegisterService;
use App\Http\Controllers\Api\Influencer\Transformers\Auth\Register;
use App\Http\Controllers\Controller;
use App\Http\Requests\Influencer\Auth\RegisterRequest;
use App\User;

class RegisterController extends Controller
{

    /**
     * @var RegisterService
     */
    private $service;

    /**
     * RegisterController constructor.
     * @param RegisterService $service
     */
    public function __construct( RegisterService $service )
    {
        $this->service = $service;
    }

    /**
     * @param RegisterRequest $request
     * @return Register
     */
    public function register( RegisterRequest $request )
    {

        $data = $this->service->register($request->only(
            'full_name',
            'username',
            'contact',
            'email',
            'password',
            'messsage'
        ));

        return new Register( $data );
    }

    public function no()
    {
        return new Register( User::find(1) );
    }

}