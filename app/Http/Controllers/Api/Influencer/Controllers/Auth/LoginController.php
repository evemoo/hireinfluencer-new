<?php

namespace App\Http\Controllers\Api\Influencer\Controllers\Auth;

use App\Http\Controllers\Api\Influencer\Services\Auth\LoginService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Influencer\Auth\LoginRequest;

class LoginController extends Controller
{

    /**
     * @var LoginService
     */
    private $service;

    /**
     * LoginController constructor.
     * @param LoginService $service
     */
    public function __construct( LoginService $service )
    {
        $this->service = $service;
    }

    /**
     * @param LoginRequest $request
     * @return mixed
     */
    public function login( LoginRequest $request )
    {

        $response = $this->service->authenticate( $request );

        return $response;
        
    }

}