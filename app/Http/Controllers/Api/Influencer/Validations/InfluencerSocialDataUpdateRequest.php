<?php

namespace App\Http\Controllers\Api\Influencer\Validations;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class InfluencerSocialDataUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'followers' => 'required',
            'url' => 'required|unique:influencer_social_datas,profile_link,'.$this->id,
            'attribute_id' => 'required|numeric'
        ];
    }

    public function messages() {
        return [
            'url.unique' => 'social profile already registered!'
        ];        
    }
}
