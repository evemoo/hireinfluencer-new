<?php

namespace App\Http\Controllers\Api\Influencer\Validations;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class InfluencerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ];
    }
}
