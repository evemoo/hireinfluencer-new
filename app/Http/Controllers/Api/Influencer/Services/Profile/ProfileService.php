<?php

namespace App\Http\Controllers\Api\Influencer\Services\Profile;

use App\Evemoo\Facades\AppHelper;
use App\Evemoo\Models\Influencer;
use App\Evemoo\Repositories\Influencer\InfluencerRepository;
use App\Evemoo\Repositories\Influencer\UserRepository;
use App\Http\Controllers\Api\Influencer\Traits\InfluencerTrait;
use App\User;
use Carbon\Carbon;
use Evemoo\Repositories\Criteria\GroupByCriteria;
use Evemoo\Repositories\Criteria\InnerJoinCriteria;
use Evemoo\Repositories\Criteria\LeftJoinCriteria;
use Evemoo\Repositories\Criteria\OrWhereCriteria;
use Evemoo\Repositories\Criteria\OrderByCriteria;
use Evemoo\Repositories\Criteria\SelectCriteria;
use Evemoo\Repositories\Criteria\WhereCriteria;

/**
 * Class ProfileService
 * @package App\Http\Controllers\Api\Influencer\Services\Profile
 */
class ProfileService
{
    use InfluencerTrait;

    /**
     * @var $influencer
     */
    private $influencer;

    /**
     * @var $user
     */
    private $user;

    /**
     * ProfileService constructor.
     */
    public function __construct(UserRepository $user, InfluencerRepository $influencer)
    {
        $this->influencer = $influencer;
        $this->user = $user;
    }

    public function create($request) {
        
        $full_name = AppHelper::split_name($request->name);

        $user_data['username'] = $request->username;
        $user_data['email'] = $request->email;
        $user_data['password'] = $request->password;
        $user_data['first_name'] = $full_name['first_name'];
        $user_data['middle_name'] = $full_name['middle_name'];
        $user_data['last_name'] = $full_name['last_name'];
        $user_data['verify_token'] = AppHelper::verifyToken();

        $new_user = $this->user->create($user_data);

        $influencer_data['user_id'] = $new_user->id;
        $influencer_data['influencer_username'] = $user_data['username'];
        $influencer_data['registration_date'] = Carbon::now();

        $new_influencer = $this->influencer->create($influencer_data);

        if ($new_user && $new_influencer) {
            return $new_user;
        } else {
            return false;
        }
    }

    public function update($request, $username) {
        
        $user = $this->user
            ->pushCriteria(new WhereCriteria(new User, 'username', $username))
            ->firstOrFail();

        if ($request->has('password')) {
            $user_data['password'] = $request->password;
        }

        if ($request->has('name')) {
            $full_name = AppHelper::split_name($request->name);
            $user_data['first_name'] = $full_name['first_name'];
            $user_data['middle_name'] = $full_name['middle_name'];
            $user_data['last_name'] = $full_name['last_name'];
        }

        if ($request->has('primary_contact')) {
            $user_data['primary_contact'] = $request->primary_contact;;
        }
        
        if ($request->has('secondary_contact')) {
            $user_data['secondary_contact'] = $request->secondary_contact;;
        }
        
        if ($request->has('gender')) {
            $user_data['gender'] = $request->gender;;
        }
        
        if ($request->has('username')) {
            $user_data['username'] = $request->username;;
        }

        $user->update($user_data);

        if ($user) {
            return $user;
        } else {
            return false;
        }
    }

    public function verifyToken($token) {
        $user = $this->user
                ->pushCriteria(new WhereCriteria(new User, 'verify_token', $token))
                ->first();

        $response = [];

        if(!$user)
        {
            $response['type'] = 'danger';
            $response['msg'] = 'Invalid Token!';
        }
        else
        {
            if($user->enabled == 1)
            {
                $response['type'] = 'warning';
                $response['msg'] = 'Already Verified! You can login now.';
            }
            elseif($user->enabled == 0)
            {
                $user->update([
                    'enabled' => 1,
                ]);
                $response['type'] = 'success';
                $response['msg'] = 'Verification Successfull! You can login now.';
            }
        }

        return $response;
    }       

    public function delete($username) {
        
        $user = $this->user
            ->pushCriteria(new WhereCriteria(new User, 'username', $username))
            ->firstOrFail();

        $influencer = $this->influencer
            ->pushCriteria(new WhereCriteria(new Influencer, 'user_id', $user->id))
            ->first();

        if ($influencer) {
            $influencer->delete();    
        }
        
        $user->delete();    

        return true;
    }

}