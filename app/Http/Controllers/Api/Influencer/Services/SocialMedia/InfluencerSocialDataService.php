<?php

namespace App\Http\Controllers\Api\Influencer\Services\SocialMedia;

use App\Evemoo\Facades\AppHelper;
use App\Evemoo\Models\Influencer;
use App\Evemoo\Models\InfluencerSocialData;
use App\Evemoo\Models\SocialMedia;
use App\Evemoo\Models\SocialMediaAttribute;
use App\Evemoo\Models\SocialMediaTranslation;
use App\Evemoo\Repositories\Influencer\InfluencerRepository;
use App\Evemoo\Repositories\Influencer\InfluencerSocialDataRepository;
use App\Evemoo\Repositories\Influencer\SocialMediaRepository;
use App\Http\Controllers\Api\Influencer\Services\Profile\ProfileService;
use App\Http\Controllers\Api\Influencer\Traits\InfluencerTrait;
use App\User;
use Carbon\Carbon;
use Evemoo\Repositories\Criteria\GroupByCriteria;
use Evemoo\Repositories\Criteria\InnerJoinCriteria;
use Evemoo\Repositories\Criteria\LeftJoinCriteria;
use Evemoo\Repositories\Criteria\OrWhereCriteria;
use Evemoo\Repositories\Criteria\OrderByCriteria;
use Evemoo\Repositories\Criteria\SelectCriteria;
use Evemoo\Repositories\Criteria\WhereCriteria;

/**
 * Class SocialMediaService
 * @package App\Http\Controllers\Api\Influencer\Services\SocialMedia
 */
class InfluencerSocialDataService
{
    use InfluencerTrait;

    /**
     * @var $influencer_data
     */
    private $influencer_data;

    /**
     * @var $social_media
     */
    private $social_media;

    /**
     * SocialMediaService constructor.
     */
    public function __construct(
        InfluencerRepository $influencer,
        SocialMediaRepository $social_media,
        InfluencerSocialDataRepository $influencer_data)
    {
        $this->influencer = $influencer;
        $this->social_media = $social_media;
        $this->influencer_data = $influencer_data;
    }

    public function getInfluencerSocialDatasByMedia($influencer_username, $media) {
        $data = $this->influencer_data
            ->pushCriteria(new SelectCriteria(['influencer_social_datas.name', 'influencer_social_datas.value', 'influencer_social_datas.profile_link', 'social_media_attributes.slug as attribute']))
            ->pushCriteria(new InnerJoinCriteria(new SocialMedia, new InfluencerSocialData, 'id', 'social_media_id'))
            ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
            ->pushCriteria(new InnerJoinCriteria(new SocialMediaAttribute, new InfluencerSocialData, 'id', 'social_media_attribute_id'))
            ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $influencer_username))
            ->pushCriteria(new WhereCriteria(new SocialMedia, 'slug', $media))
            ->get();
            
        return (object) $data;
    }

    public function getInfluencerSocialMedias($influencer_username) {
        $medias = $this->social_media
                ->pushCriteria(new SelectCriteria(['social_media.slug']))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new SocialMedia, 'social_media_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                ->pushCriteria(new InnerJoinCriteria(new SocialMediaTranslation, new SocialMedia, 'entry_id', 'id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $influencer_username))
                ->pushCriteria(new WhereCriteria(new SocialMediaTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new GroupByCriteria(new SocialMedia, 'id'))
                ->get();

        $data = [];

        foreach($medias as $media)
        {
            $data[$media->slug] = [
                'detail_page_url' => route('api.influencer.socialmedias', [$influencer_username, $media->slug]),
                'media' => $media->slug,
                'pages' => $this->getInfluencerSocialDatasByMedia($influencer_username, $media->slug)
            ];
        }

        return $data;
    }

    public function create($request, $username, $social_media) {
        $media_id = $this->getSocialMedia($social_media)->id;
        $influencer_id = $this->getInfluencer($username)->id;
        $data = $this->influencer_data->create([
                    'influencer_id' => $influencer_id,
                    'social_media_id' => $media_id,
                    'social_media_attribute_id' => $request->attribute_id,
                    'name' => $request->name,
                    'value' => $request->followers,
                    'profile_link' => $request->url
                ]);

        if ($data) {
            return $data;
        }
        else {
            return ['message' => 'Error!'];
        }
    }

    public function update($request, $username, $social_media) {
        $media_id = $this->getSocialMedia($social_media)->id;
        $influencer_id = $this->getInfluencer($username)->id;
        $data = $this->influencer_data
                ->pushCriteria(new SelectCriteria(['influencer_social_datas.*']))
                ->pushCriteria(new WhereCriteria(new InfluencerSocialData, 'id', $request->id))
                ->pushCriteria(new WhereCriteria(new InfluencerSocialData, 'social_media_id', $media_id))
                ->pushCriteria(new WhereCriteria(new InfluencerSocialData, 'influencer_id', $influencer_id))
                ->first();

        if (!$data) {
            return ['message' => 'invalid request!'];
        }
        else {
            $data->update([
                'influencer_id' => $influencer_id,
                'social_media_id' => $media_id,
                'social_media_attribute_id' => $request->attribute_id,
                'name' => $request->name,
                'value' => $request->followers,
                'profile_link' => $request->url
            ]);

            return $data;
        }
    }

    public function delete($request, $username) {
        $influencer_id = $this->getInfluencer($username)->id;
        $data = $this->influencer_data
                ->pushCriteria(new SelectCriteria(['influencer_social_datas.*']))
                ->pushCriteria(new WhereCriteria(new InfluencerSocialData, 'id', $request->id))
                ->pushCriteria(new WhereCriteria(new InfluencerSocialData, 'influencer_id', $influencer_id))
                ->first();

        if (!$data) {
            return ['message' => 'invalid request!'];
        }
        else {
            $data->delete();

            return true;
        }
    }

    public function getSocialMedia($social_media) {
        return $this->social_media
                ->pushCriteria(new SelectCriteria(['social_media.*', 'social_media_translations.name']))
                ->pushCriteria(new InnerJoinCriteria(new SocialMediaTranslation, new SocialMedia, 'entry_id', 'id'))
                ->pushCriteria(new WhereCriteria(new SocialMedia, 'slug', $social_media))
                ->pushCriteria(new WhereCriteria(new SocialMediaTranslation, 'code', config('evemoo.lang.default')))
                ->first();
    }

}