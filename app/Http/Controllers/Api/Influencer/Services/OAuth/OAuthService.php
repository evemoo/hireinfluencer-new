<?php

namespace App\Http\Controllers\Api\Influencer\Services\OAuth;

/**
 * Class OAuthService
 * @package App\Http\Controllers\Api\Influencer\Services\OAuth
 */
class OAuthService
{

    /**
     * @var $client
     */
    private $client;

    /**
     * TABLE_NAME
     */
    const TABLE_NAME = 'oauth_clients';

    /**
     * CLIENT_ID
     */
    const CLIENT_ID = 2;

    /**
     * Base URL to get authentication
     */
    const BASE_URL = 'oauth/token';


    /**
     * OAuthService constructor.
     */
    public function __construct()
    {
        $this->client = $this->getOAuth();
    }

    /**
     * Table and Model init()
     * @return mixed
     */
    protected function getOAuth()
    {
        return \DB::table(self::TABLE_NAME);
    }

    /**
     * @param $key
     * @param int $client
     * @return mixed
     */
    public function getValueByKey ( $key, $client = self::CLIENT_ID )
    {
        return $this->client->select("{$key}")->where('id', $client)->first();
    }

    /**
     * Get Client ID and SECRET
     * @return array
     */
    public function getClient()
    {
        return [
            'client_id'     => $this->getValueByKey('id'),
            'client_secret' => $this->getValueByKey('secret')
        ];
    }

}