<?php

namespace App\Http\Controllers\Api\Influencer\Services\OAuth;

/**
 * Interface OAuthInterface
 * @package App\Http\Controllers\Api\Influencer\Services\OAuth
 */
interface OAuthInterface
{

    /**
     * TABLE_NAME
     */
    const TABLE_NAME = 'oauth_clients';

    /**
     * CLIENT_ID
     */
    const CLIENT_ID = 2;

    /**
     * Base URL to get authentication
     */
    const BASE_URL = 'oauth/token';

    public function getOAuth();

}