<?php

namespace App\Http\Controllers\Api\Influencer\Services\Auth;

use App\User;

/**
 * Class RegisterService
 * @package App\Http\Controllers\Api\Influencer\Services\Auth
 */
class RegisterService
{

    /**
     * @var User
     */
    protected $user;

    /**
     * RegisterService constructor.
     * @param User $user
     */
    public function __construct( User $user )
    {
        $this->user = $user;
    }

    /**
     * @param array $userDetails
     * @return mixed
     */
    public function register( array $userDetails )
    {

        $data = $this->user->create( $userDetails );

        return $data;
    }

}