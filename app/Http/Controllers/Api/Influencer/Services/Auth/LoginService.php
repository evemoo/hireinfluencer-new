<?php

namespace App\Http\Controllers\Api\Influencer\Services\Auth;

use App\Http\Controllers\Api\Influencer\Services\OAuth\OAuthService;
use App\User;
use Illuminate\Http\Request;
use Route;

/**
 * Class LoginService
 * @package App\Http\Controllers\Api\Influencer\Services\Auth
 */
class LoginService
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @var OAuthService
     */
    protected $oauth;

    /**
     * LoginService constructor.
     * @param User $user
     * @param OAuthService $oauth
     */
    public function __construct(
        User $user,
        OAuthService $oauth
    )
    {
        $this->user   = $user;
        $this->oauth = $oauth;
    }

    /**
     * Authenticate the user
     * @param Request $request
     * @return mixed
     */
    public function authenticate( Request $request )
    {
        $request->request->add([
            'username'      => $request->get('username'),
            'password'      => $request->get('password'),
            'grant_type'    => 'password',
            $this->oauth->getClient(),
            'scope'         => '*'
        ]);

        $route = Request::create(
            $this->oauth::BASE_URL,
            'POST',
            $request->only('username', 'password', 'grant_type', 'client_id', 'client_secret', 'scope')
        );

        return Route::dispatch($route);

    }

    /**
     * Refresh Token
     * @param Request $request
     * @return mixed
     */
    public function refreshToken( Request $request )
    {
        $request->request->add([
            'grant_type'    => 'refresh_token',
            'refresh_token' => $request->get('refresh_token'),
            $this->oauth->getClient()
        ]);

        $route = Request::create(
            $this->oauth::BASE_URL,
            'POST',
            $request->only('refresh_token', 'grant_type', 'client_id', 'client_secret')
        );

        return Route::dispatch($route);
    }

}