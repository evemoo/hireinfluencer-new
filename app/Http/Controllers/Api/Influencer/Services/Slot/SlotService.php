<?php

namespace App\Http\Controllers\Api\Influencer\Services\Slot;

use App\Evemoo\Facades\AppHelper;
use App\Evemoo\Models\Category;
use App\Evemoo\Models\Influencer;
use App\Evemoo\Models\InfluencerSocialData;
use App\Evemoo\Models\Package;
use App\Evemoo\Models\PackageTranslation;
use App\Evemoo\Models\Project;
use App\Evemoo\Models\ProjectChat;
use App\Evemoo\Models\ProjectTranslation;
use App\Evemoo\Models\Review;
use App\Evemoo\Models\ReviewReply;
use App\Evemoo\Models\ReviewTranslation;
use App\Evemoo\Models\Slot;
use App\Evemoo\Models\SlotCategory;
use App\Evemoo\Models\SlotGallery;
use App\Evemoo\Models\SlotGalleryTranslation;
use App\Evemoo\Models\SlotTranslation;
use App\Evemoo\Models\SocialMedia;
use App\Evemoo\Repositories\Influencer\InfluencerRepository;
use App\Evemoo\Repositories\Influencer\PackageRepository;
use App\Evemoo\Repositories\Influencer\ProjectChatRepository;
use App\Evemoo\Repositories\Influencer\ProjectRepository;
use App\Evemoo\Repositories\Influencer\ReviewRepository;
use App\Evemoo\Repositories\Influencer\SlotGalleryRepository;
use App\Evemoo\Repositories\Influencer\SlotRepository;
use App\Http\Controllers\Api\Influencer\Traits\InfluencerTrait;
use Evemoo\Repositories\Criteria\GroupByCriteria;
use Evemoo\Repositories\Criteria\InnerJoinCriteria;
use Evemoo\Repositories\Criteria\LeftJoinCriteria;
use Evemoo\Repositories\Criteria\OrWhereCriteria;
use Evemoo\Repositories\Criteria\OrderByCriteria;
use Evemoo\Repositories\Criteria\SelectCriteria;
use Evemoo\Repositories\Criteria\WhereCriteria;

/**
 * Class SlotService
 * @package App\Http\Controllers\Api\Influencer\Services\Slot
 */
class SlotService
{
    use InfluencerTrait;

    /**
     * @var $influencer_data
     */
    private $influencer;

    /**
     * @var $slot
     */
    private $slot;

    /**
     * @var $slot_gallery
     */
    private $slot_gallery;

    /**
     * @var $package
     */
    private $package;

    /**
     * @var $project
     */
    private $project;

    /**
     * @var $review
     */
    private $review;

    /**
     * @var $chat
     */
    private $chat;

    /**
     * SlotService constructor.
     */
    public function __construct(
        SlotRepository $slot,
        ReviewRepository $review,
        PackageRepository $package,
        ProjectRepository $project,
        ProjectChatRepository $chat,
        InfluencerRepository $influencer,
        SlotGalleryRepository $slot_gallery)
    {
        $this->chat = $chat;
        $this->slot = $slot;
        $this->review = $review;
        $this->package = $package;
        $this->project = $project;
        $this->influencer = $influencer;
        $this->slot_gallery = $slot_gallery;
    }

    public function getInfluencerSlots($username) {
        $data = $this->slot
            ->pushCriteria(new SelectCriteria(['slots.*', 'slot_translations.title', 'social_media.slug as media_slug', 'social_media.icon as media_icon', 'influencers.influencer_username as username', 'categories.slug as category']))
            ->pushCriteria(new InnerJoinCriteria(new SlotTranslation, new Slot, 'entry_id', 'id'))
            ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
            ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
            ->pushCriteria(new InnerJoinCriteria(new SocialMedia, new InfluencerSocialData, 'id', 'social_media_id'))
            ->pushCriteria(new LeftjoinCriteria(new SlotCategory, new Slot, 'slot_id', 'id'))
            ->pushCriteria(new LeftjoinCriteria(new Category, new SlotCategory, 'id', 'category_id'))
            ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
            ->pushCriteria(new WhereCriteria(new SlotTranslation, 'code', config('evemoo.lang.default')))
            ->get();
            
        return $data;
    }

    public function getSlotDetails($username, $id) {
        $data = $this->slot
            ->pushCriteria(new SelectCriteria(['slots.*', 'slot_translations.title', 'social_media.slug as media_slug', 'social_media.icon as media_icon', 'influencer_social_datas.name as page_name', 'influencer_social_datas.profile_link as page_url', 'influencers.influencer_username as username', 'categories.slug as category']))
            ->pushCriteria(new InnerJoinCriteria(new SlotTranslation, new Slot, 'entry_id', 'id'))
            ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
            ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
            ->pushCriteria(new InnerJoinCriteria(new SocialMedia, new InfluencerSocialData, 'id', 'social_media_id'))
            ->pushCriteria(new LeftjoinCriteria(new SlotCategory, new Slot, 'slot_id', 'id'))
            ->pushCriteria(new LeftjoinCriteria(new Category, new SlotCategory, 'id', 'category_id'))
            ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
            ->pushCriteria(new WhereCriteria(new Slot, 'id', $id))
            ->pushCriteria(new WhereCriteria(new SlotTranslation, 'code', config('evemoo.lang.default')))
            ->first();
            
        return $data;
    }

    public function getSlotGallery($username, $id) {
        $data = $this->slot_gallery
            ->pushCriteria(new SelectCriteria(['slot_galleries.*', 'slot_gallery_translations.alt_text']))
            ->pushCriteria(new InnerJoinCriteria(new SlotGalleryTranslation, new SlotGallery, 'entry_id', 'id'))
            ->pushCriteria(new InnerJoinCriteria(new Slot, new SlotGallery, 'id', 'slot_id'))
            ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
            ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
            ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
            ->pushCriteria(new WhereCriteria(new SlotGallery, 'slot_id', $id))
            ->pushCriteria(new WhereCriteria(new SlotGalleryTranslation, 'code', config('evemoo.lang.default')))
            ->get();

        return $data;
    }

    public function getSlotProjects($username, $id) {
        $data = $this->project
                ->pushCriteria(new SelectCriteria(['projects.*', 'project_translations.title', 'package_translations.title as package_title', 'packages.slot_id', 'influencers.influencer_username as username']))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
                ->pushCriteria(new WhereCriteria(new Package, 'slot_id', $id))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new PackageTranslation, 'code', config('evemoo.lang.default')))
                ->get();

        return $data;
    }

    public function getSlotPackages($username, $id) {
        $data = $this->package
                ->pushCriteria(new SelectCriteria(['packages.delivery_in_days', 'packages.audience_size', 'packages.price', 'packages.default_package', 'package_translations.title', 'package_translations.summary', 'influencers.influencer_username as username', 'packages.slot_id', 'packages.id']))
                ->pushCriteria(new InnerJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
                ->pushCriteria(new WhereCriteria(new Package, 'slot_id', $id))
                ->pushCriteria(new WhereCriteria(new PackageTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new OrderByCriteria(new Package, 'rank', 'asc'))
                ->get();

        return $data;
    }

    public function getSlotPackageDetails($username, $slot_id, $id) {
        $data = $this->package
                ->pushCriteria(new SelectCriteria(['packages.delivery_in_days', 'packages.audience_size', 'packages.no_of_posts', 'packages.permanent_post', 'packages.additional_link', 'packages.price', 'packages.default_package', 'package_translations.title', 'package_translations.summary', 'package_translations.description', 'influencers.influencer_username as username', 'packages.slot_id', 'packages.id']))
                ->pushCriteria(new InnerJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Package, 'id', $id))
                ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
                ->pushCriteria(new WhereCriteria(new Package, 'slot_id', $slot_id))
                ->pushCriteria(new WhereCriteria(new PackageTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new OrderByCriteria(new Package, 'rank', 'asc'))
                ->first();

        return $data;
    }

    public function getSlotPackageProjects($username, $slot_id, $id) {
        $data = $this->project
                ->pushCriteria(new SelectCriteria(['projects.*', 'project_translations.title', 'package_translations.title as package_title', 'packages.slot_id', 'influencers.influencer_username as username']))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
                ->pushCriteria(new WhereCriteria(new Package, 'slot_id', $slot_id))
                ->pushCriteria(new WhereCriteria(new Package, 'id', $id))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new PackageTranslation, 'code', config('evemoo.lang.default')))
                ->get();

        return $data;
    }

    public function getProjectDetails($username, $slot_id, $id) {
        $data = $this->project
                ->pushCriteria(new SelectCriteria(['projects.*', 'project_translations.title', 'project_translations.description', 'package_translations.title as package_title', 'packages.slot_id', 'influencers.influencer_username as username']))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
                ->pushCriteria(new WhereCriteria(new Package, 'slot_id', $slot_id))
                ->pushCriteria(new WhereCriteria(new Project, 'id', $id))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new PackageTranslation, 'code', config('evemoo.lang.default')))
                ->first();

        return $data;
    }

    public function getProjectReviews($username, $slot_id, $id) {
        $data = $this->review
                ->pushCriteria(new SelectCriteria(['reviews.project_id', 'reviews.reviewer_id', 'reviews.rating', 'review_translations.comment', 'review_replies.reply_comment', 'project_translations.title as project_name']))
                ->pushCriteria(new InnerJoinCriteria(new ReviewTranslation, new Review, 'entry_id', 'id'))
                ->pushCriteria(new LeftJoinCriteria(new ReviewReply, new Review, 'review_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Project, new Review, 'id', 'project_id'))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
                ->pushCriteria(new WhereCriteria(new Slot, 'id', $slot_id))
                ->pushCriteria(new WhereCriteria(new Project, 'id', $id))
                ->pushCriteria(new WhereCriteria(new ReviewTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->get();

        return $data;
    }

    public function getProjectChat($username, $slot_id, $id) {
        $data = $this->chat
                ->pushCriteria(new SelectCriteria(['project_chats.sender_id', 'project_chats.message', 'project_translations.title as project_name']))
                ->pushCriteria(new InnerJoinCriteria(new Project, new ProjectChat, 'id', 'project_id'))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new PackageTranslation, new Package, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
                ->pushCriteria(new WhereCriteria(new Package, 'slot_id', $slot_id))
                ->pushCriteria(new WhereCriteria(new Project, 'id', $id))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new PackageTranslation, 'code', config('evemoo.lang.default')))
                ->get();

        return $data;
    }

    public function getSlotReviews($username, $id) {
        $data = $this->review
                ->pushCriteria(new SelectCriteria(['reviews.project_id', 'reviews.reviewer_id', 'reviews.rating', 'review_translations.comment', 'review_replies.reply_comment', 'project_translations.title as project_name']))
                ->pushCriteria(new InnerJoinCriteria(new ReviewTranslation, new Review, 'entry_id', 'id'))
                ->pushCriteria(new LeftJoinCriteria(new ReviewReply, new Review, 'review_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Project, new Review, 'id', 'project_id'))
                ->pushCriteria(new InnerJoinCriteria(new ProjectTranslation, new Project, 'entry_id', 'id'))
                ->pushCriteria(new InnerJoinCriteria(new Package, new Project, 'id', 'package_id'))
                ->pushCriteria(new InnerJoinCriteria(new Slot, new Package, 'id', 'slot_id'))
                ->pushCriteria(new InnerJoinCriteria(new InfluencerSocialData, new Slot, 'id', 'influencer_social_data_id'))
                ->pushCriteria(new InnerJoinCriteria(new Influencer, new InfluencerSocialData, 'id', 'influencer_id'))
                ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
                ->pushCriteria(new WhereCriteria(new Slot, 'id', $id))
                ->pushCriteria(new WhereCriteria(new ReviewTranslation, 'code', config('evemoo.lang.default')))
                ->pushCriteria(new WhereCriteria(new ProjectTranslation, 'code', config('evemoo.lang.default')))
                ->get();

        return $data;
    }
}