<?php

namespace App\Http\Controllers\Api\Influencer\Traits;

use App\Evemoo\Models\Influencer;
use App\Evemoo\Models\InfluencerTranslation;
use App\User;
use Evemoo\Repositories\Criteria\InnerJoinCriteria;
use Evemoo\Repositories\Criteria\SelectCriteria;
use Evemoo\Repositories\Criteria\WhereCriteria;

trait InfluencerTrait
{
	public function getInfluencer($username) {
		$data = $this->influencer
			->pushCriteria(new SelectCriteria(['influencers.id', 'influencers.influencer_username', 'influencers.date_of_birth', 'users.first_name', 'users.middle_name', 'users.last_name', 'users.email', 'users.gender', 'users.primary_contact', 'users.secondary_contact', 'influencer_translations.about_you', 'influencer_translations.country', 'influencer_translations.state']))
            ->pushCriteria(new InnerJoinCriteria(new User, new Influencer, 'id', 'user_id'))
            ->pushCriteria(new InnerJoinCriteria(new InfluencerTranslation, new Influencer, 'entry_id', 'id'))
            ->pushCriteria(new WhereCriteria(new InfluencerTranslation, 'code', config('evemoo.lang.default')))
            ->pushCriteria(new WhereCriteria(new Influencer, 'influencer_username', $username))
            ->firstOrFail();
            
        return (object) $data;
	}
}