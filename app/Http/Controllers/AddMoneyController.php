<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Notifications\Orders;
use App\User;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class AddMoneyController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * @var ServiceInterface
     */
    private $cart;

    public function __construct()
    {
        //parent::__construct();
        
        $ad_bc = new \App\Http\Controllers\Backend\Admin\AdminBaseController();

        /** setup PayPal api context **/
        $paypal_conf = $ad_bc->getSiteConfigs();
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['paypal_client_id'], $paypal_conf['paypal_secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
    /**
     * Show the application paywith paypalpage.
     *
     * @return \Illuminate\Http\Response
     */
    public function payWithPaypal()
    {
        return view('paywithpaypal');
    }


    /**
     * Store a details of payment with paypal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postPaymentWithpaypal(Request $request)
    {
        //dd($request->all());
        $data = $this->cart->getAll();
        $c = 0;
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        foreach($data as $key => $itm)
        {
            $item[$c] = new Item();
                $item[$c++]->setName($itm->name)
                    ->setCurrency('USD')
                    ->setQuantity((int) $itm->qty)
                    ->setPrice($itm->price);
        }
        //dd($item);
        $item_list = new ItemList();
        $item_list->setItems($item);
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal((double) \Cart::total());
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Check Your transaction details!');
        //dd($transaction);
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status')) /** Specify return URL **/
            ->setCancelUrl(URL::route('payment.status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
            //dd($payment);exit;
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::flash('error','Connection timeout');
                return Redirect::route('addmoney.paywithpaypal');
                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                /** $err_data = json_decode($ex->getData(), true); **/
                /** exit; **/
            } else {
                \Session::flash('error','Some error occur, sorry for inconvenient');
                return Redirect::route('addmoney.paywithpaypal');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }
        //dd($payment);
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::flash('paypal_payment_id', $payment->getId());
        if(isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        \Session::flash('error','Unknown error occurred');
        return Redirect::route('customer.cart.index');
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::flash('error','Payment failed');
            return Redirect::route('addmoney.paywithpaypal');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') {
            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/

            $transaction = $this->storeTransaction(Input::get('PayerID'), Input::get('token'));

            $storeOrders = $this->storeOrders($transaction->id);

            \Cart::destroy();

            \Session::flash('success','Payment success');
            return Redirect::route('customer.dashboard');
        }
        \Session::flash('error','Payment failed');
        return Redirect::route('customer.dashboard');
    }

    protected function storeTransaction($payer, $token)
    {
        try {
            $toks = ['token' => $token, 'payer' => $payer];
            $transaction = ModelTransaction::create([
                'transaction_code' => \AppHelper::getTransactionCode(),
                'invoice_code' => \AppHelper::getInvoiceCode(),
                'total_price' => \Cart::total(),
                'users_id' => auth()->user()->id,
                'user_name' => auth()->user()->username,
                'user_email' => auth()->user()->email,
                'payment_gateway_id' => 2,
                'token' => json_encode($toks),
                'status' => 0,
            ]);

        } catch(\Exception $e) {
            return redirect()->route('customer.dashboard')->with('error', $e->getMessage());
        }

        return $transaction;

    }

    protected function storeOrders($transaction)
    {
        $data = $this->cart->getAll();

        dd($data);

        foreach($data as $key => $order)
        {

            $orders[$key] = Order::create([
                'transactions_id' => $transaction,
                'package_id' => $order->id,
                'account_url' => $order->options['account_url'],
                'service' => $order->options['service_title'],
                'package_type' => $order->options['package_type_title'],
                'qty' => $order->options['real_quantity'],
                'price' => $order->price,
                'initial_count' => $this->getInitialCount($order),
                'status' => $this->getStatus($order)
            ]);
            

                $admin = $this->getAdminId();

                $admin->notify(new Orders($orders[$key], auth()->user()));
        }

        return true;

    }

    public function getPackage($order) {
        $package =  Package::select('packages.*',
            'services.type as service_type',
            'package_types.type as package_types_type')
            ->join('package_types', 'package_types.id', '=', 'packages.packages_types_id')
            ->join('services', 'services.id', '=', 'package_types.services_id')
            ->join('packages_translations', 'package_types.id', '=', 'packages_translations.entry_id')
            ->where('packages.id', $order->id)
            ->where('packages_translations.code', \ConfigHelper::evemooConfig('lang.default'))
            ->first();

        return $package;
    }

    public function getInitialCount($order)
    {
        $package = $this->getPackage($order);

        if($package) {
            if(\ConfigHelper::evemooConfig('services.'.$package->service_type.'.package_types.'.$package->package_types_type.'.auto') == 1){
                 return 101; //from api auto generate
            } else {
                return 0;
            }
        }
    }

    public function getStatus($order)
    {
        $package = $this->getPackage($order);

        if($package) {
            if(\ConfigHelper::evemooConfig('services.'.$package->service_type.'.package_types.'.$package->package_types_type.'.auto') == 1){
                return 101;
            } else {
                return 0;
            }
        }
    }

    protected function getAdminId()
    {
        $admin = User::select('users.*')
                        ->join('role_user as ru', 'ru.user_id', '=', 'users.id')
                        ->join('roles as r', 'r.id', '=', 'ru.role_id')
                        ->where('r.name', '=', 'admin')
                        ->first();

        return $admin;
    }
}
