<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Evemoo\Models\Log;
use App\Evemoo\Services\Log\LogInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogController extends AdminBaseController
{
    /**
     * @var string
     */
    public $view_path = 'backend.admin.log';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'log/general.';

    /**
     * $trans path
     * @var string
     */
    public $base_route = 'admin.log';

    /**
     * @var LogInterface
     */
    private $log;

    /**
     * LogController constructor.
     * @param LogInterface $log
     */
    public function __construct(LogInterface $log)
    {
        $this->log = $log;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(parent::loadDataToView(sprintf('%s.index', $this->view_path)), [
            'logs' =>  $this->log->getAllLogWithPaginate()
        ]);
    }

    /**
     * @param Log $log
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Log $log)
    {
        return view(parent::loadDataToView(sprintf('%s.view', $this->view_path)), [
            'log'    => $log,
            'error_process_status' => \ConfigHelper::evemooConfig('log.error_process_status')
        ]);
    }

    /**
     * @param Log $log
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Log $log, Request $request)
    {
        $request->validate([
            'status' => 'required'
        ]);

        $this->log->update($log, $request->all());

        $request->session()->flash('success', 'Log was updated successfully!');
        return redirect()->back();
    }

}
