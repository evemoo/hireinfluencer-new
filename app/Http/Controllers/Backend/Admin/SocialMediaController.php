<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Evemoo\Models\SocialMedia;
use App\Evemoo\Services\SocialMedia\SocialMediaService;
use App\Evemoo\Services\SocialMedia\SocialMediaAttributeService;
use App\Evemoo\Models\SocialMediaTranslation;
use App\Evemoo\Models\SocialMediaAttribute;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SocialMediaController extends AdminBaseController
{
    /**
     * @var string
     */
    public $view_path = 'backend.admin.socialmedia';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'socialmedia/general.';

    /**
     * $trans path
     * @var string
     */
    public $base_route = 'admin.socialmedia';

    public function __construct(
        SocialMediaService $service,
        SocialMediaAttributeService $service_attribute
    ){
        $this->service = $service;
        $this->service_attribute = $service_attribute;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $medias = SocialMedia::select('social_media.*', 'smt.name')
                                ->join('social_media_translations as smt', 'smt.entry_id', '=', 'social_media.id')
                                ->where(function ($query) use ($request) {
                                    if($request->has('lang'))
                                        $query->where('smt.code', $request->lang);
                                    else
                                        $query->where('smt.code', config('evemoo.lang.default'));
                                })
                                ->get();
        return view(parent::loadDataToView(sprintf('%s.index', $this->view_path)), [
            'medias' =>  $medias
        ]);
    }

    /**
     * @param Create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view(parent::loadDataToView(sprintf('%s.create', $this->view_path)));
    }

    /**
     * @param Create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $request->validate([
            config('evemoo.lang.default').'.*' => 'required',
            'default.*' => 'required'
        ], [
            config('evemoo.lang.default').'.name.required' => 'Title filed is required',
            'default.icon.required' => 'Icon is required'
        ]);

        $update = $this->service->create($request->except(['_token']), ['slug' => str_slug($request->en['name'], '-')]);

        return redirect()->route($this->base_route.'.index');
    }

    /**
     * @param Create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        return view(parent::loadDataToView(sprintf('%s.edit', $this->view_path)));
    }

    /**
     * @param Log $log
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Log $log, Request $request)
    {
        $request->validate([
            'status' => 'required'
        ]);

        $this->log->update($log, $request->all());

        $request->session()->flash('success', 'Log was updated successfully!');
        return redirect()->back();
    }


    public function indexAttributes(Request $request)
    {
        $attributes = SocialMediaAttribute::select('social_media_attributes.*', 'smat.name')
                                ->join('social_media_attribute_translations as smat', 'smat.entry_id', '=', 'social_media_attributes.id')
                                ->where(function ($query) use ($request) {
                                    if($request->has('lang'))
                                        $query->where('smat.code', $request->lang);
                                    else
                                        $query->where('smat.code', config('evemoo.lang.default'));
                                })
                                ->get();
        return view(parent::loadDataToView(sprintf('%s.attributes.index', $this->view_path)), [
            'attributes' =>  $attributes
        ]);
    }

    public function delete(Request $request, $id)
    {
        $data = $this->service->idExist($id);

        if ($data)
        {

            $data->translations()->delete();
            $data->delete();

            $response = 'Record is deleted Successfully.';
            return response()
                ->json($response);
        }

        return $response;
    }

    /**
     * @param Create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createAttribute()
    {
        return view(parent::loadDataToView(sprintf('%s.attributes.create', $this->view_path)));
    }

    /**
     * @param Create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeAttribute(Request $request)
    {
        $request->validate([
            config('evemoo.lang.default').'.*' => 'required',
            'default.*' => 'required'
        ], [
            config('evemoo.lang.default').'.name.required' => 'Title filed is required',
            'default.icon.required' => 'Icon is required'
        ]);

        $update = $this->service_attribute->create($request->except(['_token']), ['slug' => str_slug($request->en['name'], '-')]);

        return redirect()->route($this->base_route.'.attributes.index');
    }

    public function deleteAttribute(Request $request, $id)
    {
        $data = $this->service_attribute->idExist($id);
        if ($data)
        {

            $data->translations()->delete();
            $data->delete();

            $response = 'Record is deleted Successfully.';
            return response()
                ->json($response);
        }

        return $response;
    }

}
