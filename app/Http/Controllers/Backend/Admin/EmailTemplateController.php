<?php

namespace App\Http\Controllers\Backend\Admin;


use App\Evemoo\Services\EmailTemplate\EmailTemplateServices;
use App\Http\Requests\Admin\EmailTemplate\UpdateRequest;
use Illuminate\Http\Request;

class EmailTemplateController extends AdminBaseController
{

    /**
     * @var int
     */
    private $pagination_limit = 20;

    /**
     * @var string
     */
    public $view_path = 'backend.admin.email-template';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'email_template/general.';

    /**
     * @var string
     */
    public $base_route = 'admin.email_template';

    /**
     * @var EmailTemplateServices
     */
    private $email_template;

    /**
     * EmailTemplateController constructor.
     * @param EmailTemplateServices $emailTemplateServices
     */
    public function __construct(EmailTemplateServices $emailTemplateServices )
    {
        $this->email_template =  $emailTemplateServices;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = $this->email_template->getSearchDataWithPaginate($request, $this->pagination_limit);
        $data['filter_url'] = route($this->base_route.'.index');
        return view(parent::loadDataToView($this->view_path.'.index'), compact('data'));
    }

    /**
     * @param $attribute
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($attribute)
    {
        $data = [];
        $data = $this->email_template->idExist($attribute);

        $data['patterns'] = $this->email_template->getPatterns($attribute);
        return view(parent::loadDataToView($this->view_path.'.edit'), compact('data'));
    }

    /**
     * @param UpdateRequest $request
     * @param $attribute
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, $attribute)
    {
        $data = [];
        $data = $this->email_template->idExist($attribute);

        $attributes = [
            'title' => $request->input('title'),
            'content' => $request->input('content'),
        ];

        $update = $this->email_template->update($data['row'], $attributes);

        if (!$update) {
            $request->session()->flash('warning', admin_trans($this->trans_path. 'error.updated'));
            return redirect()->back();
        }

        $request->session()->flash('success', admin_trans($this->trans_path. 'status.updated'));
        return redirect()->route($this->base_route.'.index');

    }

}