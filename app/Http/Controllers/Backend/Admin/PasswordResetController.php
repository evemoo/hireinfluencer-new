<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Evemoo\Services\User\UserInterface;
use Illuminate\Http\Request;

class PasswordResetController extends AdminBaseController
{

    /**
     * @var string
     */
    public $view_path = 'backend.admin.user';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'user/general.';

    /**
     * @var string
     */
    public $base_route = 'admin.user';

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * UserController constructor.
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * index action
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view(parent::loadDataToView(sprintf('%s.reset.password', $this->view_path)));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(Request $request)
    {
        $request->validate(['password'     => 'required|confirmed|min:4']);

        $user = $this->user->resetPassword($request->except(['password_confirmation']));

        if (!$user) {
            $request->session()->flash('warning', admin_trans($this->trans_path. 'error.password_reset'));
            return redirect()->back();
        }

        $request->session()->flash('success', admin_trans($this->trans_path. 'status.password_reset'));
        return redirect()->back();
    }

}