<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Evemoo\Models\Setting;
use App\Evemoo\Services\Setting\SettingInterface;
use Illuminate\Http\Request;
use AppExceptionHandler;

class SystemSettingController extends AdminBaseController
{
    /**
     * @var string
     */
    public $view_path = 'backend.admin.setting';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'setting/general.';

    /**
     * @var string
     */
    public $base_route = 'admin.setting';

    /**
     * @var SettingInterface
     */
    private $setting;

    /**
     * SettingController constructor.
     * @param SettingInterface $setting
     */
    public function __construct(SettingInterface $setting)
    {
        $this->setting = $setting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(parent::loadDataToView(sprintf('%s.index', $this->view_path)), [
            'settings' =>  $this->setting->getAllSettingsWithPagination(),
            'roles' =>  $this->setting->getRoles(),
        ]);
    }

    public function update(Request $request, Setting $setting)
    {
        //dd($request->all());
        $setting  = $this->setting->update($setting, $request->except('_token'));

        if (!$setting) {
            $request->session()->flash('warning', admin_trans($this->trans_path. 'error.updated'));
            return redirect()->back();
        }

        $request->session()->flash('success', admin_trans($this->trans_path. 'status.updated'));
        return redirect()->route(sprintf('%s.index', $this->base_route));
    }
}
