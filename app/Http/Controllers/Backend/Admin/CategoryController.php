<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Evemoo\AppTrait\LangByRepo;
use Illuminate\Http\Request;
use App\Evemoo\Services\Category\CategoryServices;
use App\Evemoo\Services\Category\CatServices;
use App\Http\Requests\Admin\Category\CategoryStoreRequest;
use App\Http\Requests\Admin\Category\CategoryUpdateRequest;
use App\Evemoo\Models\Category;
use Illuminate\Support\Facades\DB;

class CategoryController extends AdminBaseController
{
    protected $base_route = 'admin.category';
    protected $view_path = 'backend.admin.category';
    protected $panel = 'Category';
    protected $folder_path;
    protected $folder_name = 'category';
    private $services, $catServices;
    protected $model;

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'category/general.';

    protected $search_params = [];

    public function __construct(
    	CategoryServices $services,
		CatServices $cat_services
    )
    {
        $this->services = $services;
        $this->catServices = $cat_services;
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request, $cat_id= null, $type= null)
    {
        $attributes = [
            'cat_id' => $cat_id,
            'type'  => $type,
            'request' => $request,
            'pagination' => 20,
            'lang' => $request->has('lang')?$request->lang:\ConfigHelper::evemooConfig('lang.default')
        ];

        $data = $this->services->getSearchDataWithPaginate($attributes);
        $data['category'] = $this->services->idExistOrNot($cat_id);

        if ($data['category'])
            $data['parent_cat_id'] = $data['category']->parent_id;

        $data['params'] = $this->setupFilterParams($request);
        $data['filter_url'] = route($this->base_route.'.index');

        return view(parent::loadDataToView($this->view_path.'.index'), compact('data','type'));
    }

    public function create($cat_id= null, $type= null)
    {

        return view(parent::loadDataToView($this->view_path.'.create'), compact('cat_id', 'type'));
    }

    public function store(CategoryStoreRequest $request, $cat_id= null, $type= null)
    {
        if (!is_null($request->default['parent_id']) && !is_null($request->input('type')))
        {
            $parent_id = $request->default['parent_id'];
            $type = $request->input('type');
        } else {
            $parent_id = 0;
        }

        $create = $this->services->create($request);

        if ($create) {
            session()->flash('success', 'Record Created Successfully');
        }

        if($request->action == 'gotoParent')
            if ($parent_id != null && $type != null)
                return redirect()
                    ->route($this->base_route.'.'.$type.'.index', ['cat_id' => $parent_id , 'type' => $type]);
            else
                return redirect()
                    ->route($this->base_route. '.index');

        elseif ($request->action == 'continue')
            if ($parent_id != null && $type != null)
                return redirect()
                    ->route($this->base_route.'.'.$type.'.create', ['cat_id' => $parent_id , 'type' => $type]);
            else
                return redirect()
                    ->route($this->base_route. '.create');
        elseif ($request->action == 'exit')
                return redirect()
                    ->route($this->base_route. '.index');
    }

    public function show(Category $category)
    {
        //
    }

	public function edit($category, $cat_id= null, $type= null) 
    {
        $cat = $this->services->idExist($category);
        $trans_cat = $this->services->getTranslations($cat);

        return view(parent::loadDataToView($this->view_path.'.edit'), compact('cat', 'trans_cat', 'cat_id', 'type'));
    }

    public function update(CategoryUpdateRequest $request, $category, $cat_id= null, $type= null)
    {
        if (!is_null($request->default['parent_id']) && !is_null($request->input('type')))
        {
            $parent_id = $request->default['parent_id'];
            $type = $request->input('type');

        } else {
            $parent_id = 0;
        }

        $data = [];
        $data = $this->services->idExist($category);
        $update = $this->services->update($data, $request);

        if ($update)
        {
            session()->flash('success', 'Record Updated Successfully');
        }

        if($request->action == 'gotoParent')
            if ($parent_id != null && $type != null)
                return redirect()
                    ->route($this->base_route.'.'.$type.'.index', ['cat_id' => $parent_id , 'type' => $type]);
            else
                return redirect()
                    ->route($this->base_route. '.index');

        elseif ($request->action == 'continue')
            if ($parent_id != null && $type != null)
                return redirect()
                    ->route($this->base_route.'.'.$type.'.edit', [$request->id, 'cat_id' => $parent_id , 'type' => $type]);
            else
                return redirect()
                    ->route($this->base_route. '.index');

        elseif ($request->action == 'exit')
            return redirect()
                ->route($this->base_route. '.index');

    }

    public function destroy($category)
    {
    	$response = '';

        $data = $this->services->idExist($category);
        $result = $this->checkIfDeleteIsPossible($category);

        if ($result)
        {
            $data->translations()->delete();
            $data->delete();
	        $response = 'Record is deleted Successfully.';
	        return response()
		        ->json($response);
        }

	    return $response;
    }

	/**
	 * Check if delete is possible
	 * check if it is associated with any other categories
	 * @param $category_id
	 *
	 * @return bool
	 */
	public function checkIfDeleteIsPossible( $category_id )
	{
		$data = [];

		$category = $this->catServices->getCategoriesByParentId($category_id);

		if (count($category) != 0)
		{
			return false;
		}

		return true;
    }

    public function unlinkImages($data)
    {
        // remove old image from folder
        if (file_exists($this->folder_path.$data['row']->image) && $data['row']->image != null)
            unlink($this->folder_path.$data['row']->image);
        foreach (config('classified.image_dimensions.category.main_image') as $dimension) {
            if (file_exists($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_'.$data['row']->image))
                unlink($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_'.$data['row']->image);
        }
    }

    protected function setupFilterParams($request)
    {
        if ($request->has('id'))
            $this->search_params['id'] = $request->get('id');
        if ($request->has('title'))
            $this->search_params['title'] = $request->get('title');
        if ($request->has('slug'))
            $this->search_params['slug'] = $request->get('slug');
        if ($request->has('status') && $request->get('status') != "all")
            $this->search_params['status'] = $request->get('status');

        return $this->search_params;
    }

}
