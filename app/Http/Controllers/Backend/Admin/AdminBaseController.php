<?php

namespace App\Http\Controllers\Backend\Admin;


use App\Http\Controllers\Controller;
use AppExceptionHandler;
use App\Evemoo\Models\Setting;

class AdminBaseController extends Controller
{
    /**
     * @param $view_path
     * @return mixed
     */
    protected function loadDataToView($view_path)
    {
        view()->composer($view_path, function ($view) use ( $view_path) {
            $view->with('trans_path', property_exists($this, 'trans_path')?$this->trans_path:'');
            $view->with('view_path',  property_exists($this, 'view_path')?$this->view_path:'');
            $view->with('base_route',  property_exists($this, 'base_route')?$this->base_route:'');
            $view->with('configs', $this->getSiteConfigs());
        });

        return $view_path;
    }    

    public function getSiteConfigs()
    {
       $settings = $this->changeJsonToArray(Setting::pluck('value', 'key')->toArray());
       
        return $settings;
    }

    public function changeJsonToArray($data)
    {
        foreach ($data as $key => $row) {
            if ($this->isJson($data[$key])) {
                $data[$key] = json_decode($row, 1);
            }
        }

        return $data;
    }

    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    protected function notify($type , $message)
    {
        notification($type, $message);
    }
}
