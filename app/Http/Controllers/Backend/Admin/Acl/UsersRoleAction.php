<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/6/2017
 * Time: 10:47 AM
 */

namespace App\Http\Controllers\Backend\Admin\Acl;

use App\Http\Controllers\AclBaseController;
use App\Evemoo\Models\Role;
use App\User;
use AppHelper;
use ConfigHelper;
use Illuminate\Http\Request;
use AclHelper;

class UsersRoleAction extends AclBaseController
{

    public $view_path      = 'admin.acl.users';
    protected $base_route  = 'admin.users';
    protected $folder_name = 'user';

    public function userRolesHtml($id)
    {
        if (!$user = User::find($id)) {
            AppHelper::unAuthorizedAccess();
        }
        $default_roles = Role::where('enabled', 1)->whereIn('name', ConfigHelper::getDefaultRoles())->get();
        return response()->json(json_encode([
            'message' => view($this->view_path.'.partials.roles', [
                'roles' => $user->roles,
                'default_roles' => $default_roles,
            ])->render(),

            'title' => 'Role(s) for <strong>'.join(' ', [$user->first_name, $user->middle_name, $user->last_name]).'</strong>'
        ]));
    }

    public function updateUserRoles(Request $request, $id)
    {
        $response = [
            'error' => true,
            'role_names_html' => ''
        ];

        if ($user = User::find($id)) {

            $response['error'] = false;
            $role_ids = [];
            foreach ($request->get('roles') as $item) {
                if ($role = AclHelper::getUserRoleModel($item)) {
                    $role_ids[] = $role->id;
                }
            }

            $user->roles()->sync($role_ids);
            foreach ($user->roles()->orderBy('name')->get() as $role) {
                $response['role_names_html'] .= '| <strong>'. $role->name.'</strong> | ';
            }

        }

        return response()->json(json_encode($response));
    }

}