<?php namespace App\Http\Controllers\Backend\Admin\Acl;

use App\Http\Controllers\AclBaseController;
use App\Http\Controllers\Backend\Admin\AdminBaseController;
use App\Evemoo\Repositories\Acl\Criteria\Role\RolesByPositionAscending;
use App\Evemoo\Repositories\Acl\Criteria\Role\RolesWithPermissions;
use App\Evemoo\Repositories\Acl\Criteria\Role\RolesByNamesAscending;
use App\Evemoo\Repositories\Acl\RoleRepository as Role;
use App\Evemoo\Repositories\Acl\PermissionRepository as Permission;
use App\Evemoo\Repositories\Acl\UserRepository as User;
use Illuminate\Http\Request;
use DB;
use AppHelper;
use App\Evemoo\Models\Route;
use AclHelper;

class RolesController extends AdminBaseController
{

    /**
     * @var Role
     */
    private $role;

    /**
     * @var Permission
     */
    private $permission;

    /**
     * @var User
     */
    private $user;

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'acl/roles/general.';



    public $panel          = 'Roles';
    public $view_path      = 'backend.admin.acl.roles';
    protected $base_route  = 'admin.roles';
    protected $folder_name = 'role';

    public $pagination_limit = 10;


    /**
     * @param Role $role
     * @param Permission $permission
     * @param User $user
     */
    public function __construct(Role $role, Permission $permission, User $user)
    {
        $this->role = $role;
        $this->permission = $permission;
        $this->user = $user;

    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $page_title = trans($this->trans_path . 'general.page.index.title'); // "Admin | Roles";
        $page_description = trans($this->trans_path . 'general.page.index.description'); // "List of roles";

        $roles = $this->role->pushCriteria(new RolesWithPermissions())
            ->pushCriteria(new RolesByPositionAscending())
            ->paginate($this->pagination_limit);

        return view($this->loadDataToView($this->view_path . '.index'), compact('roles', 'page_title', 'page_description'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $role = $this->role->find($id);

        $page_title = trans($this->trans_path . 'general.page.show.title'); // "Admin | Role | Show";
        $page_description = trans($this->trans_path . 'general.page.show.description', ['name' => $role->name]); // "Displaying role";

        $perms = $this->permission->all();

        return view($this->loadDataToView($this->view_path. '.show'), compact('role', 'perms', 'page_title', 'page_description'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $page_title = trans($this->trans_path . 'general.page.create.title'); // "Admin | Role | Create";
        $page_description = trans($this->trans_path . 'general.page.create.description'); // "Creating a new role";

        $role = new \App\Evemoo\Models\Role();
        $permission_groups = AclHelper::groupPermissionsInTreeFormat();

        return view($this->loadDataToView($this->view_path. '.create'), compact('role', 'permission_groups', 'page_title', 'page_description'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, array('name' => 'required|unique:roles', 'display_name' => 'required'));

        $attributes = $request->all();

        try {


            $permission_ids = $this->getSelectedPermissionIds($request);
            $role = $this->role->create($attributes);

            $role->savePermissions($permission_ids);
            //$role->forcePermission('basic-authenticated');

            //AppHelper::flash('success', trans($this->trans_path . 'general.status.created'));
            return redirect('/admin/roles');

        } catch (\Exception $ex) {

            \AppExceptionHandler::logMessage('error', $ex->getMessage());
            abort(500);

        }

    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $role = $this->role->find($id);
        if (!$data['row'] = $role) {
            abort(403);
        }

        if (!$role->isEditable()) {
            abort(403);
        }

        $page_title = trans($this->trans_path . 'general.page.edit.title'); // "Admin | Role | Edit";
        $page_description = trans($this->trans_path . 'general.page.edit.description', ['name' => $role->name]); // "Editing role";


        // $perms = $this->permission->all();
        $permission_groups = AclHelper::groupPermissionsInTreeFormat($this->permission->all());

//        $rolePerms = $role->perms();
//        $userCollection = \App\User::take(10)->get(['id', 'first_name', 'last_name', 'username'])->lists('full_name_and_username', 'id');
//        $userList = [''=>''] + $userCollection->all();


        return view($this->loadDataToView($this->view_path. '.edit'), compact('data', 'role', 'permission_groups', 'page_title', 'page_description'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if (!$role = $this->role->find($id)) {
            abort(403);
        }

        if (!$role->isEditable()) {
            abort(403);
        }

        $this->validate($request, array('name' => 'required', 'display_name' => 'required'));

        try {

            $attributes = $request->all();
            $permission_ids = $this->getSelectedPermissionIds($request);

            if ($role->isEditable()) {
                $role->update($attributes);
            }

            if ($role->canChangePermissions()) {
                $role->savePermissions($permission_ids);
            }

           // $role->forcePermission('basic-authenticated');

       //     AppHelper::flash('success', trans($this->trans_path . 'general.status.updated'));
            return redirect('/admin/roles');

        } catch (\Exception $ex) {

            \AppExceptionHandler::logMessage('error', $ex->getMessage());
            abort(500);

        }

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if (!$role = $this->role->find($id)) {
            abort(403);
        }

        if (!$role->isdeletable()) {
            abort(403);
        }

        $this->role->delete($id);

        AppHelper::flash('success', trans($this->trans_path . 'general.status.deleted'));

        return redirect('/admin/roles');
    }

    /**
     * Delete Confirm
     *
     * @param   int $id
     * @return  View
     */
    public function getModalDelete($id)
    {
        $error = null;

        $role = $this->role->find($id);

        if (!$role->isdeletable()) {
            abort(403);
        }

        $modal_title = trans($this->trans_path . 'dialog.delete-confirm.title');
        $modal_cancel = trans('general.button.cancel');
        $modal_ok = trans('general.button.ok');

        $role = $this->role->find($id);
        $modal_route = route('admin.roles.delete', array('id' => $role->id));

        $modal_body = trans($this->trans_path . 'dialog.delete-confirm.body', ['id' => $role->id, 'name' => $role->name]);

        return view('admin.modal_confirmation', compact('error', 'modal_route',
            'modal_title', 'modal_body', 'modal_cancel', 'modal_ok'));

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable($id)
    {
        $role = $this->role->find($id);
        $role->enabled = true;
        $role->save();


        AppHelper::flash('success', trans($this->trans_path . 'general.status.enabled'));
        return redirect('/admin/roles');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function disable($id)
    {
        //TODO: Should we protect 'admins', 'users'??

        $role = $this->role->find($id);
        $role->enabled = false;
        $role->save();

        AppHelper::flash('success', trans($this->trans_path . 'general.status.disabled'));

        return redirect('/admin/roles');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function enableSelected(Request $request)
    {
        $chkRoles = $request->input('chkRole');

        if (isset($chkRoles)) {
            foreach ($chkRoles as $role_id) {
                $role = $this->role->find($role_id);
                $role->enabled = true;
                $role->save();
            }
            AppHelper::flash('success', trans($this->trans_path . 'general.status.global-enabled'));
        } else {
            AppHelper::flash('success', trans($this->trans_path . 'general.status.no-role-selected'));
        }
        return redirect('/admin/roles');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function disableSelected(Request $request)
    {
        //TODO: Should we protect 'admins', 'users'??

        $chkRoles = $request->input('chkRole');

        if (isset($chkRoles)) {
            foreach ($chkRoles as $role_id) {
                $role = $this->role->find($role_id);
                $role->enabled = false;
                $role->save();
            }
            AppHelper::flash('success', trans($this->trans_path . 'general.status.global-disabled'));
        } else {
            AppHelper::flash('success', trans($this->trans_path . 'general.status.no-role-selected'));
        }
        return redirect('/admin/roles');
    }

    /**
     * @param Request $request
     * @return array|static[]
     */
    public function searchByName(Request $request)
    {
        $name = $request->input('query');
        $roles = DB::table('roles')
            ->select(DB::raw('id, display_name as text'))
            ->where('name', 'like', "%$name%")
            ->orWhere('display_name', 'like', "%$name%")
            ->orWhere('description', 'like', "%$name%")
            ->get();
        return $roles;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getInfo(Request $request)
    {
        $id = $request->input('id');
        $role = $this->role->find($id);

        return $role;
    }

    /**
     * Returns an array with permission ids based on selectd
     * permission group in Role Add/Edit form
     *
     * @param $request
     * @return array
     */
    private function getSelectedPermissionIds($request)
    {
        $permission_ids = [];
        $permission_config = config('evemoo-permission.route-groups');
        //dd($permission_config);

        foreach ($permission_config as $level_one_key => $level_one) {

            foreach ($level_one['groups'] as $level_two_key =>  $level_two) {

                foreach ($level_two['routes'] as $routes_key => $routes) {

                    $route_section = $level_one_key.'.'.$level_two_key.'.'.$routes_key;

                    if (!is_null($request->get('perms')) && in_array($route_section, $request->get('perms'))) {

                        foreach ($routes as $route) {

                            $route_detail = Route::select('routes.id as route_id', 'p.id as permission_id')
                                ->join('permissions as p', 'p.id', '=', 'routes.permission_id')
                                ->where('routes.name', $route)
                                ->where('routes.enabled', 1)
                                ->first();

                            if ($route_detail) {
                                $permission_ids[] = $route_detail->permission_id;
                            }

                        }

                    }


                }

            }

        }

        return $permission_ids;
    }

}