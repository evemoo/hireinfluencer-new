<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Evemoo\Models\SocialMedia;
use App\Evemoo\Models\slot;
use App\User;

class DashboardController extends AdminBaseController
{
    /**
     * $view_path
     * @var string
     */
    public $view_path = 'backend.admin.dashboard';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'dashboard/general.';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        /*for ($i=1; $i <= 50; $i++) { 
            $user = \App\User::create([
                'username'   => 'influencer'.$i,
                'email'  => 'influencer'.$i.'@gmail.com',
                'password' => bcrypt('secret'.$i),
                'first_name' => 'influencer'.$i,
                'last_name'   => 'influencer'.$i,
                'social_medias' => \AppHelper::social_links(),
                'enabled' => 1
            ]);

            $influencer = \App\Evemoo\Models\Influencer::create([
                'user_id'               => $user->id,
                'influencer_username'   => $user->username,
                'registration_date'     => \Carbon\Carbon::now(),
                'verified_date'         => \Carbon\Carbon::now(),
                'seen_by_admin'         => 1,
                'average_rating'        => $i*1.5,
                'total_reviews'         => $i,
                'date_of_birth'         => \Carbon\Carbon::now(),
                'status'                => 1,
            ]);

            foreach (config('evemoo.lang.options') as $key => $value) {
                $influencer_translation = \App\Evemoo\Models\InfluencerTranslation::create([
                    'entry_id'      => $influencer->id,
                    'code'          => $key,
                    'message'       => 'Message',
                    'about_you'     => 'About Me',
                    'gender'        => 'Male',
                    'country'       => 'Nepal',
                    'state'         => 'Kathmandu',
                ]);
            }
        }*/

        $social_medias = SocialMedia::select('social_media.*', 'smt.name')
                ->join('social_media_translations as smt', 'smt.entry_id', '=', 'social_media.id')
                ->where(function($query) use($request){
                    if($request->has('lang'))
                        $query->where('smt.code', $request->lang);
                    else
                        $query->where('smt.code', config('evemoo.lang.default'));
                })
                ->get();

        $slots = Slot::select('slots.*', 'st.title', 'st.description')
                ->join('influencers as i', 'i.id', '=', 'slots.influencer_id')
                ->join('slot_translations as st', 'st.entry_id', '=', 'slots.id')
                ->where('i.user_id', auth()->user()->id)
                ->where('st.code', config('evemoo.lang.default'))
                ->get();

        return view(parent::loadDataToView(sprintf('%s.index', $this->view_path)), [
            'social_medias' => $social_medias,
            'slots' => $slots
        ]);
    }

}
