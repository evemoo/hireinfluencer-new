<?php

namespace App\Http\Controllers\Backend\Admin;

use App\User;
use AppExceptionHandler;
use Illuminate\Http\Request;
use App\Evemoo\Models\Review;
use App\Evemoo\Models\ReviewTranslation;
use App\Evemoo\Services\User\UserInterface;
use App\Evemoo\Services\Profile\ProfileInterface;
use App\Http\Requests\Admin\User\StoreRequest;
use App\Http\Requests\Admin\User\UpdateRequest;

class UserController extends AdminBaseController
{
    /**
     * @var string
     */
    public $view_path = 'backend.admin.user';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'user/general.';

    /**
     * @var string
     */
    public $base_route = 'admin.user';

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * UserController constructor.
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user, ProfileInterface $profile)
    {
        $this->user = $user;
        $this->profile = $profile;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $role)
    {
        return view(parent::loadDataToView(sprintf('%s.index', $this->view_path)), [
            'users' =>  $this->user->getAllUsersWithPagination($request, $role),
            'stats' => $this->user->getInfluencerStats(),
            'status' => $this->user->getStatus(),
            'roles' => $this->user->getRoles(),
            'genders' => $this->user->getGenders(),
            'social_medias' => $this->user->getSocialMedias(),
            'categories' => $this->user->getCategories(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($role)
    {
        return view(parent::loadDataToView(sprintf('%s.create', $this->view_path)), ['role' => $role]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, $role)
    {
        $user  = $this->user->create($request->except(['password_confirmation']), $role);

        if (!$user) {
            $request->session()->flash('warning', admin_trans($this->trans_path. 'error.created'));
            return redirect()->back();
        }

        $request->session()->flash('success', admin_trans($this->trans_path. 'status.created'));
        return redirect()->route(sprintf('%s.index', $this->base_route), $role);
    }

    /**
     * View User Orders.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($id, Request $request)
    {
        return view(parent::loadDataToView(sprintf('%s.view', $this->view_path)), [
            'user' => $this->user->find($id),
            'slots' => $this->user->getUserSlots($id, $request),
            'finance' => $this->user->getFinance($id),
            'social_medias' => $this->user->getUserSocialMedias($id),
            'slotsBySocialMedia' => $this->user->getUserSlotsBySocialMedia($id),
            'reviews' => $this->user->getUserReviews($id),
            'user_reviews' => $this->user->getReviewsByUser($id),
            'projects' => $this->user->getUserProjects($id),
            'is_banned' => $this->user->isBanned($id)
        ]);
    }

    /**
     * View User Orders.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function slotDetail($user, $id)
    {
        return view(parent::loadDataToView(sprintf('%s.view', $this->view_path)), [
            'user' => $this->user->find($user),
            'slot' => $this->user->getSlotDetails($id, $user),
            'packages' => $this->user->getPackagesBySlot($id, $user),
            'slot_finance' => $this->user->getSlotfinance($id, $user),
            'reviews' => $this->user->getSlotReview($id),
            'category' => $this->user->getSlotCategory($id),
            'social_medias' => $this->user->getUserSocialMedias($user),
            'is_banned' => $this->user->isBanned($user)
        ]);
    }

    /**
     * View User Orders.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function socialMediaDetail($user)
    {
        return view(parent::loadDataToView(sprintf('%s.view', $this->view_path)), [
            'user' => $this->user->find($user),
            'social_medias' => $this->user->getUserSocialMedias($user),
            'social_details' => $this->user->getSocialMediaDetails($user),
            'is_banned' => $this->user->isBanned($user)
        ]);
    }

    /**
     * View User Orders.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function projectDetail($user, $slug)
    {
        return view(parent::loadDataToView(sprintf('%s.view', $this->view_path)), [
            'user' => $this->user->find($user),
            'project' => $this->user->getProjectDetails($slug, $user),
            'project_chats' => $this->user->getProjectChats($slug, $user),
            'social_medias' => $this->user->getUserSocialMedias($user),
            'reviews' => $this->user->getUserReviews($user, $slug),
            'is_banned' => $this->user->isBanned($user)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view(parent::loadDataToView(sprintf('%s.edit', $this->view_path)), [
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reviewEdit($user, $id)
    {
        $review = Review::select('reviews.*', 'rt.comment')
                ->join('review_translations as rt', 'rt.entry_id', '=', 'reviews.id')
                ->where('reviews.id', $id)
                ->where('rt.code', config('evemoo.lang.default'))
                ->first();

                //dd($review);

        return view(parent::loadDataToView(sprintf('%s.reviews.edit', $this->view_path)), [
            'review' => $review,
            'user' => $this->user->find($id),
            'social_medias' => $this->user->getUserSocialMedias($id),
        ]);
    }

    public function getMessage($user_id) {
        $messages = $this->user->getUserMessages($user_id);

        return view(parent::loadDataToView(sprintf('%s.includes.messages.message', $this->view_path)), [
            'messages' => $messages,
            'user_id' => $user_id
        ]);
    }

    public function sendMessage(Request $request, $receiver_id) {
        $validator = \Validator::make($request->all(), [
            'msg' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        } else {
            $message = $this->user->sendMessages($request->msg, $receiver_id);
            $new_msg = '<li class="out">
                            <img class="avatar" alt="" src="'.asset('img/profile/avatar.png').'">
                            <div class="message">
                                <span class="arrow"> </span>
                                <a href="javascript:;" class="name">'. $message->sender->username .'</a>
                                <span class="datetime"> at 20:40 </span>
                                <span class="body">'. $message->message_text .'</span>
                            </div>
                        </li>';
                        
            return response($new_msg);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, User $user)
    {
        $user = $this->user->update($user, $request->except(['password_confirmation']));

        if (!$user) {
            $request->session()->flash('warning', admin_trans($this->trans_path. 'error.updated'));
            return redirect()->back();
        }

        $request->session()->flash('success', admin_trans($this->trans_path. 'status.updated'));
        return redirect()->route(sprintf('%s.index', $this->base_route));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reviewUpdate(Request $request, $user, $id)
    {
        $review = Review::where('id', $id)->where('reviewer_id', $user)->first();

        $update = $review->update([
            'rating' => $request->rating,
            'published' => $request->published
        ]);

        $review_translations = ReviewTranslation::where('entry_id', $review->id)
                            ->where('code', config('evemoo.lang.default'))
                            ->update([
                                'comment' => $request->comment
                            ]);

        if (!$update || !$review_translations) {
            $request->session()->flash('warning', admin_trans($this->trans_path. 'error.updated'));
            return redirect()->back();
        }

        $request->session()->flash('success', admin_trans($this->trans_path. 'status.updated'));
        return redirect()->route(sprintf('%s.view.review', $this->base_route), $user);
    }

    public function updateAvatar(Request $request)
    {
        $response = [];
        $user = User::find($request->id);

        if($user->profile_image != null && $user->profile_image != '')
        {
            if(file_exists(public_path().'/img/profile/'.$user->profile_image))
            {
                unlink(public_path().'/img/profile/'.$user->profile_image);
            }
        }

        $imageName = $this->profile->uploadImage($request);

        $update = $user->update([
            'profile_image' => $imageName,
        ]);
        if($update)
            $response = asset('img/profile/'.$imageName);
        else
            $response = 'asdasd';

        return response()->json(json_encode($response));
    }

    public function banUser($id, Request $request)
    {
        $request->validate([
            'from' => 'required|date',
            'to' => 'date|nullable',
        ]);

        $ban = $this->user->banUser($id, $request->except(['_token']));

        if($ban)
        {
            $request->session()->flash('success', 'SuccessFully Banned!');
            return redirect()->back();
        } else {
            $request->session()->flash('warning', 'Ban Unsuccesful!');
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(User $user, Request $request)
    {
        if (!$user->delete()) {
            $request->session()->flash('warning', admin_trans($this->trans_path. 'error.deleted'));
            return redirect()->back();
        }

        $request->session()->flash('success', admin_trans($this->trans_path. 'status.deleted'));
        return redirect()->route(sprintf('%s.index', $this->base_route));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reviewDelete(Request $request, $user, $id)
    {
        $review = Review::where('id', $id)->where('reviewer_id', $user)->first();

        $del_trans = $review->trans()->delete();

        $del = $review->delete();

        if (!$del || !$del_trans) {
            $request->session()->flash('warning', admin_trans($this->trans_path. 'error.deleted'));
            return redirect()->back();
        }

        $request->session()->flash('success', admin_trans($this->trans_path. 'status.deleted'));
        return redirect()->route(sprintf('%s.view.review', $this->base_route), $user);
    }

    //Ajax Methods
    public function getSlotReview(Request $request) {

        $response = [];

        $reviews = $this->user->getSlotReview($request->id);

        $response['available'] = count($reviews) > 0?true:false;

        $response['view'] = view(sprintf('%s.includes.slots.review-timeline', $this->view_path), ['reviews' => $reviews])->render();

        return response()->json(json_encode($response));
    }

    public function getUserQuickDetails($id) {

        $response = [];

        $response = $this->user->getUserQuickDetails($id);

        return response()->json(json_encode($response));
    }

}
