<?php

namespace App\Http\Controllers\Backend;

use ConfigHelper;
use Illuminate\Http\Request;

class ErrorController
{
    /**
     * @var $error_codes
     */
    protected $error_codes;

    public $trans_path = 'errors/general.';

    public function __construct()
    {
        $this->error_codes = ConfigHelper::getConfigBykey('evemoo.error-codes');
    }

    /**
     * Loads admin error pages
     *
     * @param Request $request
     * @param null $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index($code = null)
    {


        if (in_array($code, $this->error_codes)) {

            $text = [];
            $text['code'] = admin_trans($this->trans_path.'not-found.code');
            $text['title'] = admin_trans($this->trans_path.'not-found.title');
            $text['description'] = admin_trans($this->trans_path.'not-found.description');
            $text['back_to_text'] = admin_trans($this->trans_path.'not-found.back_to_text');
            $text['back_to_link'] = route('admin.dashboard');

            switch ($code) {

                case 'no-error-code-passed':

                    $text['code'] = admin_trans($this->trans_path.'no-error-code-passed.code');
                    $text['title'] = admin_trans($this->trans_path.'no-error-code-passed.title');
                    $text['description'] = admin_trans($this->trans_path.'no-error-code-passed.description');

                    break;

                case 'system-error':

                    $text['code'] = admin_trans($this->trans_path.'system-error.code');
                    $text['title'] = admin_trans($this->trans_path.'system-error.title');
                    $text['description'] = admin_trans($this->trans_path.'system-error.description');

                    break;

                case 'invalid-request':

                    $text['code'] = admin_trans($this->trans_path.'invalid-request.code');
                    $text['title'] = admin_trans($this->trans_path.'invalid-request.title');
                    $text['description'] = admin_trans($this->trans_path.'invalid-request.description');

                    break;

                case 'un-authorized-request':

                    $text['code'] = '401';
                    $text['title'] = admin_trans($this->trans_path.'un-authorized-request.title');
                    $text['description'] = admin_trans($this->trans_path.'un-authorized-request.description');

                    break;

            }

            return view('backend.error.index', compact('text'));

        }
        else
            return redirect()->route('admin.error', [
                'code' => AppHelper::getErrorCode('invalid-request')
            ]);
    }

}