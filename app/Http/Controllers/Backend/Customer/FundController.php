<?php

namespace App\Http\Controllers\Backend\Customer;

use App\User;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use URL;
use Validator;
use Session;
use Redirect;

use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Stripe\Stripe;
use Stripe\Charge;

class FundController extends CustomerBaseController
{
    /**
     * $view_path
     * @var string
     */
    public $view_path = 'backend.customer.fund';

    private $_api_context;
    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'fund/general.';

    public function __construct()
    {
        //parent::__construct();

        /** setup PayPal api context **/
        $paypal_conf = $this->configs();
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(parent::loadDataToView(sprintf('%s.index', $this->view_path)));
    }


    public function getPaymentStatus()
    {

        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::flash('error','Payment failed');
            return Redirect::route('addmoney.paywithpaypal');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);


        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') {

            $this->storeFund();
            session()->forget('fund_payment');

            \Session::flash('success','Payment success ! Your fund has been updated successfully');
            return Redirect::route('customer.dashboard');
        }
        \Session::flash('error','Payment failed');
        return Redirect::route('customer.dashboard');
    }


    public function store(Request $request)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item = new Item();
        $item->setName('Add User Fund')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice((int) $request->get('fund'));

        $item_list = new ItemList();
        $item_list->setItems([$item]);

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal((double) $request->get('fund'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Check Your transaction details!');

        $redirect_urls = new RedirectUrls();

        $redirect_urls->setReturnUrl(URL::route('payment.fund.status')) /** Specify return URL **/
        ->setCancelUrl(URL::route('payment.fund.status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);

        }
        catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::flash('error','Connection timeout');
                return Redirect::route('addmoney.paywithpaypal');
                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                /** $err_data = json_decode($ex->getData(), true); **/
                /** exit; **/
            } else {
                \Session::flash('error','Some error occur, sorry for inconvenient');
                return Redirect::route('addmoney.paywithpaypal');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }



        //dd($payment);
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::flash('paypal_payment_id', $payment->getId());
        if(isset($redirect_url)) {
            /** redirect to paypal **/

            session()->put('fund_payment', $request->get('fund'));

            return Redirect::away($redirect_url);
        }

        \Session::flash('error','Unknown error occurred');
        return Redirect::route('customer.cart.index');
    }

    public function storeFund()
    {
        $user = User::find(auth()->user()->id);
        $user->fund = $user->fund + session('fund_payment');
        $user->save();

        return $user;
    }

    public function stripStore(Request $request)
    {

        Stripe::setApiKey(\Config::get('stripe.secret_key'));

        Charge::create([
            "amount" => (int) $request->get('fund_stripe') *100,
            "currency" => 'usd',
            "source" => $request->get('stripeToken'),
            "description" => "Test Charge"
        ]);

        $user = User::find(auth()->user()->id);
        $user->fund = $user->fund + $request->get('fund_stripe');
        $user->save();

        \Session::flash('success','Payment success ! Your fund has been updated successfully');
        return redirect()->route('customer.dashboard');
    }

}