<?php

namespace App\Http\Controllers\Backend\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Evemoo\Services\Profile\ProfileInterface;
use App\Http\Requests\Customer\Profile\UpdateRequest;

class ProfileController extends CustomerBaseController
{    
    /**
     * $view_path
     * @var string
     */
    public $view_path = 'backend.customer.profile'; 

    /**
     * $base_route
     * @var string
     */
    public $base_route = 'customer.profile';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'profile/general.';

    /**
     * @var ServiceInterface
     */
    private $profile;

    private $user;

    /**
     * DashboardController constructor.
     * @param OrderInterface $profile
     */
    public function __construct(
        ProfileInterface $profile
    )
    {
        $this->profile = $profile;
    }

    public function index()
    {
    	return view(parent::loadDataToView($this->view_path.'.index'), [
    		'profile' => $this->profile->getProfile(),
    		'details' => $this->profile->getDetails(),
            'stats'  => $this->profile->getStats()
    	]);
    }

    public function updateAvatar(Request $request)
    {
    	$response = [];
    	$user = $this->profile->getProfile();

        /*if(isset($user->profile_image) && $user->profile_image != '' && file_exists(asset('img/profile/'.$user->profile_image)))
            unlink(asset('img/profile/'.$user->profile_image));*/

    	$imageName = $this->profile->uploadImage($request);

    	$update = $user->update([
    		'profile_image' => $imageName,
    	]);
    	if($update)
	    	$response = asset('img/profile/'.$imageName);
	    else
	    	$response = 'asdasd';

    	return response()->json(json_encode($response));
    }

    public function update(UpdateRequest $request)
    {
    	//dd($request->profile);
    	$profile = $this->profile->updateProfile($request->profile);
    	$details = $this->profile->updateDetails($request->details);

    	if($profile && $details)
    		session()->flash('success', 'Profile Successfully updated!');

    	return redirect()->route($this->base_route.'.index');
    }
}
