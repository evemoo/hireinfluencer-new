<?php

namespace App\Http\Controllers\Backend\Customer;

use Illuminate\Http\Request;
use App\Evemoo\Models\Slot;

class SlotController extends CustomerBaseController
{
    /**
     * $view_path
     * @var string
     */
    public $view_path = 'backend.customer.slot';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'slot/general.';

    public $base_route = 'customer.slot';

    public function index(Request $request)
    {
    	$slots = Slot::select('slots.*', 'st.title', 'st.description')
				->join('slot_translations as st', 'st.entry_id', '=', 'slots.id')
				->where(function($query) use($request) {
					if($request->has('lang'))
						$query->where('st.code', $request->lang);
					else
						$query->where('st.code', config('evemoo.lang.default'));
				})
				->paginate(30);

		return view(parent::loadDataToView(sprintf('%s.index', $this->view_path)), ['slots', $slots]);
    }

    public function create()
    {
    	return view(parent::loadDataToView(sprintf('%s.create', $this->view_path)));
    }

    public function store(Request $request)
    {
    	
    }
}
