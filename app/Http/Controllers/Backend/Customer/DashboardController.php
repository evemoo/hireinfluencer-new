<?php

namespace App\Http\Controllers\Backend\Customer;

use App\Evemoo\Models\InfluencerSocialData;
use App\Evemoo\Models\Slot;

class DashboardController extends CustomerBaseController
{
    /**
     * $view_path
     * @var string
     */
    public $view_path = 'backend.customer.dashboard';

    /**
     * $trans path
     * @var string
     */
    public $trans_path = 'dashboard/general.';

    /**
     * DashboardController constructor.
     * @param ServiceInterface $service
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $social_medias = InfluencerSocialData::select('sm.*', 'smt.name', 'influencer_social_datas.value', 'smat.name as sma_name')
                ->join('social_media as sm', 'sm.id', '=', 'influencer_social_datas.social_media_id')
                ->join('social_media_translations as smt', 'smt.entry_id', '=', 'sm.id')
                ->join('social_media_attributes as sma', 'sma.id', '=', 'influencer_social_datas.social_media_attribute_id')
                ->join('social_media_attribute_translations as smat', 'smat.entry_id', '=', 'sma.id')
                ->join('influencers as i', 'i.id', '=', 'influencer_social_datas.influencer_id')
                ->join('users as u', 'u.id', '=', 'i.user_id')
                ->where('u.id', auth()->user()->id)
                ->where('smt.code', config('evemoo.lang.default'))
                ->where('smat.code', config('evemoo.lang.default'))
                ->get();

        $slots = Slot::select('slots.*', 'st.title', 'st.description')
                ->join('influencers as i', 'i.id', '=', 'slots.influencer_id')
                ->join('slot_translations as st', 'st.entry_id', '=', 'slots.id')
                ->where('i.user_id', auth()->user()->id)
                ->where('st.code', config('evemoo.lang.default'))
                ->get();

        return view(parent::loadDataToView(sprintf('%s.index', $this->view_path)), [
            'social_medias' => $social_medias,
            'slots' => $slots
        ]);
    }
}