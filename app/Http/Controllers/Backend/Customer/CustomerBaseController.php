<?php

namespace App\Http\Controllers\Backend\Customer;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Social\SocialBaseController;
use AppExceptionHandler;

class CustomerBaseController extends SocialBaseController
{

    /**
     * @param $view_path
     * @return mixed
     */
    protected function loadDataToView($view_path)
    {

        view()->composer($view_path, function ($view) use ( $view_path) {
            $view->with('trans_path', property_exists($this, 'trans_path')?$this->trans_path:'');
            $view->with('view_path',  property_exists($this, 'view_path')?$this->view_path:'');
            $view->with('base_route',  property_exists($this, 'base_route')?$this->base_route:'');
            $view->with('configs', $this->configs());

        });

        return $view_path;
    }

    public function configs()
    {
        $ad_bc = new \App\Http\Controllers\Backend\Admin\AdminBaseController();
        $configs = $ad_bc->getSiteConfigs();

        return $configs;
    }
    

}
