<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Stripe\Stripe;
use Stripe\Charge;
use App\Notifications\Orders;

class StripeController extends Controller
{
    /**
     * @var ServiceInterface
     */
    private $cart;

    /**
     * DashboardController constructor.
     * @param CartInterface $cart
     */
    public function __construct()
    {
    	
    }

	public function stripe(Request $request)
	{
		$configs = $this->configs();
		Stripe::setApiKey($configs['stripe_secret']);

		try {
			Charge::create([
				"amount" => (int) $this->cart->total()*100,
				"currency" => 'usd',
				"source" => $request->get('stripeToken'),
				"description" => "Test Charge"
			]);
		} catch(\Exception $e) {
			return redirect()->route('customer.dashboard')->with('error', $e->getMessage());
		}
		
		$storeTransaction = $this->storeTransaction($request);

		$storeOrders = $this->storeOrders($storeTransaction->id);

		\Cart::destroy();

		Session::flash('success', 'Payment Successful');
		return redirect()->route('customer.dashboard');
	}

	protected function storeTransaction(Request $request)
	{
		try {
			$token = ['token' => $request['stripeToken']];
			$transaction = Transaction::create([
				'transaction_code' => \AppHelper::getTransactionCode(),
				'invoice_code' => \AppHelper::getInvoiceCode(),
				'total_price' => \Cart::total(),
				'users_id' => auth()->user()->id,
				'user_name' => auth()->user()->username,
				'user_email' => auth()->user()->email,
				'payment_gateway_id' => 1,
				'token' => json_encode($token),
				'status' => 0,
			]);

		} catch(\Exception $e) {
			return redirect()->route('customer.dashboard')->with('error', $e->getMessage());
		}

		return $transaction;

	}

	protected function storeOrders($transaction)
	{
		$data = $this->cart->getAll();
		$control = new AddMoneyController($this->cart);
		try {
			foreach($data as $key => $order)
			{
				$orders[$key] = Order::create([
					'transactions_id' => $transaction,
					'package_id' => $order->id,
					'account_url' => $order->options['account_url'],
					'service' => $order->options['service_title'],
					'package_type' => $order->options['package_type_title'],
					'qty' => $order->options['real_quantity'],
					'price' => $order->price,
					'initial_count' => $control->getInitialCount($order),
	                'status' => $control->getStatus($order)
				]);

				$admin = $this->getAdminId();

        		$admin->notify(new Orders($orders[$key], auth()->user()));
			}
		} catch(\Exception $e) {
			return redirect()->route('customer.dashboard')->with('error', $e->getMessage());
		}

		return true;

	}

    protected function getAdminId()
    {
        $admin = User::select('users.*')
                        ->join('role_user as ru', 'ru.user_id', '=', 'users.id')
                        ->join('roles as r', 'r.id', '=', 'ru.role_id')
                        ->where('r.name', '=', 'admin')
                        ->first();

        return $admin;
    }

    public function configs()
    {
        $ad_bc = new \App\Http\Controllers\Backend\Admin\AdminBaseController();
        $configs = $ad_bc->getSiteConfigs();

        return $configs;
    }
}