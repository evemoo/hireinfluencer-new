<?php


if (! function_exists('pluck')) {

    /**
     * @param $collections
     * @param $field
     * @param $value
     * @return array
     */
    function pluck($collections, $field, $value) : array
    {
        $data = [];

        if($collections->count() > 0) {

            foreach ($collections as $collection) {

                $data[$collection->{$field}] = $collection->{$value};

            }

        }

        return $data;
    }

}

if (! function_exists('admin_trans')) {

    /**
     * Admin Translation Path
     *
     * @param $path
     * @return string
     */
    function admin_trans($path)
    {
        return trans("backend/admin/{$path}");
    }

}

if (! function_exists('customer_trans')) {

    /**
     * Admin Translation Path
     *
     * @param $path
     * @return string
     */
    function customer_trans($path)
    {
        return trans("backend/customer/{$path}");
    }

}

if (! function_exists('pluck_array')) {

    /**
     * @param $array
     * @param $code
     * @return array
     */
    function pluck_array($array, $code) : array
    {
        $data = [];

        if(count($array) > 0) {

            foreach ($array as $ary) {

                $data[] = $ary[$code];

            }

        }

        return $data;
    }

}

if (! function_exists('dropdownFormat')) {

    /**
     * @param $collections
     * @param $name
     * @param $value
     * @return array
     */
    function dropDownFormat($collections, $name, $value) : array
    {
        $data = [];

        foreach ($collections as $collection) {
            $data[] = [
                'name'  => $collection->{$name},
                'value' => $collection->{$value},
            ];
        }

        return $data;
    }

}

if (! function_exists('arrayDropdown')) {

    /**
     * @param $arrays
     * @return array
     */
    function arrayDropdown($arrays) : array
    {
        $data = [];
        $i    = 0;

        foreach ($arrays as $array) {

            $data[] = array_merge([
                'name'  => $array['title'],
                'value' => $array['key'],
            ], $i == 0?['selected' => true]:[]);

            $i++;
        }

        return $data;
    }

}

if (! function_exists('configArrayKeyTitle')) {

    /**
     * @param $arrays
     * @return array
     */
    function configArrayKeyTitle($arrays) : array
    {
        $data = [];

        foreach ($arrays as $array) {

            $data[$array['key']] = $array['title'];
        }

        return $data;
    }

}
