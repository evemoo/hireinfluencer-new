<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class AppHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('appExceptionHandler', function()
        {
            return new \App\EveMoo\Helpers\AppExceptionHandler;
        });

        App::bind('confighelper', function()
        {
            return new \App\EveMoo\Helpers\ConfigHelper;
        });

        App::bind('aclhelper', function()
        {
            return new \App\Evemoo\Helpers\AclHelper;
        });

        App::bind('apphelper', function()
        {
            return new \App\Evemoo\Helpers\AppHelper;
        });

        App::bind('viewhelper', function()
        {
            return new \App\Evemoo\Helpers\ViewHelper;
        });
    }
}
