<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('admin', function () {
            return $this->checkAdminRole();
        });

        Blade::if('customer', function () {
            return $this->checkCustomerRole();
        });
    }

    public function checkAdminRole()
    {
        return in_array(\ConfigHelper::getRole('admin')['slug'], auth()->user()->roles->pluck('name')->all());
    }

    public function checkCustomerRole()
    {
        return in_array(\ConfigHelper::getRole('customer')['slug'], auth()->user()->roles->pluck('name')->all());
    }

}
