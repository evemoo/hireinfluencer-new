<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group( function($router) {

                require base_path('routes/web.php');
                require base_path('routes/api.php');
                require base_path('routes/admin.php');
                require base_path('routes/customer.php');
                //require base_path('routes/oauth.php');
            });

    }

    /**
     * @param string $router
     * @param $file_name
     * @param array $prefix
     */
    protected function prefix($router, $file_name, $prefix = [])
    {

        $router->group($prefix , function () use ($file_name, $router) {

            require  base_path("routes/{$file_name}.php");

        } );

    }

}
