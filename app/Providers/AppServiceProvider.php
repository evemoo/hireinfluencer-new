<?php

namespace App\Providers;

use App\Evemoo\Facades\AppHelper;
use App\Evemoo\Services\Log\LogInterface;
use App\Evemoo\Services\Log\LogService;
use App\Evemoo\Services\Setting\SettingService;
use App\Evemoo\Services\Setting\SettingInterface;
use App\Evemoo\Services\Social\SocialInterface;
use App\Evemoo\Services\Social\SocialService;
use App\Evemoo\Services\User\UserInterface;
use App\Evemoo\Services\User\UserService;
use App\Evemoo\Services\Profile\ProfileInterface;
use App\Evemoo\Services\Profile\ProfileService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Consist interface as key and implementation as value.
     *
     * @var array
     */
    protected $bindings = [
        UserInterface::class        => UserService::class,
        LogInterface::class         => LogService::class,
        SettingInterface::class     => SettingService::class,
        SocialInterface::class      => SocialService::class,
        ProfileInterface::class     => ProfileService::class
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $this->customValidations();

        if ($this->app->environment() !== 'production')
            \DB::enableQueryLog();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ( $this->bindings as $interface => $implementation ) {
            $this->app->bind($interface, $implementation);
        }
    }

    protected function customValidations()
    {
        Validator::extend('full_name', function ($attribute, $value, $parameters, $validator) {

            $name_pcs = AppHelper::split_name($value);
            if ($name_pcs['last_name'] == '')
                return false;

            return true;

        });
    }
}
