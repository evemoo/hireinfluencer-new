<?php

namespace App\Listeners;

use App\Evemoo\Services\EmailTemplate\EmailTemplateServices;
use App\Events\CustomerRegistered;
use App\Mail\UserActivation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use AppHelper;
use ConfigHelper;

class SendVerificationEmail
{
	/*
	 * Template Services instance
	 */
	protected $templateServices;

	/**
	 * SendVerificationEmail constructor.
	 *
	 * @param EmailTemplateServices $email_template_services
	 */
	public function __construct(EmailTemplateServices $email_template_services)
	{
		$this->templateServices = $email_template_services;
	}

    /**
     * Handle the event.
     *
     * @param  CustomerRegistered  $event
     * @return void
     */
    public function handle(CustomerRegistered $event)
    {
        // send email to register user with link to active account
        //Mail::to($event->user->email)->send(new UserActivation($event->user));
        $this->templateServices->sendEmail(
		    [
			    '{APP_NAME}'               => json_decode(
                    ConfigHelper::getSiteConfigByKey('company'))->en,
			    '{USER_FULL_NAME}'         => AppHelper::getUserFullName($event->user->id),
			    '{USER_ACTIVATION_LINK}'  => route('user.verification', $event->user->verification_token) ,
			    '{WEB_LINK}'               => route('user.verification', $event->user->verification_token)
		    ],
		    'send_user_activation_code',
		    [
			    'receiver'    => $event->user->email,
			    'sender'      => ConfigHelper::getSiteConfigByKey('email')
		    ]
	    );
    }
}
