<?php

namespace App\Listeners;

use App\Events\CustomerMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendCustomerEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerMail  $event
     * @return void
     */
    public function handle(CustomerMail $event)
    {
    	$sender = $event->customer['sender'];
    	$receiver = $event->customer['receiver'];
    	$content = $event->customer['content'];

	    Mail::send('email.send_message', $content,
		    function ($message) use ($sender, $receiver)
	    {

		    $message->from($sender, $sender);

		    $message->to($receiver)->subject('Hireme');

	    });

    }
}
