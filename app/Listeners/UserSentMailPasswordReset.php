<?php

namespace App\Listeners;

use Mail;
use App\Events\ResetPassword;
use App\Mail\ResetPassword as ResetPasswordMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserSentMailPasswordReset
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ResetPassword  $event
     * @return void
     */
    public function handle(ResetPassword $event)
    {
        Mail::to($event->user->email)->send(new ResetPasswordMail($event->user, $event->password));
    }
}
