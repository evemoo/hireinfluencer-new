<?php

namespace App\Listeners;

use App\Evemoo\Services\EmailTemplate\EmailTemplateServices;
use Illuminate\Auth\Events\Registered;
use App\Notifications\WelcomeEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use ConfigHelper;
use AppHelper;

class UserRegistered
{
	/*
	 * Template Services instance
	 */
	protected $templateServices;

	/**
	 * UserRegistered constructor.
	 *
	 * @param EmailTemplateServices $email_template_services
	 */
	public function __construct(EmailTemplateServices $email_template_services)
    {
        $this->templateServices = $email_template_services;
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
	    $this->templateServices->sendEmail(
		    [
		    	'{APP_NAME}'           => json_decode(
		    	    ConfigHelper::getSiteConfigByKey('company'))->en,
			    '{USER_FULL_NAME}'     => AppHelper::getUserFullName($event->user->id),
			    '{WEB_LINK}'           => url('/')
		    ],
		    'email_welcome_template',
		    [
			    'receiver'    => $event->user->email,
			    'sender'      => ConfigHelper::getSiteConfigByKey('email')
		    ]
	    );
    }
}
