<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailTemplate extends Mailable
{
    use Queueable, SerializesModels;

	/*
	 * The dynamic parameters instance
	 * @var attr
	 */
	protected $params;


	/**
	 * Create a new message instance.
	 *
	 * @param $params
	 */
	public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->replyTo($this->params['sender'])
                    ->markdown('emails.template')
			        ->with([
				        'name'    => $this->params['receiver'],
				        'content' => $this->params['content'],
				        'url'     => $this->params['url'],
			        ]);
    }
}
