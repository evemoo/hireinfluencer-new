<?php
namespace App\Evemoo\Facades;
use Illuminate\Support\Facades\Facade;

class AppExceptionHandlerFacade extends Facade{
    protected static function getFacadeAccessor() { return 'appExceptionHandler'; }
}