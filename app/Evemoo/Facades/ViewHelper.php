<?php
namespace App\Evemoo\Facades;
use Illuminate\Support\Facades\Facade;

class ViewHelper extends Facade{
    protected static function getFacadeAccessor() { return 'viewhelper'; }
}