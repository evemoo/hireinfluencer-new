<?php
namespace App\Evemoo\Facades;
use Illuminate\Support\Facades\Facade;

class AclHelperFacade extends Facade{
    protected static function getFacadeAccessor() { return 'aclhelper'; }
}