<?php
namespace App\Evemoo\Facades;
use Illuminate\Support\Facades\Facade;

class ConfigHelper extends Facade{
    protected static function getFacadeAccessor() { return 'confighelper'; }
}