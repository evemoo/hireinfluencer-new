<?php

namespace App\Evemoo\AppTrait;


use App\Evemoo\Models\CategoryTranslation;
use App\Evemoo\Repositories\Influencer\Criteria\Category\CategoryByEntryId;
use App\Evemoo\Repositories\Influencer\Criteria\Category\CategoryByLang;
use Evemoo\Repositories\Criteria\WhereCriteria;

trait LangByRepo
{

    /**
     * @param $translation_model
     * @param $formDetails
     * @param array $extra_details
     * @return $this
     */
    public function createL($formDetails, $extra_details = [] )
    {
       $preparedDetails =  $this->prepareDetails($formDetails, $extra_details);
       try {
           $model  = $this->model_repo->create($preparedDetails['model_details']);
       } catch (\Exception $e) {
           dd($e->getMessage());
           die;
       }

        foreach ($preparedDetails['lang_details']  as $lang)
        {
           $this->model_lang_repo->create(array_merge(['entry_id' => $model->id],$lang ));
        }

        return $model;
    }

    /**
     * @param $translation_model
     * @param $formDetails
     * @param array $extra_details
     * @return $this
     */
    public function updateL($model, $formDetails, $extra_details = [])
    {
        $preparedDetails =  $this->prepareDetails($formDetails, $extra_details);

        $this->model_repo->update($preparedDetails['model_details'], $model->id);

        foreach ($preparedDetails['lang_details']  as $lang) {

            $this->model_lang_repo
                ->pushCriteria(new WhereCriteria(new CategoryTranslation, 'entry_id', $model->id))
                ->pushCriteria(new WhereCriteria(new CategoryTranslation, 'code', $lang['code']))
                ->updateLang(array_merge(['entry_id' => $model->id], $lang ));
            $this->model_lang_repo->resetModel();

        }

        return $this;
    }


    /**
     * @param $formDetails
     * @param array $extra_details
     * @return array
     */
    public function prepareDetails($formDetails, $extra_details= [])
    {
        $lang_code = pluck_array(\ConfigHelper::evemooConfig('lang.options'), 'code');

        $lang_details  = [];
        $model_details = [];

        if(count($formDetails) > 0) {

            foreach ($formDetails as $key => $details) {

                if(in_array($key, $lang_code)) {

                    $lang_details[] = array_merge($details, ['code' => $key]);

                } else {

                    $model_details[$key] =  $details;
                }

            }

        }

        return [
           'lang_details'  =>  $lang_details,
           'model_details' => array_merge($extra_details, $model_details['default'])
        ];

    }

}