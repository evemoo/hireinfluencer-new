<?php

namespace App\Evemoo\AppTrait;

use DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

trait SocialLogin
{

    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($service)
    {
        $user = Socialite::driver($service)->user();

        //dd($user);

        $findUser = User::where('username', $user->getId())->orWhere('email', $user->email)->first();

        if($findUser)
        {
            Auth::login($findUser);
            return redirect()->route('influencerpanel.dashboard');
        }
        else
        {
            $name = \AppHelper::split_name($user->name);

            $create = new User;

            $create->email = $user->email;

            $create->username = $user->getId();

            $create->gender = isset($user->user['gender'])?$user->user['gender']:null;

            $create->profile_image = $user->avatar_original;

            $create->first_name = $name['first_name'];

            $create->middle_name = $name['middle_name'];
            
            $create->last_name = $name['last_name'];

            $create->enabled = 1;

            $create->social_medias = \AppHelper::insert_social_link($service, isset($user->profileUrl)?$user->profileUrl:null);

            $create->save();

            DB::table('role_user')->insert(['user_id' => $create->id, 'role_id' => '3']);

            Auth::login($create);

            return redirect()->route('influencerpanel.dashboard');
        }
    }

}