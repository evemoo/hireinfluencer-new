<?php

namespace App\Evemoo\AppTrait;


trait Lang
{

    /**
     * @param $translation_model
     * @param $formDetails
     * @param array $extra_details
     * @return $this
     */
    public function createL($translation_model, $formDetails, $extra_details = [] )
    {
       $preparedDetails =  $this->prepareDetails($formDetails, $extra_details);
       try {
           $model  = $this->create($preparedDetails['model_details']);
       } catch (\Exception $e) {
           dd($e->getMessage());
           die;
       }

        foreach ($preparedDetails['lang_details']  as $lang)
        {
           app($translation_model)->create(array_merge(['entry_id' => $model->id],$lang ));
        }

        return $model;
    }

    /**
     * @param $translation_model
     * @param $formDetails
     * @param array $extra_details
     * @return $this
     */
    public function updateL($translation_model, $formDetails, $extra_details = [])
    {
        $preparedDetails =  $this->prepareDetails($formDetails, $extra_details);

        $this->update($preparedDetails['model_details']);

        foreach ($preparedDetails['lang_details']  as $lang) {

            app($translation_model)
                ->where('entry_id', $this->id)
                ->where('code', $lang['code'])
                ->update(array_merge(['entry_id' => $this->id], $lang ));

        }

        return $this;
    }


    /**
     * @param $formDetails
     * @param array $extra_details
     * @return array
     */
    public function prepareDetails($formDetails, $extra_details= [])
    {
        $lang_code = pluck_array(\ConfigHelper::evemooConfig('lang.options'), 'code');

        $lang_details  = [];
        $model_details = [];

        if(count($formDetails) > 0) {

            foreach ($formDetails as $key => $details) {

                if(in_array($key, $lang_code)) {

                    $lang_details[] = array_merge($details, ['code' => $key]);

                } else {

                    $model_details[$key] =  $details;
                }

            }

        }

        return [
           'lang_details'  =>  $lang_details,
           'model_details' => array_merge($extra_details, $model_details['default'])
        ];

    }

}