<?php

namespace App\Evemoo\Services\User;

interface UserInterface {


    /**
     * @param $userDetails
     * @return mixed
     */
    public function create($userDetails, $role);

    /**
     * @param $user
     * @param $userDetails
     * @return mixed
     */
    public function update($user, $userDetails);

    /**
     * @return mixed
     */
    public function getAllUsersWithPagination($request, $role);

    /**
     * @param $email
     * @return mixed
     */
    public function findAsEmail($email);

    /**
     * @param $password
     * @return mixed
     */
    public function resetPassword($password);

    /**
     * @param $user
     * @return mixed
     */
    public function getAllClientUser($user, $limit= 10);
    
    public function getStatus();

    /*public function getStats($user);*/

}