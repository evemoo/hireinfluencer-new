<?php

namespace App\Evemoo\Services\Social;

class SocialService implements SocialInterface
{

    /**
     * @param $package_id
     * @return mixed
     */
    public function getPackageInfo($package_id)
    {

        $data = \DB::table('packages')
            ->select(
                'packages.id',
                'packages.packages_types_id',
                'package_types.services_id',
                'package_types.type as package_type',
                'package_types.auto',
                'services.type as service_type'
            )
            ->leftJoin('package_types', 'package_types.id', '=', 'packages.packages_types_id')
            ->leftJoin('services', 'services.id', '=', 'package_types.services_id')
            ->where('packages.id', $package_id)
            ->first();

        return $data;

    }

}