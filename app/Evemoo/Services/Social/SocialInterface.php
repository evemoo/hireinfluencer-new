<?php

namespace App\Evemoo\Services\Social;

interface SocialInterface
{

    /**
     * Get whole package Information
     * @param $package_id
     * @return mixed
     */
    public function getPackageInfo($package_id);

}