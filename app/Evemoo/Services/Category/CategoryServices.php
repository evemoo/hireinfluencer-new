<?php

namespace App\Evemoo\Services\Category;

use App\Evemoo\AppTrait\LangByRepo;
use App\Evemoo\Models\Category;
use App\Evemoo\Models\CategoryTranslation;
use App\Evemoo\Repositories\Influencer\CategoryLangRepository;
use App\Evemoo\Repositories\Influencer\CategoryRepository;
use App\Evemoo\Repositories\Influencer\Criteria\Category\ListFilterCriteria;
use Evemoo\Repositories\Criteria\GroupByCriteria;
use Evemoo\Repositories\Criteria\InnerJoinCriteria;
use Evemoo\Repositories\Criteria\OrderByCriteria;
use Evemoo\Repositories\Criteria\SelectCriteria;
use Evemoo\Repositories\Criteria\WhereCriteria;
use Illuminate\Http\Request;

class CategoryServices
{
    use LangByRepo;

    /**
     * @var Service
     */
    private $category;

    /**
     * @var Category Repository
     */
    private $model_repo;

    /**
     * @var Cagegory Language Repository
     */
    private $model_lang_repo;

    /**
     * UserService constructor.
     *
     * @param Service $category
     * @param Repository $category_repo
     * @param  CategoryLangRepository $category_lang_repo
     */
    public function __construct(
        Category $category,
        CategoryRepository $category_repo,
        CategoryLangRepository $category_lang_repo
    )
    {
        $this->category = $category;
        $this->model_repo = $category_repo;
        $this->model_lang_repo = $category_lang_repo;
    }

    protected $search_params = [];

    public function getSearchDataWithPaginate(array $attributes)
    {
        $data = [];
        $data['rows'] = $this->model_repo
            ->pushCriteria(new InnerJoinCriteria(new CategoryTranslation, new Category, 'entry_id', 'id'))
            ->pushCriteria(new SelectCriteria([
                'categories.id', 'category_translations.title', 'categories.slug',
                'category_translations.description', 'categories.status',
                'categories.created_at', 'categories.parent_id'
            ]))
            ->pushCriteria(new WhereCriteria(new CategoryTranslation, 'code', $attributes['lang']))
            ->pushCriteria(new ListFilterCriteria($attributes))
            ->pushCriteria(new OrderByCriteria(new Category, 'id', 'desc'))
            ->pushCriteria(new GroupByCriteria(new Category, 'id'))
            ->paginate($attributes['pagination']);
        //$this->model_repo->resetModel();

        $data['cat_id'] = $attributes['cat_id'];

        if($data['cat_id']) {
            $data['category_name'] = $this->model_repo->findOrFail($attributes['cat_id'])->title;
            //$this->model_repo->resetModel();
        }

        return $data;
    }

    public function create(Request $categoryDetails)
    {
        try {

            $details = $categoryDetails->except(['_token', 'type', 'action']);

            if($details['default']['parent_id'] == null)
            {
                $details['default']['parent_id'] = 0;
            }

            $user = $this->createL(
                $details,
                ['slug' => str_slug($details[\ConfigHelper::evemooconfig('lang.default')]['title'])]
            );

            return $user;

        } catch (Exception $exception)
        {
            $message  = PHP_EOL.'Error: '.User::class.' : model not saved.';
            $message .= PHP_EOL.'Path: '.get_class($this).'@'.__FUNCTION__;
            $message .= PHP_EOL.'URl: '. request()->fullUrl();

            \AppExceptionHandler::systemError(
                config('evemoo.error-codes.model-not-saved'),
                $message,
                1
            );

            return;
        }
    }

    public function update($model, $categoryDetails)
    {
        try {

            $details = $categoryDetails->except(['_token', 'type', 'action']);

            if($details['default']['parent_id'] == null)
            {
                $details['default']['parent_id'] = 0;
            }

            $user = $this->updateL(
                $model,
                $details,
                ['slug' => str_slug($details[\ConfigHelper::evemooconfig('lang.default')]['title'])]
            );

            return $user;

        } catch (Exception $exception)
        {
            $message  = PHP_EOL.'Error: '.User::class.' : model not saved.';
            $message .= PHP_EOL.'Path: '.get_class($this).'@'.__FUNCTION__;
            $message .= PHP_EOL.'URl: '. request()->fullUrl();

            \AppExceptionHandler::systemError(
                config('evemoo.error-codes.model-not-saved'),
                $message,
                1
            );

            return;
        }
    }

    public function idExist($id)
    {
        $data = $this->model_repo->findOrFail($id);
        return $data;
    }

    public function getTranslations($cat)
    {
        $data = $this->model_repo
            ->pushCriteria(new SelectCriteria([
                'ct.id', 'ct.code', 'ct.title', 'ct.description'
            ]))
            ->pushCriteria(new InnerJoinCriteria(new CategoryTranslation, new Category, 'entry_id', 'id', 'ct'))
            ->pushCriteria(new WhereCriteria(new Category, 'id', $cat->id))
            ->get()
            ->groupBy('code');
        $this->model_repo->resetModel();

        return $data;
    }

    public function idParentExist($id)
    {
        $data['row'] = $this->model_repo
            ->pushCriteria(new WhereCriteria(new Category, 'parent_id', $id))
            ->get();
        $this->model_repo->resetModel();
        if(count($data['row']) == 0) { return abort('404'); }
        return $data;
    }

	public function idExistOrNot( $id )
	{
	    $data = $this->model_repo
            ->pushCriteria(new WhereCriteria(new Category, 'id', $id))
            ->first();
        $this->model_repo->resetModel();
        return $data;
    }

}