<?php

namespace App\Evemoo\Services\Category;

use App\Evemoo\Models\Category;
use Illuminate\Support\Facades\DB;

class CatServices {

	/*
	 *  Three level Category
	 *  First find out
	 *  if its a parent, sub-parent or child
	 *  then show their respective ones
	 */

	public function getCategoryTreeHistory( $category_id )
	{
		$data = [];

		$category = $this->getCategoryById($category_id);

		if ( $category->parent_id != 0 )
		{
			$parent = $this->getCategoryById($category->parent_id);

			if ( $parent->parent_id != 0 )
			{
				$last = $this->getCategoryById($parent->parent_id);

				$data = array($category, $parent, $last);
			}
			else
			{
				$data = array($category, $parent);
			}

		} else {
			$data = $category;
		}

		return $data;
	}

	/*
	 *  First load of the page
	 *  Get the post category_id
	 *  After that from the given category_id
	 *  get if that category has parent
	 *  and after that if the parent have their parent
	 *  get it and show
	 */
	public function getCategory ($category_id)
	{
		$data = [];

		$category = $this->getCategoryById($category_id);

		if ( $category->parent_id != 0 )
		{
			$parent = $this->getCategoryById($category->parent_id);

			if ( $parent->parent_id != 0 )
			{
				$last = $this->getCategoryById($parent->parent_id);

				return $this->getCategoriesByParentId($last->parent_id);
			}

			$data = $this->getCategoriesByParentId($parent->parent_id);
		}
		else
		{
			$data = $this->getCategoriesByParentId($category->parent_id);
		}

		return $data;
	}

	public function getCategoryById ( $category_id )
	{
		$data = [];

		$data = DB::table('categories')
		         ->select (
			         'categories.parent_id',
			         'categories.id',
			         'categories.title'
		         )
		         ->where('categories.id', $category_id)
		         ->first();

		return $data;
	}

	public function getCategoriesByParentId ( $category_id )
	{
		$data = [];

		$data = Category::where('parent_id', $category_id)
		         ->get();

		return $data;
	}

	public function pluckParentCategory()
	{
		$data = [];
		$data = Category::where('parent_id', 0)
			->pluck('title','id')
			->toArray();

		$data[0] = 'Select Category';
		ksort($data);

		return $data;
	}
}