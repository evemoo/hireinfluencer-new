<?php

namespace App\Evemoo\Services\SocialMedia;

use App\Evemoo\AppTrait\LangByRepo;
use App\Evemoo\Models\SocialMedia;
use App\Evemoo\Models\SocialMediaTranslation;
use App\Evemoo\Repositories\Influencer\SocialMediaLangRepository;
use App\Evemoo\Repositories\Influencer\SocialMediaRepository;
use App\Evemoo\Repositories\Influencer\Criteria\Category\CategoriesByIdDescending;
use App\Evemoo\Repositories\Influencer\Criteria\Category\GroupByCategoryId;
use App\Evemoo\Repositories\Influencer\Criteria\Category\ListFilterCriteria;
use App\Evemoo\Repositories\Influencer\Criteria\Category\CategoryByLang;
use App\Evemoo\Repositories\Influencer\Criteria\Category\ColumnsToSelect;
use App\Evemoo\Repositories\Influencer\Criteria\Category\JoinCategoryTranslation;
use Illuminate\Http\Request;

class SocialMediaService
{
    use LangByRepo;

    /**
     * @var Service
     */
    private $socialMedia;

    /**
     * @var Category Repository
     */
    private $model_repo;

    /**
     * @var Cagegory Language Repository
     */
    private $model_lang_repo;

    /**
     * UserService constructor.
     *
     * @param Service $socialMedia
     * @param Repository $category_repo
     * @param  CategoryLangRepository $category_lang_repo
     */
    public function __construct(
        SocialMedia $socialMedia,
        SocialMediaRepository $social_media_repo,
        SocialMediaLangRepository $social_media_lang_repo
    )
    {
        $this->socialMedia = $socialMedia;
        $this->model_repo = $social_media_repo;
        $this->model_lang_repo = $social_media_lang_repo;
    }

    protected $search_params = [];

    public function getSearchDataWithPaginate(array $attributes)
    {
        $data = [];
        $data['rows'] = $this->model_repo
            ->pushCriteria(new JoinCategoryTranslation())
            ->pushCriteria(new ColumnsToSelect([
                'categories.id', 'category_translations.title', 'categories.slug',
                'category_translations.description', 'categories.status',
                'categories.created_at', 'categories.parent_id'
            ]))
            ->pushCriteria(new CategoryByLang($attributes['lang']))
            ->pushCriteria(new ListFilterCriteria($attributes))
            ->pushCriteria(new CategoriesByIdDescending())
            ->pushCriteria(new GroupByCategoryId())
            ->paginate($attributes['pagination']);

        $data['cat_id'] = $attributes['cat_id'];

        if(count($data['cat_id']) != 0) {
            $data['category_name'] = Category::findOrFail($attributes['cat_id'])->title;
        }

        return $data;
    }

    public function create($details)
    {
        try {

            $create = $this->createL(
                $details,
                ['slug' => str_slug($details[\ConfigHelper::evemooconfig('lang.default')]['name'])]
            );

            return $create;

        } catch (Exception $exception)
        {
            $message  = PHP_EOL.'Error: '.User::class.' : model not saved.';
            $message .= PHP_EOL.'Path: '.get_class($this).'@'.__FUNCTION__;
            $message .= PHP_EOL.'URl: '. request()->fullUrl();

            \AppExceptionHandler::systemError(
                config('evemoo.error-codes.model-not-saved'),
                $message,
                1
            );

            return;
        }
    }

    public function update($model, $categoryDetails)
    {
        try {

            $details = $categoryDetails->except(['_token', 'type', 'action']);

            if($details['default']['parent_id'] == null)
            {
                $details['default']['parent_id'] = 0;
            }

            $user = $this->updateL(
                $model,
                $details,
                ['slug' => str_slug($details[\ConfigHelper::evemooconfig('lang.default')]['title'])]
            );

            return $user;

        } catch (Exception $exception)
        {
            $message  = PHP_EOL.'Error: '.User::class.' : model not saved.';
            $message .= PHP_EOL.'Path: '.get_class($this).'@'.__FUNCTION__;
            $message .= PHP_EOL.'URl: '. request()->fullUrl();

            \AppExceptionHandler::systemError(
                config('evemoo.error-codes.model-not-saved'),
                $message,
                1
            );

            return;
        }
    }

    public function idExist($id)
    {
        $data = $this->model_repo->findOrFail($id);
        return $data;
    }

    public function getTranslations($cat)
    {
        $data = $this->category->select('ct.id', 'ct.code', 'ct.title', 'ct.description')
                ->join('category_translations as ct', 'ct.entry_id', '=', 'categories.id')
                ->where('categories.id', $cat->id)
                ->get()
                ->groupBy('code');

        return $data;

        $data = $this->model_repo
            ->pushCriteria(new JoinCategoryTranslation())
            ->pushCriteria(new ColumnsToSelect([
                'category_translations.id', 'category_translations.code', 'category_translations.title',
                'category_translations.description'
            ]))
            ->pushCriteria(new CategoryById($cat->id))
            ->pushCriteria(new GroupByCategoryLangCode())
            ->all();
    }

	public function idExistOrNot( $id )
	{
		$data = Category::where('id',$id)->first();
		return $data;
    }
}