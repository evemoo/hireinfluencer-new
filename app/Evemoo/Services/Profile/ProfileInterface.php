<?php

namespace App\Evemoo\Services\Profile;

interface ProfileInterface {

    /**
     * @return mixed
     */
    public function getProfile();

    /**
     * @return mixed
     */
    public function getDetails();

    /**
     * @param $request
     * @return mixed
     */
    public function uploadImage($request);

    /**
     * @param $request
     * @return mixed
     */
    public function updateProfile($request);

    /**
     * @param $request
     * @return mixed
     */
    public function updateDetails($request);

    /**
     * @param $request
     * @return mixed
     */
    public function getStats();

}