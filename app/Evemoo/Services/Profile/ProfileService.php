<?php

namespace App\Evemoo\Services\Profile;

use Exception;
use App\User;
use App\Evemoo\Models\Role;

class ProfileService implements ProfileInterface
{
    /**
     * @var User
     */
    private $profile;

    /**
     * UserService constructor.
     * @param User $profile
     */
    public function __construct(
        User $profile
    )
    {
        $this->profile = $profile;
    }

    public function getProfile()
    {
        return $this->profile->where('id', auth()->user()->id)->first();
    }

    public function getDetails()
    {
        $details = \DB::table('user_details')->where('users_id', auth()->user()->id)->first();

        return $details;
    }

    public function uploadImage($request)
    {
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $folderPath = public_path('/img/profile');
        $image->move($folderPath, $imageName);

        return $imageName;
    }

    public function updateProfile($request)
    {
        $user = $this->getProfile();

        if(!(isset($request['password']) && isset($request['password_confirmation'])))
        {
            unset($request['password']);
        }

        unset($request['password_confirmation']);

        $user->update($request);

        return true;
    }

    public function updateDetails($request)
    {
        $user = $this->getProfile();

        $details = \DB::table('user_details')->where('users_id', $user->id)->first();

        if($details)
        {
            \DB::table('user_details')->where('users_id', $user->id)->update($request);
        }
        else
        {
            $request['users_id'] = $user->id;
            $details = \DB::table('user_details')->insert($request);
        }

        return $details;
    }

    public function getStats()
    {
        $user = $this->getProfile();

        $order_count = 0;

        $transaction = $this->transaction->where('users_id', $user->id)->get();

        foreach ($transaction as $key => $tran) {
            $order = $this->order->where('transactions_id', $tran->id)->count();
            $order_count += $order;
        }

        $stat = ['transaction' => $transaction->count(), 'order' => $order_count];

        return $stat;
    }

}