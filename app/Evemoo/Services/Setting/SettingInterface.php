<?php

namespace App\Evemoo\Services\Setting;

interface SettingInterface {

    /**
     * @return mixed
     */
    public function getAllSettingsWithPagination();

    /**
     * @param $setting
     * @param $settingDetails
     * @return mixed
     */
    public function update($setting, $settingDetails);

}