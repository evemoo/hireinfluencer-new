<?php

namespace App\Evemoo\Services\Setting;

use App\Evemoo\Models\ServiceTranslation;
use Exception;
use App\User;
use App\Evemoo\Models\Setting;
use App\Evemoo\Models\Role;

class SettingService implements SettingInterface
{
    /**
     * @var Setting
     */
    private $setting;

    /**
     * UserService constructor.
     * @param Setting $setting
     */
    public function __construct(
        Setting $setting
    )
    {
        $this->setting = $setting;
    }

    /**
     * @return mixed
     */
    public function getAllSettingsWithPagination()
    {
        $settings = $this->changeJsonToArray($this->setting->pluck('value', 'key')->toArray());
        //dd($settings);
        return $settings;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $roles = Role::pluck('display_name', 'name');

        return $roles;
    }

    /**
     * @param $setting
     * @param $serviceDetails
     * @return mixed
     */
    public function update($setting, $serviceDetails)
    {
        /*try {
            // /dd($serviceDetails);*/

            $data = $this->prepareDataToUpdate($serviceDetails);
                foreach ($data as $key => $value) {
                    $setting = Setting::where('key', $key)->first();
                    switch ($key) {

                        case 'social_links':
                            if ($setting) {
                                $setting->value = $this->prepareSocialLinks($value);
                                $setting->save();
                            }
                            break;

                        default:
                            if ($setting) {
                                $setting->value = $value;
                                $setting->save();
                                //dd($setting);
                            }
                            break;
                    }
                    //dd($key);

                }
            return $data;

        /*} catch (Exception $exception)
        {
            $message  = PHP_EOL.'Error: '.User::class.' : model not saved.';
            $message .= PHP_EOL.'Path: '.get_class($this).'@'.__FUNCTION__;
            $message .= PHP_EOL.'URl: '. request()->fullUrl();

            \AppExceptionHandler::systemError(
                config('evemoo.error-codes.model-not-saved'),
                $message,
                1
            );

            return;
        }*/
    }

    /** Helper Methods **/

    public function changeJsonToArray($data)
    {
        foreach ($data as $key => $row) {
            if ($this->isJson($data[$key])) {
                $data[$key] = json_decode($row, 1);
            }
        }

        return $data;
    }

    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function prepareDataToUpdate($request)
    {
        foreach ($request as $key => $value) {
            if (is_array($value)) {
                $data[$key] = json_encode($value);
            }
            else
                $data[$key] = $value;
        }

        return $data;
    }

    public function prepareSocialLinks($value)
    {
        $settings = Setting::pluck('value', 'key')->toArray();
        $social_links = json_decode($settings['social_links'], 1);
        $data = json_decode($value, 1);
        foreach ($data as $key => $value) {
            $data[$key]['title'] = $social_links[$key]['title'];
        }

        return json_encode($data);
    }

}