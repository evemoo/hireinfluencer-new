<?php

namespace App\Evemoo\Services\EmailTemplate;

use App\Evemoo\Models\EmailTemplate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class EmailTemplateServices
{
    protected $search_params = [];

    public function getSearchDataWithPaginate($request, $pagination = 10)
    {
        $data = [];
        $data['rows'] = EmailTemplate::select('id', 'title',
            'slug', 'content')
            ->where(function ($query) use ($request) {
                if ($request->has('title'))
                {
                    $query->where('title', 'like', '%' .$request->get('title'). '%');
                    $this->search_params['title'] = $request->get('title');
                }
                if ($request->has('slug'))
                {
                    $query->where('slug', 'like', '%' .$request->get('slug'). '%');
                    $this->search_params['slug'] = $request->get('slug');
                }
            })
            ->orderBy('id', 'DESC')
            ->paginate($pagination);
        $data['params'] = $this->search_params;
        $data['url'] = URL::current();
        return $data;
    }

    public function create($request)
    {
        $data = EmailTemplate::create($request);

        return true;
    }

    public function idExist($id)
    {
        $data['row'] = EmailTemplate::findOrFail($id);
        return $data;
    }

    public function update($model, $request)
    {
        $model->update($request);
        return true;
    }

	/**
	 * @param $request
	 *
	 * @return array
	 */
	public function getPatterns($request)
    {
        $data = [];
        $data = EmailTemplate::select( 'email_template_patterns.pattern as etp_pattern', 'email_template_patterns.label as etp_label')
                            ->join('pattern_template', 'pattern_template.template_id', '=', 'email_templates.id')
                            ->join('email_template_patterns', 'email_template_patterns.id', '=', 'pattern_template.pattern_id')
                            ->where('email_templates.id', $request)
                            ->get();
        return $data;
    }

	/**
	 * Get template and patterns  by template slug
	 * @param $template_slug
	 *
	 * @return array
	 */
	public function getTemplateAndPatterns ($template_slug)
    {
    	$data = [];

    	$data = EmailTemplate::select (
    		    'email_templates.id',
	            'email_templates.title',
	            'email_templates.slug',
	            'email_templates.content',
		        'etp.pattern as etp_pattern',
		        'etp.pattern as etp_pattern'
		    )
		    ->leftJoin('pattern_template as pt', 'pt.template_id', '=', 'email_templates.id')
		    ->leftJoin('email_template_patterns as etp', 'etp.id', '=', 'pt.pattern_id')
		    ->where('email_templates.slug', $template_slug)
		    ->first();

    	return $data;

    }


	/**
	 * @param $params
	 * @param string $template
	 * @param $request
	 */
	public function sendEmail ($params, $template = 'email_send_message', $request)
	{
		$data['template'] = $this->getTemplateAndPatterns($template);

		$content = $data['template']->content;

		$result = [
			'content' => $this->replaceWithData($params, $content)
		];

		$receiver = $request['receiver'];

		$sender = $request['sender'] ;

		$url = $params['{WEB_LINK}'];

		$this->mail($sender, $receiver, $result, $url);
	}

    /**
     * Add where you want to add button
     * Specifically for url
     * @return array
     */
    public function btnPatternArray()
    {
        return [
            '{USER_ACTIVATION_LINK}'
        ];
	}

    /**
     * Add button html if it's a button
     * @param $key
     * @param $value
     * @return string
     */
    public function addBtnToUrl($key, $value)
    {
        if (in_array($key, $this->btnPatternArray()))
        {
            return '<a href="'.$value.'" class="button button-blue" target="_blank">Click here</a>';
        }
        return $value;
    }

	/**
	 * @param $params
	 * @param $content
	 *
	 * @return mixed
	 */
	public function replaceWithData($params, $content)
	{
		$patterns     = [];
		$replacements = [];

		foreach ($params as $key =>  $value) {
			$patterns[]     = $key;
			$replacements[] = $this->addBtnToUrl($key, $value);
		}
		return $this->stripTags( preg_replace($patterns, $replacements, $content) );
	}

	/**
	 * @param $content
	 *
	 * @return mixed
	 */
	public function stripTags($content)
	{
		return str_replace(['{', '}'],'', $content);
	}

	/**
	 * @param $sender
	 * @param $receiver
	 * @param $content
	 *
	 */
	public function mail($sender, $receiver, $content, $url)
	{
		$data = [];

		$data = [
			'sender'    => $sender,
			'receiver'  => $receiver,
			'content'   => $content,
			'url'       => $url
		];

		Mail::to($receiver)->send(new \App\Mail\EmailTemplate($data));

	}

}