<?php

namespace App\Evemoo\Services\Log;

interface LogInterface {

    /**
     * Get All Log
     * @return mixed
     */
    public function getAllLogWithPaginate();

    /**
     * @param $log
     * @param $logDetails
     * @return mixed
     */
    public function update($log, $logDetails );



}