<?php

namespace App\Evemoo\Services\Log;

use App\Evemoo\Models\Log;

class LogService implements LogInterface
{

    /**
     * @var Log
     */
    private $log;

    /**
     * LogService constructor.
     * @param Log $log
     */
    public function __construct(
        Log $log
    )
    {
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function getAllLogWithPaginate()
    {
       $log = $this->log->latest()->paginate(10);

       return $log;
    }

    /**
     * @param $log
     * @param $logDetails
     * @return mixed
     */
    public function update($log, $logDetails)
    {
        $log->error_process_status = $logDetails['status'];
        $log->save();

        return $log;
    }
}