<?php

namespace App\Evemoo\Payment\Services\Stripe;

use App\Evemoo\Payment\Traits\PaymentTrait;
use Stripe\Charge;
use Stripe\Stripe;

class StripeService
{
    use PaymentTrait;

    public function payWithStripe($request)
    {
        $configs = $this->configs();

        Stripe::setApiKey($configs['stripe_secret']);

        try {
            Charge::create([
                "amount" => 'price in integer',
                "currency" => 'usd',
                "source" => $request->get('stripeToken'),
                "description" => "Test Charge"
            ]);
        } catch(\Exception $e) {
            return redirect()->route('home')->with('error', $e->getMessage());
        }
        
        $storeOrders = $this->storeOrder($request);

        if($storeOrders)
            return $storeOrders;
        else
            return false;
    }
}