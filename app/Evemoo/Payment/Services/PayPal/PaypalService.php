<?php

namespace App\Evemoo\Payment\Services\PayPal;

use App\Evemoo\Payment\Traits\PaymentTrait;
use Session;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaypalService
{
    use PaymentTrait;

    private $_api_context;

    public function setupApiContext() {
        $paypal_conf = $this->configs();
        $api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['paypal_client_id'], $paypal_conf['paypal_secret']));
        $api_context->setConfig($paypal_conf['settings']);

        return $api_context;
    }

    /**
     * Store a details of payment with paypal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function payWithPaypal(Request $request)
    {
        $this->_api_context = $this->setupApiContext();

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item = new Item();
        $item->setName('product name')
            ->setCurrency('USD')
            ->setQuantity(5)
            ->setPrice(10);

        $item_list = new ItemList();
        $item_list->setItems($item);

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal(100);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Check Your transaction details!');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status')) /** Specify return URL **/
            ->setCancelUrl(URL::route('payment.status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::flash('error','Connection timeout');
                return Redirect::route('home');
                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                /** $err_data = json_decode($ex->getData(), true); **/
                /** exit; **/
            } else {
                \Session::flash('error','Some error occur, sorry for inconvenient');
                return Redirect::route('home');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::flash('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }

        Session::flash('error','Unknown error occurred');

        return Redirect::route('home');
    }

    public function getPaymentStatus()
    {
        $this->_api_context = $this->setupApiContext();

        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');

        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::flash('error','Payment failed');
            return Redirect::route('addmoney.paywithpaypal');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/

        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/

        if ($result->getState() == 'approved') {
            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/

            $storeOrders = $this->storeOrder($request);

            Session::flash('success','Payment success');

            return Redirect::route('home');
        }

        Session::flash('error','Payment failed');

        return Redirect::route('home');
    }
}