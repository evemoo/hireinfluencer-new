<?php namespace App\Evemoo\Repositories;

use App\Evemoo\Repositories\src\Traits\WhereCallBackCriteriaPusher;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Bosnadev\Repositories\Criteria\Criteria;
use Illuminate\Container\Container as App;
use Illuminate\Support\Collection;

class BaseRepository extends Repository {

    use WhereCallBackCriteriaPusher;

    protected $reset_criteria = false;
    protected $modelUse;

    public function __construct(App $app, Collection $collection)
    {
        $this->modelUse = collect([]);
        parent::__construct($app, $collection);
    }

    public function model(){}

    /**
     * Resets current model instance to new instance
     *
     * @return mixed
     */
    public function resetModel()
    {
        $this->makeModel();
    }

    /**
     * Gets first row from the queried data set
     *
     * @param array $columns
     * @return mixed
     */
    public function first($columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->first($columns);
    }

    /**
     * Gets all rows from the queried data set
     *
     * @param array $columns
     * @return mixed
     */
    public function get($columns = array('*'))
    {
        $this->applyCriteria();

        return $this->model->get($columns);
    }

    /**
     * get only two columns from a row from the queried data set
     *
     * @param array $columns
     * @return mixed
     */
    public function pluck($value, $key)
    {
        $this->applyCriteria();

        return $this->model->pluck($value, $key);
    }

    /**
     * Update Language contents
     *
     * @param array $data
     * @return bool
     */
    public function updateLang(array $data)
    {
        $this->applyCriteria();

        if (!($model = $this->model->first())) {
            return false;
        }

        return $model->fill($data)->save();
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function findOrFail($id, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->findOrFail($id, $columns);
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function firstOrFail($columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->firstOrFail($columns);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function count()
    {
        $this->applyCriteria();
        return $this->model->count();
    }

    /**
     * @param Criteria $criteria
     * @return $this
     */
    public function pushCriteria(Criteria $criteria)
    {
        /**
         * Overwriting criteria set to false.
         * So, same criteria can be used multiple times to make an query
         */
        $this->preventCriteriaOverwriting = false;

        if ($this->preventCriteriaOverwriting) {
            // Find existing criteria
            $key = $this->criteria->search(function ($item) use ($criteria) {
                return (is_object($item) && (get_class($item) == get_class($criteria)));
            });

            // Remove old criteria
            if (is_int($key)) {
                $this->criteria->offsetUnset($key);
            }
        }

        $this->validateModel();
        $this->criteria->push($criteria);
        return $this;
    }

    /**
     * @return $this
     */
    public function applyCriteria()
    {
        if ($this->skipCriteria === true)
            return $this;

        foreach ($this->getCriteria() as $criteria) {
            if ($criteria instanceof Criteria)
                $this->model = $criteria->apply($this->model, $this);
        }

        $this->modelUse->push(get_class($this->model));

        $this->resetCriteria();
        return $this;
    }

    /**
     * Add empty collection to criteria
     */
    public function resetCriteria()
    {
        $this->criteria = collect([]);

        return $this;
    }

    /**
     * Resets model after every query is execuited,
     * so, old criterias and query builder won't duplicate
     */
    public function validateModel()
    {
        $key = $this->modelUse->search(function ($item) {
            return ($item == get_class($this->model));
        });

        if (is_int($key)) {
            $this->modelUse->offsetUnset($key);
            $this->resetModel();
        }
    }
    
}
