<?php namespace App\Evemoo\Repositories\Common;

use App\Evemoo\Models\Message;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use App\Evemoo\Repositories\BaseRepository;

class MessageRepository extends BaseRepository {

    public function model()
    {
        return Message::class;
    }

}
