<?php namespace App\Evemoo\Repositories\Acl;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class PermissionRepository extends Repository {

    public function model()
    {
        return 'App\Evemoo\Models\Permission';
    }

}
