<?php namespace App\Evemoo\Repositories\Acl;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class UserRepository extends Repository {

    public function model()
    {
        return 'App\User';
    }

}
