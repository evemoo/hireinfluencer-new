<?php namespace App\Evemoo\Repositories\Acl;

use App\Evemoo\Models\RoleUser;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class RoleUserRepository extends BaseRepository {

    public function model()
    {
        return RoleUser::class;
    }

}
