<?php namespace App\Evemoo\Repositories\Acl;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use App\Evemoo\Repositories\BaseRepository;

class RoleRepository extends BaseRepository {

    public function model()
    {
        return 'App\Evemoo\Models\Role';
    }

}
