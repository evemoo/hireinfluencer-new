<?php namespace App\Evemoo\Repositories\Acl;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class RouteRepository extends Repository {

    public function model()
    {
        return 'App\Evemoo\Models\Route';
    }

}
