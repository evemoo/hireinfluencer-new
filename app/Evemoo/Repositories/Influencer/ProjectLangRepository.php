<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\ProjectTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class ProjectLangRepository extends BaseRepository {

    public function model()
    {
        return ProjectTranslation::class;
    }

}
