<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\PackageExtraWork;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class PackageExtraWorkRepository extends Repository {

    public function model()
    {
        return PackageExtraWork::class;
    }

}
