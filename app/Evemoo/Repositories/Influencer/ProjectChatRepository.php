<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\ProjectChat;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use App\Evemoo\Repositories\BaseRepository;

class ProjectChatRepository extends BaseRepository {

    public function model()
    {
        return ProjectChat::class;
    }

}
