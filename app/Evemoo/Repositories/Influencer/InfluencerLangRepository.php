<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\InfluencerTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class InfluencerLangRepository extends BaseRepository {

    public function model()
    {
        return InfluencerTranslation::class;
    }

}
