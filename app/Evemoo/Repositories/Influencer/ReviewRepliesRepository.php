<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\ReviewReplies;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class ReviewRepliesRepository extends Repository {

    public function model()
    {
        return ReviewReplies::class;
    }

}
