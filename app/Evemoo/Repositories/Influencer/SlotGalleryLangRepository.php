<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\SlotGalleryTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class SlotGalleryLangRepository extends BaseRepository {

    public function model()
    {
        return SlotGalleryTranslation::class;
    }

}
