<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\PackageTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class PackageLangRepository extends BaseRepository {

    public function model()
    {
        return PackageTranslation::class;
    }

}
