<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\InfluencerSocialData;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class InfluencerSocialDataRepository extends BaseRepository {

    public function model()
    {
        return InfluencerSocialData::class;
    }

}
