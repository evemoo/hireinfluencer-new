<?php namespace App\Evemoo\Repositories\Influencer;

use App\User;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class UserRepository extends BaseRepository {

    public function model()
    {
        return User::class;
    }

}
