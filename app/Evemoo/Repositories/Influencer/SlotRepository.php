<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\Slot;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class SlotRepository extends BaseRepository {

    public function model()
    {
        return Slot::class;
    }

}
