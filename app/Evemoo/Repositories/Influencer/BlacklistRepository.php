<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\Blacklist;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use App\Evemoo\Repositories\BaseRepository;

class BlacklistRepository extends BaseRepository {

    public function model()
    {
        return Blacklist::class;
    }

}
