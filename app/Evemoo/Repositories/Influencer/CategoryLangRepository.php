<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\CategoryTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class CategoryLangRepository extends BaseRepository {

    public function model()
    {
        return CategoryTranslation::class;
    }

}
