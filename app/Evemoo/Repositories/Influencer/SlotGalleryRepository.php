<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\SlotGallery;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use App\Evemoo\Repositories\BaseRepository;

class SlotGalleryRepository extends BaseRepository {

    public function model()
    {
        return SlotGallery::class;
    }

}
