<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\Influencer;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use App\Evemoo\Repositories\BaseRepository;

class InfluencerRepository extends BaseRepository {

    public function model()
    {
        return Influencer::class;
    }

}
