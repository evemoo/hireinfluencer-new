<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\SlotTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class SlotLangRepository extends BaseRepository {

    public function model()
    {
        return SlotTranslation::class;
    }

}
