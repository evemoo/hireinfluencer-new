<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\SocialMedia;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class SocialMediaRepository extends BaseRepository {

    public function model()
    {
        return SocialMedia::class;
    }

}
