<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\SocialMediaAttribute;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class SocialMediaAttributeRepository extends BaseRepository {

    public function model()
    {
        return SocialMediaAttribute::class;
    }

}
