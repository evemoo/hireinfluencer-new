<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\SlotCategory;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class SlotCategoryRepository extends Repository {

    public function model()
    {
        return SlotCategory::class;
    }

}
