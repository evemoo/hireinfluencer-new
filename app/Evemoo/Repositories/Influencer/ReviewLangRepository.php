<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\ReviewTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class ReviewLangRepository extends BaseRepository {

    public function model()
    {
        return ReviewTranslation::class;
    }

}
