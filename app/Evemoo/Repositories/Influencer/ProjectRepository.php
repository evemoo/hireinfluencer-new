<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\Project;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class ProjectRepository extends BaseRepository {

    public function model()
    {
        return Project::class;
    }

}
