<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\PackageExtraWorkTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class PackageExtraWorkLangRepository extends BaseRepository {

    public function model()
    {
        return PackageExtraWorkTranslation::class;
    }

}
