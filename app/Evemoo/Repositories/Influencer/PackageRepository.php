<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\Package;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use App\Evemoo\Repositories\BaseRepository;

class PackageRepository extends BaseRepository {

    public function model()
    {
        return Package::class;
    }

}
