<?php namespace App\Evemoo\Repositories\Influencer\Criteria\User;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;

class UserByName extends Criteria {
    /**
     * @var Category Title
     */
    private $name;

    /**
     * CategoryMatchingTitle constructor.
     * @param $name Category Title
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        return $model->where('users.first_name', 'like', '%'.$this->name.'%')->orWhere('middle_name', 'like', '%'.$this->name.'%')->orWhere('last_name', 'like', '%'.$this->name.'%');
    }

}
