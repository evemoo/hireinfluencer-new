<?php namespace App\Evemoo\Repositories\Influencer\Criteria\User;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;

class UserByContact extends Criteria {
    /**
     * @var Category Title
     */
    private $contact;

    /**
     * CategoryMatchingTitle constructor.
     * @param $contact Category Title
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        return $model->where('users.primary_contact', 'LIKE', '%'.$this->contact.'%')->orWhere('users.secondary_contact', 'LIKE', '%'.$this->contact.'%');
    }

}
