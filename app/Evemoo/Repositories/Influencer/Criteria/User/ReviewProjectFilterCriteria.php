<?php namespace App\Evemoo\Repositories\Influencer\Criteria\User;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Faker\Provider\Base;
use App\Evemoo\Repositories\Influencer\Criteria\Category\CategoryById;
use Evemoo\Repositories\Criteria\WhereCriteria;
use App\Evemoo\Models\Project;

class ReviewProjectFilterCriteria extends Criteria {

    private $project_slug;

    private $baseRepo;

    public function __construct($project_slug)
    {
        $this->project_slug = $project_slug;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        $this->pushWhereCallBackCriteria($model, $repository);

        return $model->where(function ($query) use ($repository){
            $repository->applyWhereCallBackCriteria($query);
        });

        $repository->resetWhereCallBackProp();
    }


    /**
     * Push all where callback criteria to collection instance
     *
     * @param $model
     * @param $repository
     */
    protected function pushWhereCallBackCriteria($model, $repository)
    {
        if ($this->project_slug != null)
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new Project, 'slug', $this->project_slug));
        }
    }

}
