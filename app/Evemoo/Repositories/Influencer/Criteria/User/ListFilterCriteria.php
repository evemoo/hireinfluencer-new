<?php namespace App\Evemoo\Repositories\Influencer\Criteria\User;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Faker\Provider\Base;
use App\Evemoo\Repositories\Influencer\Criteria\Category\CategoryById;
use Evemoo\Repositories\Criteria\WhereCriteria;
use App\Evemoo\Models\SocialMedia;
use App\User;
use App\Evemoo\Models\Category;

class ListFilterCriteria extends Criteria {

    private $request;

    private $baseRepo;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        $this->pushWhereCallBackCriteria($model, $repository);

        return $model->where(function ($query) use ($repository){
            $repository->applyWhereCallBackCriteria($query);
        });

        $repository->resetWhereCallBackProp();
    }


    /**
     * Push all where callback criteria to collection instance
     *
     * @param $model
     * @param $repository
     */
    protected function pushWhereCallBackCriteria($model, $repository)
    {
        if ($this->request->has('full_name') && $this->request->full_name != null)
        {
            $names = explode(" ", $this->request->get('full_name'));
            foreach ($names as $key => $name) {
                $repository->pushWhereCallBackCriteria(new UserByName($name));
            }
        }

        if ($this->request->has('email') && $this->request->email != null)
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new User, 'email', '%'.$this->request->get('email').'%', 'like'));
        }

        if ($this->request->has('category') && $this->request->category != null)
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new Category, 'id', $this->request->get('category')));
        }

        if ($this->request->has('social_media') && $this->request->social_media != null)
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new SocialMedia, 'id', $this->request->get('social_media')));
        }

        if ($this->request->has('gender') && $this->request->gender != null)
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new User, 'gender', $this->request->get('gender')));
        }

        if ($this->request->has('dob') && $this->request->dob != null)
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new User, 'birth_date', $this->request->get('dob')));
        }

        if ($this->request->has('contact') && $this->request->contact != null)
        {
            $repository->pushWhereCallBackCriteria(new UserByContact($this->request->get('title')));
        }

        if ($this->request->has('keyword') && $this->request->keyword != null)
        {
            $repository->pushWhereCallBackCriteria(new UserByKeyWord($this->request->get('title')));
        }

        if ($this->request->has('status') && $this->request->get('status') != "all")
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new User, 'status', $this->request->get('status')));
        }


    }

}
