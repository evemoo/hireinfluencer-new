<?php namespace App\Evemoo\Repositories\Influencer\Criteria\User;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;

class UserByKeyWord extends Criteria {
    /**
     * @var Category Title
     */
    private $keyword;

    /**
     * CategoryMatchingTitle constructor.
     * @param $keyword Category Title
     */
    public function __construct($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        return $model->where('influencer_social_datas.name', 'like', '%'.$this->keyword.'%')->orWhere('slot_translations.title', 'like', '%'.$this->keyword.'%')->orWhere('package_translations.title', 'like', '%'.$this->keyword.'%')->orWhere('project_translations.title', 'like', '%'.$this->keyword.'%');
    }

}
