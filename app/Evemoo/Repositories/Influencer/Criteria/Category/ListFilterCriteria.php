<?php namespace App\Evemoo\Repositories\Influencer\Criteria\Category;

use App\Evemoo\Models\Category;
use App\Evemoo\Models\CategoryTranslation;
use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Evemoo\Repositories\Criteria\WhereCriteria;
use Faker\Provider\Base;

class ListFilterCriteria extends Criteria {

    private $attributes;

    private $baseRepo;

    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        $this->pushWhereCallBackCriteria($model, $repository);

        return $model->where(function ($query) use ($repository){
            $repository->applyWhereCallBackCriteria($query);
        });

        $repository->resetWhereCallBackProp();
    }


    /**
     * Push all where callback criteria to collection instance
     *
     * @param $model
     * @param $repository
     */
    protected function pushWhereCallBackCriteria($model, $repository)
    {
        $repository->pushWhereCallBackCriteria(new WhereCriteria(new Category, 'parent_id', $this->attributes['cat_id'] != null?:0));

        if ($this->attributes['request']->has('id'))
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new Category, 'id', $this->attributes['request']->get('id')));
        }

        if ($this->attributes['request']->has('title'))
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new CategoryTranslation, 'title', '%'.$this->attributes['request']->get('title').'%', 'like'));
        }

        if ($this->attributes['request']->has('slug'))
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new Category, 'slug', '%'.$this->attributes['request']->get('slug'). '%', 'like'));
        }

        if ($this->attributes['request']->has('status') && $this->attributes['request']->get('status') != "all")
        {
            $repository->pushWhereCallBackCriteria(new WhereCriteria(new Category, 'status', $this->attributes['request']->get('status')));
        }


    }

}
