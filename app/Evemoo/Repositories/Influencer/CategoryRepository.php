<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\Category;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class CategoryRepository extends BaseRepository {

    public function model()
    {
        return Category::class;
    }

}
