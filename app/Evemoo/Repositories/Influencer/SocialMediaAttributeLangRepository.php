<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\SocialMediaAttributeTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class SocialMediaAttributeLangRepository extends BaseRepository {

    public function model()
    {
        return SocialMediaAttributeTranslation::class;
    }

}
