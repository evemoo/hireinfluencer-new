<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\Review;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use App\Evemoo\Repositories\BaseRepository;

class ReviewRepository extends BaseRepository {

    public function model()
    {
        return Review::class;
    }

}
