<?php namespace App\Evemoo\Repositories\Influencer;

use App\Evemoo\Models\SocialMediaTranslation;
use App\Evemoo\Repositories\BaseRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class SocialMediaLangRepository extends BaseRepository {

    public function model()
    {
        return SocialMediaTranslation::class;
    }

}
