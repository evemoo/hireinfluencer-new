<?php namespace Evemoo\Repositories\Criteria;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;

class LeftJoinCriteria extends Criteria {

    protected $model;
    protected $model_alias;
    protected $related_model;
    protected $related_model_alias;
    protected $model_column;
    protected $related_model_column;

    /**
     * InnerJoinCriteria constructor.
     * @param $model Model to be joined
     * @param $related_model Model to be joined with
     * @param string $model_column
     * @param string $related_model_column
     * @param string $model_alias alias for table name
     * @param string $related_model_alias alias for the related table name
     */
    public function __construct(
        $model,
        $related_model,
        $model_column,
        $related_model_column,
        $model_alias = '',
        $related_model_alias = ''
    )
    {
        $this->model = $model;
        $this->related_model = $related_model;
        $this->model_column = $model_column;
        $this->related_model_column = $related_model_column;
        $this->model_alias = $model_alias;
        $this->related_model_alias = $related_model_alias;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        return $model->leftJoin(
            join('', [
                $this->model->getTable(),
                $this->model_alias !== ''?' as '.$this->model_alias:''
            ]),
            $this->model_alias !== ''?$this->model_alias . '.' . $this->model_column:$this->model->getTable().'.'.$this->model_column,
            '=',
            $this->related_model_alias !== ''?$this->related_model_alias . '.' . $this->related_model_column:$this->related_model->getTable().'.'.$this->related_model_column
        );
    }

}
