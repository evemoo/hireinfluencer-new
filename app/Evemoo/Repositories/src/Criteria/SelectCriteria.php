<?php namespace Evemoo\Repositories\Criteria;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;

class SelectCriteria extends Criteria {

    private $columns;

    public function __construct(array $columns)
    {
        $this->columns = $columns;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        return $model->select($this->columns);
    }

}
