<?php namespace Evemoo\Repositories\Criteria;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;

class WhereCriteria extends Criteria {

    protected $model;
    protected $column;
    protected $operation;
    protected $value;
    protected $model_alias;

    /**
     * WhereCriteria constructor.
     * @param $model
     * @param $column column to be searched on
     * @param $value value to be searched
     * @param string $operation operation to be performed
     * @param string $model_alias overwrite table name by alias
     */
    public function __construct($model, $column, $value, $operation = '=', $model_alias = '')
    {
        $this->model = $model;
        $this->column = $column;
        $this->value = $value;
        $this->operation = $operation;
        $this->model_alias = $model_alias;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        return $model->where(
            $this->model_alias !== ''?:$this->model->getTable().'.'.$this->column,
            $this->operation,
            $this->value
        );
    }

}
