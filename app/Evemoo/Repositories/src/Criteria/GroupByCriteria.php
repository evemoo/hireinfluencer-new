<?php namespace Evemoo\Repositories\Criteria;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;

class GroupByCriteria extends Criteria {

    protected $model;
    protected $column;
    protected $model_alias;

    /**
     * GroupByCriteria constructor.
     * @param $model
     * @param $column
     * @param string $model_alias overites table name if passed
     */
    public function __construct($model, $column, $model_alias = '')
    {
        $this->model = $model;
        $this->column = $column;
        $this->model_alias = $model_alias;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        return $model->groupBy(
            $this->model_alias !== ''?:$this->model->getTable().'.'.$this->column
        );
    }

}
