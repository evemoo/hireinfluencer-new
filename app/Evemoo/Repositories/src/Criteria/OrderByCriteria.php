<?php namespace Evemoo\Repositories\Criteria;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;

class OrderByCriteria extends Criteria {

    protected $model;
    protected $column;
    protected $sort;
    protected $model_alias;

    /**
     * OrderByCriteria constructor.
     * @param $model
     * @param $column
     * @param string $sort
     * @param string $model_alias
     */
    public function __construct($model, $column, $sort = 'asc', $model_alias = '')
    {
        $this->model = $model;
        $this->column = $column;
        $this->sort = $sort;
        $this->model_alias = $model_alias;
    }

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        return $model->orderBy(
            $this->model_alias !== ''?:$this->model->getTable().'.'.$this->column,
            $this->sort
        );
    }

}
