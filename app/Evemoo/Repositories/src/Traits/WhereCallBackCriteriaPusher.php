<?php
namespace App\Evemoo\Repositories\src\Traits;

use Bosnadev\Repositories\Criteria\Criteria;
use Illuminate\Support\Collection;

trait WhereCallBackCriteriaPusher
{
    private $whereCallBacks;

    /**
     * Initializes Collection instance and push criteria to it
     *
     * @param Criteria $criteria
     */
    public function pushWhereCallBackCriteria(Criteria $criteria)
    {
        $this->initWhereCallBackCallBackProp();
        $this->whereCallBacks->push($criteria);
    }

    /**
     * Applies criteria (conditions) to passwed query builder ($query)
     *
     * @param $query
     */
    public function applyWhereCallBackCriteria($query)
    {
        if($this->getWhereCallBackCriterias())
        {
            foreach ($this->getWhereCallBackCriterias() as $criteria) {
                if ($criteria instanceof Criteria)
                    $criteria->apply($query, $this);
            }
        }

    }

    /**
     * @return mixed
     */
    public function getWhereCallBackCriterias()
    {
        return $this->whereCallBacks;
    }

    protected function initWhereCallBackCallBackProp()
    {
        if (!$this->whereCallBacks)
            $this->whereCallBacks = new Collection();
    }

    public function resetWhereCallBackProp()
    {
        $this->whereCallBacks = '';
    }

}