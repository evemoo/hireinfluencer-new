**Below is a demo Criteria Class implementing WhereCallBackCriteriaPusher functions:**


> NOTE: Developer need to first know how to create criteria class

> Ref. for repository package: https://github.com/bosnadev/repository

```php
class ListFilterCriteria extends Criteria {
  
    private $attributes;
    
    private $baseRepo;
    
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }
    
    
    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply( $model, Repository $repository )
    {
        $this->pushWhereCallBackCriteria($model, $repository);
        
        return $model->where(function ($query) use ($repository){
            $repository->applyWhereCallBackCriteria($query);
        });
        
        $repository->resetWhereCallBackProp();
    }
    
    /**
     * Push all where callback criteria to collection instance
     *
     * @param $model
     * @param $repository
     */
    protected function pushWhereCallBackCriteria($model, $repository)
    {
        $repository->pushWhereCallBackCriteria(new CategoriesByParentId($this->attributes['cat_id'] != null?$this->attributes['cat_id']:0));

    }

}
```

