<?php

namespace App\Evemoo\Social\Services\YouTube;

use App\Evemoo\Social\Services\SocialBaseInterface;

interface YouTubeInterface extends SocialBaseInterface
{

    /**
     * @param $video_id
     * @return mixed
     */
    public function getVideoStatistics($video_id );

}