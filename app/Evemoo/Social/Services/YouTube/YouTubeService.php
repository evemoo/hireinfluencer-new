<?php

namespace App\Evemoo\Social\Services\YouTube;

use App\Evemoo\Social\Services\SocialBaseService;

class YouTubeService extends SocialBaseService implements YouTubeInterface
{

    /**
     * @param $query
     * @return array
     */
    public function searchParams( $query )
    {

        $data = [];

        $data['endpoint'] = $this->getValue('youtube', 'YOUTUBE_ENDPOINT');

        $data['params']   = [
            'key'         => $this->getValue('youtube', 'YOUTUBE_DEVELOPER_KEY'),
            'q'           => $query,
            'order'       => $this->getValue('youtube', 'YOUTUBE_ORDERBY', 'VIEWCOUNT'),
            'part'        => $this->getValue('youtube', 'YOUTUBE_PART', 'SNIPPET'),
            'type'        => $this->getValue('youtube', 'YOUTUBE_SEARCH_TYPE', 'CHANNEL'),
            'maxResults'  => $this->getValue('youtube', 'YOUTUBE_RESULT_LIMIT')
        ];

        return $data;

    }

    /**
     * @param $profile
     * @return array
     */
    public function profileParams( $profile )
    {
        $data = [];

        $data['endpoint'] = $this->getValue('youtube', 'YOUTUBE_PROFILE_ENDPOINT');

        $data['params']   = [
            'key'         => $this->getValue('youtube', 'YOUTUBE_DEVELOPER_KEY'),
            'id'          => $profile,
            'part'        => $this->getAllValue('youtube', 'YOUTUBE_PART'),
        ];

       return $data;
    }

    /**
     * @param $video_id
     * @return array
     */
    public function getVideoStatistics($video_id )
    {
        $data = [];

        $data['endpoint'] = $this->getValue('youtube', 'YOUTUBE_VIDEO_ENDPOINT');

        $data['params']   = [
            'key'         => $this->getValue('youtube', 'YOUTUBE_DEVELOPER_KEY'),
            'id'          => $video_id,
            'part'        => $this->getAllValue('youtube', 'YOUTUBE_PART'),
        ];

        return $data;
    }

}