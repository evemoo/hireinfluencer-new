<?php

namespace App\Evemoo\Social\Services\Facebook;

use App\Evemoo\Social\Services\SocialBaseService;

class FacebookService extends SocialBaseService implements FacebookInterface
{

    /**
     * @param $query
     * @return array
     */
    public function searchParams( $query )
    {

        $data = [];

        $data['endpoint'] = $this->getUserEndpoint('facebook', 'FACEBOOK_ENDPOINT', $query);

        $data['params']   = [
            'access_token'      => $this->getValue('facebook', 'FACEBOOK_ACCESS_TOKEN'),
            'fields'            => 'id,fan_count,name'
        ];

        return $data;

    }

    /**
     * @param $profile
     * @return array
     */
    public function profileParams( $profile )
    {
        $data = [];

        $data['endpoint'] = $this->getUserEndpoint('facebook', 'FACEBOOK_PROFILE_ENDPOINT', $profile);

        $data['params']   = [
            'access_token'      => $this->getValue('facebook', 'FACEBOOK_ACCESS_TOKEN'),
            'fields'            => $this->getValue('facebook', 'FACEBOOK_FIELDS'),
            'pretty'            => $this->getValue('facebook', 'FACEBOOK_PRETTY'),
            'summary'           => $this->getValue('facebook', 'FACEBOOK_SUMMARY')
        ];

        return $data;
    }

    /**
     * @param $page_id
     * @return array
     */
    public function getPageRating ( $page_id )
    {
        $data = [];

        $data['endpoint'] = $this->getUserEndpoint('facebook', 'FACEBOOK_PAGE_RATING', $page_id);

        $data['params']   = [
            'access_token'      => $this->getValue('facebook', 'FACEBOOK_ACCESS_TOKEN'),
            'fields'            => $this->getValue('facebook', 'FACEBOOK_FIELDS'),
            'pretty'            => $this->getValue('facebook', 'FACEBOOK_PRETTY'),
            'summary'           => $this->getValue('facebook', 'FACEBOOK_SUMMARY')
        ];

        return $data;
    }

    /**
     * Get single post like,share,comment count
     * @param $post_id
     * @return array
     */
    public function getPostStatistics($post_id)
    {
        $data = [];

        $data['endpoint'] = $this->getUserEndpoint('facebook', 'FACEBOOK_POST_ENDPOINT', $post_id);

        $data['params']   = [
            'access_token'      => $this->getValue('facebook', 'FACEBOOK_ACCESS_TOKEN'),
            'fields'            => $this->getValue('facebook', 'FACEBOOK_FIELDS'),
            'pretty'            => $this->getValue('facebook', 'FACEBOOK_PRETTY'),
            'summary'           => $this->getValue('facebook', 'FACEBOOK_SUMMARY')
        ];

        return $data;
    }

}
