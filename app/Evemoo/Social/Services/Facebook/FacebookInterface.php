<?php

namespace App\Evemoo\Social\Services\Facebook;

use App\Evemoo\Social\Services\SocialBaseInterface;

interface FacebookInterface extends SocialBaseInterface
{

    /**
     * @param $page_id
     * @return mixed
     */
    public function getPageRating($page_id);

    /**
     * @param $post_id
     * @return mixed
     */
    public function getPostStatistics($post_id);

}