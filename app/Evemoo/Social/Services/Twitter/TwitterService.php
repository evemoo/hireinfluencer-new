<?php

namespace App\Evemoo\Social\Services\Twitter;

use App\Evemoo\Social\Services\SocialBaseService;

class TwitterService extends SocialBaseService
{

    /**
     * @return tmhOAuth
     */
    public function connection()
    {
        return $connection = new tmhOAuth([
            'consumer_key'      => $this->getValue('twitter', 'TWITTER_CONSUMER_KEY'),
            'consumer_secret'   => $this->getValue('twitter', 'TWITTER_CONSUMER_SECRET'),
            'user_token'        => $this->getValue('twitter', 'TWITTER_ACCESS_TOKEN_KEY'),
            'user_secret'       => $this->getValue('twitter', 'TWITTER_ACCESS_TOKEN_SECRET')
        ]);
    }

    /**
     * @param $profile
     * @return array|string
     */
    public function getStatistics($profile )
    {

        $data = [];

        $connection = $this->connection();

        $parameters = [];

        $parameters['screen_name'] = $profile;
        $twitter_path = $this->getValue('twitter', 'TWITTER_PROFILE_ENDPOINT');

        $http_code = $connection->request('GET', $connection->url($twitter_path), $parameters );

        return $this->response($http_code, $connection, 'User');

    }

    /**
     * Search using user profile
     * @param $query
     * @return array|string
     */
    public function searchTweet($query)
    {
        $data = [];

        $connection = $this->connection();

        $parameters = [];

        $parameters['screen_name'] = $query;
        $parameters['count'] = 10;

        $twitter_path = $this->getValue('twitter', 'TWITTER_TWEET_SEARCH');

        $http_code = $connection->request('GET', $connection->url($twitter_path), $parameters );

        return $this->response($http_code, $connection, 'User');
    }

    /**
     * @param $tweet_id
     * @return array|string
     */
    public function getTweetStatistics($tweet_id)
    {
        $data = [];

        $connection = $this->connection();

        $parameters = [];

        $parameters['id'] = $tweet_id;
        $twitter_path = $this->getValue('twitter', 'TWITTER_SPECIFIC_TWEET_SEARCH');

        $http_code = $connection->request('GET', $connection->url($twitter_path), $parameters );

        return $this->response($http_code, $connection, 'Tweet');

    }

    /**
     * @param $http_code
     * @param $connection
     * @param $model
     * @return array|string
     */
    public function response($http_code, $connection, $model)
    {
        if ($http_code === 200)
        {
            $response = strip_tags($connection->response['response']);

            return $response;
        }
        else {

            $data = [
                'status'    => $http_code,
                'response'  => $connection->response['error'],
                'errors'    => $model.' not Found.'
            ];

            return $data;

        }
    }

}