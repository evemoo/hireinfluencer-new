<?php

namespace App\Evemoo\Social\Services\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\JsonResponse;

class HttpService implements HttpInterface
{

    /*
     * @var HTTP
     */
    protected $http;

    /**
     * HttpService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->http = $client;
    }

    public function getOrPost ( $url, $data = null, $method = 'get' )
    {

        if ($method == 'get')
        {
            return $this->send( $url, $data );
        }

        return $this->post( $url, $data );

    }

    /**
     * @param $url
     * @param $data
     * @return JsonResponse|mixed|string
     */
    public function send( $url, $data = null )
    {

        try {

            $result = $this->http->get($url, [
                'query' => $data
            ]);

            $response = json_decode((string)$result->getBody());

        }
        catch (ClientException $e) {

            $response = [
                'errors' => 'Client Exception.',
                'status' => $e->getCode()
            ];

        }
        catch (ConnectException $e) {

            $response = [
                'errors' => 'Internal server error.',
                'status' => $e->getCode()
            ];

        }
        catch (RequestException $e) {

            if ($e->hasResponse()) {

                $exception = (string)$e->getResponse()->getBody();

                $exception = json_decode($exception);

                return new JsonResponse($exception, $e->getCode());

            } else {

                return new JsonResponse($e->getMessage(), 503);

            }
        }

        return $response;
    }

    /**
     * @param $url
     * @param $data
     * @return JsonResponse|mixed|string
     */
    public function post( $url, $data = null )
    {

        try {

            $result = $this->http->post($url, [
                'query' => $data
            ]);

            $response = json_decode( (string) $result->getBody() );

            $response = $response->access_token;

        }
        catch (ClientException $e) {

            $response = [
                'errors' => $e->getMessage(),
                'status' => $e->getCode()
            ];

        }
        catch (ConnectException $e) {

            $response = [
                'errors' => 'Internal server error.',
                'status' => $e->getCode()
            ];

        }
        catch (RequestException $e) {

            if ($e->hasResponse()) {

                $exception = (string)$e->getResponse()->getBody();

                $exception = json_decode($exception);

                return new JsonResponse($exception, $e->getCode());

            } else {

                return new JsonResponse($e->getMessage(), 503);

            }
        }

        return $response;
    }


}