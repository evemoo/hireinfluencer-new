<?php

namespace App\Evemoo\Social\Services\Http;

interface HttpInterface
{

    /**
     * @param $url
     * @param null $data
     * @return mixed
     */
    public function send( $url, $data = null );

}