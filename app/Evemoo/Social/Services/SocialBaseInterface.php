<?php

namespace App\Evemoo\Social\Services;


interface SocialBaseInterface
{

    /**
     * @param $query
     * @return mixed
     */
    public function searchParams( $query );

    /**
     * @param $profile
     * @return mixed
     */
    public function profileParams( $profile );

}