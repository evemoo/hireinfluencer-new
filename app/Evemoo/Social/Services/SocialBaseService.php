<?php

namespace App\Evemoo\Social\Services;

class SocialBaseService
{

    /*
     * @var Configuration path
     */
    protected $config_path         = 'socialproviders.providers';

    /*
    * @var default provider
    */
    protected $default_provider = 'default';

    /*
    * @var default key
    */
    protected $default_key      = 'message';

    /**
     * @param $provider
     * @param $value
     * @param null $last
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getConfig($provider, $value, $last = null)
    {
        if (empty($last))
        {
            return config($this->config_path . '.' . $provider . '.' . $value);
        }

        return config($this->config_path . '.' . $provider . '.' . $value . '.' . $last);

    }

    /**
     * @param $provider
     * @param $value
     * @param null $last
     * @return \Illuminate\Config\Repository|mixed|string
     */
    public function getValue($provider, $value, $last = null)
    {
        $result = $this->getConfig($provider, $value, $last);

        if ($result == '') {

            return $this->errorResponse(
                $this->getConfig($this->default_provider, $this->default_key),
                $provider,
                $value);

        }

        return $result;
    }

    /**
     * @param $provider
     * @param $value
     * @param $append
     * @return string
     */
    public function getUserEndpoint($provider, $value, $append)
    {
        $endpoint = $this->getConfig($provider, $value);

        if ($endpoint == '') {

            return $this->errorResponse(
                $this->getConfig($this->default_provider, $this->default_key),
                $provider,
                $value);

        }

        return sprintf($endpoint, $append);
    }

    /**
     * @param string $separator
     * @param $provider
     * @param $value
     * @return array
     */
    public function getAllValue( $provider, $value, $separator = ',' )
    {
        $result = $this->getConfig($provider, $value);

        return implode($separator, $result);
    }


    /**
     * Error Response
     * @param $message
     * @param $provider
     * @param $value
     * @return string
     */
    public function errorResponse($message, $provider, $value)
    {
        return sprintf($message, Ucfirst($provider), $value);
    }

}