<?php

namespace App\Evemoo\Social\Services\Instagram;

use App\Evemoo\Social\Services\SocialBaseInterface;

interface InstagramInterface extends SocialBaseInterface
{

    /**
     * @param $post_id
     * @return mixed
     */
    public function getPostStatistics($post_id );

}