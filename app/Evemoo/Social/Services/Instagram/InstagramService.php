<?php

namespace App\Evemoo\Social\Services\Instagram;

use App\Evemoo\Social\Services\SocialBaseService;

class InstagramService extends SocialBaseService implements InstagramInterface
{

    /**
     * @param $query
     * @return array
     */
    public function searchParams( $query )
    {

        $data = [];

        $data['endpoint'] = $this->getValue('instagram', 'INSTAGRAM_ENDPOINT');

        $data['params']   = [
            'access_token'      => $this->getValue('instagram', 'INSTAGRAM_ACCESS_TOKEN'),
                'q'             => $query,
                'count'         => $this->getValue('instagram', 'INSTAGRAM_RESULT_LIMIT')
        ];

        return $data;

    }

    /**
     * @param $profile
     * @return array
     */
    public function profileParams( $profile )
    {
        $data = [];

        $data['endpoint'] = $this->getUserEndpoint('instagram', 'INSTAGRAM_PROFILE_ENDPOINT', $profile);

        $data['params']   = null;

        return $data;
    }

    /**
     * @param $post_id
     * @return array
     */
    public function getPostStatistics($post_id )
    {
        $data = [];

        $data['endpoint'] = $this->getUserEndpoint('instagram', 'INSTAGRAM_POST_ENDPOINT', $post_id);

        $data['params']   = [
            'access_token'      => $this->getValue('instagram', 'INSTAGRAM_ACCESS_TOKEN'),
        ];

        return $data;
    }

}