<?php

namespace App\Evemoo\Social\Traits;

trait SocialTrait
{

    /**
     * @param $query
     * @return \Illuminate\Http\JsonResponse
     */
    public function search( $query )
    {

        $data = [];

        $result = $this->service->searchParams($query);

        $data = $this->http->send(
            $result['endpoint'], $result['params']
        );

        return response()
            ->json($data);

    }

    /**
     * @param $profile
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatistics($profile)
    {

        $data = [];

        $result = $this->service->profileParams($profile);

        $data = $this->http->send(
            $result['endpoint'], $result['params']
        );

        return response()
            ->json($data);

    }

}