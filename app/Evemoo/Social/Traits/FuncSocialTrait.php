<?php

namespace App\Evemoo\Social\Traits;

trait FuncSocialTrait
{
    /**
     * @var $default_controller_path
     */
    protected $default_controller_path = 'App\Http\Controllers\Social';

    /**
     * @param $package
     * @param $query
     * @return mixed|string
     */
    public function func_youtube($package, $query)
    {
        $service = 'YoutubeController';

        switch ($package->package_type) {
            case "likes":
                return $this->getVideoStatistic($service, $query, 'likes');
                break;
            case "share":
                return 0;
                break;
            case "comment":
                return $this->getVideoStatistic($service, $query, 'comment');
                break;
            case "views":
                return $this->getVideoStatistic($service, $query, 'views');
                break;
            case "subscriber":
                return $this->getStatistic($service, $query, 'subscriber');
                break;
            default:
                return $this->errorHandler('Package Type', $package->package_type);
        }
    }

    /**
     * @param $package
     * @param $query
     * @return int|mixed|string
     */
    public function func_facebook($package, $query)
    {
        $service = 'FacebookController';

        switch ($package->package_type) {
            case "5-star-rating":
                return 0;
                break;
            case "comment":
                return $this->getPostStatistic($service, $query, 'comment');
                break;
            case "likes":
                return $this->getBasicInfo($service, $query, 'likes');
                break;
            case "post-likes":
                return $this->getPostStatistic($service, $query, 'post-likes');
                break;
            case "share":
                return $this->getPostStatistic($service, $query, 'share');
                break;
            default:
                return $this->errorHandler('Package Type', $package->package_type);
        }
    }

    /**
     * @param $package
     * @param $query
     * @return mixed|string
     */
    public function func_instagram($package, $query)
    {
        $service = 'InstagramController';

        switch ($package->package_type) {
            case "likes":
                return $this->getPostStatistic($service, $query, 'likes');
                break;
            case "share":
                return $this->getPostStatistic($service, $query, 'share');
                break;
            case "comment":
                return $this->getPostStatistic($service, $query, 'comment');
                break;
            case "followers":
                return $this->getStatistic($service, $query, 'followers');
                break;
            default:
                return $this->errorHandler('Package Type', $package->package_type);
        }
    }

    /**
     * @param $package
     * @param $query
     * @return mixed|string
     */
    public function func_twitter($package, $query)
    {
        $service = 'TwitterController';

        switch ($package->package_type) {
            case "retweets":
                return $this->getSearchTweet($service, $query, 'retweets');
                break;
            case "likes":
                return $this->getTweetById($service, $query, 'likes');
                break;
            case "followers":
                return $this->getStatistic($service, $query, 'followers');
                break;
            default:
                return $this->errorHandler('Package Type', $package->package_type);
        }
    }

    /**
     * @param $service
     * @param $profile
     * @param string $type
     * @return mixed
     */
    protected function getStatistic ($service, $profile, $type = '' )
    {
        return app($this->default_controller_path.'\\'.$service)
            ->getStatistics($profile, $type);
    }

    /**
     * Youtube Video Statistic
     * @param $service
     * @param $profile
     * @param string $type
     * @return mixed
     */
    protected function getVideoStatistic ($service, $profile, $type = '' )
    {
        return app($this->default_controller_path.'\\'.$service)
            ->getVideoStatistics($profile, $type);
    }

    /**
     * Facebook & Instagram Post Statistics
     * @param $service
     * @param $profile
     * @param string $type
     * @return mixed
     */
    protected function getPostStatistic ( $service, $profile, $type = '' )
    {
        return app($this->default_controller_path.'\\'.$service)
            ->getPostStatistics($profile, $type);
    }

    /**
     * Get facebook page id, likes, name
     * @param $service
     * @param $query
     * @param string $type
     * @return mixed
     */
    protected function getBasicInfo( $service, $query, $type = '' )
    {
        return app($this->default_controller_path.'\\'.$service)
            ->search($query, $type);
    }

    /**
     * Search tweet by user profile
     * @param $service
     * @param $query
     * @return mixed
     */
    protected function getSearchTweet($service, $query, $type = '')
    {
        return app($this->default_controller_path.'\\'.$service)
            ->searchTweet($query, $type);
    }

    /**
     * Get Tweet Statistics by Tweet Id
     * @param $service
     * @param $query
     * @return mixed
     */
    protected function getTweetById($service, $query, $type = '')
    {
        return app($this->default_controller_path.'\\'.$service)
            ->getTweet($query, $type);
    }

}