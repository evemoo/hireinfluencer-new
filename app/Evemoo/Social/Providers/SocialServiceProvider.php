<?php

namespace App\Evemoo\Social\Providers;

use Illuminate\Support\ServiceProvider;

use App\Evemoo\Social\Services\Facebook\FacebookInterface;
use App\Evemoo\Social\Services\Facebook\FacebookService;
use App\Evemoo\Social\Services\Http\HttpInterface;
use App\Evemoo\Social\Services\Http\HttpService;
use App\Evemoo\Social\Services\Instagram\InstagramInterface;
use App\Evemoo\Social\Services\Instagram\InstagramService;
use App\Evemoo\Social\Services\YouTube\YouTubeInterface;
use App\Evemoo\Social\Services\YouTube\YouTubeService;

class SocialServiceProvider extends ServiceProvider
{

    /**
     * Consist interface as key and implementation as value.
     *
     * @var array
     */
    protected $bindings = [

        HttpInterface::class        => HttpService::class,
        InstagramInterface::class   => InstagramService::class,
        FacebookInterface::class    => FacebookService::class,
        YouTubeInterface::class     => YouTubeService::class

    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application social services.
     *
     * @return void
     */
    public function register()
    {
        foreach ( $this->bindings as $interface => $implementation ) {
            $this->app->bind($interface, $implementation);
        }

    }

}