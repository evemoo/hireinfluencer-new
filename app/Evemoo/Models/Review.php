<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	protected $fillable = ['project_id', 'reviewer_id', 'rating', 'published'];

    public function reviewer()
    {
    	return $this->belongsTo(\App\User::class, 'reviewer_id');
    }

    public function trans()
    {
    	return $this->hasMany(ReviewTranslation::class, 'entry_id');
    }

    public function reply()
    {
    	return $this->hasOne(ReviewReply::class, 'review_id');
    }
}
