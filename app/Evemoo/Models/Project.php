<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['slot_id', 'slug', 'budget', 'final_deal_price', 'status'];
}
