<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    protected $fillable = ['influencer_social_data_id', 'influencer_id', 'banner', 'status', 'can_deliver_within', 'average_rating', 'total_reviews', 'category_id'];

    public function finance()
    {
    	return $this->has(SlotFinancial::class, 'slot_id');
    }

    public function gallery()
    {
    	return $this->hasMany(SlotGallery::class, 'slot_id');
    }

    public function category()
    {
    	return $this->belongsTo(SlotCagtegory::class, 'category_id');
    }

    public function projects()
    {
    	return $this->hasMany(Projects::class, 'slot_id');
    }

    public function socialMedia()
    {
    	return $this->belongsTo(SocialMedia::class, 'social_media_id');
    }

    public function influencer()
    {
    	return $this->belongsTo(Influencer::class, 'influencer_id');
    }
}
