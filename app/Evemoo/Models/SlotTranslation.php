<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class SlotTranslation extends Model
{
    protected $fillable = ['entry_id', 'code', 'title', 'description'];
}
