<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectChat extends Model
{
    protected $fillable = ['project_id', 'sender_id', 'receiver_id'];

    public function sender()
    {
    	return $this->belongsTo(\App\User::class, 'sender_id');
    }

    public function receiver()
    {
    	return $this->belongsTo(\App\User::class, 'receiver_id');
    }
}
