<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMediaAttributeTranslation extends Model
{
    protected $fillable = ['entry_id', 'code', 'name'];
}
