<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewTranslation extends Model
{
    protected $table = 'review_translations';

	protected $fillable = ['entry_id', 'code', 'comment'];
}
