<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $fillable = [
        'title', 'slug', 'content'
    ];

    public function patterns()
    {
        return $this->belongsToMany(EmailTemplatePattern::class,'pattern_template', 'template_id', 'pattern_id');
    }
}
