<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMediaAttribute extends Model
{
    protected $fillable = ['slug'];

    public function translations()
    {
    	return $this->hasMany(SocialMediaAttributeTranslation::class, 'entry_id');
    }
}
