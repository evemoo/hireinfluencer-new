<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    protected $fillable = ['user_id', 'start_date', 'end_date', 'comment'];
}
