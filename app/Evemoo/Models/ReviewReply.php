<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewReply extends Model
{
    protected $fillable = ['review_id', 'reply_comment'];
}
