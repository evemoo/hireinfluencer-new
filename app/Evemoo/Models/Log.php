<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'error_no',
        'error_code',
        'error_msg',
        'error_process_status',
        'error_level',
    ];
}

