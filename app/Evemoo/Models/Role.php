<?php namespace App\Evemoo\Models;

use App\Scopes\LeaveSomeRoles;
use Zizaco\Entrust\EntrustRole;
use Config, AclHelper, ConfigHelper;

class Role extends EntrustRole {

    /**
     * @var array
     */
    protected $fillable = ['name', 'display_name', 'description', 'resync_on_login', 'enabled', 'position'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        //static::addGlobalScope(new LeaveSomeRoles());
    }

    /**
     * Many-to-Many relations with the user model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(Config::get('auth.providers.users.model'), Config::get('entrust.role_user_table'),Config::get('entrust.role_foreign_key'),Config::get('entrust.user_foreign_key'));
        // return $this->belongsToMany(Config::get('auth.model'), Config::get('entrust.role_user_table'));
    }

    public function scopeLeaveRoutes($query)
    {
        return $query->whereNotIn('name', ConfigHelper::getConfigByKey('evemoo.excluded-roles'));
    }

    /**
     * @return bool
     */
    public function isEditable()
    {
        if (AclHelper::hasAdministrativeAccess())
            return true;
        else
            return false;

    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users roles from deletion
        if ( ('admins' == $this->name)
            || ('users' == $this->name)
        ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function canChangeStatus()
    {
        // Protect the admins and users roles from deletion
        if ( ('admins' == $this->name)
            || ('users' == $this->name)
            || ('super-admin' == $this->name)
            || ('support-admin' == $this->name)
            || ('agent' == $this->name)
            || ('seller' == $this->name)
            || ('buyer' == $this->name)
        ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function canChangePermissions()
    {
        // Protect the admins role from permissions changes
        if ('admins' == $this->name) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function canChangeMembership()
    {
        // Protect the users role from membership changes
        if ('users' == $this->name) {
            return false;
        }

        return true;
    }
    

    /**
     * @param $role
     * @return bool
     */
    public static function isForced($role)
    {
        if ('users' == $role->name) {
            return true;
        }

        return false;
    }

    public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }

        // Return true if the role has is assigned the given permission.
        if ( $this->perms()->where('id' , $perm->id)->first() ) {
            return true;
        }
        // Otherwise
        return false;
    }

    /**
     *
     * Force the role to have the given permission.
     *
     * @param $permissionName
     */
    public function forcePermission($permissionName)
    {
        // If the role has not been given a the said permission
        if (null == $this->perms()->where('name', $permissionName)->first()) {
            // Load the given permission and attach it to the role.
            $permToForce = Permission::where('name', $permissionName)->first();
            $this->perms()->attach($permToForce->id);
        }
    }



}
