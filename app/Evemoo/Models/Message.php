<?php

namespace App\Evemoo\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	protected $fillable = ['sender_id', 'receiver_id', 'message_text', 'image'];

    public function sender() {
    	return $this->belongsTo(User::class, 'sender_id');
    }
}
