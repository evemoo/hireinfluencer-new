<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplatePattern extends Model
{
    public $table = "email_template_patterns";

    protected $fillable = [
        'email_template_id', 'pattern' , 'label'
    ];

    public function templates()
    {
        return $this->belongsToMany(EmailTemplate::class, 'pattern_template', 'pattern_id', 'template_id');
    }
}
