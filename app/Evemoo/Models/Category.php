<?php

namespace App\Evemoo\Models;

use App\Evemoo\AppTrait\Lang;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Lang;
    
    protected $fillable = [
        'parent_id', 'slug', 'status'
    ];

    public function childs() {
        return $this->hasMany(Category::class,'parent_id','id') ;
    }

    public function translations() {
        return $this->hasMany(CategoryTranslation::class,'entry_id') ;
    }

    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public static function tree()
    {
        return static::with(implode('.', array_fill(0, 5, 'children')))
            ->where('parent_id', '=', 0)->get();
    }

    protected function hasChild($parent_id)
    {
        $child = Category::where('parent_id', $parent_id)->get();
        if($child->count() > 0)
            return true;
        else
            return false;
    }

    protected function hasParent($id)
    {
        $parent = Category::where('id', $id)->get();
        if($parent->count() > 0)
            return true;
        else
            return false;
    }

    public function getCountAttribute()
    {
        $show_count = $this->show_count;
        if($show_count == true)
        {
            if($this->hasChild($this->id))
            {
                //$counts = $this->advertisement->count();
                $counts = 0;
                $child = Category::where('parent_id', $this->id)->where('status', 1)->get();
                foreach($child as $ch)
                {
                    //$ads = Advertisement::where('category_id', $ch->id)->IsPublishable()->count();
                    $ads = 0;
                    $counts += $ads;
                    if($this->hasChild($ch->id))
                    {
                        $subChild = Category::where('parent_id', $ch->id)->where('status', 1)->get();
                        foreach($subChild as $subC)
                        {
                            //$subAds = Advertisement::where('category_id', $subC->id)->IsPublishable()->count();
                            $subAds = 0;
                            $counts += $subAds;
                        }
                    }
                }
                $count = " (".$counts.")";
            }                
            else
                //$count = " (".$this->advertisement->count().")";
                $count = " (0)";
        }
        else
            $count = "";
        return $count;
    }

    public function getSelected($slug, $select)
    {
        $selected = "";
        if(isset($slug) && $slug != 'all')
        {
            $current = Category::where('slug', $slug)->first();
            if($slug == $select)
                $selected = "selected";
            elseif($this->hasParent($current->parent_id))
            {
                $parent = Category::where('id', $current->parent_id)->where('status', 1)->first();

                if($this->hasParent($parent->parent_id))
                {
                    $mega_parent = Category::where('id', $parent->parent_id)->where('status', 1)->first();
                    if($mega_parent->slug == $select)
                        $selected = "selected";
                }
                elseif ($parent->slug == $select) {
                    $selected = "selected";
                }
            }
        }

        return $selected;
    }

    public function childCat()
    {
    	if($this->parent_id !=  0) {
    		return $this->parent_id;
	    }
    	return $this->id;
    }
}
