<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class InfluencerTranslation extends Model
{
    protected $fillable = ['entry_id', 'code', 'message', 'about_you', 'gender', 'country', 'state'];
}
