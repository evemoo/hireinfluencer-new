<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class SlotGalleryTranslation extends Model
{
    protected $fillable = ['entry_id', 'code', 'alt_text'];
}
