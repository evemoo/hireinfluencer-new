<?php

namespace App\Evemoo\Models;

use App\Evemoo\AppTrait\Lang;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use Lang;

    protected $table = 'system_configurations';

    protected $fillable = [
        'key',
        'value',
    ];

}
