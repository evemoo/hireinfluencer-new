<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectTranslation extends Model
{
    protected $fillable = ['entry_id', 'code', 'title', 'description'];
}
