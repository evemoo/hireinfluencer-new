<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class SlotGallery extends Model
{
	protected $table = 'slot_galleries';

    protected $fillable = ['slot_id', 'image', 'rank', 'status'];
}
