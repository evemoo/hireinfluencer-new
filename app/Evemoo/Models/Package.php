<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ['slot_id', 'delivery_in_days', 'audience_size', 'no_of_posts', 'permanent_post', 'additional_link', 'additional_link', 'default_package', 'rank'];

    public function extras()
    {
    	return $this->hasMany(PackageExtraWork::class, 'package_id');
    }
}
