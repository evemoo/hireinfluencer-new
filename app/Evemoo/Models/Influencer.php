<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class Influencer extends Model
{
    protected $fillable = ['user_id', 'influencer_username', 'registration_date', 'verified_date', 'seen_by_admin', 'average_rating', 'total_reviews', 'date_of_birth', 'status'];
}
