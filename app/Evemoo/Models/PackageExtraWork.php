<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class PackageExtraWork extends Model
{
    public function translation()
    {
    	return $this->hasOne(PackageExtraWorkTranslation::class, 'entry_id')->where('package_extra_work_translations.code', 'en');
    }
}
