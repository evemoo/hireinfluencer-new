<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class InfluencerSocialData extends Model
{
    protected $fillable = ['influencer_id', 'social_media_id', 'social_media_attribute_id', 'value', 'profile_link', 'name'];
}
