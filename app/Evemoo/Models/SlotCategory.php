<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class SlotCategory extends Model
{
	protected $table = 'slot_category';

    protected $fillable = ['category_id', 'slot_id'];
}
