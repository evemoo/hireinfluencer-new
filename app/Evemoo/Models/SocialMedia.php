<?php

namespace App\Evemoo\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    protected $fillable = ['icon', 'slug', 'image'];

    public function translations()
    {
    	return $this->hasMany(SocialMediaTranslation::class, 'entry_id');
    }
}
