<?php

namespace App\Evemoo\Helpers;

use App\Evemoo\Models\Role;
use App\User;
use Illuminate\Support\Facades\DB;

class AppHelper
{
    protected $active_role;
    protected $site_configs = null;
    protected $auth_user;
    protected $auth_user_details;

    /**
     * Generates View base path from view file path
     *
     * @param $view_path View to which value is to be assigned
     * @param bool $with_file_name Must be true if View Path is sent with file name
     * @return string View base path
     */
    public function getBasePathFromViewPath($view_path, $with_file_name = false)
    {
        $path_arr = explode('.', $view_path);

        // Remove Last value which MUST be view file name
        if ($with_file_name)
            array_pop($path_arr);

        // Map the Translation directory
        return implode('/', $path_arr) . '/';
    }


    /**
     * @param $data
     * @param $key
     * @param $value
     * @return array
     */
    public function changeToKeyValArray($data, $key, $value)
    {
        $tmp = [];
        foreach ($data as $item) {

            $tmp[$item->$key] = $item->$value;
        }

        return $tmp;
    }

    /** * generate random string for testing in act tab
     *
     * @param int $length
     * @return string
     */
    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function formatDate($format, $date)
    {
        return date($format, strtotime($date));
    }

    public function validateDateTimeFormat($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }


    public function shortenHtmlContent($content, $offset = 60)
    {
        if ($content == null)
            return "";

        $content = strip_tags($content);

        if (strlen($content) <= $offset)
            return $content;

        $pos = strpos($content, ' ', $offset);
        return substr($content, 0, $pos) . ' ....';

    }

    /**
     * Path for config
     */
    public function getPathToConfig($value)
    {
        return \ConfigHelper::getConfigByKey('evemoo.' . $value);
    }

    public function getDataAsItsProperty($property, $field)
    {
        if(is_array($property))
            return $property[$field];
        else
            return $property->$field;
    }

    public function checkUserRole($roles, $role)
    {
        foreach ($roles as $r) {
            if ($r->name == $role)
                return "checked";
        }

        return "";
    }

    public function getRandomString($length = 20) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];

        }
        return implode($pass); //turn the array into a string
    }

    public function getInvoiceCode()
    {
        $existing_code = Transaction::max('invoice_code');
        if($existing_code)
        {
            $code = $existing_code+1;
        }
        else
        {
            $code = 10001;
        }
        return $code;
    }

    public function getTransactionCode()
    {
        $existing_code = Transaction::max('transaction_code');

        if($existing_code)
        {
            $code = $existing_code+1;
        }
        else
        {
            $code = 555555;
        }
        return $code;
    }

    public function verifyToken()
    {
        $rand = md5(rand(1, 99999999));
        $check = User::where('verify_token', $rand)->first();
        if($check)
        {
            verifyToken();
        }
        else
            return $rand;
    }

    public function split_name($name) {
        $parts = array();

        while ( strlen( trim($name)) > 0 ) {
            $name = trim($name);
            $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $parts[] = $string;
            $name = trim( preg_replace('#'.$string.'#', '', $name ) );
        }

        if (empty($parts)) {
            return false;
        }

        $parts = array_reverse($parts);
        $name = array();
        $name['first_name'] = $parts[0];
        $name['middle_name'] = (isset($parts[2])) ? $parts[1] : '';
        $name['last_name'] = (isset($parts[2])) ? $parts[2] : ( isset($parts[1]) ? $parts[1] : '');

        return $name;
    }

    public function social_links()
    {
        return json_encode([
                    'instagram' => [
                        'title' => 'Instagram',
                        'link' => '',
                        'icon' => '<i class="fa fa-instagram"></i>',
                    ],
                    'fb' => [
                        'title' => 'Facebook',
                        'link' => '',
                        'icon' => '<i class="fa fa-facebook-f"></i>',
                    ],
                    'twitter' => [
                        'title' => 'Twitter',
                        'link' => '',
                        'icon' => '<i class="fa fa-twitter"></i>',
                    ],
                    'google' => [
                        'title' => 'Google+',
                        'link' => '',
                        'icon' => '<i class="fa fa-google"></i>',
                    ],
                    'linkedin' => [
                        'title' => 'LinkedIn',
                        'link' => '',
                        'icon' => '<i class="fa fa-linkedin"></i>',
                    ],
                ]);
    }

    public function insert_social_link($service, $link = null)
    {
        $links = json_decode($this->social_links());
        switch ($service) {
            case 'facebook':
                $links->fb->link = $link;
                break;

            case 'google':
                $links->google->link = $link;
                break;

            case 'twitter':
                $links->twitter->link = $link;
                break;
            
            default:
                # code...
                break;
        }

        return json_encode($links);
    }

    public function getUserFullName($id)
    {
        $full_name = '';

        $user = User::where('id', $id)->first();

        if (isset($user)) {

            $full_name = $user->first_name . ' ' .
                $user->middle_name . ' ' .
                $user->last_name;

        }

        return $full_name;
    }

    public function getFullName($names)
    {
        $stack = [$names->first_name, $names->middle_name, $names->last_name];

        $full_name = '';

        foreach ($stack as $key => $name) {
            if ($name != null) {
                $full_name = $full_name.($key != 0?' ':'').$name;
            }
        }

        return $full_name;
    }

}