<?php
namespace App\EveMoo\Helpers;

use App;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Lang;
use Log;
use Mail;

class AppExceptionHandler
{
    // TODO: Load Route Not Found Page
    public function routeNotFound()
    {
        dd('Invalid route request made.');
    }

    /**
     * Redirect To error pages for Http request
     * Responses with error to Ajax Request
     *
     * @param null $error_code Error Type
     * @param null $message Message to be log for future reference
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function unAuthorizedAccess($error_code = null, $message = null)
    {
        if (!request()->is('admin/*')) {
            $base_route = 'error';
            $trans_path = 'errors/general.';
        } else {
            $base_route = 'admin.error';
            $trans_path = 'admin/errors/general.';
        }

        if ($error_code ||
            in_array($error_code, \ConfigHelper::getConfigByKey('evemoo.error-codes'))) {

            if ($message) {
                Log::error('------- '. $error_code .' ---------');
                Log::error(request()->fullUrl());
                Log::error('message: '. $message);
                Log::error('------- END: '. $error_code .' ---------'.PHP_EOL);
            }

            if (request()->ajax()) {
                return response(trans($trans_path.$error_code.'.title'), trans($trans_path.$error_code.'.code'))->send();
                die;
            }

            return redirect()->route($base_route, $error_code)->send();
            die;

        } else
            $this->systemError($this->getErrorCode('no-error-code-passed'));

    }

    /**
     * @param null $error_code Type of error
     * @param null $message Error message to log
     * @param bool $terminate_request true | false
     *             If true: log error, email to admin , redirect to error page
     *             If false: log error, email to admin
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function systemError($error_code = null, $message = null, $terminate_request = true)
    {
        $error_no = $this->generateUniqueErrorNo();

        if (!request()->is('admin/*')) {
            $base_route = 'error';
            $trans_path = 'errors/general.';
        } else {
            $base_route = 'admin.error';
            $trans_path = 'admin/errors/general.';
        }

        // error log to database
        App\Evemoo\Models\Log::create([
            'error_no'   => $error_no,
            'error_code' => $error_code,
            'error_msg' => $message,
            'error_process_status' => 'not_seen',
            'error_level' => 'warning',
        ]);

        if (!$error_code) {

            $this->logMessage('error', $this->prepareMessage(
                $error_no,
                'no-error-code-passed',
                'No error code passed to AppExceptionHandler::systemError() while accessing url: '. request()->fullUrl()
            ));

            $this->emailAppException([
                'error_type' => 'no-error-code-passed',
                'error_code' => 'no-error-code-passed',
                'error_no' => $error_no
            ]);

            if (request()->ajax()) {
                return response(
                    trans($trans_path.'no-error-code-passed'.'.title'),
                    trans($trans_path.'no-error-code-passed'.'.code')
                )->send();
                die;
            }

            return redirect()->route($base_route, 'no-error-code-passed')->send();
            die;

        }
        else {

            // Log error for developer
            $this->logMessage('error', $this->prepareMessage(
                $error_no,
                $error_code,
                $message
            ));

            // mail error notification with error number
            $this->emailAppException([
                'error_type' => 'system-error',
                'error_code' => 'system-error',
                'error_no' => $error_no
            ]);

            if ($terminate_request) {

                if (request()->ajax()) {
                    return response(
                        trans($trans_path.'system-error'.'.title'),
                        trans($trans_path.'system-error'.'.code')
                    )->send();
                    die;
                }

                return redirect()->route($base_route, 'system-error')->send();
                die;

            }

        }



    }


    /**
     * Method to log messages using LARAVEL error Log
     *
     * @param $error_type error|success|info|etc
     * @param $message Message to log
     */
    public function logMessage($error_type, $message)
    {
        Log::useFiles(storage_path().'/logs/exceptions.log');
        Log::$error_type($message);
        return;

    }

    /**
     * Email Exception to Admin according to error type
     *
     * @param array $params
     */
    public function emailAppException($params = [])
    {
        if (App::environment() == 'production') {

            $data = [];
            // TODO: Load Site config from DB
            $data['site_configs'] = ''; //\AppHelper::getSiteConfigs();
            $data['receiving_email'] = '';

            // If receiving email not set in site configuration
            // use from config (neptrox.php)
            if (!empty($data['site_configs'])
                && isset($data['site_configs']['RECEIVING_EMAIL'])
                && !empty($data['site_configs']['RECEIVING_EMAIL'])
                && filter_var($data['site_configs']['RECEIVING_EMAIL'], FILTER_VALIDATE_EMAIL) !== false
            ) {
                $data['receiving_email'] = $data['site_configs']['RECEIVING_EMAIL'];
            } else
                $data['receiving_email'] = \ConfigHelper::getConfigByKey('evemoo.site-configuration-keys.RECEIVING_EMAIL');

            switch ($params['error_type']) {

                case 'system-error':
                case 'no-error-code-passed':
                    $data['message'] = Lang::get('general.error.message-'.$params['error_code'],
                        [
                            'error_no' => $params['error_no']
                        ]
                    );
                    break;

                case 'site_config':
                    $data['message'] = Lang::get('general.error.' . $params['error_code'], ['key' => $params['config_key']]);
                    break;

            }

            Mail::send('email.exception',
                ['data' => $data],
                function ($m) use ($data) {
                    $m->to($data['receiving_email']);
                    $m->subject(Lang::get('general.error.EXCEPTION_EMAIL_SUBJECT'));
                }
            );

        }
    }

    public function authorizeOwner($username) {
        if (Auth::user()->username != $username) {
            throw new AuthenticationException;            
        }
    }

    public static function generateUniqueErrorNo($length = 5)
    {
        $result = '';
        for ($i = 0; $i < $length; $i++)
        {
            $num = rand(97, 122);
            $result .= chr($num);
        }
        return strtoupper($result)."-".rand(125412,785421);
    }

    public function prepareMessage($error_no, $errorCode, $errorMessage)
    {
        $message = "\n";
        $message .= "===============================================================================================\n";
        $message .= "***********************************************************************************************\n";
        $message .= "===============================================================================================\n";
        $message .= "Date & Time : ". date('d/m/Y H:i:s')."\n";
        $message .= "-----------------------------------------------------------------------------------------------\n";
        $message .= "Error No : ". $error_no."\n";
        $message .= "-----------------------------------------------------------------------------------------------\n";
        $message .= "-----------------------------------------------------------------------------------------------\n";
        $message .= "Error Code : ". $errorCode."\n";
        $message .= "-----------------------------------------------------------------------------------------------\n";
        $message .= "-----------------------------------------------------------------------------------------------\n";
        $message .= "Error : ". $errorMessage ."\n";
        $message .= "-----------------------------------------------------------------------------------------------\n";
        $message .= "===============================================================================================\n";
        $message .= "***********************************************************************************************\n";
        $message .= "===============================================================================================\n";

        return $message;
    }

}