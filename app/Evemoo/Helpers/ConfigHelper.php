<?php

namespace App\EveMoo\Helpers;


use App\Evemoo\Models\Setting;

class ConfigHelper
{

    private $evemoo = 'evemoo';
    private $site_config;

    /**
     * @return string
     */
    public function evemooConfig($value) {

        return $this->getConfigByKey($this->evemoo.'.'.$value);

    }

    public function getSiteConfigBykey($key)
    {
        if (!$this->site_config) {
            $this->site_config = Setting::pluck('value', 'key');
        }

        return $this->site_config[$key];
    }

    /**
     * Default Currency...
     */
    public function defaultCurrency($code=null)
    {
        if(is_null($code))
            $val = '.symbol';
        else
            $val = '.code';

        return $this->evemooConfig('currency.options.'.$this->evemooConfig('currency.default').$val);
    }

    /**
     * @param $value
     * @return string
     */
    public function getRole($value) {

        return $this->evemooConfig('user.roles.'.$value);

    }

    /**
     * @param $error_type
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getErrorCode($error_type)
    {
        return $this->evemooConfig('error-codes.'.$error_type);
    }

    /**
     * @param $keyPath
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getConfigByKey($keyPath)
    {
        if (!config($keyPath)) {
            $message  = PHP_EOL.'Error: '.$keyPath.' : config not found.';
            $message .= PHP_EOL.'Path: '.get_class($this).'@'.__FUNCTION__;
            $message .= PHP_EOL.'URl: '. request()->fullUrl();
            \AppExceptionHandler::systemError(
                $this->getErrorCode('system-config-not-found'),
                $message,
                1
            );
        }

        return config($keyPath);
    }

}