<?php

namespace App\EveMoo\Helpers;

use Carbon\Carbon;

class ViewHelper
{
    /**
     * @return string
     */
    public function getEnquiryStatus($status) {

        switch($status) {
            case '0':
                return '<span class="label label-warning">Pending Response</span>';
                break;

            case '1':
                return '<span class="label label-primary">Negotiating</span>';
                break;

            case '2':
                return '<span class="label label-success">Accepted</span>';
                break;

            case '3':
                return '<span class="label label-danger">Canceled</span>';
                break;

            default:
                return null;
                break;
        }

    }

    /**
     * @return string
     */
    public function getOrderStatus($status) {

        switch($status) {
            case '0':
                return '<span class="label label-warning">Pending Response</span>';
                break;

            case '1':
                return '<span class="label label-primary">Processing</span>';
                break;

            case '2':
                return '<span class="label label-success">Complete</span>';
                break;

            default:
                return null;
                break;
        }

    }

    /**
     * @return string
     */
    public function getHumanReadableDate($date) {

        $new_date = date_format($date, 'M d, Y');

        return $new_date;

    }

    public function getDateAsHumanDiff($date ,$date_format = 'M d, Y')
    {
        $created = Carbon::parse($date);
        $now = Carbon::now();
        $date = ($created->diff($now)->days < 5)
            ? $created->diffForHumans()
            :date($date_format, strtotime($created));

        return $date;
    }

    /**
     * Returns error message form Laravel Error Message Bag
     *
     * @param $field Input field name
     */
    public function getFormError($field)
    {
        if(request()->session()->has('errors')) {
            $errors = request()->session()->get('errors');
            if ($errors->has($field))
                echo '<span class="help-block">'.$errors->first($field).'</span>';
        }
    }

    /**
     * Finds if field has got some error
     *
     * @param $field Input Field Name
     * @return bool
     */
    public function hasValidationError($field)
    {
        if(request()->session()->has('errors')) {
            $errors = request()->session()->get('errors');
            return $errors->first($field)?'has-error':'';
        }

        return false;
    }


}