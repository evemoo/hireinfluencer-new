<?php

namespace App\Evemoo\GeoLocate\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class GeoLocationBlocking
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $geo_location = $this->makeRequest($request);

        $block_list = $this->blockList();

        if(array_key_exists($geo_location, $block_list['geo']) || array_key_exists($request->ip(), $block_list['ip']))
            return redirect()->route('blocked');

        return $next($request);
    }

    private function makeRequest($request)
    {
        $client_ip = $request->ip();

        $url = "https://ipinfo.io/110.44.121.32";

        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        $data = curl_exec($curl);

        curl_close($curl);

        $return_value = json_decode($data);

        return $return_value->country;
    }

    private function blockList()
    {
        $abc = new \App\Http\Controllers\Backend\Admin\AdminBaseController();
        $configs = $abc->getSiteConfigs();

        return ['geo' => $configs['geo_block'], 'ip' => $configs['ip_block']];
    }
}
