<?php
return [
    'status'                      => [
        'created'                 => 'Enquiry successfully created.',
        'updated'                 => 'Enquiry successfully updated.',
        'deleted'                 => 'Enquiry successfully deleted.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
    ],
    'button'                      => [
        'custom-enquiry'          => 'Make Custom Package',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Admin | Enquiries | Lists',
            'description'         => 'List of Enquiries',
        ],
        'create'                  => [
            'title'               => 'Admin | Enquiries | Create',
            'description'         => 'Creating a new Enquiries',
        ],
        'edit'                    => [
            'title'               => 'Admin | Enquiries | Edit',
            'description'         => 'Editing Enquiries: :name',
        ],
        'view'                    => [
            'title'               => 'Admin | Enquiries | Response',
            'description'         => 'Respond Enquiries: :name',
        ],
    ],
    'content'                => [
        'common'               => [
            'active'              => 'Active',
            'enquiryID'           => 'Enquiry ID',
            'status'              => 'Status',
            'dashboard'           => 'Dashboard',
            'enquiries'           => 'Enquiries',
            'enquiry'             => 'Enquiry',
            'inactive'            => 'InActive',
            'title'               => 'Title',
            'service'             => 'Service',
            'deadline'            => 'Deadline',
            'message'             => 'Message',
            'action'              => 'Action',

        ],
        'add' =>                [
            'create'            => 'Create'
        ],
        'list' => [
            'id'                  => 'ID',
            'action'              => 'Action',
            'search'              => 'Search',
            'list'                => 'List',
            'edit'                => 'Edit',
            'view'                => 'Respond',
            'delete'              => 'Delete',
            'description'         => 'List Of Enquiries'
        ],
        'respond'     =>[
            'respond'                       => 'Respond',
            'enquiry-service'               => 'Service:',
            'enquiry-by'                    => 'Enquiry By',
            'enquiry-user'                  => 'User:',
            'enquiry-deadline'              => 'Deadline:',
            'enquiry-status'                => 'Status:',
            'enquiry-discussion'            => [
                'title'         => 'Discussion',
                'at'            => 'at',
                'placeholder'   => 'Type a message here...',
            ],
            'enquiry-suggestions'           => 'Suggested Packages',
            'enquiry-suggestion'            => [
                'title'                         => 'Suggestion',
                'price'                         => 'Price:',
                'price-per'                     => 'per',
                'duration'                      => 'Duration:',
                'duration-days'                 => 'days',
            ],            
        ],
        'delete'                  => [
            'confirm'             => 'Are You Sure?',
        ],
    ],


];
