<?php
return [

    'page'            => [
        'title'       => 'Admin | Email Template',
        'description' => 'Email Template Configurations',
    ],

    'content' => [
        'dashboard'       => 'Dashboard',
        'description'     => 'Dashboard Description',
        'setting'         => 'Email Template',
        'list'            => 'List',


        'title'           => 'Title for the email template',
        'slug'            => 'Slug for the email template',
        'description'     => 'Description for the email template'
    ]

];
