<?php

return [
  'un-authorized-request' => [
      'title'       => 'UnAuthorized Requests',
      'description' => 'UnAuthorized Request Panel'
  ],
  'not-found' => [
    'back_to_text' => 'Back To Link'
  ],
];