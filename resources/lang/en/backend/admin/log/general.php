<?php
return [
    'status'                      => [
        'updated'                 => 'Log successfully updated.',
        'deleted'                 => 'Log successfully deleted.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
    ],
    'button'                      => [
        'view'                    => 'View',
        'update'                  => 'Update',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Admin | Logs | Lists',
            'description'         => 'List of Logs',
        ],
        'view'                  => [
            'title'               => 'Admin | Log | View',
            'description'         => 'View Log',
        ],
    ],

    'content'                      => [
        'common'                   => [
            'dashboard'            => 'Dashboard',
            'error_no'             => 'Error Number',
            'error_code'           => 'Error Code',
            'error_msg'            => 'Error Message',
            'error_process_status' => 'Error Status',
            'error_level'          => 'Error Level',
            'log'                  => 'Log',
        ],
        'view' =>                [
            'status'            => 'Status'
        ],
        'list' => [
            'id'                  => 'ID',
            'action'              => 'Action',
            'search'              => 'Search',
            'list'                => 'List',
            'description'         => 'List of logs'
        ],
    ],


];
