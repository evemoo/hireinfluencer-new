<?php
return [
	'status'                      => [
        'updated'                 => 'Service successfully updated.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
    ],
    'button'                      => [
        'cancel'                  => 'Cancel',
        'save'                    => 'Save',
    ],
    'page'                        => [
            'title'               => 'Admin | Settings',
            'description'         => 'Site Configuration Settings'
        ],
    'content'                  => [
            'status'           => 'Status',
            'dashboard'        => 'Dashboard',
            'setting'          => 'Settings',
            'list'             => 'List',
            'active'           => 'Active',
            'inactive'         => 'InActive',
            'company-name'      => 'Company Name',
            'domain'            => 'Domain',
            'email'             => 'Email',
            'landline'          => 'Landline',
            'mobile'            => 'Mobile',
            'fax'               => 'Fax',
            'logo'              => 'Logo',
            'map'               => 'Google Map',
            'location'          => 'Location',
            'working-hours'     => 'Working Hours',
            'social-media-links'=> 'Social Media Links',
            'icon'              => 'Icon',

            'seo-title'         => 'SEO Title',
            'seo-description'   => 'SEO Description',
            'seo-keywords'      => 'SEO Keywords',
            'seo-contact'       => 'Contact Page SEO Title',

            'reg-title'         => 'Registration',
            'reg-form-title-1'  => 'Register Form Title 1',
            'reg-form-title-2'  => 'Register Form Title 2',
            'reg-success-msg'   => 'Registration Success Message',
            'reg-fail-msg'      => 'Registration Fail Message',

            'login-title'       => 'Login',
            'login-form-title-1'=> 'Login Form Title 1',
            'login-form-title-2'=> 'Login Form Title 2',
            'login-fail-msg'    => 'Login Fail Message',

            'pass-reset'        => 'Password Reset',
            'pass-reset-red'    => 'Password Reset and Redirect',
            'pass-reset-title-1'=> 'Password Reset Form Title 1',
            'pass-reset-title-2'=> 'Password Reset Form Title 2',
            'pass-reset-success'=> 'Password Reset Success Message',
            'pass-reset-fail'   => 'Password Reset Success Message',

            'admin-roles'       => 'Admin Type Roles',
            'customer-roles'    => 'Customer Type Roles',
            'role-after-reg'    => 'Assign Role After Registration',

            'paypal_client_id'  => 'Paypal Client Id',
            'paypal_secret'     => 'Paypal secret Id',
            'stripe_client_id'  => 'Stripe Client Id',
            'stripe_secret'     => 'Stripe secret Id',

            'tabs'             => [
                'general'       => 'General',
                'seo'           => 'SEO',
                'content'       => 'Content',
                'config'        => 'Config',
                'payment'       => 'Payment',
            ],
        ],
];