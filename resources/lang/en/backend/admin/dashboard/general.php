<?php
return [

    'page'            => [
        'title'       => 'Admin | Dashboard',
        'description' => 'Dashboard Panel',
    ],

    'content' => [
        'dashboard'       => 'Dashboard',
        'description'     => 'Dashboard Description',
    ]

];
