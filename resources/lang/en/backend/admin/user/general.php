<?php
return [
    'status'                      => [
        'created'                 => 'User successfully created.',
        'updated'                 => 'User successfully updated.',
        'deleted'                 => 'User successfully deleted.',
        'password_reset'          => 'Password reset successfully.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
        'created'                 => 'User is not created.',
        'updated'                 => 'User is not updated.',
        'password_reset'          => 'Password is not reset.'
    ],
    'button'                      => [
        'add'                     => 'Create New User',
        'cancel'                  => 'Cancel',
        'edit'                    => 'Edit',
        'save'                    => 'Save User',
        'update'                  => 'Update User',
        'update-order'            => 'Update Order',
        'reset'                   => 'Reset Password',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Admin | Users | Lists',
            'description'         => 'List of Users',
        ],
        'create'                  => [
            'title'               => 'Admin | User | Create',
            'description'         => 'Creating a new User'
        ],
        'view'                  => [
            'title'               => 'Admin | User | View',
            'description'         => 'Viewing a User'
        ],
        'edit'                    => [
            'title'               => 'Admin | User | Edit',
            'description'         => 'Editing User: :name',
        ],
        'reset'                    => [
            'title'               => 'Admin | User | Password Reset',
            'description'         => 'User Password Reset',
        ],
    ],
    'content'                  => [
        'common'               => [
            'first_name'       => 'First Name',
            'middle_name'      => 'Middle Name',
            'last_name'        => 'Last Name',
            'name'             => 'Username',
            'email'            => 'Email',
            'password'         => 'Password',
            'confirm_password' => 'Confirm Password',
            'status'           => 'Status',
            'dashboard'        => 'Dashboard',
            'user'             => 'Users',
            'profile'          => 'Profile',
            'active'           => 'Active',
            'inactive'         => 'InActive',
            'role'             => 'Role'

        ],
        'add' =>                [
            'create'            => 'Create',
            'description'       => 'Create New User'
        ],
        'list' => [
            'action'              => 'Action',
            'description'         => 'List of user description',
            'full_name'           => 'Full Name',
            'id'                  => 'ID',
            'list'                => 'List',
            'edit'                => 'Edit',
            'view'                => 'View',
            'delete'              => 'Delete',
            'search'              => 'Search'
        ],
        'view' => [
            'view'              => 'View',
            'description'       => 'List of user orders',
            'package'           => 'Package',
            'account-url'           => 'Account URL',
            'service'           => 'Service',
            'package-type'      => 'Package Type',
            'qty'               => 'Quantity',
            'price'             => 'Price',
            'status'            => 'Status',
            'ini-count'         => 'Initial Count',
        ],
        'edit'     =>[
            'edit' => 'Edit',
            'description'       => 'Edit User'
        ],
        'reset'     =>[
            'password'          => 'Reset Password',
            'description'       => 'Reset User Password'
        ],
        'delete'                  => [
            'confirm'             => 'Are You Sure?',
        ],

        'overview' => [
            'review'        => 'Reviews on your Slots',
            'slot'          => 'Slot',
            'review-by'     => 'Review By',
            'rating'        => 'Rating',
            'recent-slots'  => 'Your Recent Slots',
            'status'        => [
                'pending'   => 'Pending',
                'accepted'  => 'Accepted',
                'rejected'  => 'Rejected'
            ]
        ],
        'account-setting'   => [
            'profile-account'           => 'Profile Account',
            'basic-info'                => 'Basic Info',
            'social-media-links'        => 'Social Media Links',
            'change-avatar'             => 'Change Avatar',
            'change-password'           => 'Change Password',
            'first-name'                => 'First Name',
            'middle-name'               => 'Middle Name',
            'last-name'                 => 'Last Name',
            'username'                  => 'Username',
            'email'                     => 'Email',
            'primary-contact'           => 'Primary Contact Number',
            'secondary-contact'         => 'Secondary Contact Number',
            'gender'                    => [
                'title'     => 'Gender',
                'select'    => '-- select gender --',
                'male'      => 'Male',
                'female'    => 'Female'
            ],
            'dob'                       => 'Date of Birth',
            'select-image'              => 'Select Image',
            'image-change'              => 'Change',
            'remove-image'              => 'Remove',
            'note'                      => 'NOTE!',
            'note-msg'                  => 'Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only',
            'new-password'              => 'New Password',
            'retype-password'           => 'Re-type New Password',
            'button'                    => [
                'save'      => 'Save',
                'reset'     => 'Reset'
            ]
        ],
        'help'              => [],
    ],


];
