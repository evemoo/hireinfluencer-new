<?php
return [
    'status'                      => [
        'created'                 => 'Package Type successfully created.',
        'updated'                 => 'Package Type successfully updated.',
        'deleted'                 => 'Package Type successfully deleted.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
    ],
    'button'                      => [
        'add'                     =>  'Create Package Type',
        'cancel'                  => 'Cancel',
        'edit'                    => 'Edit',
        'save'                    => 'Save',
        'update'                  => 'Update',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Admin | Package Types | Lists',
            'description'         => 'List of Package Types',
        ],
        'create'                  => [
            'title'               => 'Admin | Package Type | Create',
            'description'         => 'Creating a new Package Type',
        ],
        'edit'                    => [
            'title'               => 'Admin | Package Type | Edit',
            'description'         => 'Editing Package Type: :name',
        ],
    ],
    'content'                  => [
        'common'               => [
            'active'           => 'Active',
            'status'           => 'Status',
            'dashboard'        => 'Dashboard',
            'package_type'     => 'Package Types',
            'inactive'         => 'InActive',
            'rank'             => 'Rank',
            'language'         => 'Change Language',

        ],
        'add' =>                [
            'create'            => 'Create'
        ],
        'list' => [
            'id'                  => 'ID',
            'action'              => 'Action',
            'service'             => 'Service Title',
            'title'               => 'Title',
            'hint'                => 'Hint',
            'search'              => 'Search',
            'list'                => 'List',
            'edit'                => 'Edit',
            'delete'              => 'Delete',
            'description'         => 'List Of Package Type'
        ],
        'edit'     =>[
            'edit' => 'Edit'
        ],
        'delete'                  => [
            'confirm'             => 'Are You Sure?',
        ],
    ],


];
