<?php
return [
    'status'                      => [
        'created'                 => 'Package successfully created.',
        'updated'                 => 'Package successfully updated.',
        'deleted'                 => 'Package successfully deleted.',
        'customer_custom_payment_created' => 'Custom package customer payment successfully created.',
        'customer_custom_payment_updated' => 'Custom package customer payment successfully updated.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
    ],
    'button'                      => [
        'add'                     =>  'Create Package',
        'cancel'                  => 'Cancel',
        'edit'                    => 'Edit',
        'save'                    => 'Save',
        'update'                  => 'Update',
        'customer_payment'        => 'Customer Payment',
        'add-customer'            => 'Add Customer',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Admin | Packages | Lists',
            'description'         => 'List of Packages',
        ],
        'create'                  => [
            'title'               => 'Admin | Package | Create',
            'description'         => 'Creating a new Package',
        ],
        'edit'                    => [
            'title'               => 'Admin | Package | Edit',
            'description'         => 'Editing Package: :name',
        ],
    ],
    'content'                => [
        'common'               => [
            'active'              => 'Active',
            'details'             => 'Details',
            'status'              => 'Status',
            'dashboard'           => 'Dashboard',
            'package'             => 'Packages',
            'inactive'            => 'InActive',
            'title'               => 'Title',
            'package_type'        => 'Package Types',
            'quantity'            => 'Quantity',
            'old_price'           => 'Old Price',
            'new_price'           => 'New Price',
            'delivery_in_minimum' => 'Delivery In Minimum',
            'delivery_in_maximum' => 'Delivery In Maximum',
            'per-rate'            => 'Per (1) Rate',
            'rank'                => 'Rank',
            'automatic'           => 'Automatic',
            'true'                => 'Yes',
            'false'               => 'No',
            'language'            => 'Change Language',
            'services'            => 'Services',
            'package_types'       => 'Package Types',

        ],
        'add' =>                [
            'create'            => 'Create'
        ],
        'list' => [
            'id'                  => 'ID',
            'action'              => 'Action',
            'deadline'            => 'Delivery Time',
            'price'               => 'Price',
            'day'                 => 'days',
            'search'              => 'Search',
            'list'                => 'List',
            'edit'                => 'Edit',
            'delete'              => 'Delete',
            'description'         => 'List Of Package'
        ],
        'edit'     =>[
            'edit' => 'Edit'
        ],
        'payment'     =>[
            'package'        => 'Package Name',
            'customer'       => 'Choose Customer',
            'price'          => 'Price For Customer',
        ],
        'delete'                  => [
            'confirm'             => 'Are You Sure?',
        ],
    ],


];
