<?php
return [
    'status'                      => [
        'created'                 => 'Social Media successfully created.',
        'updated'                 => 'Social Media successfully updated.',
        'deleted'                 => 'Social Media successfully deleted.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
        'created'                 => 'Social Media is not created.',
        'updated'                 => 'Social Media is not updated.',
        'password_reset'          => 'Password is not reset.'
    ],
    'button'                      => [
        'add'                     => 'Create New Social Media',
        'cancel'                  => 'Cancel',
        'edit'                    => 'Edit',
        'save'                    => 'Save Social Media',
        'update'                  => 'Update Social Media',
        'update-order'            => 'Update Order',
        'reset'                   => 'Reset Password',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Admin | Social Media | Lists',
            'description'         => 'List of Social Media',
        ],
        'create'                  => [
            'title'               => 'Admin | Social Media | Create',
            'description'         => 'Creating a new Social Media'
        ],
        'view'                  => [
            'title'               => 'Admin | Social Media | View',
            'description'         => 'Viewing a Social Media'
        ],
        'edit'                    => [
            'title'               => 'Admin | Social Media | Edit',
            'description'         => 'Editing Social Media: :name',
        ],
        'reset'                    => [
            'title'               => 'Admin | Social Media | Password Reset',
            'description'         => 'Social Media Password Reset',
        ],
    ],
    'content'                  => [
        'common'               => [
            'status'           => 'Status',
            'dashboard'        => 'Dashboard',
            'social-media'     => 'Social Media',
            'active'           => 'Active',
            'inactive'         => 'InActive',

        ],
        'add' =>                [
            'create'            => 'Create',
            'description'       => 'Create New User'
        ],
        'list' => [
            'action'              => 'Action',
            'description'         => 'List of social medias',
            'name'                => 'Name',
            'slug'                => 'Slug',
            'icon'                => 'Icon',
            'list'                => 'List',
            'edit'                => 'Edit',
            'view'                => 'View',
            'delete'              => 'Delete',
            'search'              => 'Search'
        ],
        'view' => [
            'view'              => 'View',
            'description'       => 'List of user orders',
            'package'           => 'Package',
            'account-url'           => 'Account URL',
            'service'           => 'Service',
            'package-type'      => 'Package Type',
            'qty'               => 'Quantity',
            'price'             => 'Price',
            'status'            => 'Status',
            'ini-count'         => 'Initial Count',
        ],
        'edit'     =>[
            'edit' => 'Edit',
            'description'       => 'Edit User'
        ],
        'reset'     =>[
            'password'          => 'Reset Password',
            'description'       => 'Reset User Password'
        ],
        'delete'                  => [
            'confirm'             => 'Are You Sure?',
        ],

        'overview' => [
            'review'        => 'Reviews on your Slots',
            'slot'          => 'Slot',
            'review-by'     => 'Review By',
            'rating'        => 'Rating',
            'recent-slots'  => 'Your Recent Slots',
            'status'        => [
                'pending'   => 'Pending',
                'accepted'  => 'Accepted',
                'rejected'  => 'Rejected'
            ]
        ],
        'account-setting'   => [
            'profile-account'           => 'Profile Account',
            'basic-info'                => 'Basic Info',
            'social-media-links'        => 'Social Media Links',
            'change-avatar'             => 'Change Avatar',
            'change-password'           => 'Change Password',
            'first-name'                => 'First Name',
            'middle-name'               => 'Middle Name',
            'last-name'                 => 'Last Name',
            'username'                  => 'Username',
            'email'                     => 'Email',
            'primary-contact'           => 'Primary Contact Number',
            'secondary-contact'         => 'Secondary Contact Number',
            'gender'                    => [
                'title'     => 'Gender',
                'select'    => '-- select gender --',
                'male'      => 'Male',
                'female'    => 'Female'
            ],
            'dob'                       => 'Date of Birth',
            'select-image'              => 'Select Image',
            'image-change'              => 'Change',
            'remove-image'              => 'Remove',
            'note'                      => 'NOTE!',
            'note-msg'                  => 'Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only',
            'new-password'              => 'New Password',
            'retype-password'           => 'Re-type New Password',
            'button'                    => [
                'save'      => 'Save',
                'reset'     => 'Reset'
            ]
        ],
        'help'              => [],
    ],


];
