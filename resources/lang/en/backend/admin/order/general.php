<?php
return [
    'status'                      => [
        'updated'                 => 'Order successfully updated.',
        'deleted'                 => 'Order successfully deleted.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
    ],
    'button'                      => [
        'view'                    => 'View',
        'update'                  => 'Update',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Admin | Orders | Lists',
            'description'         => 'List of Orders',
        ],
        'view'                  => [
            'title'               => 'Admin | Order | View',
            'description'         => 'View Order',
        ],
    ],

    'content'                  => [
        'common'               => [
            'dashboard'        => 'Dashboard',
            'transaction_code' => 'Transaction Code',
            'invoice_code'     => 'Invoice Code',
            'total_price'      => 'Total Price',
            'user_name'        => 'UserName',
            'user_email'       => 'Email',
            'order'            => 'Order',
            'status'           => 'Status',
        ],
        'view' =>                [
            'package'           => 'Package',
            'account-url'       => 'Account URL',
            'service'           => 'Service',
            'package-type'      => 'Package Type',
            'qty'               => 'Quantity',
            'price'             => 'Price',
        ],
        'list' => [
            'id'                  => 'ID',
            'action'              => 'Action',
            'search'              => 'Search',
            'list'                => 'List',
            'description'         => 'List of Transactions'
        ],
    ],


];
