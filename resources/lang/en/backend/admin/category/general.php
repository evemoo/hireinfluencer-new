<?php
    return [
        'status'                      => [
            'created'                 => 'Category successfully created.',
            'updated'                 => 'Category successfully updated.',
            'deleted'                 => 'Category successfully deleted.',
        ],
        'error'                       => [
            'no-data-found'           => ' Data not found.',
        ],
        'button'                      => [
            'create'            => 'Create new category',
            'save-n-parent'     => 'Save & Goto Parent',
            'save-n-continue'   => 'Save & Continue',
            'save'              => 'Save',
            'back'              => 'Back'
        ],
        'page'                        => [
            'index'                   => [
                'title'               => 'Admin | Category | Lists',
                'description'         => 'List of Category',
            ],
            'create'                  => [
                'title'               => 'Admin | Category | Create',
                'description'         => 'Creating a new Category',
            ],
            'edit'                    => [
                'title'               => 'Admin | Category | Edit',
                'description'         => 'Editing Category: :name',
            ],
            'view'                    => [
                'title'               => 'Admin | Category | Response',
                'description'         => 'Respond Category: :name',
            ],
        ],
        'content'                => [
            'common'               => [
                'active'              => 'Active',
                'status'              => 'Status',
                'dashboard'           => 'Dashboard',
                'categories'          => 'Categories',
                'category'            => 'Category',
                'sub-categories'      => 'SubCategories',
                'inactive'            => 'InActive',
                'title'               => 'Title',
                'slug'                => 'Slug',
                'action'              => 'Action',

            ],
            'add' =>                [
                'create'            => 'Create',
                'title'             => 'Enter Title',
                'slug'              => 'Enter Slug',
                'icon'              => 'Upload Icon',
                'description'       => 'Enter Description',
                'project-count'     => 'Show project count under this category',
                'status'            => 'Select Status'
            ],
            'list' => [
                'id'                  => 'Enter ID',
                'title'               => 'Enter Title',
                'slug'                => 'Enter Slug',
                'search'              => 'Search',
                'edit'                => 'Edit',
                'list'                => 'List',
                'create'              => 'Create',
                'delete'              => 'Delete',
                'description'         => 'List Of Categories'
            ],
            'delete'                  => [
                'confirm'             => 'Are You Sure?',
            ],
        ],


    ];
