<?php
return [
    'status'                      => [
        'created'                 => 'Service successfully created.',
        'updated'                 => 'Service successfully updated.',
        'deleted'                 => 'Service successfully deleted.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
    ],
    'button'                      => [
        'add'                     =>  'Create Service',
        'cancel'                  => 'Cancel',
        'edit'                    => 'Edit',
        'save'                    => 'Save',
        'update'                  => 'Update',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Admin | Services | Lists',
            'description'         => 'List of Services',
            'table-title'         => 'Services list',
        ],
        'create'                  => [
            'title'               => 'Admin | Service | Create',
            'description'         => 'Creating a new Service',
            'section-title'       => 'New Service',
            'menu-name'           => 'Service Name',
            'menu-value-name'     => 'Service Value Name',
        ],
        'edit'                    => [
            'title'               => 'Admin | Service | Edit',
            'description'         => 'Editing Service: :name',
            'section-title'       => 'Edit Service',
        ],
    ],
    'content'                  => [
        'common'               => [
            'status'           => 'Status',
            'dashboard'        => 'Dashboard',
            'service'          => 'Services',
            'title'            => 'Title',
            'hint'             => 'Hint',
            'icon'             => 'Icon',
            'action'           => 'Action',
            'active'           => 'Active',
            'inactive'         => 'InActive',
            'language'         => 'Choose Language',

        ],
        'add' =>                [
            'create'            => 'Create'
        ],
        'list' => [
            'id'                  => 'ID',
            'action'              => 'Action',
            'search'              => 'Search',
            'list'                => 'List',
            'edit'                => 'Edit',
            'delete'              => 'Delete',
            'description'         => 'List Of Service'
        ],
        'edit'     =>[
            'edit' => 'Edit'
        ],
        'delete'                  => [
            'confirm'             => 'Are You Sure?',
        ],
    ],


];
