<?php
return [
    'status'                      => [
        'updated'                 => 'Profile successfully updated.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
    ],
    'button'                      => [
        'reset'                    => 'Reset',
        'save'                  => 'save',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Customer | Profile',
            'description'         => 'Customer Profile',
        ],
        'view'                  => [
            'title'               => 'Admin | Order | View',
            'description'         => 'View Order',
        ],
    ],

    'content'                  => [
        'dashboard'     =>  'Dashboard',
        'user'          =>  'User',
        'profile'       =>  'Profile',
        'acc-setting'   =>  'Account Setting',
        'profile-acc'   =>  'Profile Account',
        'basic-info'    =>  'Basic Info',
        'detail-info'   =>  'Detail Info',
        'change-pass'   =>  'Change Password'
    ],


];
