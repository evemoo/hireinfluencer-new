<?php
return [

    'page'            => [
        'title'       => 'Admin | Dashboard',
        'description' => 'Dashboard Panel',
    ],
    'content' => [
        'dashboard'       => 'Dashboard',
        'description'     => 'Dashboard Description',
        'common' => [
            'dashboard' => 'Dashboard',
            'order' => 'Order',
            'list' => 'List',
            'fund' => 'Fund'
        ],
        'list' => [
            'list'        => 'List',
            'description' => 'Add Fund',
        ]
    ]

];
