<?php
return [

    'page'            => [
        'title'       => 'Customer | Dashboard',
        'description' => 'Dashboard Panel',
    ],

    'content' => [
        'dashboard'   => 'Customer',
        'description' => 'You might be interested in following service',
        'no_qty'          => 'No Qty',
        'no_package_type' => 'No Package Type',
        'no_price'        => 'No Price',
        'account_url'     => 'Account Url',
        'qty'             => 'Quantity',
        'add_to_cart'     => 'Add To Cart',
    ]

];
