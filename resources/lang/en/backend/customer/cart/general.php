<?php
return [

    'page'            => [
        'title'       => 'Customer | Cart',
        'description' => 'Cart Panel',
    ],

    'content' => [
        'cart'   => 'Cart',
        'description' => 'Cart List',
        'dashboard'   => 'Dashboard',
        'list'        =>  'List',
        'name'        =>  'Name',
        'username'        =>  'Username/ Url',
        'quantity'        =>  'Quantity',
        'delivery'        =>  'Delivery',
        'price'        =>  'Price',
        'total'        =>  'Total',
        'to'            => 'to',
        'day'           => 'days',
        'no-data-found' => 'No Services Added In Cart'
    ]

];
