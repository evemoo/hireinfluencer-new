<?php
return [

    'error' => [
        'no-data-found' => ' Data not found.',
        'created' => 'Enquiry not Submitted',
    ],

    'status' => [
    	'created' => 'Enquiry successfully Submitted',
    	'updated' => 'Enquiry successfully Updated',
    	'deleted' => 'Enquiry successfully Deleted',
    ],

    'page' => [
    	'index' => [
	        'title'       => 'Customer | Enquiry',
	        'description' => 'Your Enquiries',
    	],
    	'create' => [
	        'title'       => 'Customer | Enquiry | Create',
	        'description' => 'Create an Enquiry',
	        'head' => 'Enquiry',
	        'sub-head' => 'New',
    	],
        'edit' => [
            'title'       => 'Customer | Enquiry | Edit',
            'description' => 'Edit an Enquiry',
            'head' => 'Enquiry',
            'sub-head' => 'Edit',
        ],
        'View' => [
            'title'       => 'Customer | Enquiry | View',
            'description' => 'View an Enquiry',
            'head' => 'Enquiry',
            'sub-head' => 'New',
        ],
    ],

    'button' => [
    	'add' => 'Make Enquiry',
        'edit' => 'Update Enquiry',
        'accept' => 'Accept Deal',
    	'cancel' => 'Cancel Deal',
    ],

    'content' => [
    	'common' => [
	        'dashboard'   => 'Dashboard',
	        'title'   => 'Enquiry',
	        'description' => 'Customer Description',
            'subject' => 'Enquiry Subject',
            'service' => 'Platform',
            'deadline' => 'Deadline',
            'message' => 'Message',
            'qty'   => 'Quantity',
            'account_url'   => 'Account Url',
    	],
    	'list' => [
    		'list' => 'List',
    		'description' => 'Your Enquiries',
    		'id' => 'Enquiry ID',
    		'title' => 'Title',
    		'service' => 'Service',
    		'deadline' => 'Deadline',
    		'message' => 'Message',
    		'status' => 'Status',
    		'action' => 'Action',
    	],
    	'add' => [
    		'list' => 'Create',
    	],
    	'edit' => [
    		'list' => 'Edit',
    	],
        'view' => [
            'list' => 'View',
            'user' => 'User',
            'enquiry-by' => 'Enquiry By',
            'enquiry-discussion'            => [
                'title'         => 'Discussion',
                'at'            => 'at',
                'placeholder'   => 'Type a message here...',
            ],
            'enquiry-suggestions'           => 'Suggested Packages',
            'enquiry-suggestion'            => [
                'title'                         => 'Suggestion',
                'price'                         => 'Price:',
                'price-per'                     => 'per',
                'duration'                      => 'Duration:',
                'duration-days'                 => 'days',
            ],            
        ],
    	'delete' => [
    		'list' => 'Delete',
    	],
    ]

];