<?php
return [

    'status'                      => [
        'created'                 => 'User successfully created.',
        'updated'                 => 'User successfully updated.',
        'deleted'                 => 'User successfully deleted.',
    ],
    'error'                       => [
        'no-data-found'           => ' Data not found.',
    ],
    'button'                      => [
        'add'                     =>  'Create User',
        'cancel'                  => 'Cancel',
        'edit'                    => 'Edit',
        'save'                    => 'Save',
        'update'                  => 'Update',
    ],
    'page'                        => [
        'index'                   => [
            'title'               => 'Admin | Users | Lists',
            'description'         => 'List of Users',
            'table-title'         => 'Users list',
        ],
        'create'                  => [
            'title'               => 'Admin | User | Create',
            'description'         => 'Creating a new User',
            'section-title'       => 'New User',
            'menu-name'           => 'User Name',
            'menu-value-name'     => 'User Value Name',
        ],
        'edit'                    => [
            'title'               => 'Admin | User | Edit',
            'description'         => 'Editing User: :name',
            'section-title'       => 'Edit User',
        ],
    ],
    'content'                  => [
        'common'               => [
            'first_name'       => 'First Name',
            'middle_name'      => 'Middle Name',
            'last_name'        => 'Last Name',
            'name'             => 'Name',
            'email'            => 'Email',
            'password'         => 'Password',
            'confirm_password' => 'Confirm Password',
            'status'           => 'Status',
            'user'            => 'प्रयोगकर्ता'

        ],
        'add' =>                [

        ],
        'list' => [
            'id'                  => 'ID',
            'action'              => 'Action',
            'search'              => 'Search',
            'list'                => 'List'
        ],
        'edit'                    =>[

        ],
        'delete'                  => [
            'confirm'             => 'Are You Sure?',
        ],
    ],


];
