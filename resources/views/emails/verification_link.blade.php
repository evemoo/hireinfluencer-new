<style>
    h1 {
        color: blue;
    }
</style>
<h1>Please, click the link below to active your account.</h1>
<br>
<a href="{{ route('user.verification', $user->verification_token) }}">{{ $user->verification_token }}</a>