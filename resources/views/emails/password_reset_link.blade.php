<style>
    h1 {
        color: blue;
    }
</style>
<h1>Please, click the link below to reset your password.</h1>
<br>
<a href="{{ route('user.password.reset.token', $user->verification_token) }}">
    Click Here
</a>