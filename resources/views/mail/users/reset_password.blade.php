@component('mail::message')
# Password Reset

 Your Rested Password is here {{ $password }}.

@component('mail::button', ['url' => route('login') ])
 Login here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent