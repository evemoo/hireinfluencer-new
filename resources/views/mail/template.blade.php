<!DOCTYPE html>
<html>
	<body>
		<h1>Welcome to HireInfluencer, <small>{{ $user->username }}</small></h1>
		<p>Verify your account by clicking on the link:</p>
		<p><a href="{{ 'http://localhost:8080/login?token='.$user->verify_token }}">http://localhost:8080/login?token={{ $user->verify_token }}</a></p>
	</body>
</html>