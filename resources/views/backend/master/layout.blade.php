<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    @yield('meta')

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/backend/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('semantic.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/backend/css/form.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/backend/css/input.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/backend/css/dropdown.min.css') }}">

    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('assets/backend/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('assets/backend/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('assets/backend/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
    <link rel="stylesheet" href="{{ asset('css/sweetalert.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/backend/css/jquery-ui.css') }}">


    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('style')
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>


</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

<div id="app" class="page-wrapper">

    @include('backend.includes.header')

    <div class="clearfix"></div>

    <div class="page-container">

        @include('backend.includes.sidebar')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">

            @yield('content')

        </div>
        <!-- END CONTENT -->

    </div>
    <!-- END CONTAINER -->

    @include('backend.includes.footer')

</div>

{{--<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>--}}

    <script src="{{ asset('assets/backend/js/respond.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/excanvas.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/ie8.fix.min.js') }}"></script>

    <!-- BEGIN CORE PLUGINS -->
    <script src="{{ asset('assets/backend/js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/js.cookie.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.blockui.min.js') }}" type="text/javascript"></script>


    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ asset('assets/backend/js/app.min.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/backend/js/layout.min.js') }}" type="text/javascript"></script>



    <script src="{{ asset('assets/backend/js/demo.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/quick-sidebar.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/quick-nav.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/jquery-ui.js') }}"></script>

    <script type="text/javascript">
        $( function() {
            $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
          } );
    </script>

    <script src="{{ asset('assets/backend/js/semantic.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/form.min.js') }}"></script>
    <script src="{{ asset('assets/backend/js/dropdown.js') }}"></script>

    <script type="text/javascript">
        function markAsRead()
        {
            $.get('/markAsRead');
            $('.notif').html('0');
        }
    </script>


@yield('script')

</body>

</html>