<script type="text/javascript">    
    ( function () {
        $(document).on('click', '.send', function (e) {

            var msg = $('.msg').val();

            if (msg == '' || msg == undefined) {
                return false;
            }

            var url = $(this).attr('data-href');

            var user = {{ $enquiry->id }};
            console.log(user);

            var role = '{{ auth()->user()->hasRole(['admin'])?'1':'0' }}';

                $.ajax({
                    type: "POST",
                    url: url,
                    headers: {
                        'X-CSRF-Token': '{!! csrf_token() !!}'
                    },
                    data: {'id':user, 'message':msg, 'from':role},
                    success: function (response) {
                        data = $.parseJSON(response);
                        if (data.error == true) {
                            alert('Failed!');
                        }
                        else {
                            $('.msg').val(null);
                            $('.chats').append(data.data);
                        }
                    },
                });
            });

    })();
</script>