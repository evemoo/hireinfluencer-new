<div class="">
    <!-- BEGIN PORTLET-->
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-hide hide"></i>
                <span class="caption-subject font-hide bold uppercase">{{ admin_trans($trans_path.'content.respond.enquiry-discussion.title') }}</span>
            </div>
        </div>
        <div class="portlet-body" id="chats">
            <div class="scroller" style="height: 525px;" data-always-visible="1" data-rail-visible1="1">
                <ul class="chats">
                    @foreach($enquiry->discussions as $data)
                        @include('backend.common.discussion.discussion_msg')
                    @endforeach
                </ul>
            </div>
            <div class="chat-form">
                <div class="input-cont">
                    <input class="form-control msg" type="text" placeholder="{{ admin_trans($trans_path.'content.respond.enquiry-discussion.placeholder') }}" /> </div>
                <div class="btn-cont">
                    <span class="arrow"> </span>
                    <a href="javascript:;" class="btn blue icn-only send" data-href="{{ route('post.discussion') }}">
                        <i class="fa fa-space-shuttle icon-white"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- END PORTLET-->
</div>