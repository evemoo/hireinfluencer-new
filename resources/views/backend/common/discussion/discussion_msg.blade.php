@if(auth()->user()->hasRole('admin'))
	<li class="{{ $data->from == 1?'out':'in' }}">
	    <img class="avatar" alt="" src="{{ asset('img/avatar.png') }}" />
	    <div class="message">
	        <span class="arrow"> </span>
	        <a href="javascript:;" class="name"> {{ $data->from == 0?$data->enquiry->user->username:'admin' }} </a>
	        <span class="datetime"> {{ admin_trans($trans_path.'content.respond.enquiry-discussion.at') }} {{ $data->created_at }} </span>
	        <span class="body"> {{ $data->message }} </span>
	    </div>
	</li>
@elseif(auth()->user()->hasRole('customer'))
	<li class="{{ $data->from == 0?'out':'in' }}">
	    <img class="avatar" alt="" src="{{ asset('img/avatar.png') }}" />
	    <div class="message">
	        <span class="arrow"> </span>
	        <a href="javascript:;" class="name"> {{ $data->from == 0?$data->enquiry->user->username:'admin' }} </a>
	        <span class="datetime"> {{ admin_trans($trans_path.'content.respond.enquiry-discussion.at') }} {{ $data->created_at }} </span>
	        <span class="body"> {{ $data->message }} </span>
	    </div>
	</li>
@endif