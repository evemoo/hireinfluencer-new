@foreach (['danger', 'warning', 'success', 'info'] as $msg)

    @if(Session::has($msg))

        <div class="note note-{{ $msg }}">
            <h3>Successfully</h3>
            <p>{{ Session::get($msg) }}</p>
        </div>

    @endif
@endforeach