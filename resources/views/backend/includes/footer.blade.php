<div class="page-footer">
    <div class="page-footer-inner"> {{ date('Y') }} &copy; Application By
        <a target="_blank" href="http://evemoo.com">Evemoo</a>
    </div>

    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>