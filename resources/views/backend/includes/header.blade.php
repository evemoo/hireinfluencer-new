<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="index.html">
                @isset($configs['logo'])
                <img src="{{ asset($configs['logo']) }}" alt="{{ $configs['company'][ConfigHelper::evemooConfig('lang.default')] }}" class="logo-default" />
                @endisset
            </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>
        <!-- END LOGO -->

        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">

                <li class="dropdown dropdown-extended dropdown-notification"
                    id="header_notification_bar" onclick="markAsRead()">

                    <a href="javascript:;" class="dropdown-toggle"
                       data-toggle="dropdown"
                       data-hover="dropdown"
                       data-close-others="true">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-default notif"> {{ count(auth()->user()->unreadNotifications) }} </span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <span class="bold">{{ count(auth()->user()->unreadNotifications) }}
                                    {{ trans('backend/common/general.pending') }}
                                </span> {{ trans('backend/common/general.notification') }}</h3>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                @foreach(auth()->user()->unreadNotifications as $notification)
                                <li>
                                    @foreach(auth()->user()->roles as $r)
                                        @if(isset($notification->data['enquiry']))
                                            @if($r->name == 'admin')
                                                <a href="{{ route('admin.enquiry.view', $notification->data['enquiry']['enquiry_id']) }}">
                                            @elseif($r->name == 'customer')
                                                <a href="{{ route('customer.enquiry.view', $notification->data['enquiry']['enquiry_id']) }}">
                                            @endif
                                        @elseif($notification->data['order'])
                                            @if($r->name == 'admin')
                                                <a href="{{ route('admin.order.view', $notification->data['order']['transactions_id']) }}">
                                            @elseif($r->name == 'customer')
                                                <a href="{{ route('customer.order.view', $notification->data['order']['transactions_id']) }}">
                                            @endif
                                        @endif
                                        <span class="time">{{ ViewHelper::getDateAsHumanDiff($notification->created_at) }}</span>
                                        <span class="details">
                                            @if(isset($notification->data['enquiry']))
                                                @if($r->name == 'admin')
                                                    {{ $notification->data['sender']['username'].'made an enquiry: '.$notification->data['enquiry']['subject'] }}
                                                @elseif($r->name == 'customer')
                                                    {{ 'Admin responded to your enquiry: '.$notification->data['enquiry']['subject'] }}
                                                @endif
                                            @elseif(isset($notification->data['order']))
                                                @if($r->name == 'admin')
                                                    {{ 'New Order by '.$notification->data['sender']['username'] }}
                                                @elseif($r->name == 'customer')
                                                    {{ 'Your order is '.$notification->data['order']['status'].'!' }}
                                                @endif
                                            @endif
                                        </span>
                                    @endforeach
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="../assets/layouts/layout/img/avatar3_small.jpg" />
                        <span class="username username-hide-on-mobile"> {{ auth()->user()->username }} </span>
                        {{-- <i class="fa fa-angle-down"></i> --}}
                    </a>
                    {{-- <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="page_user_profile_1.html">
                                <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li>
                            <a href="app_calendar.html">
                                <i class="icon-calendar"></i> My Calendar </a>
                        </li>
                        <li>
                            <a href="app_inbox.html">
                                <i class="icon-envelope-open"></i> My Inbox
                                <span class="badge badge-danger"> 3 </span>
                            </a>
                        </li>
                        <li>
                            <a href="app_todo.html">
                                <i class="icon-rocket"></i> My Tasks
                                <span class="badge badge-success"> 7 </span>
                            </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="page_user_lock_1.html">
                                <i class="icon-lock"></i> Lock Screen </a>
                        </li>
                        <li>
                            <a href="page_user_login_1.html">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul> --}}
                </li>

            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->

    </div>
    <!-- END HEADER INNER -->
</div>