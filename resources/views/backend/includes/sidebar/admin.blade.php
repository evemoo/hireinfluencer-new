@if (AclHelper::isRouteAccessable('admin.dashboard:GET'))
    <li class="nav-item {!! Request::is('admin/dashboard')?'active':"" !!}">
        <a href="{{ route('admin.dashboard') }}" class="nav-link nav-toggle">
            <i class="fa fa-dashboard"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>
@endif


@if (AclHelper::isRouteAccessable('admin.email_template.index:GET'))
    <li class="nav-item">
        <a href="{{ route('admin.email_template.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-envelope-o"></i>
            <span class="title">Email Template</span>
        </a>
    </li>
@endif

@if (AclHelper::isRouteAccessable('admin.category.index:GET'))
    <li class="nav-item">
        <a href="{{ route('admin.category.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-sliders"></i>
            <span class="title">Category Configuration</span>
        </a>
    </li>
@endif

@if (AclHelper::isRouteAccessable('admin.socialmedia.index:GET'))
    <li class="nav-item start {!! Request::is('admin/social-media*')?'active open':"" !!}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-steam"></i>
            <span class="title">Social Media</span>
            <span class="selected"></span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">

            <li class="nav-item start {!! Request::is('admin/social-media')?'active open':""   !!}">
                <a href="{{ route('admin.socialmedia.index') }}" class="nav-link ">
                    <i class="fa fa-facebook"></i>
                    <span class="title">List</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="nav-item start {!! Request::is('admin/social-media/attributes*')?'active open':""   !!}">
                <a href="{{ route('admin.socialmedia.attributes.index') }}" class="nav-link ">
                    <i class="fa fa-thumbs-up"></i>
                    <span class="title">Attributes</span>
                    <span class="selected"></span>
                </a>
            </li>

        </ul>
    </li>
@endif

@if (AclHelper::isRouteAccessable('admin.setting.index:GET'))
    <li class="nav-item">
        <a href="{{ route('admin.setting.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-cogs"></i>
            <span class="title">Site Configuration</span>
        </a>
    </li>
@endif

@if (AclHelper::isRoutesAccessable([
    'admin.roles.index:GET',
    'admin.routes.index:GET',
    'admin.permissions.index:GET'
   ]))

    <li class="nav-item start {!! Request::is('admin/roles*')?'active open':""
                || Request::is('admin/routes*')?'active open':""
                || Request::is('admin/permissions*')?'active open':""
                !!} ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-lg fa-fw fa-pencil-square-o"></i>
            <span class="title">Acl</span>
            <span class="selected"></span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">

            <li class="nav-item start {!! Request::is('admin/roles*')?'active open':""   !!}">
                <a href="{{ route('admin.roles.index') }}" class="nav-link ">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Roles</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="nav-item start {!! Request::is('admin/permissions*')?'active open':""   !!}">
                <a href="{{ route('admin.permissions.index') }}" class="nav-link ">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Permissions</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="nav-item start {!! Request::is('admin/routes*')?'active open':""   !!}">
                <a href="{{ route('admin.routes.index') }}" class="nav-link ">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Routes</span>
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
    </li>
@endif


@if (AclHelper::isRouteAccessable('admin.user.index:GET'))
    <li class="nav-item  {!! Request::is('admin/user*')?'active':"" !!}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-users"></i>
            <span class="title">Users</span>
            <span class="arrow"></span>
        </a>

        <ul class="sub-menu">

            <li class="nav-item start {!! Request::is('admin/user/influencer')?'active open':""   !!}">
                <a href="{{ route('admin.user.index', ['role' => 'influencer']) }}" class="nav-link ">
                    <i class="fa fa-bullseye"></i>
                    <span class="title">Influencers</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="nav-item start {!! Request::is('admin/user/brand*')?'active open':""   !!}">
                <a href="{{ route('admin.user.index', ['role' => 'brand']) }}" class="nav-link ">
                    <i class="fa fa-compass"></i>
                    <span class="title">Brands</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="nav-item start {!! Request::is('admin/user/admin*')?'active open':""   !!}">
                <a href="{{ route('admin.user.index', ['role' => 'admin']) }}" class="nav-link ">
                    <i class="fa fa-dot-circle-o"></i>
                    <span class="title">Admin</span>
                    <span class="selected"></span>
                </a>
            </li>

        </ul>
    </li>
@endif
@if (AclHelper::isRouteAccessable('admin.user.reset.password:GET'))
    <li class="nav-item {!! Request::is('admin/reset/password')?'active':"" !!}">
        <a href="{{ route('admin.user.reset.password') }}" class="nav-link nav-toggle">
            <i class="fa fa-gear"></i>
            <span class="title">Reset Password</span>
        </a>
    </li>
@endif
@if (AclHelper::isRouteAccessable('admin.log.index:GET'))
    <li class="nav-item {!! Request::is('admin/log*')?'active':"" !!}">
        <a href="{{ route('admin.log.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-bug"></i>
            <span class="title">Error Log</span>
        </a>
    </li>
@endif
