
    <li class="nav-item {!! Request::is('customer/dashboard')?'active':"" !!}">
        <a href="{{ route('customer.dashboard') }}" class="nav-link nav-toggle">
            <i class="fa fa-dashboard"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>


@if (AclHelper::isRouteAccessable('customer.cart.index:GET'))
    <li class="nav-item {!! Request::is('customer/cart')?'active':"" !!}">
        <a href="{{ route('customer.cart.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-shopping-cart"></i>
            <span class="title">Cart</span>
        </a>
    </li>
@endif

@if (AclHelper::isRouteAccessable('customer.fund.index:GET'))
    <li class="nav-item">
        <a href="{{ route('customer.fund.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-money"></i>
            <span class="title">Fund</span>
        </a>
    </li>
@endif


    <li class="nav-item">
        <a href="{{ route('customer.profile.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-user"></i>
            <span class="title">Profile</span>
        </a>
    </li>

    <li class="nav-item {!! Request::is('customer/slot')?'active':"" !!}">
        <a href="{{ route('customer.slot.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-dashboard"></i>
            <span class="title">Slots</span>
        </a>
    </li>

    <li class="nav-item {!! Request::is('customer/review')?'active':"" !!}">
        <a href="{{ route('customer.review.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-dashboard"></i>
            <span class="title">Reviews</span>
        </a>
    </li>

    <li class="nav-item {!! Request::is('customer/project')?'active':"" !!}">
        <a href="{{ route('customer.project.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-dashboard"></i>
            <span class="title">Projects</span>
        </a>
    </li>

    <li class="nav-item {!! Request::is('customer/convo')?'active':"" !!}">
        <a href="{{ route('customer.convo.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-dashboard"></i>
            <span class="title">Conversations</span>
        </a>
    </li>

