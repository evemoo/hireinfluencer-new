@extends('backend.master.layout')

@section('content')

    <div class="page-content">

        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> {{ $text['title'] }}
            <small>{{ $text['description'] }}</small>
        </h1>

        <div class="row" id="users_vue_app">
            <div class="col-md-12">

               <table>
                   <thead>
                   <tr><th>{{ $text['code'] }} |</th>

                       <th>     |</th>
                       <th>   |</th>
                       <th>  {{ $text['back_to_text'] }} |</th>
                          <th> {{ $text['back_to_link'] }} |</th>
                   </tr>
                   </thead>
               </table>

            </div>
        </div>

    </div>

@endsection