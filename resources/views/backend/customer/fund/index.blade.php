@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ customer_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ customer_trans($trans_path.'page.index.description') }}" name="description" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('customer.dashboard') }}">
                        {{ customer_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{--{{ route($base_route.'.index') }}--}}">
                        {{ customer_trans($trans_path.'content.common.order') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ customer_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br />

        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">

                    <div class="caption">
                        <i class="fa fa-list font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">
                                {{ customer_trans($trans_path.'content.list.description') }}
                            </span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">

                        <div class="portlet-body">

                            <h3>Your Total Fund : {{ ConfigHelper::defaultCurrency(). ''. auth()->user()->fund }}</h3>

                            <div class="ui form">

                                <div class="field">
                                    <label> {{ customer_trans($trans_path.'content.common.fund') }} </label>

                                    <input type="number"
                                           onchange="$('.fund_paypal').val($(this).val());
                                            $('.fund_stripe').val($(this).val());"
                                           placeholder="Fund">
                                </div>

                                <form action="{!! URL::route('customer.fund.store') !!}" method="post" class="inline">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="fund" class="fund_paypal">

                                    <h3>Add Fund With :</h3>

                                <!-- Identify your business so that you can collect the payments. -->
                                    <input type="hidden" name="business" value="rohit.kapali9-facilitator@gmail.com">

                                    <!-- Specify a Buy Now button. -->
                                    <input type="hidden" name="cmd" value="_xclick">

                                    <!-- Specify details about the item that buyers will purchase. -->

                                    <!-- Display the payment button. -->
                                    {{-- <input type="image" name="submit" border="0"
                                    src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-medium.png"
                                    alt="Check out with PayPal"> --}}
                                    <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-medium.png"
                                         alt="Check out with PayPal"
                                         onclick="submit()"
                                         onmouseover=""
                                         style="cursor: pointer;" />

                                </form>

                                <form action="{{ route('customer.fund.stripe.store') }}" method="POST" class="inline">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="fund_stripe" class="fund_stripe">

                                    <script src="https://checkout.stripe.com/checkout.js"
                                            class="stripe-button"
                                            data-key="{{ Config::get('stripe.publishable_key') }}"
                                            data-name="Social Media Panel"
                                            data-description="Stripe Payment"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-label="Pay With Stripe"
                                            data-email="{{ auth()->check()?auth()->user()->email:null }}"
                                            data-locale="auto">
                                    </script>
                                </form>

                                <script type="text/javascript">

                                    $('.while_change_fund').on('input', function () {

                                    });

                                    // Create a Stripe client
                                    var stripe = Stripe('{{ Config::get('stripe.publishable_key') }}');

                                    // Create an instance of Elements
                                    var elements = stripe.elements();

                                    // Custom styling can be passed to options when creating an Element.
                                    // (Note that this demo uses a wider set of styles than the guide below.)
                                    var style = {
                                        base: {
                                            color: '#32325d',
                                            lineHeight: '18px',
                                            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                                            fontSmoothing: 'antialiased',
                                            fontSize: '16px',
                                            '::placeholder': {
                                                color: '#aab7c4'
                                            }
                                        },
                                        invalid: {
                                            color: '#fa755a',
                                            iconColor: '#fa755a'
                                        }
                                    };

                                    // Create an instance of the card Element
                                    var card = elements.create('card', {style: style});

                                    // Add an instance of the card Element into the `card-element` <div>
                                    card.mount('#card-element');

                                    // Handle real-time validation errors from the card Element.
                                    card.addEventListener('change', function(event) {
                                        var displayError = document.getElementById('card-errors');
                                        if (event.error) {
                                            displayError.textContent = event.error.message;
                                        } else {
                                            displayError.textContent = '';
                                        }
                                    });

                                    // Handle form submission
                                    var form = document.getElementById('payment-form');
                                    form.addEventListener('submit', function(event) {
                                        event.preventDefault();

                                        stripe.createToken(card).then(function(result) {
                                            if (result.error) {
                                                // Inform the user if there was an error
                                                var errorElement = document.getElementById('card-errors');
                                                errorElement.textContent = result.error.message;
                                            } else {
                                                // Send the token to your server
                                                stripeTokenHandler(result.token);
                                            }
                                        });
                                    });
                                </script>

                                <div class="ui error message"></div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>

    </div>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@endsection
