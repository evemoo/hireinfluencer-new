@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ customer_trans($trans_path.'page.title') }}</title>
    <meta content="{{ customer_trans($trans_path.'page.description') }}" name="description" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('customer.dashboard') }}">
                        {{ customer_trans($trans_path.'content.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{--{{ route($base_route.'.index') }}--}}">
                        {{ customer_trans($trans_path.'content.cart') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ customer_trans($trans_path.'content.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br />

        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">

                    <div class="caption">
                        <i class="fa fa-list font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">
                                {{ customer_trans($trans_path.'content.description') }}
                            </span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">

                        @include('backend.common.flash')

                        <table class="table table-hover table-light">
                            <thead class="flip-content">
                            <tr>
                                <th> </th>
                                <th>{{ customer_trans($trans_path.'content.name') }}</th>
                                <th>{{ customer_trans($trans_path.'content.username') }}</th>
                                <th>{{ customer_trans($trans_path.'content.quantity') }}</th>
                                <th>{{ customer_trans($trans_path.'content.delivery') }}</th>
                                <th>{{ customer_trans($trans_path.'content.price') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if($carts->count() > 0)
                                @foreach($carts as $cart)

                                    <tr>
                                        <td>
                                            <a href="" data-row-id="{{ $cart->rowId }}" class="remove_cart">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </td>
                                        <td>
                                            {{ $cart->options['service_title'] }} -  {{ $cart->options['package_type_title'] }}
                                        </td>
                                        <td>
                                            <input type="text"
                                                   class="form-control"
                                                   name="account_url"
                                                   value=" {{ $cart->options['account_url'] }} " >
                                        </td>
                                        <td> 
                                            @if($cart->options['type'] == ConfigHelper::evemooConfig('package.custom_package.key'))
                                                {{ $cart->qty }}
                                            @else
                                                {{ $cart->options['real_quantity'] * $cart->qty }}
                                            @endif
                                        </td>
                                        <td>
                                            {{ $cart->options['delivery_minimum'] }}
                                            {{ customer_trans($trans_path.'content.to') }}
                                            {{ $cart->options['delivery_maximum'] }}
                                            {{ customer_trans($trans_path.'content.day') }}
                                        </td>
                                        <td> {{ ConfigHelper::defaultCurrency().($cart->price * $cart->qty) }} </td>
                                    </tr>
                                @endforeach

                            @else
                                <tr>
                                    <td colspan="6" class="pull-right">{{ customer_trans($trans_path.'content.no-data-found') }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td colspan="5" style="text-align:right;">{{ customer_trans($trans_path.'content.total') }}</td>
                                <td >{{ ConfigHelper::defaultCurrency() }}<p class="total inline">{{ Cart::total() }}</p></td>
                            </tr>

                            </tbody>
                        </table>


                    </div>
                    <form action="{{ route('customer.payment.cart') }}" id="customer-wallet" method="POST">{!! csrf_field() !!}</form>
                    @if(Cart::count() > 0)

                        <div class="pull-left">
                            <a class="btn btn-primary" href="{{ route('customer.cart.test') }}">CLick tests</a>
                        </div>

                        <div class="pull-right">

                            <a  onclick="$('#customer-wallet').submit()">
                                <button class="btn btn-primary"> Pay From Wallet ( ${{ auth()->user()->fund }} )</button>
                            </a>

                            
                            @include('backend.customer.cart.includes.paypal')

                            @include('backend.customer.cart.includes.stripe')

                        </div>
                    @endif
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>

    </div>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script>
        (function () {
            $('.remove_cart').click(function (e) {
                e.preventDefault();

                var row_id = $(this).attr('data-row-id');
                var $this = $(this);

                $.ajax({
                    method: 'POST',
                    url: '{{ route('api.customer.cart.remove') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        row_id : row_id,
                    },
                    success : function (response) {
                        toastr.success(response.message, 'Success!', {timer:5000})
                        $this.parent().parent().remove();
                        $('body').find('p.total').html(response.total);
                    },

                });
            })
        })()
    </script>


@endsection
