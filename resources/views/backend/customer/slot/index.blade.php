@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ customer_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ customer_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('content')

    <div class="page-content">
        
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ route('customer.dashboard') }}">{{ customer_trans($trans_path.'content.common.dashboard') }}</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ route($base_route.'.index') }}">{{ customer_trans($trans_path.'content.common.title') }}</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>{{ customer_trans($trans_path.'content.list.list') }}</span>
                    </li>
                </ul>
                <div class="page-toolbar"></div>
            </div>
            
            <div class="row">

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">

                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">
                                {{ customer_trans($trans_path.'content.list.description') }}
                            </span>
                        </div>


                        <div class="actions">

                            <div class="btn-group btn-group-devided" data-toggle="buttons">

                                <a href="{{ route($base_route.'.create') }}"
                                   onclick="location.href = '{{ route($base_route.'.create') }}'">

                                    <button type="button"
                                            class="btn btn-primary mt-ladda-btn ladda-button btn-circle"
                                            data-style="expand-right">
                                        <span class="ladda-label">
                                            <i class="fa fa-plus-circle"></i>
                                            {{ customer_trans($trans_path.'button.add') }}
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </a>
                            </div>

                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            @include('backend.common.flash')

                            <table class="table table-hover table-light">
                                <thead class="flip-content">
                                <tr>
                                    <th width="10%"> {{ customer_trans($trans_path.'content.list.id') }} </th>
                                    <th>{{ customer_trans($trans_path.'content.list.title') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.service') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.deadline') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.message') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.status') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.action') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>


        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
    </div>

@endsection

@section('script')

    <script>
        $('.ui .dropdown').dropdown();

        


    </script>

@endsection
