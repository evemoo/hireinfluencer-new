@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ customer_trans($trans_path.'page.title') }}</title>
    <meta content="{{ customer_trans($trans_path.'page.description') }}" name="description" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ customer_trans($trans_path.'page.title') }}
                    </a>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        @include('backend.common.flash')

        <h1 class="page-title"> {{ customer_trans($trans_path.'content.dashboard') }}</h1>
        <p>{{ customer_trans($trans_path.'content.description') }}</p>
        <div class="row">

        <!-- BEGIN DASHBOARD STATS 1-->
        
        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->

        </div>
        <hr />
        <div>
            <h1>Your Social Medias</h1>
            <ul style="list-style: none;">
                @foreach($social_medias as $media)
                    <li><i class="{{ $media->icon }}"></i>{{ $media->name .' - '. $media->value .' '.$media->sma_name }}</li>
                @endforeach
            </ul>
        </div>
        <hr />
        <br />
        <div>
            <h1>Your Slots</h1>
            <div class="tabbable tabbable-tabdrop">
                <ul class="nav nav-tabs">
                    @foreach($social_medias as $key => $sm)
                        <li class="{{ $key == 0?'active':'' }}">
                            <a href="#tab{{ $sm->id }}" data-toggle="tab" aria-expanded="true"><i class="{{ $sm->icon }}"></i>{{ $sm->name }}</a>
                        </li>
                    @endforeach
                    
                </ul>
                <div class="tab-content">
                    @foreach($social_medias as $sm)
                        <div class="tab-pane {{ $sm->id == 1?'active':'' }}" id="tab{{ $sm->id }}">
                            @foreach($slots->where('social_media_id', $sm->id) as $record)
                                {{ isset($record->title)?$record->title:'x' }}
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">

        </div>

        <div class="clearfix"></div>

        <!-- END DASHBOARD STATS 1-->
    </div>

@endsection

@section('script')

@endsection
