<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#basic" data-toggle="tab">Basic Info</a>
                        </li>
                        <li>
                            <a href="#detail" data-toggle="tab">Detail Info</a>
                        </li>
                        <li>
                            <a href="#avatar" data-toggle="tab">Change Avatar</a>
                        </li>
                        <li>
                            <a href="#password" data-toggle="tab">Change Password</a>
                        </li>
                    </ul>
                    </ul>
                </div>
                <div class="portlet-body">
                    <form action="{{ route($base_route.'.update') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="basic">
                                <div class="form-group">
                                    <label class="control-label">First Name</label>
                                    <input type="text" placeholder="First Name" class="form-control" name="profile[first_name]" value="{{ $profile->first_name }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Middle Name</label>
                                    <input type="text" placeholder="Middle Name" class="form-control" name="profile[middle_name]" value="{{ $profile->middle_name }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <input type="text" placeholder="Last Name" class="form-control" name="profile[last_name]" value="{{ $profile->last_name }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Primary Contact Number</label>
                                    <input type="text" placeholder="Primary Number" class="form-control" name="profile[primary_contact]" value="{{ $profile->primary_contact }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Secondary Contact Number</label>
                                    <input type="text" placeholder="Secondary Number" class="form-control" name="profile[secondary_contact]" value="{{ $profile->secondary_contact }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Gender</label>
                                    <select class="form-control" name="profile[gender]">
                                        <option value="">-- select gender --</option>
                                        <option value="male" {{ $profile->gender == 'male'?'selected':'' }}>Male</option>
                                        <option value="female" {{ $profile->gender == 'female'?'selected':'' }}>Female</option>
                                    </select> </div>
                                <div class="form-group">
                                    <label class="control-label">Date Of Birth</label>
                                    <input type="text" id="datepicker" placeholder="DOB" name="profile[birth_date]" class="form-control datepicker" value="{{ date('Y-m-d', strtotime($profile->birth_date)) }}">
                                </div>
                            </div>
                            <!-- END PERSONAL INFO TAB -->
                            <!-- CHANGE AVATAR TAB -->
                            <div class="tab-pane" id="avatar">
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="hidden" value="" name="..."><input type="file" name="" class="avatar"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                        <span class="label label-danger">NOTE! </span>
                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                    </div>
                                </div>
                            </div>
                            <!-- END CHANGE AVATAR TAB -->
                            <!-- CHANGE PASSWORD TAB -->
                            <div class="tab-pane" id="password">
                                    <div class="form-group">
                                        <label class="control-label">New Password</label>
                                        <input type="password" class="form-control" name="profile[password]"> </div>
                                    <div class="form-group">
                                        <label class="control-label">Re-type New Password</label>
                                        <input type="password" class="form-control" name="profile[password_confirmation]"> </div>
                            </div>
                            <!-- END CHANGE PASSWORD TAB -->
                            <!-- PRIVACY SETTINGS TAB -->
                            <div class="tab-pane" id="detail">
                                <div class="form-group">
                                    <label class="control-label">Primary Address</label>
                                    <input type="text" placeholder="Primary Address" class="form-control" name="details[address_line_primary]" value="{{ $details?$details->address_line_primary:'' }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Secondary Address</label>
                                    <input type="text" placeholder="Secondary Address" class="form-control" name="details[address_line_secondary]" value="{{ $details?$details->address_line_secondary:'' }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Facebook</label>
                                    <input type="text" placeholder="Facebook" class="form-control" name="details[fb_url]" value="{{ $details?$details->fb_url:'' }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Twitter</label>
                                    <input type="text" placeholder="Twitter" class="form-control" name="details[twitter_url]" value="{{ $details?$details->twitter_url:'' }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Youtube</label>
                                    <input type="text" placeholder="Youtube" class="form-control" name="details[youtube_url]" value="{{ $details?$details->youtube_url:'' }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">Instagram</label>
                                    <input type="text" placeholder="Youtube" class="form-control" name="details[instagram_url]" value="{{ $details?$details->instagram_url:'' }}"> </div>
                                <div class="form-group">
                                    <label class="control-label">LinkedIn</label>
                                    <input type="text" id="text" placeholder="LinkedIn" name="details[linkedin_url]" class="form-control" value="{{ $details?$details->linkedin_url:'' }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Google +</label>
                                    <input type="text" id="text" placeholder="Google +" name="details[google_plus_url]" class="form-control" value="{{ $details?$details->google_plus_url:'' }}">
                                </div>
                            </div>
                            <!-- END PRIVACY SETTINGS TAB -->
                        </div>
                        <button type="submit" class="btn blue">Save</button>
                        <button type="reset" class="btn red">Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->