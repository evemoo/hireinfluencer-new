@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ customer_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ customer_trans($trans_path.'page.index.description') }}" name="description" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/css/profile.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/css/bootstrap-fileinput.css') }}">
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('customer.dashboard') }}">
                        {{ customer_trans($trans_path.'content.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{--{{ route($base_route.'.index') }}--}}">
                        {{ customer_trans($trans_path.'content.user') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ customer_trans($trans_path.'content.profile') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-12">
            	@include('backend.common.flash')
            	@if(count($errors) > 0)
            		@foreach($errors as $error)
            			{{ $error }}
            		@endforeach
            	@endif
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light profile-sidebar-portlet ">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img src="{{ asset('img/profile/'.$profile->avatar()) }}" class="img-responsive avtr" alt=""> </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"> {{ $profile->first_name.' '.$profile->last_name }} </div>
                            <div class="profile-usertitle-job"> {{ $profile->username }} </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="">
                                    <a href="javascript:;">
                                        <i class="fa fa-cog"></i> Account Settings </a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->
                    <div class="portlet light ">
                        <!-- STAT -->
                        <div class="row list-separated profile-stat">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="uppercase profile-stat-title"> {{ $stats['transaction'] }} </div>
                                <div class="uppercase profile-stat-text"> Transactions </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="uppercase profile-stat-title"> {{ $stats['order'] }} </div>
                                <div class="uppercase profile-stat-text"> Orders </div>
                            </div>
                            {{-- <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title"> 61 </div>
                                <div class="uppercase profile-stat-text"> Uploads </div>
                            </div> --}}
                        </div>
                        <!-- END STAT -->
                        <div>
                            <h4 class="profile-desc-title">Contact {{ $configs['company'][ConfigHelper::evemooConfig('lang.default')] }}</h4>
                            {{-- <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span> --}}
                            <div class="margin-top-20 profile-desc-link">
                                <i class="fa fa-globe"></i>
                                <a href="{{ $configs['domain'] }}" target="_blank">{{ $configs['domain'] }}</a>
                            </div>
                            @foreach($configs['social_links'] as $social)
                                @if($social['link'] != '')
                                    <div class="margin-top-20 profile-desc-link">
                                        {!! $social['icon'] !!}
                                        <a href="{{ $social['link'] }}">{{ $social['title'] }}</a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                @include($view_path.'.includes.form')
            </div>
        </div>

    </div>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ asset('assets/backend/js/bootstrap-fileinput.js') }}"></script>

    <script type="text/javascript">
    	//$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });

    	$('.avatar').on('change', function(){
    		var $this = $(this);
    		var file_data = $this.prop('files')[0];
	        var form_data = new FormData();
	        form_data.append("image", file_data);
	        form_data.append("_token", '{{ csrf_token() }}');
	        $.ajax({
	            url: '{{ route($base_route.'.updateAvatar') }}',
	            method: 'POST',
	            data: form_data,
	            contentType: false,
	            processData: false,
	            success: function (response) {
	                var data = $.parseJSON(response);
	                console.log(data);
	                $('.avtr').attr('src', data);
	            }
	        });
    	});
    </script>

@endsection
