@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ customer_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ customer_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('content')

    <div class="page-content">
        
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ route('customer.dashboard') }}">{{ customer_trans($trans_path.'content.common.dashboard') }}</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ route($base_route.'.index') }}">{{ customer_trans($trans_path.'content.common.title') }}</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>{{ customer_trans($trans_path.'content.list.list') }}</span>
                    </li>
                </ul>
                <div class="page-toolbar"></div>
            </div>
            
            <div class="row">

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">

                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">
                                {{ customer_trans($trans_path.'content.list.description') }}
                            </span>
                        </div>


                        <div class="actions">

                            <div class="btn-group btn-group-devided" data-toggle="buttons">

                                <a href="{{ route($base_route.'.create') }}"
                                   onclick="location.href = '{{ route($base_route.'.create') }}'">

                                    <button type="button"
                                            class="btn btn-primary mt-ladda-btn ladda-button btn-circle"
                                            data-style="expand-right">
                                        <span class="ladda-label">
                                            <i class="fa fa-plus-circle"></i>
                                            {{ customer_trans($trans_path.'button.add') }}
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </a>
                            </div>

                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            @include('backend.common.flash')

                            <table class="table table-hover table-light">
                                <thead class="flip-content">
                                <tr>
                                    <th width="10%"> {{ customer_trans($trans_path.'content.list.id') }} </th>
                                    <th>{{ customer_trans($trans_path.'content.list.title') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.service') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.deadline') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.message') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.status') }}</th>
                                    <th>{{ customer_trans($trans_path.'content.list.action') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($enquiries->count() > 0)
                                    @foreach($enquiries as $enquiry)
                                        <tr>
                                            <td> {{ $enquiry->enquiry_id }} </td>
                                            <td> {{ $enquiry->subject }} </td>
                                            <td> {{ $enquiry->title }} </td>
                                            <td> {{ $enquiry->deadline }} </td>
                                            <td> {{ substr($enquiry->message, 0, 200) }}  </td>
                                            <td> {!! ViewHelper::getEnquiryStatus($enquiry->status) !!} </td>
                                            <td>
                                                <a href="{{ route($base_route.'.edit', $enquiry->enquiry_id) }}"
                                                   class="btn btn-outline btn-circle btn-sm blue">
                                                    <i class="fa fa-pencil"></i>
                                                    {{ customer_trans($trans_path.'content.edit.list') }}
                                                </a>

                                                <a href="{{ route($base_route.'.view', $enquiry->enquiry_id) }}"
                                                   class="btn btn-outline btn-circle btn-sm purple">
                                                    <i class="fa fa-eye"></i>
                                                    {{ customer_trans($trans_path.'content.view.list') }}
                                                </a>

                                                <a data-href="{{ route($base_route.'.delete', $enquiry->id) }}"
                                                   class="btn btn-outline btn-circle btn-sm red  delete-record-warning">
                                                    <i class="fa fa-trash"></i>
                                                    {{ customer_trans($trans_path.'content.delete.list') }}
                                                </a>

                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="6">{{ $enquiries->appends(request()->all())->render() }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="6">{{ customer_trans($trans_path.'error.no-data-found') }}</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>


        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
    </div>

@endsection

@section('script')

    <script>
        $('.ui .dropdown').dropdown();

        function changePackgesAsPackageType() {

        }

        $(document).on('click', '.delete-record-warning', function (e) {
            e.preventDefault();

            var url = $(this).attr('data-href');

            swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                },
                function (isConfirm) {

                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            headers: {
                                'X-CSRF-Token': '{{ csrf_token() }}'
                            },
                            success: function (data) {
                                swal({
                                    title: "Deleted!",
                                    text: "The user is successfully deleted!",
                                    type: "success",
                                    timer: 5000
                                });

                                window.location.replace("{{ route($base_route.'.index') }}");
                            },
                        });
                    } else {
                    }

                });
        });


    </script>

@endsection
