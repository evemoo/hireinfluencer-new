@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ customer_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ customer_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('style')

    <link href="{{ asset('assets/backend/css/blog.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ customer_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">
                        {{ customer_trans($trans_path.'content.common.title') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ customer_trans($trans_path.'content.view.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-title"> {{ customer_trans($trans_path.'content.common.title') }}
                    <small> > {{ customer_trans($trans_path.'content.view.list') }} </small>
                </h1>
            </div>
        </div>

        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">

            <div class="blog-page blog-content-2">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="blog-single-content bordered blog-container">
                            <div class="blog-single-head">
                                <h1 class="blog-single-head-title">{{ $enquiry->subject }}</h1>
                                <div class="blog-single-head-date">
                                    <i class="fa fa-calendar font-blue"></i>
                                    <a href="javascript:;">{{ ViewHelper::getHumanReadableDate($enquiry->created_at) }}</a>
                                </div>
                            </div>
                            <div class="blog-single-desc">
                                <p><strong>{{ customer_trans($trans_path.'content.common.service') }}: </strong>{{ $enquiry->title }}</p>
                                <p>{{ $enquiry->message }}</p>
                            </div>
                            <hr>
                            @include('backend.common.discussion.discussion')
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="blog-single-sidebar bordered blog-container">
                            <div class="blog-single-sidebar-recent">
                                <h3 class="blog-sidebar-title uppercase">{{ customer_trans($trans_path.'content.view.enquiry-by') }}</h3>
                                <p><strong>{{ customer_trans($trans_path.'content.view.user') }}: </strong>{{ $enquiry->user->username }}</p>
                                <p><strong>{{ customer_trans($trans_path.'content.common.deadline') }}: </strong>{{ ViewHelper::getHumanReadableDate($enquiry->deadline) }}</p>
                                <p><strong>{{ customer_trans($trans_path.'content.list.status') }}: </strong>{!! ViewHelper::getEnquiryStatus($enquiry->status) !!}</p>
                            </div>
                            <div class="blog-single-sidebar-recent">
                                <h3 class="blog-sidebar-title uppercase">{{ customer_trans($trans_path.'content.view.enquiry-suggestions') }}</h3>
                                    <hr>
                                @foreach($packages as $key => $package)
                                <div>
                                    <h4><strong>{{ customer_trans($trans_path.'content.view.enquiry-suggestion.title') }} {{ ++$key }}</strong></h4>
                                    <p><strong>{{ customer_trans($trans_path.'content.view.enquiry-suggestion.price') }} </strong>{{ ConfigHelper::defaultCurrency().$package->new_price }} {{ customer_trans($trans_path.'content.view.enquiry-suggestion.price-per') }} {{ $package->package_types->slug }}</p>
                                    <p><strong>{{ customer_trans($trans_path.'content.view.enquiry-suggestion.duration') }} </strong>{{ $package->delivery_in_min }} - {{ $package->delivery_in_max }} {{ customer_trans($trans_path.'content.view.enquiry-suggestion.duration-days') }}</p>
                                    {{ customer_trans($trans_path.'content.common.qty') }}
                                    {!! Form::number('qty', 1, [
                                           'attr-service-id' => $package->id,
                                           'placeholder'     => customer_trans($trans_path.'content.common.qty'),
                                           'class'           => 'changeable_qty form-control custom_qty_'.$package->id,
                                           'min'             => 1,
                                           'id'              => "custom_package_qty_".$package->id
                                     ]) !!}
                                    <em style="color: red;" id="custom_qty_{{ $package->id }}"></em>

                                    {{ customer_trans($trans_path.'content.common.account_url') }}
                                    {!! Form::text('account_url', null, [
                                           'id'          => 'custom_package_account_url_'.$package->id,
                                           'placeholder' => customer_trans($trans_path.'content.common.account_url'),
                                            'class'      => 'form-control'
                                     ]) !!}
                                    <em style="color: red;" id="custom_account_url_{{ $package->id }}"></em>
                                    <p><button class="btn btn-success">{{ customer_trans($trans_path.'button.accept') }}</button></p>
                                </div>
                                    <hr>
                                @endforeach
                                <p><button class="btn btn-danger cancel" dhref="{{ route('cancel.enquiry', $enquiry->enquiry_id) }}">{{ customer_trans($trans_path.'button.cancel') }}</button></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->

    </div>

@endsection

@section('script')


    @include('backend.common.discussion.discussion-js')


    <script type="text/javascript">
        
        ( function () {
            $(document).on('click', '.cancel', function (e) {

                var url = $(this).attr('dhref');

                    $.ajax({
                        url: url,
                        method: "POST",
                        headers: {
                            'X-CSRF-Token': '{!! csrf_token() !!}'
                        },
                        success: function (response) {
                            window.location = '{{ route('customer.enquiry.index') }}';
                        },
                    });
                });
        })();

    </script>


@endsection
