@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ customer_trans($trans_path.'page.create.title') }}</title>
    <meta content="{{ customer_trans($trans_path.'page.create.description') }}" name="description" />
@endsection

@section('style')

   <style>
       .has-error {
           color: red;
       }

       .dropdown.optgroup .divider {
           border-top: none !important;
       }
   </style>
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('customer.dashboard') }}">{{ customer_trans($trans_path.'content.common.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ customer_trans($trans_path.'content.common.title') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ customer_trans($trans_path.'content.add.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-title"> {{ customer_trans($trans_path.'page.create.head') }}
                    <small> > {{ customer_trans($trans_path.'page.create.sub-head') }} </small>
                </h1>
            </div>
        </div>

        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">

            {!! Form::open(['route' => $base_route.'.store', 'method' => 'post', 'id'=> 'enquiry_store_form']) !!}

            <div class="portlet-body">

                <div class="ui form">

                    @include($view_path.'.partials._form')

                    <div class="ui primary button save-enquiry" tabindex="0">{{ customer_trans($trans_path.'button.add') }}</div>
                    <div class="ui error message"></div>
                </div>

            </div>

            {!! Form::close() !!}

        </div>

        <div class="clearfix"></div>


    </div>
@endsection

@section('script')

    <script>

        (function () {

            $('.service_id_dropdown')
                .dropdown({
                    apiSettings: {
                        maxSelections: 3,
                        url: '{{ URL::to('/')  }}'+'/api/admin/service/{query}'
                    },
                  });

            $('.save-enquiry').click(function () {
                $('#enquiry_store_form').submit();
            });

            $('#enquiry_store_form')
                .form({
                    on: 'blur',
                    fields: {
                        subject: {
                            identifier: 'subject',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Subject is required'
                                }
                            ]
                        },
                        service_id: {
                            identifier: 'service_id',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Platform must be selected'
                                }
                            ]
                        },
                        deadline: {
                            identifier: 'deadline',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Deadline must be specified'
                                }
                            ]
                        },
                        message: {
                            identifier: 'message',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Message is required'
                                }
                            ]
                        }

                    },

                    onInvalid: function (message) {

                        if(message[0]) {

                            $(this)
                                .next()
                                .remove();
                            $(this).after('<div class="ui basic red pointing prompt label transition visible">'
                                         + message[0]+
                                      '</div>');

                            $('.error-showable-data')
                                .removeClass('invisible')
                                .addClass('visible')
                                .html('')
                                .html('Please Check Something is missing !!');
                        }

                    },
                    onValid: function () {
                        $(this).next().remove();
                    }
                });

        })();



    </script>

@endsection

