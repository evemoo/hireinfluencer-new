    <div class="field">

        <label> {{ customer_trans($trans_path.'content.common.subject') }} </label>
        {!! Form::text('subject',
            null,
         [
          'id' => 'subject',
          'placeholder' => 'Enquiry Subject'
          ]) !!}
    </div>

<div class="two fields">

    <div class="field">
        <label>{{ customer_trans($trans_path.'content.common.service') }}</label>
        <select name="service_id" class="ui fluid search dropdown service_id_dropdown" id="service_id">
            <option class="active selected"
                    value="{{ isset($enquiry)?$enquiry->service_id:'' }}">
                @if(isset($enquiry))
                    {{ ViewHelper::service($enquiry->service_id) }}
                @endif

            </option>
        </select>
    </div>

    <div class="field">

        <label>{{ customer_trans($trans_path.'content.common.deadline') }}</label>
        {!! Form::text('deadline',
           null,
         ['id' => 'deadline', 'class' => 'datepicker']) !!}

    </div>

</div>

    <div class="field">
        <label>{{ customer_trans($trans_path.'content.common.message') }}</label>
        {!! Form::textarea('message', null, [
            'id'          => 'message',
            'placeholder' => 'What do you want?'
        ]) !!}

        @if (count($errors) > 0 && $errors->has('default.qty'))
            <p class="help-block has-error">{{ $errors->first('default.qty') }}</p>
        @endif
    </div>

