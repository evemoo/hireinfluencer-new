@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('content')
    <div class="page-content">


        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">{{ admin_trans($trans_path.'content.common.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('admin.log.index') }}">{{ admin_trans($trans_path.'content.common.log') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

      <br />

        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">{{ admin_trans($trans_path.'content.list.description') }}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            <table class="table table-hover table-light">
                                <thead>
                                <tr>
                                    <th>{{ admin_trans($trans_path.'content.common.error_no') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.common.error_code') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.common.error_process_status') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.common.error_level') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.list.action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($logs as $log)
                                    <tr>
                                        <td>{{ $log->error_no }}</td>
                                        <td>{{ $log->error_code }}</td>
                                        <td>{{ ConfigHelper::evemooConfig('log.error_process_status.'.$log->error_process_status.'.title') }}</td>
                                        <td>{{ $log->error_level }}</td>
                                        <td>
                                            <a href="{{ route('admin.log.view', $log->id) }}"
                                               class="btn btn-outline btn-circle btn-sm blue">
                                                <i class="fa fa-eye"></i> {{ admin_trans($trans_path.'button.view') }} </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

    </div>
@endsection