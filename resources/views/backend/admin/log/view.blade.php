@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.view.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.view.description') }}" name="description" />
@endsection

@section('content')
    <div class="page-content">


        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">{{ admin_trans($trans_path.'content.common.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('admin.log.index') }}">{{ admin_trans($trans_path.'content.common.log') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

      <br />

        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">{{ admin_trans($trans_path.'content.list.description') }}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            @include('backend.common.flash')
                            <div class="row">

                                <div class="col-lg-8">
                                    <ul class="list-group">
                                        <li class="list-group-item">{{ admin_trans($trans_path.'content.common.error_no') }}   : {{ $log->error_no }}</li>
                                        <li class="list-group-item">{{ admin_trans($trans_path.'content.common.error_code') }} : {{ $log->error_code }}</li>
                                        <li class="list-group-item">{{ admin_trans($trans_path.'content.common.error_process_status') }} : {{ ConfigHelper::evemooConfig('log.error_process_status.'.$log->error_process_status.'.title') }}</li>
                                        <li class="list-group-item">{{ admin_trans($trans_path.'content.common.error_level') }} : {{ $log->error_level }}</li>
                                        <li class="list-group-item">{{ admin_trans($trans_path.'content.common.error_msg') }} : <br />
                                            &nbsp;&nbsp;
                                            &nbsp;&nbsp;
                                            &nbsp;&nbsp;
                                            <code>{{ $log->error_msg }}</code>
                                        </li>

                                    </ul>
                                </div>

                                <div class="col-lg-4">

                                    <form action="{{ route('admin.log.update', $log->id ) }}" method="POST">
                                        {!! csrf_field() !!}
                                        <ul class="list-group">
                                            <li class="list-group-item">

                                                <div class="field">
                                                    <label>{{ admin_trans($trans_path.'content.view.status') }}</label>
                                                    <div class="ui dropdown selection">
                                                        <input type="hidden" name="status">
                                                        <div class="text">
                                                            {{ $log->error_process_status?ConfigHelper::evemooConfig('log.error_process_status.'.$log->error_process_status.'.title'):'Select Status' }}
                                                        </div>
                                                        <i class="dropdown icon"></i>
                                                        <div class="menu">
                                                            @foreach($error_process_status as $status)
                                                                <div class="item {{ $log->error_process_status == $status['key']?'active selected':'' }}"
                                                                     data-value="{{ $status['key'] }}">
                                                                    {{ $status['title'] }}
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>

                                            </li>
                                            <li class="list-group-item">
                                                <button type="submit" class="btn btn-primary">{{ admin_trans($trans_path.'button.update') }}</button>
                                            </li>
                                        </ul>

                                    </form>


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

    </div>
@endsection

@section('script')

    <script>
        $('.ui.dropdown').dropdown();
    </script>

@endsection