@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.description') }}" name="description" />
@endsection

@section('style')
    <link href="{{ asset('panel/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')


    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">{{ admin_trans($trans_path.'content.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('admin.email_template.index') }}">{{ admin_trans($trans_path.'content.setting') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> {{ admin_trans($trans_path.'content.setting') }}</h1>
        <!-- END PAGE TITLE-->

        <!-- BEGIN DASHBOARD STATS 1-->
        <div class="row">

            <div class="col-md-8">
                {{--@if($errors->all())--}}
                    {{--@foreach ($errors->all() as $error)--}}
                        {{--<code>{{ $error }}</code>--}}
                    {{--@endforeach--}}
                {{--@endif--}}
                {!! Form::model($data['row'], ['route' => [$base_route.'.update', $data['row']->id], 'method' => 'post']) !!}

                {!! Form::hidden('id', $data['row']->id) !!}
                    @include($view_path.'.includes._form')
                {!! Form::close() !!}
            </div>

            <div class="col-md-4">
                @include($view_path.'.includes._pattern')
            </div>

        </div>
        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
    </div>
@endsection

@section('script')
    <script src="{{ asset('panel/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('panel/assets/pages/scripts/components-editors.min.js') }}"></script>
@endsection
