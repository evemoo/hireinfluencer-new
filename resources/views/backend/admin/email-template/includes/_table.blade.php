<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
            <thead>
            <tr>
                {{--<th>ID</th>--}}
                <th>Used For</th>
                <th >Template Name/Slug</th>
                <th><i class="fa fa-cog"></i>&nbsp; Control</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($data['rows'] as $data)
                <tr class="odd gradeX">
{{--                    <td>{{ $data->id }}</td>--}}
                    <td width="40%;"><b>{{ $data->title }}</b></td>
                    <td>
                        <b>{{ $data->slug }}</b>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route($base_route.'.edit', ['attribute' => $data->id]) }}">
                                        <i class="fa fa-pencil"></i> Edit Template </a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">No Email Template Found.</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->