<div class="ui form">

    <div class="two fields">

        <div class="field">

            <label>{{ admin_trans($trans_path.'content.title') }}</label>
            {!! Form::text('title',
                 null,
                 [
                    'id' => 'title',
                    'placeholder' => 'Title for the email template'
                 ])
            !!}
        </div>


        <div class="field">

            <label>
                {{ admin_trans($trans_path.'content.slug') }}
                <small>&nbsp;('Used to identify the template')</small>
            </label>
            {!! Form::text('slug',
                    null,
                    [
                        'id'          => 'slug',
                        'placeholder' => 'Slug for the email template',
                        'disabled'    => 'true',
                        'readonly'    => 'true',
                        'style'       => 'opacity: 0.6;'
                    ])
            !!}
        </div>

    </div>

    <div class="field">

        <label>
            {{ admin_trans($trans_path.'content.description') }}
        </label>

        {!! Form::textarea('content', null,
            ['placeholder' => 'Enter Content', 'class' => 'form-control description', 'id' => 'summernote_1']) !!}
    </div>


</div>

<footer>
    <button type="submit" class="btn btn-primary">Submit</button>
    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
</footer>