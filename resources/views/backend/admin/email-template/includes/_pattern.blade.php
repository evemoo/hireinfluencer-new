<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-edit font-dark"></i>
            <span class="caption-subject font-dark bold uppercase"><strong>Pattern</strong> You Must Use</span>
        </div>
    </div>
    <div class="portlet-body">
        @forelse ($data['patterns'] as $pattern)
            <p><b>{{ $pattern->etp_pattern }}</b></p>
            <p><small>{{ $pattern->etp_label }}</small></p>
            <hr>
        @empty
            <p>No pattern added.</p>
        @endforelse
    </div>
</div>