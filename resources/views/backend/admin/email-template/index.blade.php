@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.description') }}" name="description" />
@endsection

@section('style')
    <link href="{{ asset('panel/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('panel/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')


    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">{{ admin_trans($trans_path.'content.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('admin.email_template.index') }}">{{ admin_trans($trans_path.'content.setting') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> {{ admin_trans($trans_path.'content.setting') }}</h1>
        <!-- END PAGE TITLE-->

        <!-- BEGIN DASHBOARD STATS 1-->
        <div class="row">

            <div class="col-md-12">
                @include($view_path.'.includes._table')
            </div>

        </div>
        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
    </div>
@endsection

@section('script')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('panel/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('panel/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('panel/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('panel/assets/pages/scripts/table-datatables-managed.min.js') }}"></script>
@endsection
