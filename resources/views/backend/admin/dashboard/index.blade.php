@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.description') }}" name="description" />
@endsection

@section('content')

    <div class="page-content">

		<div class="page-bar">
	        <ul class="page-breadcrumb">
	            <li>
	                <a href="{{ route('admin.dashboard') }}">
	                    {{ admin_trans($trans_path.'content.dashboard') }}
	                </a>
	                <i class="fa fa-circle"></i>
	            </li>
	        </ul>
	        <div class="page-toolbar"></div>
	    </div>
        <!-- BEGIN PAGE TITLE-->
        <div>
            <h1 class="page-title"> {{ admin_trans($trans_path.'content.dashboard') }}</h1>

            <div class="actions pull-right">

                <div class="ui dropdown selection">
                    <input type="hidden" class="lang onchange_lang_redirection"  name="lang">
                    <div class="text">
                        @if(request('lang'))
                            {{ ConfigHelper::evemooConfig('lang.options.'.request('lang').'.title') }}
                        @else
                            {{ ConfigHelper::evemooConfig('lang.options.'.ConfigHelper::evemooConfig('lang.default').'.title') }}
                        @endif
                    </div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        @foreach(ConfigHelper::evemooConfig('lang.options') as $options)
                            <div class="item" {{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'active selected':'' }}
                                 data-value="{{ $options['code'] }}">{{ $options['title'] }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE TITLE-->

        <!-- BEGIN DASHBOARD STATS 1-->
        <div class="row">
            <div>
                <h1># of Social Medias</h1>
                <table class="table">
                    <thead>
                        <th>#</th>
                        <th>Name</th>
                        <th>Icon</th>
                    </thead>
                    <tbody>
                        @foreach($social_medias as $key => $media)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $media->name }}</td>
                                <td><i class="{{ $media->icon }}"></i>{{ $media->icon }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
    </div>
@endsection

@section('script')
    <script>

        (function () {

            $('.onchange_lang_redirection').change(function () {
                var lang =  $(this).val();

                location.href = '{{ URL::current() }}'+'?lang='+lang;
            })

            $('.ui.dropdown').dropdown();

        })();

    </script>
@endsection
