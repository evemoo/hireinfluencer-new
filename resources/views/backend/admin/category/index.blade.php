@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">
                        {{ admin_trans($trans_path.'content.common.category') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br />

        <div class="row">

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">

                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">
                                {{ admin_trans($trans_path.'content.list.description') }}
                            </span>
                        </div>

                        <div class="actions">

                            <div class="ui dropdown selection">
                                <input type="hidden" class="lang onchange_lang_redirection"  name="lang">
                                <div class="text">
                                    @if(request('lang'))
                                        {{ ConfigHelper::evemooConfig('lang.options.'.request('lang').'.title') }}
                                    @else
                                        {{ ConfigHelper::evemooConfig('lang.options.'.ConfigHelper::evemooConfig('lang.default').'.title') }}
                                    @endif
                                </div>
                                <i class="dropdown icon"></i>
                                <div class="menu">
                                    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)
                                        <div class="item" {{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'active selected':'' }}
                                             data-value="{{ $options['code'] }}">{{ $options['title'] }}
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                @if($type == null)
                                    <a href="{{ route($base_route.'.create') }}" onclick="location.href = '{{ route($base_route.'.create') }}'">

                                        <button type="button"
                                                class="btn btn-primary mt-ladda-btn ladda-button btn-circle"
                                                data-style="expand-right">
                                            <span class="ladda-label">
                                                <i class="fa fa-plus-circle"></i>
                                                {{ admin_trans($trans_path.'button.create') }}
                                            </span>
                                            <span class="ladda-spinner"></span>
                                        </button>
                                    </a>
                                @else
                                    <a href="{{ route('admin.category.level2.create', [$data['category']->id, $type]) }}" onclick="location.href = '{{ route('admin.category.level2.create', [$data['category']->id, $type]) }}'">

                                        <button type="button"
                                                class="btn btn-primary mt-ladda-btn ladda-button btn-circle"
                                                data-style="expand-right">
                                            <span class="ladda-label">
                                                <i class="fa fa-plus-circle"></i>
                                                {{ admin_trans($trans_path.'button.create') }}
                                            </span>
                                            <span class="ladda-spinner"></span>
                                        </button>
                                    </a>
                                @endif
                            </div>

                        </div>

                    </div>

                    @include('backend.common.flash')

                    <div class="portlet-body">
                        <div class="table-scrollable">

                            <table class="table table-hover table-light">
                                <thead>
                                    <tr>
                                        <th style="width:11%"><i class="fa fa-hashtag"></i></th>
                                        <th style="width:45%">{{ admin_trans($trans_path.'content.common.title') }}</th>
                                        <th style="width:15%">{{ admin_trans($trans_path.'content.common.slug') }}</th>
                                        <th style="width:10%">{{ admin_trans($trans_path.'content.common.status') }}</th>
                                        <th style="width:15%"><i class="fa fa-cogs"></i></th>
                                    </tr>
                                    <tr>
                                        <th>
                                            {!! Form::number('id', request('id'), ['class' => 'form-control', 'placeholder' => admin_trans($trans_path.'content.list.id'), 'id' => 'filter_by_id']) !!}
                                        </th>
                                        <th data-class="expand">
                                            {!! Form::text('title', request('title'), ['class' => 'form-control', 'placeholder' => admin_trans($trans_path.'content.list.title'), 'id' => 'filter_by_title']) !!}
                                        </th>
                                        <th >
                                            {!! Form::text('slug', request('slug'), ['class' => 'form-control', 'placeholder' => admin_trans($trans_path.'content.list.slug'), 'id' => 'filter_by_slug']) !!}
                                        </th>
                                        <th>
                                            {!! Form::select('status', ['all' => 'All','1' => 'Active', '0' => 'Inactive'], request('status'), ['id' => 'filter_by_status', 'class' => 'form-control']) !!}
                                        </th>
                                        <th>
                                            {{ Form::button(admin_trans($trans_path.'content.list.search'), ['id' => 'filter_btn', 'class' => 'btn btn-warning btn-sm'] )  }}
                                        </th>
                                    </tr>
                                </thead>

                                <tbody id="sortable">

                                @forelse($data['rows'] as $row)
                                    <tr>
                                        <td>{{ $row->id }}</td>
                                        <td>{{ $row->title }}{{ $row->getCountAttribute() }}
                                            <span class="pull-right">
                                                @if($type == 'level2')

                                                    <a href="{{ route('admin.category.level3.index', ['cat_id' => $row['id'], 'type' => 'level3']) }}" class="btn btn-primary btn-xs">
                                                    <i class="fa fa-eye"></i>&nbsp;Sub Categories</a>
                                                    
                                                @elseif ($type == 'level3')

                                                    <a href="{{ route('admin.category.level4.index', ['cat_id' => $row['id'], 'type' => 'level4']) }}" class="btn btn-primary btn-xs">
                                                    <i class="fa fa-eye"></i>&nbsp;Sub Categories</a>

                                                @elseif ($type == null)

                                                    <a href="{{ route('admin.category.level2.index', ['cat_id' => $row['id'], 'type' => 'level2']) }}" class="btn btn-primary btn-xs">
                                                    <i class="fa fa-eye"></i>&nbsp;{{ admin_trans($trans_path.'content.common.sub-categories') }}</a>
                                                @endif
                                            </span>
                                        </td>
                                        <td>{{ $row->slug }}</td>
                                        <td>
                                            @if ($row->status == 1)
                                                <span class="center-block padding-5 label label-success">{{ admin_trans($trans_path.'content.common.active') }}</span>
                                            @else
                                                <span class="center-block padding-5 label label-danger">{{ admin_trans($trans_path.'content.common.inactive') }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            <nav>
                                                @if($type != null)
                                                    <a href="{{ route('admin.category.'.$type.'.edit', [$row->id, $data['category']->id, $type]) }}" class="btn btn-outline btn-circle btn-sm blue">
                                                        <em class="fa fa-pencil fa-1"></em>{{ admin_trans($trans_path.'content.list.edit') }}</a>
                                                @else
                                                    <a href="{{ route('admin.category.edit', $row->id) }}" class="btn btn-outline btn-circle btn-sm blue">
                                                        <em class="fa fa-pencil fa-1"></em>{{ admin_trans($trans_path.'content.list.edit') }}</a>
                                                @endif
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <a  class="btn btn-outline btn-circle btn-sm red" id="delete-item" data-id="{{$row->id}}" data-href="{{ route($base_route.'.delete', $row->id) }}">
                                                    <em class="fa fa-trash-o fa-1"></em>{{ admin_trans($trans_path.'content.list.delete') }}</a>
                                            </nav>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">
                                                @if($type != null)
                                                    No {{ ucfirst($type) }} found.<a href="{{ route('admin.category.'.$type.'.create', ['cat_id' => $data['cat_id'], 'type' => 'level2']) }}">click here to add a new {{ strtolower($type) }}.</a>
                                                @else
                                                    No {{ ucfirst($type) ? ucfirst($type) : 'Category' }} found.<a href="{{ route('admin.category.create') }}">click here to add a new {{ strtolower($type) ? strtolower($type) : 'category' }}.</a>
                                                @endif
                                        </td>
                                    </tr>
                                @endforelse

                                </tbody>

                            </table>

                            <nav>
                                {!! $data['rows']->links() !!}
                            </nav>
                        </div>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>

        </div>

@endsection

@section('script')
    <!-- Select2 4.0.0 -->
    {{-- <script src="{{ asset(ConfigHelper::getConfigByKey('evemoo.asset.admin.js'). 'libs/select2/js/select2.min.js') }}"></script> --}}
    <script>

        (function () {

            $('.onchange_lang_redirection').change(function () {
                var lang =  $(this).val();

                location.href = '{{ URL::current() }}'+'?lang='+lang;
            })

            $('.ui.dropdown').dropdown();

        })();

    </script>

    <script type="text/javascript">
        (function () {

            $(document).ready(function() {
                $(".select-global-perm").select2();
                $(".select-perms").select2();
            });
            // advance search
            $(document).ready(function () {
                $(function() {
                    $('#filter_btn').click(function () {

                        var url     = '{{ Request::url() }}';
                        var id      = $('#filter_by_id').val();
                        var title   = $('#filter_by_title').val();
                        var slug    = $('#filter_by_slug').val();
                        var status  = $('#filter_by_status').val();

                        url = url + '?filter=show';

                        if (id ) {
                            url = url + '&id=' + id;
                        }
                        if (title ) {
                            url = url + '&title=' + title;
                        }
                        if (slug ) {
                            url = url + '&slug=' + slug;
                        }
                        if (status ) {
                            url = url + '&status=' + status;
                        }
                        location.href = url;
                    });
                });
            });

        })();

    </script>

    @include('backend.admin.common.delete-js')
@endsection
