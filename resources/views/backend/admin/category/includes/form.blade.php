<div class="ui form">

    <input name="default[parent_id]" type="hidden" value="{{ $cat_id }}">
    <input name="type" type="hidden" value="{{ $type }}">
    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)
        <div class="field hide-this-field"
            id="title_{{ $options['code'] }}"
            style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">
            <label>{{ admin_trans($trans_path.'content.add.title') }}</label>
            <label class="input {{ $errors->has($options['code'].'.title') ? ' state-error' : '' }}">
                {!! Form::text($options['code'].'[title]', isset($trans_cat[$options['code']][0]->title)?$trans_cat[$options['code']][0]->title:null, ['id' => 'title', 'class' => 'title']) !!}
            </label>
            @if ($errors->has($options['code'].'.title'))
                <div class="note note-error">{{ $errors->first($options['code'].'.title') }}</div>
            @endif
        </div>
    @endforeach

    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)
        <div class="field hide-this-field"
            id="description_{{ $options['code'] }}"
            style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">
            <label>{{ admin_trans($trans_path.'content.add.description') }}</label>
            <label class="input {{ $errors->has($options['code'].'.description') ? ' state-error' : '' }}">
                {!! Form::textarea($options['code'].'[description]', isset($trans_cat[$options['code']][0]->description)?$trans_cat[$options['code']][0]->description:null, ['id' => 'description', 'class' => 'form-control']) !!}
            </label>
            @if ($errors->has($options['code'].'.description'))
                <div class="note note-error">{{ $errors->first($options['code'].'.description') }}</div>
            @endif
        </div>
    @endforeach

    <div class="field">
        <label>{{ admin_trans($trans_path.'content.add.status') }}</label>
        <label class="select">
            {!! Form::select('default[status]', ['1' => admin_trans($trans_path.'content.common.active'), '0' => admin_trans($trans_path.'content.common.inactive')], isset($cat->status)?$cat->status:1, []) !!} <i></i> 
        </label>
        @if ($errors->has('default.status'))
            <div class="note note-error">{{ $errors->first('default.status') }}</div>
        @endif
    </div>

    <footer>
        <button type="submit" class="btn btn-primary save-category" name="action" value="gotoParent">Save & Goto Parent</button>
		<button type="submit" class="btn btn-primary save-category" name="action" value="continue">Save & continue</button>
		<button type="submit" class="btn btn-primary save-category" name="action" value="exit">Save</button>
		<a class="btn btn-default" href="{{ Request::server('HTTP_REFERER') }}">Back</a>
    </footer>

</div>