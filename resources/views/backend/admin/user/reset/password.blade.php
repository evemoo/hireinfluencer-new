@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('style')

@endsection

@section('content')
    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">{{ admin_trans($trans_path.'content.common.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index', 'admin') }}">{{ admin_trans($trans_path.'content.common.user') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.reset.password') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> {{ admin_trans($trans_path.'content.common.user') }}
            <small> > {{ admin_trans($trans_path.'content.reset.password') }}</small>
        </h1>

    <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            @include('backend.common.flash')

            {!! Form::open(['route' => 'admin.user.reset.password', 'method' => 'post', 'id'=> 'users_store_form']) !!}
            <div class="portlet-body">

                <div class="field">
                    <strong>{{ admin_trans($trans_path.'content.common.name') }}</strong>: {{ auth()->user()->username }}
                </div>
                <br>
                <div class="field">
                    <strong>{{ admin_trans($trans_path.'content.common.email') }}</strong>: {{ auth()->user()->email }}
                </div>
                <br>
                <div class="ui form">

                    <div class="field">
                        <label>{{ admin_trans($trans_path.'content.common.password') }}</label>
                        {!! Form::password('password', [
                             'id' => 'password',
                             'placeholder' => admin_trans($trans_path.'content.common.password')
                        ]) !!}
                        @if (count($errors) > 0 && $errors->has('password'))
                            <p class="help-block has-error">{{ $errors->first('password') }}</p>
                        @endif
                    </div>
                    <div class="field">
                        <label>{{ admin_trans($trans_path.'content.common.confirm_password') }}</label>
                        {!! Form::password('password_confirmation', [
                            'id'          => 'password_confirmation',
                            'placeholder' => admin_trans($trans_path.'content.common.confirm_password')
                        ]) !!}
                        @if (count($errors) > 0 && $errors->has('confirm_password'))
                            <p class="help-block has-error">{{ $errors->first('confirm_password') }}</p>
                        @endif
                    </div>

                    <div class="ui primary button save-user" tabindex="0">{{ admin_trans($trans_path.'button.reset') }}</div>

                </div>

            </div>


            {!! Form::close() !!}

        </div>

        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->

    </div>
@endsection

@section('script')



    <script>

        (function () {

            $('.save-user').click(function () {
                $('#users_store_form').submit();
            });

            $('#users_store_form')
                .form({
                    on: 'blur',
                    fields: {
                        password: {
                            identifier: 'password',
                            rules: [
                                {
                                    type: 'length[' + 4 + ']',
                                    prompt: 'Password must be at least 4 characters in length'
                                }
                            ]
                        },
                        password_confirmation: {
                            identifier: 'password_confirmation',
                            rules: [
                                {
                                    type: 'match[password]',
                                    prompt: 'Mismatched password'
                                }
                            ]
                        },
                    },
                    onInvalid: function (message) {

                        if(message[0]) {

                            $(this)
                                .next()
                                .remove();
                            $(this).after('<div class="ui basic red pointing prompt label transition visible">'
                                + message[0]+
                                '</div>');

                            $('.error-showable-data')
                                .removeClass('invisible')
                                .addClass('visible')
                                .html('')
                                .html('Please Check Something is missing !!');
                        }

                    },
                    onValid: function () {
                        $(this).next().remove();
                    }
                });

            $('.ui.dropdown').dropdown();

        })();



    </script>

@endsection

