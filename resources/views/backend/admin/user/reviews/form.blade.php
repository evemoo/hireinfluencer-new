<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Edit Review</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{ route($base_route.'.view.review.update', [$user->id, $review->id]) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="tab-content">
                            <!-- BASIC INFO TAB -->
                            <div class="tab-pane active" id="basic">
                                <div class="form-group">
                                    <label class="control-label">Rating</label>
                                    <select name="rating" class="form-control">
                                        @for($i=5; $i>0; $i--)
                                            <option value="{{ $i }}" {{ $i == $review->rating?'selected':'' }}>{{ $i }}</option>
                                        @endfor
                                    </select> 
                                    @if ($errors->has('rating'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('rating') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Comment</label>
                                    <textarea class="form-control" rows="7" name="comment">{{ $review->comment }}</textarea>
                                    @if ($errors->has('comment'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('comment') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Published</label>
                                    <select name="published" class="form-control">
                                        <option value="1" {{ $review->published == 1?'selected':'' }}>Published</option>
                                        <option value="0" {{ $review->published == 0?'selected':'' }}>Draft</option>
                                    </select> 
                                    @if ($errors->has('rating'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('rating') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!-- END BASIC INFO TAB -->
                        </div>
                        <button type="submit" class="btn blue">{{ admin_trans($trans_path.'content.account-setting.button.save') }}</button>
                        <button type="reset" class="btn red">{{ admin_trans($trans_path.'content.account-setting.button.reset') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT