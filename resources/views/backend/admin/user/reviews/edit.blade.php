@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/css/profile.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/css/bootstrap-fileinput.css') }}">
@endsection

@section('content')

    <div class="page-content" style="background: #eef1f5;">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('customer.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{--{{ route($base_route.'.index') }}--}}">
                        {{ admin_trans($trans_path.'content.common.user') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.common.profile') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-12">
                @include('backend.common.flash')
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light profile-sidebar-portlet ">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img src="{{ $user->avatar() }}" class="img-responsive avtr" alt=""> </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"> {{ $user->first_name.' '.$user->last_name }} </div>
                            <div class="profile-usertitle-job"> {{ $user->username }} </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li>
                                    <a href="{{ route($base_route.'.view.overview', $user->id) }}">
                                        <i class="fa fa-home"></i> Overview </a>
                                </li>
                                <li>
                                    <a href="{{ route($base_route.'.view.setting', $user->id) }}">
                                        <i class="fa fa-cog"></i> Account Settings </a>
                                </li>
                                <li>
                                    <a href="{{ route($base_route.'.view.slot', $user->id) }}">
                                        <i class="fa fa-clone"></i> Slots </a>
                                </li>
                                <li class="active">
                                    <a href="{{ route($base_route.'.view.review', $user->id) }}">
                                        <i class="fa fa-send"></i> Reviews </a>
                                </li>
                                <li>
                                    <a href="{{ route($base_route.'.view.help', $user->id) }}">
                                        <i class="fa fa-info-circle"></i> Help </a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->
                    <div class="portlet light ">
                        <!-- STAT -->
                        <div class="row list-separated profile-stat">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="uppercase profile-stat-title"> {{ $social_medias->count() }} </div>
                                <div class="uppercase profile-stat-text"> Social Medias Linked </div>
                            </div>
                        </div>
                        <!-- END STAT -->
                        <div>
                            <h4 class="profile-desc-title">Linked Social Medias </h4>
                            @foreach($social_medias as $social)
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="{{ $social['icon'] }}"></i>
                                    <a href="{{ $social['url'] }}" target="_blank">{{ $social['name'] }}</a>
                                </div>
                            @endforeach
                        </div>
                        <hr>
                        <div>
                            <h4 class="profile-desc-title">Contact {{ $configs['company'][ConfigHelper::evemooConfig('lang.default')] }}</h4>
                            {{-- <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span> --}}
                            <div class="margin-top-20 profile-desc-link">
                                <i class="fa fa-globe"></i>
                                <a href="{{ $configs['domain'] }}" target="_blank">{{ $configs['domain'] }}</a>
                            </div>
                            @foreach($configs['social_links'] as $social)
                                @if($social['link'] != '')
                                    <div class="margin-top-20 profile-desc-link">
                                        {!! $social['icon'] !!}
                                        <a href="{{ $social['link'] }}">{{ $social['title'] }}</a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                @include($view_path.'.reviews.form')
            </div>
        </div>

    </div>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ asset('assets/backend/js/bootstrap-fileinput.js') }}"></script>
@endsection
