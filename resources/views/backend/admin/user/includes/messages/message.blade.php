@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('content')

	<div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">
                        {{ admin_trans($trans_path.'content.common.user') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Message</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>
        <div class="portlet-body" id="chats">
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 500px;"><div class="scroller" style="height: 500px; overflow: hidden; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                <ul class="chats">
                	@foreach($messages->reverse() as $message)
	                    <li class="{{ $message->sender_id == auth()->user()->id?'out':'in' }}">
	                        <img class="avatar" alt="" src="{{ asset('img/profile/avatar.png') }}">
	                        <div class="message">
	                            <span class="arrow"> </span>
	                            <a href="javascript:;" class="name"> {{ $message->sender->username }} </a>
	                            <span class="datetime"> at 20:40 </span>
	                            <span class="body"> {{ $message->message_text }} </span>
	                        </div>
	                    </li>
                    @endforeach
                </ul>
            </div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
            <div class="chat-form">
                <div class="input-cont">
                    <input class="form-control msg" type="text" placeholder="Type a message here..."> </div>
                <div class="btn-cont send" data-href="{{ route($base_route.'.msg.send', $user_id) }}">
                    <span class="arrow"> </span>
                    <a href="javascript:;" class="btn blue icn-only">
                        <i class="fa fa-check icon-white"></i>
                    </a>
                </div>
            </div>
        </div>
	</div>

@endsection

@section('script')

	<script type="text/javascript">
		$('.send').on('click', function(){
			var $this = $(this);
			var msg = $this.parent().find('.msg').val();
			var url = $this.data('href');
			if(msg != '' && msg != undefined && msg != null)
				$.ajax({
					method: 'POST',
					url: url,
					headers: {
	                    'X-CSRF-Token': '{!! csrf_token() !!}'
	                },
	                data: { 'msg' : msg },
	                success: function (response) {
	                    if(response.errors)
	                    {
	                    	console.log(response.errors.msg[0]);
							errorBorder('red');
	                    	
	                    } else {
		                    $this.parent().find('.msg').val('');
		                    $('.chats').append(response);
							errorBorder('green');
						}
	                },
				});
			else {
				errorBorder('red');
			}

			function errorBorder(color) {
				$this.parent().find('.msg').css('border-color', color);
				setTimeout(function() { $this.parent().find('.msg').css('border-color', '') }, 2000);
			}
		});
	</script>

@endsection