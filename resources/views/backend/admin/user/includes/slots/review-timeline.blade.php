<!-- TIMELINE ITEM -->
@if($reviews)
    @foreach($reviews as $review)
        <div class="timeline-item">
            <div class="timeline-badge">
                <img class="timeline-badge-userpic" src="{{ asset('img/profile/avatar.png') }}"> </div>
                <div class="timeline-body">
                    <div class="timeline-body-arrow"> </div>
                    <div class="timeline-body-head">
                        <div class="timeline-body-head-caption">
                            <a href="javascript:;" class="timeline-body-title font-blue-madison">{{ $review->reviewer->username }}</a>
                            <span class="timeline-body-time font-grey-cascade">{{ $review->rating }} <span class="stars">{{ $review->rating }}</span></span>
                        </div>
                        <div class="timeline-body-head-actions">
                            <div class="btn-group">
                                <button class="btn btn-circle green btn-outline btn-sm dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="javascript:;">Action </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Another action </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Something else here </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;">Separated link </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-body-content">
                        <span class="font-grey-cascade"> {{ $review->trans->where('code', config('evemoo.lang.default'))[0]->comment }} </span>
                    </div>
                    @if($review->reply)
                        <div style="padding: 5px 0 0 15px; display: block;">
                            <img src="{{ asset('img/profile/avatar.png') }}" class="circle">
                            <div style="display: inline-block; padding-left: 5px;">
                                <span class="font-blue-madison">{{ $review->influencer_username }}</span><br>
                                <span class="font-grey-cascade">{{ $review->reply->reply_comment }}</span>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
@endif