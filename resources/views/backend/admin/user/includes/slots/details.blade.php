<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-4">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">{{ $slot->title }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="text-center">
                        <img src="{{ $slot->banner }}">
                    </div>
                    <div>
                        <span class="caption-subject font-blue-madison bold uppercase">Packages</span>
                    </div>
                    @if(isset($packages))
                        <table class="table">
                            <thead>
                                <th>Social Media</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Delivery In</th>
                                <th>Audience Size</th>
                                <th></i></span></th>
                            </thead>
                            <tbody>
                                @foreach($packages as $package)
                                <tr>
                                    <td>{{ $slot->name }}</td>
                                    <td>{{ $package->title }}</td>
                                    <td>{{ '$'.$package->price }}</td>
                                    <td>{{ $package->delivery_in_days.' days' }}</td>
                                    <td>{{ $package->audience_size }}</td>
                                    <td><span class="font-blue-madison more-info" onmouseover="this.style.cursor='pointer'" data-toggle="modal" data-target="#modal-{{ $package->id }}"><i class="fa fa-info-circle fa-lg"></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="col-md-12 text-center">
                            <h2>This Slot does not have a Package</h2>
                        </div>
                    @endif
                    <br>
                    <div class="text-center">
                        {{ $slot->description }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Financial</span>
                    </div>
                </div>
                <div class="portlet-body">
                    
                    <div class="row number-stats margin-bottom-30">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="stat-left">
                                <div class="stat-chart">
                                    <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                    <div id="sparkline_bar"></div>
                                </div>
                                <div class="stat-number">
                                    <div class="title"> Influencers Total </div>
                                    <div class="number"> {{ '$'.$slot_finance['total']->total*0.95 }} </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="stat-right">
                                <div class="stat-chart">
                                    <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                    <div id="sparkline_bar2"></div>
                                </div>
                                <div class="stat-number">
                                    <div class="title"> Commission to HireInfluencer </div>
                                    <div class="number"> {{ '$'.$slot_finance['total']->total*0.05 }} </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-scrollable table-scrollable-borderless">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr class="uppercase">
                                    <th> Title </th>
                                    <th> Earning </th>
                                    <th> Commission </th>
                                </tr>
                            </thead>
                            @if(isset($slot_finance['list']))
                                @foreach($slot_finance['list'] as $list)
                                    @if($list->final_deal_price != null)
                                        <tr>
                                            <td>
                                                <a href="javascript:;" class="primary-link">{{ $list->title }}</a>
                                            </td>
                                            <td> {{ '$'.$list->final_deal_price*0.95 }} </td>
                                            <td> {{ '$'.$list->final_deal_price*0.05 }} </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Gallery</span>
                    </div>
                </div>
                <div class="row">
                    @foreach($slot->gallery as $gallery)
                        <div class="col-md-4">
                            <img src="http://laravel-nepal.com/wp-content/uploads/2018/01/Rshfv-1024x576.png" width="100%">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Related Projects</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable table-scrollable-borderless">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr class="uppercase">
                                    <th> Title </th>
                                    <th> Budget </th>
                                    <th> Status </th>
                                </tr>
                            </thead>
                            @if(isset($slot_finance['list']))
                                @foreach($slot_finance['list'] as $list)
                                    <tr>
                                        <td>
                                            <a href="{{ route($base_route.'.view.slot.project', [$user->id, $list->slug]) }}" class="primary-link">{{ $list->title }}</a>
                                        </td>
                                        <td> {{ '$'.$list->budget }} </td>
                                        <td> {{ $list->status }} </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Slot Category</span>
                    </div>
                </div>
                <div class="portlet-body">
                    @if($category)
                        <h3>{{ $category->title }}</h3>
                        <span>{{ $category->description }}</span>
                    @else
                        <span>No Category Linked!</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">{{ count($reviews) }} Reviews 
                            <span class="stars">{{ $slot->average_rating != null?$slot->average_rating:0 }}</span> {{ $slot->average_rating != null?$slot->average_rating:0 }} 
                            <span class="inlines">avg</span>
                        </span>
                    </div>
                </div>
                @include($view_path.'.partials.review')
            </div>
        </div>
    </div>
    @foreach($packages as $package)
        <div id="modal-{{ $package->id }}" class="modal fade" role="dialog" style="top: 30%">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{ $package->title }} - Extra Charges</h4>
              </div>
              <div class="modal-body">
                <table class="table">
                    <thead>
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Extra Charge</th>
                        <th>Delivery Days</th>
                    </thead>
                    @if(count($package->extras) > 0)
                        @foreach($package->extras as $extra)
                            <tr>
                                <td>{{ $extra->translation->title }}</td>
                                <td>{{ $extra->translation->summary }}</td>
                                <td>{{ '$'.$extra->extra_price }}</td>
                                <td>{{ $extra->extra_delivery_days.' days' }}</td>
                            </tr>
                        @endforeach
                    @else
                        <p>No Data!</p>
                    @endif
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
    @endforeach
</div>
<!-- END PROFILE CONTENT -->