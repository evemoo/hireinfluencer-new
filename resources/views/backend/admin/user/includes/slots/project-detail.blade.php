<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">{{ $project->title }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table">
                        <thead>
                            <th>Budget</th>
                            <th>Final Deal Price</th>
                            <th>Initialized On</th>
                            <th>Completed On</th>
                            <th>status</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ '$'.$project->budget }}</td>
                                <td>{{ '$'.$project->final_deal_price }}</td>
                                <td>{{ AppHelper::formatDate('M d,Y', $project->initialized_date) }}</td>
                                <td>{{ $project->completed_date?AppHelper::formatDate('M d,Y', $project->completed_date):'NA' }}</td>
                                <td>{{ $project->status }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <div class="row text-center">
                        {{ $project->description }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light portlet-fit">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Chat</span>
                    </div>
                </div>
                <div class="portlet-body" id="chats">
                    <ul class="chats">
                        @foreach($project_chats as $chat)
                            <li class="{{ $chat->sender_id == $user->id?'in':'out' }}">
                                <img class="avatar" alt="" src="{{ asset('img/profile/avatar.png') }}">
                                <div class="message">
                                    <span class="arrow"> </span>
                                    <a href="javascript:;" class="name"> {{ $chat->sender->username }} </a>
                                    <span class="datetime"> at {{ AppHelper::formatDate('H:i - M d, Y', $chat->created_at) }} </span>
                                    <span class="body"> {{ $chat->message }} </span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-6">
            <div class="portlet light portlet-fit ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-calendar-check-o font-green"></i>
                        <span class="caption-subject bold font-green uppercase"> {{ admin_trans($trans_path.'content.overview.review') }} </span>
                        <span class="caption-helper">Slot Review</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="timeline">
                        <!-- TIMELINE ITEM -->
                        @foreach($reviews as $review)
                            <div class="timeline-item">
                                <div class="timeline-badge">
                                    <img class="timeline-badge-userpic" src="{{ asset('img/profile/avatar.png') }}"> </div>
                                <div class="timeline-body">
                                    <div class="timeline-body-arrow"> </div>
                                    <div class="timeline-body-head">
                                        <div class="timeline-body-head-caption">
                                            <a href="javascript:;" class="timeline-body-title font-blue-madison">{{ $review->reviewer->username }}</a>
                                            <span class="timeline-body-time font-grey-cascade">{{ $review->rating }} <span class="stars">{{ $review->rating }}</span></span>
                                        </div>
                                        <div class="timeline-body-head-actions">
                                            <div class="btn-group">
                                                <button class="btn btn-circle green btn-outline btn-sm dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li>
                                                        <a href="javascript:;">Action </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Another action </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Something else here </a>
                                                    </li>
                                                    <li class="divider"> </li>
                                                    <li>
                                                        <a href="javascript:;">Separated link </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="timeline-body-content">
                                        <span class="font-grey-cascade"> {{ $review->comment }} </span>
                                    </div>
                                    @if($review->reply)
                                        <div style="padding: 5px 0 0 15px; display: block;">
                                            <img src="{{ asset('img/profile/avatar.png') }}" class="circle">
                                            <div style="display: inline-block; padding-left: 5px;">
                                                <span class="font-blue-madison">{{ $user->username }}</span><br>
                                                <span class="font-grey-cascade">{{ $review->reply->reply_comment }}</span>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->