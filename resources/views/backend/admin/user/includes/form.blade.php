<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">{{ admin_trans($trans_path.'content.account-setting.profile-account') }}</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#basic" data-toggle="tab">{{ admin_trans($trans_path.'content.account-setting.basic-info') }}</a>
                        </li>
                        <li>
                            <a href="#social" data-toggle="tab">{{ admin_trans($trans_path.'content.account-setting.social-media-links') }}</a>
                        </li>
                        <li>
                            <a href="#avatar" data-toggle="tab">{{ admin_trans($trans_path.'content.account-setting.change-avatar') }}</a>
                        </li>
                        <li>
                            <a href="#password" data-toggle="tab">{{ admin_trans($trans_path.'content.account-setting.change-password') }}</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">
                    <form action="{{ route($base_route.'.update', $user->id) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <input type="hidden" name="id", value="{{ $user->id }}" class="id">
                            <div class="tab-pane active" id="basic">
                                <div class="form-group">
                                    <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.first-name') }}</label>
                                    <input type="text" placeholder="First Name" class="form-control" name="first_name" value="{{ $user->first_name }}"> 
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.middle-name') }}</label>
                                    <input type="text" placeholder="Middle Name" class="form-control" name="middle_name" value="{{ $user->middle_name }}"> 
                                    @if ($errors->has('middle_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('middle_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.last-name') }}</label>
                                    <input type="text" placeholder="Last Name" class="form-control" name="last_name" value="{{ $user->last_name }}"> 
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.username') }}</label>
                                    <input type="text" placeholder="User Name" class="form-control" name="username" value="{{ $user->username }}"> 
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.email') }}</label>
                                    <input type="text" placeholder="Email Address" class="form-control" name="email" value="{{ $user->email }}"> 
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.primary-contact') }}</label>
                                    <input type="text" placeholder="Primary Number" class="form-control" name="primary_contact" value="{{ $user->primary_contact }}"> 
                                    @if ($errors->has('primary_contact'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('primary_contact') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.secondary-contact') }}</label>
                                    <input type="text" placeholder="Secondary Number" class="form-control" name="secondary_contact" value="{{ $user->secondary_contact }}"> 
                                    @if ($errors->has('secondary_contact'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('secondary_contact') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.gender.title') }}</label>
                                    <select class="form-control" name="gender">
                                        <option value="">{{ admin_trans($trans_path.'content.account-setting.gender.select') }}</option>
                                        <option value="male" {{ $user->gender == 'male'?'selected':'' }}>{{ admin_trans($trans_path.'content.account-setting.gender.male') }}</option>
                                        <option value="female" {{ $user->gender == 'female'?'selected':'' }}>{{ admin_trans($trans_path.'content.account-setting.gender.female') }}</option>
                                    </select> 
                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.dob') }}</label>
                                    <input type="text" id="datepicker" placeholder="DOB" name="birth_date" class="form-control datepicker" value="{{ date('Y-m-d', strtotime($user->birth_date)) }}">
                                
                                    @if ($errors->has('birth_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('birth_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!-- END PERSONAL INFO TAB -->
                            <!-- CHANGE AVATAR TAB -->
                            <div class="tab-pane" id="avatar">
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> {{ admin_trans($trans_path.'content.account-setting.select-image') }} </span>
                                                <span class="fileinput-exists"> {{ admin_trans($trans_path.'content.account-setting.image-change') }} </span>
                                                <input type="hidden" value="" name="..."><input type="file" name="" class="avatar"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> {{ admin_trans($trans_path.'content.account-setting.image-remove') }} </a>
                                        </div>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                        <span class="label label-danger">{{ admin_trans($trans_path.'content.account-setting.note') }} </span>
                                        <span> {{ admin_trans($trans_path.'content.account-setting.note-msg') }} </span>
                                    </div>
                                </div>
                            </div>
                            <!-- END CHANGE AVATAR TAB -->
                            <!-- CHANGE PASSWORD TAB -->
                            <div class="tab-pane" id="password">
                                    <div class="form-group">
                                        <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.new-password') }}</label>
                                        <input type="password" class="form-control" name="password"> </div>
                                    <div class="form-group">
                                        <label class="control-label">{{ admin_trans($trans_path.'content.account-setting.retype-password') }}</label>
                                        <input type="password" class="form-control" name="password_confirmation"> </div>
                            </div>
                            <!-- END CHANGE PASSWORD TAB -->
                            <!-- PRIVACY SETTINGS TAB -->
                            <div class="tab-pane" id="social">
                                @foreach($user->social_medias as $key => $social)
                                <div class="form-group">
                                    <label class="control-label">{!! $social['icon'] !!}  <strong>{{ $social['title'] }}</strong></label>
                                    <input type="text" placeholder="{{ $social['title'] }} Profile URL" class="form-control" name="social_medias[{{ $key }}]" value="{{ $social['link'] }}"> 
                                </div>
                                @endforeach
                            </div>
                            <!-- END PRIVACY SETTINGS TAB -->
                        </div>
                        <button type="submit" class="btn blue">{{ admin_trans($trans_path.'content.account-setting.button.save') }}</button>
                        <button type="reset" class="btn red">{{ admin_trans($trans_path.'content.account-setting.button.reset') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT