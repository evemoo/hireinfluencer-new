<div class="profile-content">
    <div class="row">
        @foreach($social_medias as $media)
            <div class="col-md-6">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption"><i class="{{ $media->icon }}"></i>{{ $media->name }}</div>
                    </div>
                    <div class="portlet-body">
                        @foreach($social_details as $key => $details)
                            @if($key == $media->slug)
                            <table class="table">
                                <thead>
                                    <th>Account Name</th>
                                    <th>Account Url</th>
                                    <th>Account Followings</th>
                                </thead>
                                @foreach($details as $detail)
                                    <tr>
                                        <td>{{ $detail->name }}</td>
                                        <td>{{ $detail->profile_link }}</td>
                                        <td>{{ $detail->value }}</td>
                                    </tr>
                                @endforeach
                            </table>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>