<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">{{ admin_trans($trans_path.'content.account-setting.profile-account') }}</span>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route($base_route.'.view.setting.edit', $user->id) }}"><button class="btn btn-primary">Edit</button></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="portlet">
                                <div class="portlet-title tabbable-line">
                                    <div class="">
                                        <span class="caption-subject font-blue-madison bold uppercase">Full Name:</span> {{ $user->first_name.' '.$user->middle_name.' '.$user->last_name }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="portlet">
                                <div class="portlet-title tabbable-line">
                                    <div class="">
                                        <span class="caption-subject font-blue-madison bold uppercase">Date of Birth:</span> {{ $user->birth_date?AppHelper::formatDate('M d,Y', $user->birth_date):'NA' }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="portlet">
                                <div class="portlet-title tabbable-line">
                                    <div class="">
                                        <span class="caption-subject font-blue-madison bold uppercase">Username:</span> {{ $user->username }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="portlet">
                                <div class="portlet-title tabbable-line">
                                    <div class="">
                                        <span class="caption-subject font-blue-madison bold uppercase">Email:</span> {{ $user->email }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="portlet">
                                <div class="portlet-title tabbable-line">
                                    <div class="">
                                        <span class="caption-subject font-blue-madison bold uppercase">Primary Contact:</span> {{ $user->primary_contact?$user->primary_contact:'NA' }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="portlet">
                                <div class="portlet-title tabbable-line">
                                    <div class="">
                                        <span class="caption-subject font-blue-madison bold uppercase">Secondary Contact:</span> {{ $user->secondary_contact?$user->secondary_contact:'NA' }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="portlet">
                                <div class="portlet-title tabbable-line">
                                    <div class="">
                                        <span class="caption-subject font-blue-madison bold uppercase">Gender:</span> {{ $user->gender?$user->gender:'NA' }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT