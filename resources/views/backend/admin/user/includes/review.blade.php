<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Reviews</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table">
                            <thead>
                                <th>Date</th>
                                <th>Project - Slot</th>
                                <th>Rating</th>
                                <th>Comment</th>
                                <th>Published</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach($user_reviews as $review)
                                    <tr>
                                        <td>{{ AppHelper::formatDate('M d, Y', $review->created_at) }}</td>
                                        <td>{{ $review->title.' - '.$review->stitle }}</td>
                                        <td>{{ $review->rating }}</td>
                                        <td>{{ substr($review->comment, 0, 100) }}</td>
                                        <td>{!! $review->published == 1?'<i class="fa fa-circle" style="color: lightgreen;"></i>':'<i class="fa fa-circle" style="color: orange;"></i>' !!}</td>
                                        <td>
                                            <a href="{{ route($base_route.'.view.review.edit', [$user->id, $review->id]) }}"
                                                  class="btn btn-outline btn-circle btn-sm blue">
                                                <i class="fa fa-pencil"></i>
                                            </a>

                                            <a data-href="{{ route($base_route.'.view.review.delete', [$user->id, $review->id]) }}"
                                               class="btn btn-outline btn-circle btn-sm red  delete-record-warning">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->