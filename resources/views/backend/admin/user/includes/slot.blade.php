<div class="profile-content">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet light">
                <h1>Your Slots</h1>
                <div class="tabbable tabbable-tabdrop">
                    <ul class="nav nav-tabs">
                        @foreach($social_medias as $key => $sm)
                            <li class="{{ $key == 0?'active':'' }}">
                                <a href="#tab-{{ $sm->slug }}" data-toggle="tab" aria-expanded="true"><i class="{{ $sm->icon }}"></i>{{ $sm->name }}</a>
                            </li>
                        @endforeach
                        
                    </ul>
                    <div class="tab-content">
                        @foreach($slotsBySocialMedia as $key => $slots_by_media)
                            <div class="tab-pane {{ $key == 'facebook'?'active':'' }}" id="tab-{{ $key }}">
                                <table class="table">
                                    <thead>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Account Url</th>
                                        <th>Delivery In</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        @foreach($slots_by_media as $slot)
                                        <tr>
                                            <td>
                                                <img src="http://hireinfluencer.localhost.com/img/profile/avatar.png" width="40">
                                            </td>
                                            <td>
                                                {{ $slot->title }}
                                            </td>
                                            <td>
                                                {{ $slot->name }}
                                            </td>
                                            <td>
                                                {{ $slot->can_deliver_within.' days' }}
                                            </td>
                                            <td>
                                                @if($slot->status == 0)
                                                    <span class="badge badge-empty badge-warning"></span> Inactive </span>
                                                @elseif($slot->status == 1)
                                                    <span class="badge badge-empty badge-success"></span> Active </span>
                                                @else
                                                    <span class="badge badge-empty badge-danger"></span> {{ admin_trans($trans_path.'content.overview.status.rejected') }} </span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route($base_route.'.view.slot.detail', [$user->id, $slot->id]) }}">
                                                    <button class="btn btn-info btn-sm">Show Details</button>
                                                </a>
                                                <button class="btn btn-primary btn-sm load-review" data-id="{{ $slot->id }}">Show Review <i class="fa fa-hand-o-right"></i></button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet light portlet-fit ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-calendar-check-o font-green"></i>
                        <span class="caption-subject bold font-green uppercase"> {{ admin_trans($trans_path.'content.overview.review') }} </span>
                        <span class="caption-helper">Slot Review</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    @include($view_path.'.partials.review')
                </div>
            </div>
        </div>
    </div>
</div>