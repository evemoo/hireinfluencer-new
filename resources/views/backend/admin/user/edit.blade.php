@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.edit.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.edit.description') }}" name="description" />
@endsection

@section('style')
    <style>
        .has-error {
            color: red;
        }
    </style>
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">
                        {{ admin_trans($trans_path.'content.common.user') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.edit.edit') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> {{ admin_trans($trans_path.'content.common.user') }}
            <small> > {{ admin_trans($trans_path.'content.edit.description') }}</small>
        </h1>

        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            {!! Form::model($user, [
                'method'  => 'POST',
                'route'   => [ $base_route.'.update', $user->id ],
                 'id'=> 'users_store_form',
                'enctype' => 'multipart/form-data'
            ]) !!}

            {!! Form::hidden('id', $user->id) !!}

            <div class="portlet-body">

                <div class="ui form">

                    @include('backend.admin.user.partials._form')

                    <div class="ui primary button save-user" tabindex="0">{{ admin_trans($trans_path.'button.update') }}</div>

                </div>

            </div>


            {!! Form::close() !!}

        </div>

        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->

    </div>
@endsection

@section('script')

    <script>

        (function () {
            $('.ui.radio.checkbox')
                .checkbox()
            ;
            $('.save-user').click(function () {
                $('#users_store_form').submit();
            });


            $('#users_store_form')
                .form({
                    on: 'blur',
                    fields: {
                        name: {
                            identifier: 'name',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Please enter your name'
                                }
                            ]
                        },
                        first_name: {
                            identifier: 'first_name',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Please enter your first name'
                                }
                            ]
                        },
                        last_name: {
                            identifier: 'last_name',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Please enter your last name'
                                }
                            ]
                        },
                        email: {
                            identifier: 'email',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Please enter your email'
                                }
                            ]
                        },

                        password_confirmation: {
                            identifier: 'password_confirmation',
                            rules: [
                                {
                                    type: 'match[password]',
                                    prompt: 'Mismatched password'
                                }
                            ]
                        },
                    },
                    onInvalid: function (message) {

                        if(message[0]) {

                            $(this)
                                .next()
                                .remove();

                            console.log($(this));

                            $(this).after('<div class="ui basic red pointing prompt label transition visible">'
                                         + message[0]+
                                      '</div>');

                            $('.error-showable-data')
                                .removeClass('invisible')
                                .addClass('visible')
                                .html('')
                                .html('Please Check Something is missing !!');
                        }

                    },
                    onValid: function () {
                        $(this).next().remove();
                    }
                });

            $('.ui.dropdown').dropdown();

        })();



    </script>

@endsection

