@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('content')

    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index', $users[0]->name) }}">
                        {{ admin_trans($trans_path.'content.common.user') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br />
        @if(Request::is('admin/user/influencer'))
            <div class="stats">
                <div class="row">
                    <div class="col-md-2">
                        <h4>Influencers <badge class="badge badge-primary">{{ $stats['influencers_count'] }}</badge></h4>
                    </div>
                    <div class="col-md-2">
                        <h4>Slots <badge class="badge badge-info">{{ $stats['slots_count'] }}</badge></h4>
                    </div>
                    <div class="col-md-2">
                        <h4>Completed Projects <badge class="badge badge-success">{{ $stats['project_completed_count'] }}</badge></h4>
                    </div>
                    <div class="col-md-2">
                        <h4>Ongoing Projects <badge class="badge badge-default">{{ $stats['project_ongoing_count'] }}</badge></h4>
                    </div>
                    <div class="col-md-2">
                        <h4>Total Earnings <badge class="badge badge-warning">{{ '$'.$stats['total_earnings']*0.95 }}</badge></h4>
                    </div>
                    <div class="col-md-2">
                        <h4>Commission <badge class="badge badge-danger">{{ '$'.$stats['total_earnings']*0.05 }}</badge></h4>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">

                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">
                                {{ admin_trans($trans_path.'content.list.description') }}
                            </span>
                        </div>

                        <div class="actions">

                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <a href="{{ route($base_route.'.create', $users[0]->name) }}" onclick="location.href = '{{ route($base_route.'.create', $users[0]->name) }}'">

                                    <button type="button"
                                            class="btn btn-primary mt-ladda-btn ladda-button btn-circle"
                                            data-style="expand-right">
                                        <span class="ladda-label">
                                            <i class="fa fa-plus-circle"></i>
                                            {{ admin_trans($trans_path.'button.add') }}
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </a>
                            </div>

                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <button class="pull-right btn btn-primary filter-toggle">Filters</button>
                        </div>
                        <div class="filter" {{ count($_GET) > 0?'':'hidden' }}>
                            {!! Form::open(['route' => [$base_route.'.index', $users[0]->name], 'method' => 'get']) !!}
                            <div class="row form-body"> 
                                <div class="col-md-2 form-group form-md-line-input">
                                    {!! Form::text('full_name', null, ['class' => 'form-control', 'placeholder' => 'Full Name goes here!']) !!}
                                </div>
                                <div class="col-md-2 form-group form-md-line-input">
                                    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email Address goes here!']) !!}
                                </div>
                                <div class="col-md-2 form-group form-md-line-input">
                                    {!! Form::text('contact', null, ['class' => 'form-control', 'placeholder' => 'Contact Number goes here!']) !!}
                                </div>
                                <div class="col-md-2 form-group form-md-line-input">
                                    {!! Form::select('gender', $genders, null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-md-2 form-group form-md-line-input">
                                    {!! Form::text('dob', null, ['class' => 'form-control', 'placeholder' => 'Date of Birth dd/mm/yyyy']) !!}
                                </div>
                                <div class="col-md-2 form-group form-md-line-input">
                                    {!! Form::select('category', $categories, null, ['class' => 'form-control', 'placeholder' => 'Category - All']) !!}
                                </div>
                                <div class="col-md-2 form-group form-md-line-input">
                                    {!! Form::select('social_media', $social_medias, null, ['class' => 'form-control', 'placeholder' => 'Social Media - All']) !!}
                                </div>
                                <div class="col-md-2 form-group form-md-line-input">
                                    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-md-2 form-group form-md-line-input">
                                    {!! Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'Search for anything else!']) !!}
                                </div>
                                <div class="col-md-2 form-group form-md-line-input">
                                    {{  Form::submit('Search', ['id' => 'filter_btn', 'class' => 'btn btn-warning btn-sm filter-search'] )  }}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="table-scrollable">

                            @include('backend.common.flash')

                            <table class="table table-hover table-light">
                                <thead class="flip-content">
                                <tr>
                                    <th>{{ admin_trans($trans_path.'content.list.full_name') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.common.role') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.common.email') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.common.status') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.list.action') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($users->count() > 0)

                                    @foreach($users as $user)
                                        <tr>
                                            <td>
                                                {{ $user->first_name . ' ' . $user->middle_name . ' ' . $user->last_name  }}
                                            </td>
                                            <td> 
                                                @if($user->name == 'admin')
                                                    <span class="label label-sm label-success">
                                                        {{ 'Admin' }}
                                                    </span>
                                                @elseif($user->name == 'brand')
                                                    <span class="label label-sm label-warning">
                                                        {{ 'Brand' }}
                                                    </span>
                                                @elseif($user->name == 'influencer')
                                                    <span class="label label-sm label-info">
                                                        {{ 'Influencer' }}
                                                    </span>
                                                @endif 
                                            </td>
                                            <td> {{ $user->email }} </td>
                                            <td>
                                                @if( $user->enabled == 1)
                                                    <span class="label label-sm label-success">
                                                        {{ admin_trans($trans_path.'content.common.active') }}
                                                    </span>
                                                @else
                                                    <span class="label label-sm label-warning">
                                                    {{ admin_trans($trans_path.'content.common.inactive') }}
                                                    </span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route($base_route.'.view.overview', $user->id) }}"
                                                      class="btn btn-outline btn-circle btn-sm green">
                                                    <i class="fa fa-eye"></i>
                                                    {{ admin_trans($trans_path.'content.list.view') }}
                                                </a>

                                                <a href="{{ route($base_route.'.view.setting.edit', $user->id) }}"
                                                      class="btn btn-outline btn-circle btn-sm blue">
                                                    <i class="fa fa-pencil"></i>
                                                    {{ admin_trans($trans_path.'content.list.edit') }}
                                                </a>

                                                <a data-href="{{ route($base_route.'.delete', $user->id) }}"
                                                   class="btn btn-outline btn-circle btn-sm red  delete-record-warning">
                                                    <i class="fa fa-trash"></i>
                                                    {{ admin_trans($trans_path.'content.list.delete') }}
                                                </a>

                                                <a data-href="{{ route($base_route.'.quickdetails', $user->id) }}" data-name="{{ $user->first_name.' '.$user->middle_name.' '.$user->last_name  }}"
                                                   class="btn btn-outline btn-circle btn-sm purple quick-details" data-toggle="modal" data-target="#modal">
                                                    <i class="fa fa-trash"></i>
                                                    Quick Details
                                                </a>

                                                <a href="{{ route($base_route.'.msg', $user->id) }}" class="btn btn-outline btn-circle btn-sm yellow msg">
                                                    <i class="fa fa-envelope"></i>
                                                    Msg
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="6">{{ $users->render() }}</td>
                                    </tr>

                                @else
                                    <tr>
                                        <td colspan="6">{{ admin_trans($trans_path.'error.no-data-found') }}</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div id="modal" class="modal fade" role="dialog" style="top: 30%">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <table class="table">
                    <thead>
                        <th>Total Slots</th>
                        <th>Projects Completed</th>
                        <th>Projects Ongoing</th>
                        <th>Total Earnings</th>
                        <th>Total Commission</th>
                    </thead>
                    <tr class="data text-center">
                        
                    </tr>
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>

    </div>

@endsection


@section('script')
    <script>
        $(document).on('click', '.delete-record-warning', function (e) {
            e.preventDefault();

            var url = $(this).attr('data-href');

            swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                },
                function (isConfirm) {

                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            headers: {
                                'X-CSRF-Token': '{!! csrf_token() !!}'
                            },
                            success: function (data) {
                                swal({
                                    title: "Deleted!",
                                    text: "The user is successfully deleted!",
                                    type: "success",
                                    timer: 5000
                                });

                                window.location.replace("{{ route($base_route.'.index', $users[0]->name) }}");
                            },
                        });
                    } else {
                    }

                });
        });

        $(document).on('click', '.quick-details', function (e) {
            e.preventDefault();

            var url = $(this).attr('data-href');
            var name = $(this).attr('data-name');

            $.ajax({
                type: "get",
                url: url,
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    var tr = '<th>'+data.slots_count+'</th><th>'+data.project_completed_count+'</th><th>'+data.project_ongoing_count+'</th><th>$'+data.total_earnings*0.95+'</th><th>$'+data.total_earnings*0.05+'</th>';
                    $('#modal').find('.modal-title').html(name);
                    $('#modal').find('tr.data').html(tr);
                },
            });
        });

        $(document).on('click', '.filter-toggle', function(){
            $('.filter').toggle('show');
        });

        $(document).on('click', '.filter-search', function (e) {

            e.preventDefault();

            var allInputs = $('form').serializeArray();
            var base_url = window.location.href.split('?');
            var url = base_url[0];
            var final_url = url;

            for(i=1; i<allInputs.length; i++){
                if(allInputs[i].value != '' && allInputs[i].value != undefined && allInputs[i].value != null && allInputs[i].value != 'all')
                {
                    var split = allInputs[i].value.split(" ");
                    if(final_url == url)
                    {
                        if(split.length > 1)
                        {
                            final_url = final_url+'?'+allInputs[i].name+'=';
                            var merge = "";
                            for(i=0; i<split.length; i++)
                            {
                                final_url = final_url+merge+split[i];
                                merge = '+';
                            }
                        }
                        else
                            final_url = final_url+'?'+allInputs[i].name+'='+allInputs[i].value;
                    }
                    else
                    {
                        if(split.length > 1)
                        {
                            final_url = final_url+'&'+allInputs[i].name+'=';
                            var merge = "";
                            for(i=0; i<split.length; i++)
                            {
                                final_url = final_url+merge+split[i];
                                merge = '+';
                            }
                        }
                        else
                            final_url = final_url+'&'+allInputs[i].name+'='+allInputs[i].value
                    }
                }
            }

            window.location.replace(final_url);
        });
    </script>
@endsection

