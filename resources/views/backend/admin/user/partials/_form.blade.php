<div class="three fields">
    <div class="field">
        <label>{{ admin_trans($trans_path.'content.common.first_name') }}</label>
        {!! Form::text('first_name', null, [
            'id' => 'first_name',
            'placeholder' => admin_trans($trans_path.'content.common.first_name')
        ]) !!}
        @if (count($errors) > 0 && $errors->has('first_name'))
            <p class="help-block has-error">{{ $errors->first('first_name') }}</p>
        @endif
    </div>

    <div class="field">
        <label>{{ admin_trans($trans_path.'content.common.middle_name') }}</label>
        {!! Form::text('middle_name', null, [
            'id' => 'middle_name',
            'placeholder' => admin_trans($trans_path.'content.common.middle_name')
         ]) !!}
    </div>

    <div class="field">
        <label>{{ admin_trans($trans_path.'content.common.last_name') }}</label>
        {!! Form::text('last_name', null, [
            'id' => 'last_name',
            'placeholder' => admin_trans($trans_path.'content.common.last_name')
        ]) !!}
        @if (count($errors) > 0 && $errors->has('last_name'))
            <p class="help-block has-error">{{ $errors->first('last_name') }}</p>
        @endif
    </div>

</div>
<div class="two fields">
    <div class="field">
        <label>{{ admin_trans($trans_path.'content.common.name') }}</label>
        {!! Form::text('username', null, [
             'id'          => 'name',
             'placeholder' => admin_trans($trans_path.'content.common.name')
          ]) !!}
        @if (count($errors) > 0 && $errors->has('username'))
            <p class="help-block has-error">{{ $errors->first('username') }}</p>
        @endif
    </div>

    <div class="field">
        <label>{{ admin_trans($trans_path.'content.common.email') }}</label>
        {!! Form::email('email', null, [
            'id' => 'email',
            'placeholder' => admin_trans($trans_path.'content.common.email')
        ]) !!}
        @if (count($errors) > 0 && $errors->has('email'))
            <p class="help-block has-error">{{ $errors->first('email') }}</p>
        @endif
    </div>
</div>
<div class="field">
    <label>{{ admin_trans($trans_path.'content.common.password') }}</label>
    {!! Form::password('password', [
         'id' => 'password',
         'placeholder' => admin_trans($trans_path.'content.common.password')
    ]) !!}
    @if (count($errors) > 0 && $errors->has('password'))
        <p class="help-block has-error">{{ $errors->first('password') }}</p>
    @endif
</div>
<div class="field">
    <label>{{ admin_trans($trans_path.'content.common.confirm_password') }}</label>
    {!! Form::password('password_confirmation', [
        'id'          => 'password_confirmation',
        'placeholder' => admin_trans($trans_path.'content.common.confirm_password')
    ]) !!}
    @if (count($errors) > 0 && $errors->has('confirm_password'))
        <p class="help-block has-error">{{ $errors->first('confirm_password') }}</p>
    @endif
</div>

<div class="ui form">
    <div class="inline fields">
        <label for="fruit">{{  admin_trans($trans_path.'content.common.status') }}</label>
        <div class="field">
            <div class="ui radio checkbox">
                {!! Form::radio('enabled', 1, true, ['class' => 'hidden']) !!}
                {{--  <input type="radio" name="fruit" checked="" tabindex="0" class="hidden">--}}

                <label>{{ admin_trans($trans_path.'content.common.active') }}</label>
            </div>
        </div>
        <div class="field">
            <div class="ui radio checkbox">

                {!! Form::radio('enabled', 0, false, ['class' => 'hidden']) !!}

                <label>{{ admin_trans($trans_path.'content.common.inactive') }}</label>
            </div>
        </div>
    </div>
</div>