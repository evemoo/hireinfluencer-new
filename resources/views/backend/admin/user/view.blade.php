@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/css/profile.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/css/bootstrap-fileinput.css') }}">
@endsection

@section('style')
    <style type="text/css">
        span.stars, span.stars span {
            display: inline-block;
            background: url({{ asset('img/stars.png') }}) 0 -16px repeat-x;
            width: 80px;
            height: 16px;
        }

        span.stars span {
            background-position: 0 0;
        }
    </style>
@endsection

@section('content')

    <div class="page-content" style="background: #eef1f5;">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('customer.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{--{{ route($base_route.'.index') }}--}}">
                        {{ admin_trans($trans_path.'content.common.user') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.common.profile') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-12">
                @include('backend.common.flash')
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light profile-sidebar-portlet ">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img src="{{ $user->avatar() }}" class="img-responsive avtr" alt=""> </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"> {{ $user->first_name.' '.$user->last_name }} </div>
                            <div class="profile-usertitle-job"> {{ $user->username }} </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="{!! Request::is('admin/user/**/view/overview')?'active':"" !!}">
                                    <a href="{{ route($base_route.'.view.overview', $user->id) }}">
                                        <i class="fa fa-home"></i> Overview </a>
                                </li>
                                <li class="{!! Request::is('admin/user/**/view/setting*')?'active':"" !!}">
                                    <a href="{{ route($base_route.'.view.setting', $user->id) }}">
                                        <i class="fa fa-cog"></i> Account Settings </a>
                                </li>
                                <li class="{!! Request::is('admin/user/**/view/slot*')?'active':"" !!}">
                                    <a href="{{ route($base_route.'.view.slot', $user->id) }}">
                                        <i class="fa fa-clone"></i> Slots </a>
                                </li>
                                <li class="{!! Request::is('admin/user/**/view/review')?'active':"" !!}">
                                    <a href="{{ route($base_route.'.view.review', $user->id) }}">
                                        <i class="fa fa-send"></i> Reviews </a>
                                </li>
                                <li class="{!! Request::is('admin/user/**/view/help')?'active':"" !!}">
                                    <a href="{{ route($base_route.'.view.help', $user->id) }}">
                                        <i class="fa fa-info-circle"></i> Help </a>
                                </li>
                                <li class="">
                                    <a href="javascript:;" class="ban-user">
                                        <i class="fa fa-ban"></i> Ban User {{ $is_banned?'(Banned)':'' }}</a>
                                        @if(count($errors) > 0)
                                            <span class="text-center" style="color: red;">There were some error!</span>
                                        @endif
                                        <div class="ban-form" style="width: 90%; padding-left: 15px;" hidden>
                                            <form class="form-group" action="{{ route($base_route.'.ban', $user->id) }}" method="POST">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="ban_id" value="{{ $is_banned?$is_banned->id:'' }}">
                                                <input type="text" name="from" value="{{ $is_banned?$is_banned->start_date:'' }}" placeholder="Ban Start Date" class="form-control datepicker">
                                                <input type="text" name="to" value="{{ $is_banned?$is_banned->end_date:'' }}" placeholder="Ban End Date" class="form-control datepicker">
                                                <textarea name="comment" class="form-control" placeholder="Why am I banned?">{{ $is_banned?$is_banned->comment:'' }}</textarea>
                                                <button class="btn btn-primary ban">Ban</button>
                                            </form>
                                        </div>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->
                    <div class="portlet light ">
                        <!-- STAT -->
                        <div class="row list-separated profile-stat">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="uppercase profile-stat-title"> {{ $social_medias->count() }} </div>
                                <div class="uppercase profile-stat-text"> Social Medias Linked </div>
                            </div>
                        </div>
                        <!-- END STAT -->
                        <div>
                            <h4 class="profile-desc-title">Linked Social Medias </h4>
                            @foreach($social_medias as $social)
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="{{ $social['icon'] }}"></i>
                                    <a href="{{ $social['profile_link'] }}" target="_blank">{{ $social['name'] }}</a>
                                </div>
                            @endforeach
                            <span class="pull-right"><i class="fa fa-hand-o-right"></i> <a href="{{ route($base_route.'.view.socialmedia', $user->id) }}">All Details</a></span>
                        </div>
                        <hr>
                        <div>
                            <h4 class="profile-desc-title">Contact {{ $configs['company'][ConfigHelper::evemooConfig('lang.default')] }}</h4>
                            {{-- <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span> --}}
                            <div class="margin-top-20 profile-desc-link">
                                <i class="fa fa-globe"></i>
                                <a href="{{ $configs['domain'] }}" target="_blank">{{ $configs['domain'] }}</a>
                            </div>
                            @foreach($configs['social_links'] as $social)
                                @if($social['link'] != '')
                                    <div class="margin-top-20 profile-desc-link">
                                        {!! $social['icon'] !!}
                                        <a href="{{ $social['link'] }}">{{ $social['title'] }}</a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                @if(Request::is('admin/user/**/view/overview'))
                    @include($view_path.'.includes.overview')
                @elseif(Request::is('admin/user/**/view/setting'))
                    @include($view_path.'.includes.profile')           
                @elseif(Request::is('admin/user/**/view/setting/edit'))
                    @include($view_path.'.includes.form')            
                @elseif(Request::is('admin/user/**/view/slot'))
                    @include($view_path.'.includes.slot')                   
                @elseif(Request::is('admin/user/**/view/slot/project*'))
                    @include($view_path.'.includes.slots.project-detail')             
                @elseif(Request::is('admin/user/**/view/slot/*'))
                    @include($view_path.'.includes.slots.details')       
                @elseif(Request::is('admin/user/**/view/review'))
                    @include($view_path.'.includes.review')              
                @elseif(Request::is('admin/user/**/view/social-media'))
                    @include($view_path.'.includes.social')
                @elseif(Request::is('admin/user/**/view/help'))
                    @include($view_path.'.includes.help')
                @endif
            </div>
        </div>

    </div>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ asset('assets/backend/js/bootstrap-fileinput.js') }}"></script>

    <script type="text/javascript">
        //$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });

        $('.avatar').on('change', function(){
            var $this = $(this);
            var file_data = $this.prop('files')[0];
            var user_id = $('.id').val();
            var form_data = new FormData();
            form_data.append("image", file_data);
            form_data.append("id", user_id);
            form_data.append("_token", '{{ csrf_token() }}');
            $.ajax({
                url: '{{ route($base_route.'.updateAvatar') }}',
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                success: function (response) {
                    var data = $.parseJSON(response);
                    console.log(data);
                    $('.avtr').attr('src', data);
                }
            });
        });

        $(document).on('click', '.delete-record-warning', function (e) {
            e.preventDefault();

            var url = $(this).attr('data-href');

            swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                },
                function (isConfirm) {

                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            headers: {
                                'X-CSRF-Token': '{!! csrf_token() !!}'
                            },
                            success: function (data) {
                                swal({
                                    title: "Deleted!",
                                    text: "The user is successfully deleted!",
                                    type: "success",
                                    timer: 5000
                                });

                                window.location.replace("{{ route($base_route.'.view.review', $user->id) }}");
                            },
                        });
                    } else {
                    }

                });
        });

        $(document).on('click', '.ban-user', function(e){
            e.preventDefault();
            $('.ban-form').toggle('show');
        });

        $('.load-review').on('click', function(){
            var $this = $(this);
            var id = $this.data('id');

            $.ajax({
                url: '{{ route($base_route.'.getSlotReview') }}',
                method: 'GET',
                data: {id:id},
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                },

                success: function(response) {
                    var data = $.parseJSON(response);
                    $('.timeline').html(' ');
                    if (data.available) {
                        $('.timeline').html(data.view);
                        $('span.stars').stars();
                    }
                    else {
                        $('.timeline').html('No Data!');
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        $.fn.stars = function() {
            return $(this).each(function() {
                // Get the value
                var val = parseFloat($(this).html());
                // Make sure that the value is in 0 - 5 range, multiply to get width
                var size = Math.max(0, (Math.min(5, val))) * 16;
                // Create stars holder
                var $span = $('<span />').width(size);
                // Replace the numerical value with stars
                $(this).html($span);
            });
        }

        $(function() {
            $('span.stars').stars();
        });
    </script>

@endsection
