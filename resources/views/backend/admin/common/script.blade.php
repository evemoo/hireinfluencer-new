<script src="{{ asset('js/bootbox.min.js') }}"></script>

<script>



    function deleteSingleData(id) {

        var url = "{{ route($base_route.'.destroy', ['id' => 'replace-text']) }}";

        bootboxModelConfirm({
            title: "Are You Sure?",
            message: 'You will not be able to recover the data!',
            href: url.replace("replace-text", id)
        });

        return false;

    }

    function deleteMultipleWithConfirm() {

        if( ifDataIsChecked("Please Select Data to Delete !") == true ) {

            bootboxModelConfirm({
                title: "Are You Sure you want to delete the selected data?",
                message: 'You will not be able to recover the selected data!',
                route: '{{ route($base_route.'.delete-selected') }}'
            });

            return false;

        }

    }

    function disableWithConfirm() {

        if( ifDataIsChecked("Please Select Data to Disable !") == true ) {

            bootboxModelConfirm({
                title: "Are You Sure you want to disable the selected data?",
                message: 'You will not be able to recover the selected data!',
                route:'{{ route($base_route.'.disable-selected') }}'
            });

            return false;

        }

    }

    function enableWithConfirm() {

        if( ifDataIsChecked("Please Select Data to Enable !") == true ) {

            bootboxModelConfirm({
                title: "Are You Sure you want to enable the selected data?",
                message: 'You will not be able to recover the selected data!',
                route:'{{ route($base_route.'.enable-selected') }}'
            });

            return false;

        }

    }

    function ifDataIsChecked(message) {

        if( ! $('[type="checkbox"]').is(":checked") ) {

            notify({
                message: message,
                color: '#5bc0de',
                icon: 'fa fa-info-circle'
            });

            return false;

        }

        return true;
    }

    function toggleCheckbox($this) {

        checkboxes = document.getElementsByName('chkData[]');

        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = $this.prop('checked')?true:false;
        }
    }

    function searchWithManagedUrl(url, searchObj) {

        var query = '';


        $.each( searchObj, function ( key, value ) {

            query = manageUrl( key, value, query);

        });

        return location.href = url + query;

    }

    function manageUrl( field, obj, query ) {

        return getValueAsType(field, obj, query );

    }

    function getValueAsType(field, obj, query ) {

        var data = '';

        if(obj.attr("type") == "checkbox") {

            if( obj.is(":checked") ) {

                data = manageQuery(field, 1, query);

            }

        } else {

            data = manageQuery(field, obj.val(), query);

        }

        return data;
    }

    function manageQuery(field, value, query) {


        if(value !== '') {

            if (query == '') {

                query = '?' + field + '=' + value;

            } else {

                query = query + '&' + field + '=' + value;

            }
        }


        return query;
    }

</script>