@if (app()->environment() !== 'production')
    @if($errors->all())
        <div class="alert alert-danger alert-block">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading">Errors! <small>Environment development</small></h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @endif

@if($errors->all())
    <div class="alert alert-danger alert-block">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h6 class="alert-heading">You have some form errors. Please check below.!</h6>
    </div>
    @endif

