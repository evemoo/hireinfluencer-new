<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script>
    function bootboxModelConfirmWithAjaxRequest(data) {
        var token = '{{ csrf_token() }}';
        var img_html = "<img src='{{ asset('images/images.jpg') }}' id='image-view' style='width:280px; max-height:150px; padding-bottom: 10px;'>";

        bootbox.confirm({
            title: data.title,
            message: data.message,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if(result == true){
                    deleteImage(data.href, token, img_html);
                }
            }
        });

    }

    function deleteImage(target, token, img_html) {

        $.ajax({
            type: 'POST',
            url: target,
            data: {'_token': token},
            success: function (message) {
                if (message) {
                    $('#image-destroy').html('').html(img_html);
                    notify({
                        message: message,
                        color: '#739E73',
                        icon: 'fa fa-thumbs-up'
                    });
                }
            }
        });
    }
</script>