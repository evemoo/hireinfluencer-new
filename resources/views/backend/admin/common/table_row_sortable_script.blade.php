<script src="{{ asset('assets/admin/js/libs/jquery-ui-1.10.3.min.js') }}"></script>
<script>

    $(document).ready(function () {
        sortable_tables.sorting_field_table();
    });

    var sortable_tables =
        {
            sorting_field_table: function()
            {
                $('.fixed-width-table tbody').sortable({
                    helper: sortable_tables.fixWidthHelper,
                    update: function( event, ui) {
                        upDateSortedData();
                    }
                }).disableSelection();
            },

            fixWidthHelper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            }
        }

</script>