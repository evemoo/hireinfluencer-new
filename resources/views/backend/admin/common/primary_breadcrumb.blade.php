<!-- RIBBON -->
<div id="ribbon">

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li><a href="{{ $dashboard_route }}">Home</a></li>

        <li>
            @if(isset($no_list) && $no_list == true)
                <a href="">
            @else
                <a href="{{ route($base_route.'.index') }}">

            @endif
                {{ $panel }}
                </a>
        </li>
        <li>{{ $page_title }}</li>
    </ol>

</div>
<!-- END RIBBON -->