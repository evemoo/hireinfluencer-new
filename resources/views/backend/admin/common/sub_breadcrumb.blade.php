<div class="row">

    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa {{ $page_icon }}"></i> {{ str_singular($panel).' Manager' }} <span>&gt; {{ $page_title }}</span>
        </h1>
    </div>

    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks">

            @if( $page_title == 'List' )

                @if(!isset($create))

                    @if (AclHelper::isRouteAccessable($base_route.'.create'.':GET'))

                        <li class="sparks-info">
                            <a class="btn btn-primary" id="create_btn" href="{{ route($base_route.'.create') }}">
                                <i class="fa fa-plus-circle"></i> Add New {{ str_singular($panel) }} </a>  
                        </li>

                    @endif

                @endif

                @if(isset($sortable))

                   @if (AclHelper::isRouteAccessable($base_route.'.order-menu'.':GET'))
                        <li class="sparks-info">
                            <a class="btn btn-primary" href="{{ route($base_route.'.order-menu') }}">
                                <i class="fa fa-list"></i> Ordering Menu </a>
                        </li>
                   @endif

                @endif

            {{--@if(!isset($remove_status_option))--}}

                {{--@if (AclHelper::isRouteAccessable($base_route.'.enable-selected'.':POST'))--}}
                    {{--<li class="sparks-info">--}}
                        {{--<a class="btn btn-primary" onclick="enableWithConfirm()"--}}
                           {{--title="Click to Active Selected Data" >--}}
                            {{--<i class="fa fa-check-circle-o"></i> Enable Checked </a>--}}
                    {{--</li>--}}
                {{--@endif--}}

                {{--@if (AclHelper::isRouteAccessable($base_route.'.disable-selected'.':POST'))--}}
                    {{--<li class="sparks-info">--}}
                        {{--<a class="btn btn-primary" onclick="disableWithConfirm()"--}}
                           {{--title="Click to InActive Selected Data"><i class="fa fa-ban"></i> Disable Checked </a>--}}
                    {{--</li>--}}
                {{--@endif--}}

            {{--@endif--}}

                {{--@if (AclHelper::isRouteAccessable($base_route.'.delete-selected'.':POST'))--}}
                    {{--<li class="sparks-info">--}}
                        {{--<a class="btn btn-primary" onclick="deleteMultipleWithConfirm()"--}}
                           {{--title="Click to Delete Selected Data">--}}
                            {{--<i class="fa fa-trash-o"></i> Delete Checked </a>--}}
                    {{--</li>--}}
                {{--@endif--}}

                    @if (AclHelper::isRouteAccessable($base_route.'.index'.':GET'))
                        <li class="sparks-info">
                            <a class="btn btn-warning" id="filter_toggle_btn" href="javascript:void(0);"
                               title="Click to Show Filter" attr-filter="{{ !request('filter')?'in-active':'active' }}">
                                <i class="fa fa-filter"></i>&nbsp;&nbsp;&nbsp;{{ !request('filter')?'Filter':'Hide Filter' }}
                            </a>
                        </li>
                    @endif

            @elseif($page_title == 'Edit')


                @if (AclHelper::isRouteAccessable($base_route.'.create'.':GET') && Request::segment(2) != 'email-template')

                    <li class="sparks-info">
                        <a class="btn btn-primary" id="create_btn" href="{{ route($base_route.'.create') }}">
                            <i class="fa fa-plus-circle"></i> Add New {{ str_singular($panel) }} </a>
                    </li>

                @endif

                @if (AclHelper::isRouteAccessable($base_route.'.index'.':GET'))
                    <li class="sparks-info">
                        <a class="btn btn-primary" href="{{ route($base_route.'.index') }}"
                           title="{{ str_singular($panel) }} List" >
                            <i class="fa fa-list"></i> {{ str_singular($panel) }} List </a>
                    </li>
                @endif

                {{--@if (AclHelper::isRouteAccessable($base_route.'.destroy'.':GET'))--}}
                    {{--<li class="sparks-info">--}}
                        {{--<a class="btn btn-danger" onclick="deleteSingleData({{ $data['row']->id }})"--}}
                           {{--title="Click to Delete Selected Data">--}}
                            {{--<i class="fa fa-trash-o"></i> Delete Checked </a>--}}
                    {{--</li>--}}
                {{--@endif--}}

            @elseif($page_title == 'Create')
                @if (AclHelper::isRouteAccessable($base_route.'.index'.':GET'))
                    <li class="sparks-info">
                        <a class="btn btn-primary" href="{{ route($base_route.'.index') }}"
                           title="{{ str_singular($panel) }} List" >
                            <i class="fa fa-list"></i> {{ str_singular($panel) }} List </a>
                    </li>
                @endif
            @else

                @if (AclHelper::isRouteAccessable($base_route.'.index'.':GET'))
                    <li class="sparks-info">
                        <a class="btn btn-primary" href="{{ route($base_route.'.index') }}"
                           title="{{ str_singular($panel) }} List" >
                            <i class="fa fa-list"></i> {{ str_singular($panel) }} List </a>
                    </li>
                @endif

                    @if (AclHelper::isRouteAccessable($base_route.'.index'.':GET'))
                    <li class="sparks-info">
                        <a class="btn btn-warning" id="filter_toggle_btn" href="javascript:void(0);"
                           title="Click to Show Filter" attr-filter="{{ !request('filter')?'in-active':'active' }}">
                            <i class="fa fa-filter"></i>&nbsp;&nbsp;&nbsp;{{ !request('filter')?'Filter':'Hide Filter' }}
                        </a>
                    </li>
                    @endif

                    @if (AclHelper::isRouteAccessable($base_route.'.show'.':GET'))
                        <li class="sparks-info">
                            <a class="btn btn-primary" href="{{ route($base_route.'.index') }}"
                               title="{{ str_singular($panel) }} List" >
                                <i class="fa fa-list"></i> {{ str_singular($panel) }} List </a>
                        </li>
                    @endif

                @if(!isset($create))

                    @if (AclHelper::isRouteAccessable($base_route.'.create'.':GET'))

                        <li class="sparks-info">
                            <a class="btn btn-primary" id="create_btn" href="{{ route($base_route.'.create') }}">
                                <i class="fa fa-plus-circle"></i> Add New {{ str_singular($panel) }} </a>
                        </li>

                    @endif

                @endif

                @if(!isset($edit))

                    @if (AclHelper::isRouteAccessable($base_route.'.edit'.':GET'))

                        <li class="sparks-info">
                            <a class="btn btn-primary" href="{{ route($base_route.'.edit', $data['row']->id) }}">
                                <i class="fa fa-pencil"></i> Edit {{ str_singular($panel) }} </a>
                        </li>

                    @endif

                @endif

                @if (AclHelper::isRouteAccessable($base_route.'.destroy'.':GET'))

                    <li class="sparks-info">
                        <a class="btn btn-danger" onclick="deleteSingleData({{ $data['row']->id }})"
                           title="Click to Delete Selected Data">
                            <i class="fa fa-trash-o"></i> Delete Checked </a>
                    </li>

                @endif

            @endif

            @if(view()->exists($view_path.'.partials.route_loader'))
                @include($view_path.'.partials.route_loader')
                @endif

                @if (isset($append) && view()->exists($append))
                    @include($append)
                @endif

                @if(view()->exists($view_path.'.partials.permission_generate'))
                    @include($view_path.'.partials.permission_generate')
                @endif

        </ul>
    </div>

</div>