<div class="row" id="filter_form_wrapper" {!! !request()->has('filter')?'style="display: none;"':'' !!}>
    <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget jarviswidget-sortable" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" role="widget">
            <!-- widget options:
            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

            data-widget-colorbutton="false"
            data-widget-editbutton="false"
            data-widget-togglebutton="false"
            data-widget-deletebutton="false"
            data-widget-fullscreenbutton="false"
            data-widget-custombutton="false"
            data-widget-collapsed="true"
            data-widget-sortable="false"

            -->
            <header role="heading">
                <span class="widget-icon"> <i class="fa fa-filter"></i> </span>
                <h2>Filter</h2>
                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

            <!-- widget div-->
            <div role="content">

                <div class="widget-body">

                    {!! $slot !!}

                    <div class="form-actions">
                        <button type="button" id="filter_form_btn" class="btn btn-success btn-sm">
                            <i class="fa fa-filter"></i>
                            Filter
                        </button>
                    </div>

                </div>

            </div>

        </div>


    </article>
</div>

@section('script-js')

    @parent

    <script>
        $(document).ready(function () {
            $('#filter_toggle_btn').click(function () {
                $('#filter_form_wrapper').toggle();
                if ($(this).attr('attr-filter') == 'in-active') {
                    $(this).attr('attr-filter', 'active');
                    $(this).html('<i class="fa fa-filter"></i>&nbsp;&nbsp;&nbsp; Hide Filter');
                }
                else {
                    $(this).attr('attr-filter', 'in-active');
                    $(this).html('<i class="fa fa-filter"></i>&nbsp;&nbsp;&nbsp; Filter');
                }
            });


        });
    </script>
    @endsection