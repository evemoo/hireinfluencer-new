<fieldset>

    <section>
        {!! Form::label('seo_title', 'Seo Title') !!}
        <label class="input {{ AppHelper::ShowValidationClass($errors, 'seo_title') }}">
            {!! Form::text('seo_title', null,['placeholder' =>'Seo Title', 'required']) !!}
        </label>
        {{ AppHelper::showValidationMessage($errors, 'seo_title') }}
    </section>

    <section>
        {!! Form::label('seo_keyword', 'Seo Keyword') !!}
        <label class="input {{ AppHelper::ShowValidationClass($errors, 'seo_keyword') }}">
            {!! Form::text('seo_keyword', null,['placeholder' =>'Seo Keyword', 'required']) !!}
        </label>
        {{ AppHelper::showValidationMessage($errors, 'seo_keyword') }}
    </section>

    <section>
        {!! Form::label('seo_description', 'Seo Description') !!}
        <label class="textarea {{ AppHelper::ShowValidationClass($errors, 'seo_description') }}">
            {!! Form::textarea('seo_description', null,['placeholder' =>'Seo Description', 'required']) !!}
        </label>
        {{ AppHelper::showValidationMessage($errors, 'seo_description') }}
    </section>

</fieldset>


<footer>
    {!! Form::submit($submit,['class' => 'btn btn-primary', 'id' => 'submit']) !!}
</footer>