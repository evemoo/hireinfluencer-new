<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
    (function () {
        
        $(document).on('click', '#delete-item', function (e) {
            e.preventDefault();
            var $this = this;
            var id = $(this).data('id');
            var url = $(this).data('href');
            swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                },
                function(isConfirm) {
                    if(isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            headers:
                                {
                                    'X-CSRF-Token': $('input[name="_token"]').val()
                                },
                            data: {id:id},
                            success: function (data) {
                                console.log(data);

                                if(data.length == 0 && data == "")
                                {
                                    swal({
                                        title: "Cannot Delete!",
                                        text: 'The record cannot be deleted because its associated with some advertisements/categories.',
                                        type: "warning",
                                        timer: 5000
                                    });
                                } else {
                                    swal({
                                        title: "Deleted!",
                                        text: data,
                                        type: "success",
                                        timer: 5000
                                    });

                                    $this.closest('tr').remove();
                                }
                            },
                        });
                    } else {
                        //toastr.success('The deletion was cancelled!', 'Cancelled', {timer:5000});
                    }

                });
        });

    })();
</script>