//COUNTRY BLOCK

$('#country-code').DataTable({
    order: [[1, 'asc']],
    "columns": [
        null,
        null,
        { "searchable": false }
      ]
});

$('#block-list').DataTable({
    order: [[1, 'asc']],
    "columns": [
        null,
        null,
        { "searchable": false }
      ]
});

$('body').on('click', '.move', function(){
    var $this = $(this);
    var tr = $this.closest('tr');
    var c_code = tr.find('.key').html();
    var c_name = tr.find('.name').html();

    $('#block-list').dataTable().fnDestroy();
    $('#block-list').append(newRowGeoBlock(c_code, c_name));
    $('#block-list').dataTable({
        order: [[1, 'asc']],
        "columns": [
            null,
            null,
            { "searchable": false }
          ]
    });

    $('#country-code').dataTable().fnDestroy();
    tr.remove();
    $('#country-code').dataTable({
        order: [[1, 'asc']],
    "columns": [
        null,
        null,
        { "searchable": false }
      ]
    });
});

$('body').on('click', '.remove', function(){
    var $this = $(this);
    var tr = $this.closest('tr');
    var c_code = tr.find('.key').html();
    var c_name = tr.find('.name').find('input').val();

    $('#country-code').dataTable().fnDestroy();
    $('#country-code').append(newRowCountryList(c_code, c_name));
    $('#country-code').dataTable({
        order: [[1, 'asc']],
        "columns": [
            null,
            null,
            { "searchable": false }
          ]
    });

    $('#block-list').dataTable().fnDestroy();
    tr.remove();
    $('#block-list').dataTable({
        order: [[1, 'asc']],
        "columns": [
            null,
            null,
            { "searchable": false }
          ]
    });
});

function newRowGeoBlock(c_code, c_name) {
    var code_input = '<input class="code form-control" type="text" name="geo_block['+c_code+']" value="'+c_name+'" readonly hidden>'
    var row = '<tr><td class="key">'+c_code+'</td><td class="name">'+c_name+code_input+'</td><td><a href="javascript:;" class="remove">remove <i class="fa fa-close"></i></a></td></tr>';
    return row;
}

function newRowCountryList(c_code, c_name) {
    var row = '<tr><td class="key">'+c_code+'</td><td class="name">'+c_name+'</td><td><a href="javascript:;" class="move">move <i class="fa fa-arrow-right"></i></a></td></tr>';
    return row;
}