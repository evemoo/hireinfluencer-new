//IP BLOCK

$('#block-list-ip').DataTable({
    "columns": [
        null,
        { "searchable": false }
      ]
});

$('body').on('click', '.move-ip', function(){
    var $this = $(this);
    var row = $this.closest('.row');
    var val = row.find('#to-block-ip').val();

    $.validator.addMethod('IP4Checker', function(value) {
        var ip = "^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$";
            return value.match(ip);
        }, 'Invalid IP address');

        var ip = /^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/;
        if (val.match(ip)) {
            $('#invalid-ip').hide();
        }
        else {
            $('#invalid-ip').show();
            return false;
        }

    $('#block-list-ip').dataTable().fnDestroy();
    $('#block-list-ip').append(newRowIP(val));
    $('#block-list-ip').dataTable({
        "columns": [
        null,
        { "searchable": false }
      ]
    });

    row.find('#to-block-ip').val('');
});

$('body').on('click', '.remove-ip', function(){
    var $this = $(this);
    var tr = $this.closest('tr');

    $('#block-list-ip').dataTable().fnDestroy();
    tr.remove();
    $('#block-list-ip').dataTable({
        "columns": [
            null,
            { "searchable": false }
          ]
    });
});

function newRowIP(val) {
    var code_input = '<input class="code form-control" type="text" name="ip_block['+val+']" value="'+val+'" readonly hidden>'
    var row = '<tr><td>'+val+code_input+'</td><td><a href="javascript:;" class="remove-ip">remove <i class="fa fa-close"></i></a></td></tr>';
    return row;
}