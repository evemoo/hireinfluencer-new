<div class="ui form">
	<div class="two fields">

		<div class="field">
			<label>Country List</label>
            <table class="table" id="country-code">
            	<thead>
	            	<th>Code</th>
	            	<th>Country</th>
	            	<th>Action</th>
	            </thead>
	            @foreach(config('country-codes.country_codes') as $key => $country)
	            	@if(!array_key_exists($key, $settings['geo_block']))
			            <tr>
			            	<td class="key">{{ $key }}</td>
			            	<td class="name">{{ $country }}</td>
			            	<td><a href="javascript:;" class="move">move <i class="fa fa-arrow-right"></i></a></td>
			            </tr>
		            @endif
	            @endforeach
            </table>
        </div>

		@if (isset($settings['geo_block']))

		    <div class="field">
				<label>Block List</label>
	            <table class="table" id="block-list">
	            	<thead>
		            	<th>#</th>
		            	<th>Country</th>
		            	<th>Action</th>
		            </thead>
		            @foreach($settings['geo_block'] as $key => $geo)
		            	<tr>
			            	<td class="key">{{ $key }}</td>
			            	<td class="name">{{ $geo }}<input class="code form-control" type="text" name="geo_block[{{ $key }}]" value="{{ $geo }}" readonly hidden></td>
			            	<td><a href="javascript:;" class="remove">remove <i class="fa fa-close"></i></a></td>
			            </tr>
		            @endforeach
	            </table>
	        </div>

		@endif

	</div>

	<div class="two fields">

		<div class="field">
			<label>IP to Block</label>
			<div class="row">
				<label style="padding-left: 15px;">Block Specific IP Address</label>
				<div class="clearfix"></div>
				<div class="col-md-8">
					<input type="text" name="ip" id="to-block-ip" placeholder="IP Address e.g. 192.168.0.1" class="form-control" required pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$">
					<p id="invalid-ip" style="color: red;" hidden>Invalid Ip</p>
				</div>
				<div class="col-md-4">
					<button class="btn btn-info move-ip" type="button">add <i class="fa fa-arrow-right"></i></button>
				</div>
			</div>
        </div>

		@if (isset($settings['ip_block']))

		    <div class="field">
				<label>Block List</label>
	            <table class="table" id="block-list-ip">
	            	<thead>
		            	<th>IP</th>
		            	<th>Action</th>
		            </thead>
		            @foreach($settings['ip_block'] as $key => $ip)
		            	<tr>
			            	<td class="name">{{ $ip }}<input class="code form-control" type="text" name="ip_block[{{ $key }}]" value="{{ $ip }}" readonly hidden></td>
			            	<td><a href="javascript:;" class="remove-ip">remove <i class="fa fa-close"></i></a></td>
			            </tr>
		            @endforeach
	            </table>
	        </div>

		@endif

	</div>
</div>