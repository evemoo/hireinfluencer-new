<div class="ui form">

	<h3>{{ admin_trans($trans_path.'content.reg-title') }}</h3>

	<div class="two fields">

		@if (isset($settings['register_form_title_1']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="register_form_title_1_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.reg-form-title-1') }}</label>
		            {!! Form::text('register_form_title_1['.$options["code"].']',
		             $settings['register_form_title_1'][$options["code"]],
		             [
		              'id' => 'register_form_title_1',
		              'placeholder' => 'Register Form Title 1'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.register_form_title_1'))
		                    <p class="help-block has-error">{{ $errors->first('en.register_form_title_1') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['register_form_title_2']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="register_form_title_2_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.reg-form-title-2') }}</label>
		            {!! Form::text('register_form_title_2['.$options["code"].']',
		             $settings['register_form_title_2'][$options["code"]],
		             [
		              'id' => 'register_form_title_2',
		              'placeholder' => 'Register Form Title 2'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.register_form_title_2'))
		                    <p class="help-block has-error">{{ $errors->first('en.register_form_title_2') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

	</div>

	<div class="two fields">

		@if (isset($settings['registration_success_message']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="registration_success_message_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.reg-success-msg') }}</label>
		            {!! Form::text('registration_success_message['.$options["code"].']',
		             $settings['registration_success_message'][$options["code"]],
		             [
		              'id' => 'registration_success_message',
		              'placeholder' => 'Registration Success Message'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.registration_success_message'))
		                    <p class="help-block has-error">{{ $errors->first('en.registration_success_message') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['registration_fail_message']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="registration_fail_message_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.reg-fail-msg') }}</label>
		            {!! Form::text('registration_fail_message['.$options["code"].']',
		             $settings['registration_fail_message'][$options["code"]],
		             [
		              'id' => 'registration_fail_message',
		              'placeholder' => 'Registration Fail Message'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.registration_fail_message'))
		                    <p class="help-block has-error">{{ $errors->first('en.registration_fail_message') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

	</div>

	<hr>

	<h3>{{ admin_trans($trans_path.'content.login-title') }}</h3>

	<div class="two fields">

		@if (isset($settings['login_form_title_1']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="login_form_title_1_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.login-form-title-1') }}</label>
		            {!! Form::text('login_form_title_1['.$options["code"].']',
		             $settings['login_form_title_1'][$options["code"]],
		             [
		              'id' => 'login_form_title_1',
		              'placeholder' => 'Login Form Title 1'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.login_form_title_1'))
		                    <p class="help-block has-error">{{ $errors->first('en.login_form_title_1') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['login_form_title_2']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="login_form_title_2_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.login-form-title-2') }}</label>
		            {!! Form::text('login_form_title_2['.$options["code"].']',
		             $settings['login_form_title_2'][$options["code"]],
		             [
		              'id' => 'login_form_title_2',
		              'placeholder' => 'Login Form Title 2'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.login_form_title_2'))
		                    <p class="help-block has-error">{{ $errors->first('en.login_form_title_2') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

	</div>

	<div class="two fields">

		@if (isset($settings['login_fail_message']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="login_fail_message_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.login-fail-msg') }}</label>
		            {!! Form::text('login_fail_message['.$options["code"].']',
		             $settings['login_fail_message'][$options["code"]],
		             [
		              'id' => 'login_fail_message',
		              'placeholder' => 'Login Fail Message'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.login_fail_message'))
		                    <p class="help-block has-error">{{ $errors->first('en.login_fail_message') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

	</div>

	<hr>

	<h3>{{ admin_trans($trans_path.'content.pass-reset') }}</h3>

	<div class="two fields">

		@if (isset($settings['password_reset_request_form_title_1']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="password_reset_request_form_title_1_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.pass-reset-title-1') }}</label>
		            {!! Form::text('password_reset_request_form_title_1['.$options["code"].']',
		             $settings['password_reset_request_form_title_1'][$options["code"]],
		             [
		              'id' => 'password_reset_request_form_title_1',
		              'placeholder' => 'Password Reset Form Title 1'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.password_reset_request_form_title_1'))
		                    <p class="help-block has-error">{{ $errors->first('en.password_reset_request_form_title_1') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['password_reset_request_form_title_2']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="password_reset_request_form_title_2_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.pass-reset-title-2') }}</label>
		            {!! Form::text('password_reset_request_form_title_2['.$options["code"].']',
		             $settings['password_reset_request_form_title_2'][$options["code"]],
		             [
		              'id' => 'password_reset_request_form_title_2',
		              'placeholder' => 'Password Reset Form Title 2'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.password_reset_request_form_title_2'))
		                    <p class="help-block has-error">{{ $errors->first('en.password_reset_request_form_title_2') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

	</div>

	<div class="two fields">

		@if (isset($settings['password_reset_request_success_message']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="password_reset_request_success_message_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.pass-reset-success') }}</label>
		            {!! Form::text('password_reset_request_success_message['.$options["code"].']',
		             $settings['password_reset_request_success_message'][$options["code"]],
		             [
		              'id' => 'password_reset_request_success_message',
		              'placeholder' => 'Password Reset Success Message'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.password_reset_request_success_message'))
		                    <p class="help-block has-error">{{ $errors->first('en.password_reset_request_success_message') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['password_reset_request_fail_message']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="password_reset_request_fail_message_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.pass-reset-fail') }}</label>
		            {!! Form::text('password_reset_request_fail_message['.$options["code"].']',
		             $settings['password_reset_request_fail_message'][$options["code"]],
		             [
		              'id' => 'password_reset_request_fail_message',
		              'placeholder' => 'Password Reset Fail Message'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.password_reset_request_fail_message'))
		                    <p class="help-block has-error">{{ $errors->first('en.password_reset_request_fail_message') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

	</div>

	<hr>

	<h3>{{ admin_trans($trans_path.'content.pass-reset-red') }}</h3>

	<div class="two fields">

		@if (isset($settings['password_reset_form_title_1']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="password_reset_form_title_1_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.pass-reset-title-1') }}</label>
		            {!! Form::text('password_reset_form_title_1['.$options["code"].']',
		             $settings['password_reset_form_title_1'][$options["code"]],
		             [
		              'id' => 'password_reset_form_title_1',
		              'placeholder' => 'Password Reset Form Title 1'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.password_reset_form_title_1'))
		                    <p class="help-block has-error">{{ $errors->first('en.password_reset_form_title_1') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['password_reset_form_title_2']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="password_reset_form_title_2_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.pass-reset-title-2') }}</label>
		            {!! Form::text('password_reset_form_title_2['.$options["code"].']',
		             $settings['password_reset_form_title_2'][$options["code"]],
		             [
		              'id' => 'password_reset_form_title_2',
		              'placeholder' => 'Password Reset Form Title 2'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.password_reset_form_title_2'))
		                    <p class="help-block has-error">{{ $errors->first('en.password_reset_form_title_2') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

	</div>

	<div class="two fields">

		@if (isset($settings['password_reset_success_message']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="password_reset_success_message_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.pass-reset-success') }}</label>
		            {!! Form::text('password_reset_success_message['.$options["code"].']',
		             $settings['password_reset_success_message'][$options["code"]],
		             [
		              'id' => 'password_reset_success_message',
		              'placeholder' => 'Password Reset Success Message'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.password_reset_success_message'))
		                    <p class="help-block has-error">{{ $errors->first('en.password_reset_success_message') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['password_reset_fail_message']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="password_reset_fail_message_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.pass-reset-fail') }}</label>
		            {!! Form::text('password_reset_fail_message['.$options["code"].']',
		             $settings['password_reset_fail_message'][$options["code"]],
		             [
		              'id' => 'password_reset_fail_message',
		              'placeholder' => 'Password Reset Fail Message'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.password_reset_fail_message'))
		                    <p class="help-block has-error">{{ $errors->first('en.password_reset_fail_message') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

	</div>

</div>