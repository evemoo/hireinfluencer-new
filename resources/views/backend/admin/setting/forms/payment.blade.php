<div class="ui form">
	<div class="three fields">

		@if (isset($settings['paypal_status']))

			<div class="field">

	            <label>Active</label>
	            Yes {!! Form::radio('paypal_status', 1, $settings['paypal_status'] == 1?true:false) !!}
	            No {!! Form::radio('paypal_status', 0, $settings['paypal_status'] == 0?true:false) !!}
	        </div>

		@endif

		@if (isset($settings['paypal_client_id']))

			<div class="field">

	            <label>{{ admin_trans($trans_path.'content.paypal_client_id') }}</label>
	            {!! Form::text('paypal_client_id',
	             $settings['paypal_client_id'],
	             [
	              'id' => 'paypal_client_id',
	              'placeholder' => 'Paypal Client Id'
	              ]) !!}
	        </div>

		@endif

		@if (isset($settings['paypal_secret']))

		    <div class="field">

	            <label>{{ admin_trans($trans_path.'content.paypal_secret') }}</label>
	            {!! Form::text('paypal_secret',
	             $settings['paypal_secret'],
	             [
	              'id' => 'paypal_secret',
	              'placeholder' => 'Paypal Secret Id'
	              ]) !!}
	        </div>

		@endif

	</div>

	<div class="three fields">

		@if (isset($settings['stripe_status']))

			<div class="field">

	            <label>Active</label>
	            Yes {!! Form::radio('stripe_status', 1, $settings['stripe_status'] == 1?true:false) !!}
	            No {!! Form::radio('stripe_status', 0, $settings['stripe_status'] == 0?true:false) !!}
	        </div>

		@endif

		@if (isset($settings['stripe_client_id']))

			<div class="field">

	            <label>{{ admin_trans($trans_path.'content.stripe_client_id') }}</label>
	            {!! Form::text('stripe_client_id',
	             $settings['stripe_client_id'],
	             [
	              'id' => 'stripe_client_id',
	              'placeholder' => 'stripe Client Id'
	              ]) !!}
	        </div>

		@endif

		@if (isset($settings['stripe_secret']))

		    <div class="field">

	            <label>{{ admin_trans($trans_path.'content.stripe_secret') }}</label>
	            {!! Form::text('stripe_secret',
	             $settings['stripe_secret'],
	             [
	              'id' => 'stripe_secret',
	              'placeholder' => 'stripe Secret Id'
	              ]) !!}
	        </div>

		@endif

	</div>
</div>