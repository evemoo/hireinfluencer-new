<div class="ui form">

	<div class="two fields">

		@if (isset($settings['admin_type_users']))

			<div class="field">

		            <label>{{ admin_trans($trans_path.'content.admin-roles') }}</label>
		            {!! Form::select('admin_type_users[]', $roles,
		             $settings['admin_type_users'],
		             [
		              'id' => 'select2',
		              'class' => 'select2-multi',
		              'multiple',
		              ]) !!}
		        </div>

		@endif

		@if (isset($settings['normal_type_user']))

			<div class="field">

		            <label>{{ admin_trans($trans_path.'content.customer-roles') }}</label>
		            {!! Form::select('normal_type_user[]', $roles, $settings['normal_type_user'],
		             [
		              'id' => 'select2',
		              'class' => 'select2-multi',
		              'multiple',
		              ]) !!}
		        </div>

		@endif

	</div>

	<div class="">

		@if (isset($settings['role_to_assign_after_registration']))

			<div class="field">

		            <label>{{ admin_trans($trans_path.'content.role-after-reg') }}</label>
		            {!! Form::select('role_to_assign_after_registration', $roles, null,
		             [
		              'id' => 'role_to_assign_after_registration',
		              ]) !!}
		        </div>

		@endif
	
	</div>

</div>