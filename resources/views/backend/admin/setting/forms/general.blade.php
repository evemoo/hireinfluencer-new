<div class="ui form">

	<div class="two fields">

		@if (isset($settings['company']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="company_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.company-name') }}</label>
		            {!! Form::text('company['.$options["code"].']', $settings['company'][$options["code"]], ['id' => 'company', 'placeholder' => 'Company']) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.company'))
		                    <p class="help-block has-error">{{ $errors->first('en.company') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['domain']))

			<div class="field">

		            <label>{{ admin_trans($trans_path.'content.domain') }}</label>
		            {!! Form::text('domain',
		             $settings['domain'],
		             [
		              'id' => 'domain',
		              'placeholder' => 'Company Domain'
		              ]) !!}
		        </div>

		@endif

	</div>

	<div class="three fields">

		@if (isset($settings['email']))

			<div class="field">

		            <label>{{ admin_trans($trans_path.'content.email') }}</label>
		            {!! Form::text('email',
		             $settings['email'],
		             [
		              'id' => 'email',
		              'placeholder' => 'Email'
		              ]) !!}
		        </div>

		@endif

		@if (isset($settings['phone']))

		    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="phone_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.landline') }}</label>
		            {!! Form::text('phone['.$options["code"].']',
		             $settings['phone'][$options["code"]],
		             [
		              'id' => 'phone',
		              'placeholder' => 'Company Landline Number'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.phone'))
		                    <p class="help-block has-error">{{ $errors->first('en.phone') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['mobile']))

		    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="mobile_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.mobile') }}</label>
		            {!! Form::text('mobile['.$options["code"].']',
		             $settings['mobile'][$options["code"]],
		             [
		              'id' => 'mobile',
		              'placeholder' => 'Mobile Number'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.mobile'))
		                    <p class="help-block has-error">{{ $errors->first('en.mobile') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif
	
	</div>

	<div class="three fields">

		@if (isset($settings['fax']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="fax_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.fax') }}</label>
		            {!! Form::text('fax['.$options["code"].']',
		             $settings['fax'][$options["code"]],
		             [
		              'id' => 'fax',
		              'placeholder' => 'Fax'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.fax'))
		                    <p class="help-block has-error">{{ $errors->first('en.fax') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['location']))

		    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="location_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.location') }}</label>
		            {!! Form::text('location['.$options["code"].']',
		             $settings['location'][$options["code"]],
		             [
		              'id' => 'location',
		              'placeholder' => 'Location'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.location'))
		                    <p class="help-block has-error">{{ $errors->first('en.location') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['working_hours']))

		    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="working_hours_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.working-hours') }}</label>
		            {!! Form::text('working_hours['.$options["code"].']',
		             $settings['working_hours'][$options["code"]],
		             [
		              'id' => 'working_hours',
		              'placeholder' => 'Working Hours'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.working_hours'))
		                    <p class="help-block has-error">{{ $errors->first('en.working_hours') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif
	
	</div>

	<div class="two fields">

		@if (isset($settings['map']))

			<div class="field">

		            <label>{{ admin_trans($trans_path.'content.map') }}</label>
		            {!! Form::text('map',
		             $settings['map'],
		             [
		              'id' => 'map',
		              'placeholder' => admin_trans($trans_path.'content.map')
		              ]) !!}
		        </div>

		@endif

		@if (isset($settings['logo']))

			<div class="field">

		            <label>{{ admin_trans($trans_path.'content.logo') }}</label>
		            {!! Form::text('logo',
		             $settings['logo'],
		             [
		              'id' => 'logo',
		              'placeholder' => 'Logo'
		              ]) !!}
		        </div>

		@endif
	
	</div>

	<hr>
	<h3>Social Media Links</h3>

	@foreach($settings['social_links'] as $key => $social)

		<div class="two fields">

			<div class="field">

	            <label>{{ $social['title'] }}</label>
	            {!! Form::text('social_links['.$key.'][link]',
	             $social['link'],
	             [
	              'id' => 'link',
	              'placeholder' => 'Link'
	              ]) !!}
	        </div>

	        <div class="field">

	            <label>{{ admin_trans($trans_path.'content.icon') }}</label>
	            {!! Form::text('social_links['.$key.'][icon]]',
	             $social['icon'],
	             [
	              'id' => 'icon',
	              'placeholder' => 'Icon'
	              ]) !!}
	        </div>
	
		</div>

	@endforeach

</div>