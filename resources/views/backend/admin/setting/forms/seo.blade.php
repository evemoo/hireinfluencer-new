<div class="ui form">

	<div class="two fields">

		@if (isset($settings['seo_title']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="seo_title_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.seo-title') }}</label>
		            {!! Form::text('seo_title['.$options["code"].']',
		             $settings['seo_title'][$options["code"]],
		             [
		              'id' => 'seo_title',
		              'placeholder' => 'SEO Title'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.seo_title'))
		                    <p class="help-block has-error">{{ $errors->first('en.seo_title') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

		@if (isset($settings['seo_keywords']))

		    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="seo_keywords_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.seo-keywords') }}</label>
		            {!! Form::text('seo_keywords['.$options["code"].']',
		             $settings['seo_keywords'][$options["code"]],
		             [
		              'id' => 'seo_keywords',
		              'placeholder' => 'SEO Keywords'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.seo_keywords'))
		                    <p class="help-block has-error">{{ $errors->first('en.seo_keywords') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif

	</div>

	<div class="">

		@if (isset($settings['seo_description']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="seo_description_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.seo-description') }}</label>
		            {!! Form::textarea('seo_description['.$options["code"].']',
		             $settings['seo_description'][$options["code"]],
		             [
		              'id' => 'seo_description',
		              'placeholder' => 'SEO Description'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.seo_description'))
		                    <p class="help-block has-error">{{ $errors->first('en.seo_description') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif
	
	</div>

	<div class="">

		@if (isset($settings['contact_page_seo_title']))

			@foreach(ConfigHelper::evemooConfig('lang.options') as $options)

			<div class="field hide-this-field"
		             id="contact_page_seo_title_{{ $options['code'] }}"
		             style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">

		            <label>{{ admin_trans($trans_path.'content.seo-contact') }}</label>
		            {!! Form::text('contact_page_seo_title['.$options["code"].']',
		             $settings['contact_page_seo_title'][$options["code"]],
		             [
		              'id' => 'contact_page_seo_title',
		              'placeholder' => 'Contact Page SEO Title'
		              ]) !!}
		            @if($options['code'] == 'en')
		                @if (count($errors) > 0 && $errors->has('en.contact_page_seo_title'))
		                    <p class="help-block has-error">{{ $errors->first('en.contact_page_seo_title') }}</p>
		                @endif
		            @endif
		        </div>

		    @endforeach

		@endif
	
	</div>

</div>