@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.description') }}" name="description" />
@endsection


@section('style')   
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/css/dataTables.min.css') }}">
    <style>
        .code {
            color: #000 !important;
            background-color: #fbfcfd !important;
            border: 1px solid rgba(0,0,0,0) !important;
            padding: 0 !important;
            margin-top: -7px !important;
        }
    </style>
@endsection

@section('content')
    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">{{ admin_trans($trans_path.'content.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('admin.setting.index') }}">{{ admin_trans($trans_path.'content.setting') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-title"> {{ admin_trans($trans_path.'content.setting') }}
                    <small> > {{ admin_trans($trans_path.'content.list') }} </small>
                </h1>
            </div>
            <div class="col-lg-6">
                <div class="field pull-right" style="padding-top: 20px;">
                    <div class="ui dropdown selection">
                        <input type="hidden" class="lang"  name="lang">
                        <div class="text">
                            {{ ConfigHelper::evemooConfig('lang.options.'.ConfigHelper::evemooConfig('lang.default').'.title') }}
                        </div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            @foreach(ConfigHelper::evemooConfig('lang.options') as $options)
                                <div class="item" {{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'active selected':'' }}
                                     data-value="{{ $options['code'] }}">{{ $options['title'] }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">

            {!! Form::open([
               'method'  => 'POST',
               'route'   => [ $base_route.'.update' ],
                'id'=> 'settings_form',
               'enctype' => 'multipart/form-data'
           ]) !!}

            <div class="tabbable-line">

                <ul class="nav nav-tabs">

                    <li class="active">
                        <a href="#tab_1_1" data-toggle="tab" aria-expanded="true">{{ admin_trans($trans_path.'content.tabs.general') }}</a>
                    </li>

                    <li class="">
                        <a href="#tab_1_2" data-toggle="tab" aria-expanded="false">{{ admin_trans($trans_path.'content.tabs.seo') }}</a>
                    </li>

                    <li class="">
                        <a href="#tab_1_3" data-toggle="tab" aria-expanded="false">{{ admin_trans($trans_path.'content.tabs.content') }}</a>
                    </li>

                    <li class="">
                        <a href="#tab_1_4" data-toggle="tab" aria-expanded="false">{{ admin_trans($trans_path.'content.tabs.config') }}</a>
                    </li>

                    <li class="">
                        <a href="#tab_1_5" data-toggle="tab" aria-expanded="false">{{ admin_trans($trans_path.'content.tabs.payment') }}</a>
                    </li>

                    <li class="">
                        <a href="#tab_1_6" data-toggle="tab" aria-expanded="false">Geo Location Handle</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        @include('backend.admin.setting.forms.general')
                    </div>

                    <div class="tab-pane" id="tab_1_2">
                        @include('backend.admin.setting.forms.seo')
                    </div>

                    <div class="tab-pane" id="tab_1_3">
                        @include('backend.admin.setting.forms.content')
                    </div>

                    <div class="tab-pane" id="tab_1_4">
                        @include('backend.admin.setting.forms.config')
                    </div>

                    <div class="tab-pane" id="tab_1_5">
                        @include('backend.admin.setting.forms.payment')
                    </div>

                    <div class="tab-pane" id="tab_1_6">
                        @include('backend.admin.setting.forms.geo')
                    </div>
                </div>

            </div>

                <div class="ui form">

                    {{-- @include('backend.admin.service.partials._form') --}}

                    <div class="ui primary button save-setting" tabindex="0">{{ admin_trans($trans_path.'button.save') }}</div>

                </div>

            </div>


            {!! Form::close() !!}

        </div>

        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->

    </div>
@endsection

@section('script')

    <script src="{{ asset('assets/backend/js/dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.validate.min.js') }}" type="text/javascript"></script>

    <script>

        (function () {

            $('.lang').change(function (e) {

                $('.hide-this-field').hide();

                //general
                $('#company_'+$('.lang').val()).show();
                $('#phone_'+$('.lang').val()).show();
                $('#mobile_'+$('.lang').val()).show();
                $('#fax_'+$('.lang').val()).show();
                $('#location_'+$('.lang').val()).show();
                $('#working_hours_'+$('.lang').val()).show();

                //seo
                $('#seo_title_'+$('.lang').val()).show();
                $('#seo_keywords_'+$('.lang').val()).show();
                $('#seo_description_'+$('.lang').val()).show();
                $('#contact_page_seo_title_'+$('.lang').val()).show();

                //content
                $('#register_form_title_1_'+$('.lang').val()).show();
                $('#register_form_title_2_'+$('.lang').val()).show();
                $('#registration_success_message_'+$('.lang').val()).show();
                $('#registration_fail_message_'+$('.lang').val()).show();
                $('#login_form_title_1_'+$('.lang').val()).show();
                $('#login_form_title_2_'+$('.lang').val()).show();
                $('#login_fail_message_'+$('.lang').val()).show();
                $('#password_reset_request_form_title_1_'+$('.lang').val()).show();
                $('#password_reset_request_form_title_2_'+$('.lang').val()).show();
                $('#password_reset_request_success_message_'+$('.lang').val()).show();
                $('#password_reset_request_fail_message_'+$('.lang').val()).show();
                $('#password_reset_form_title_1_'+$('.lang').val()).show();
                $('#password_reset_form_title_2_'+$('.lang').val()).show();
                $('#password_reset_success_message_'+$('.lang').val()).show();
                $('#password_reset_fail_message_'+$('.lang').val()).show();

            });

            $('.save-setting').click(function () {
                $('#settings_form').submit();
            });

            $('.ui.dropdown').dropdown();

            @include('backend.admin.setting.includes.scripts.geo-block')

            @include('backend.admin.setting.includes.scripts.ip-block')

        })();

    </script>


@endsection

