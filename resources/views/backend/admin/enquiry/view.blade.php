@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('style')

    <link href="{{ asset('assets/backend/css/blog.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">
                        {{ admin_trans($trans_path.'content.common.enquiry') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.view') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-title"> {{ admin_trans($trans_path.'content.common.enquiries') }}
                    <small> > {{ admin_trans($trans_path.'content.list.view') }} </small>
                </h1>
            </div>
        </div>

        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">

            <div class="blog-page blog-content-2">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="blog-single-content bordered blog-container">
                            <div class="blog-single-head">
                                <h1 class="blog-single-head-title">{{ $enquiry->subject }}</h1>
                                <div class="blog-single-head-date">
                                    <i class="fa fa-calendar font-blue"></i>
                                    <a href="javascript:;">{{ ViewHelper::getHumanReadableDate($enquiry->created_at) }}</a>
                                </div>
                            </div>
                            <div class="blog-single-desc">
                                <p><strong>{{ admin_trans($trans_path.'content.respond.enquiry-service') }} </strong>{{ $enquiry->title }}</p>
                                <p>{{ $enquiry->message }}</p>
                            </div>
                            <hr>
                            @include('backend.common.discussion.discussion')
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="blog-single-sidebar bordered blog-container">
                            <div class="blog-single-sidebar-search">
                                <div class="input-icon right">
                                    <a href="{{ route('admin.package.create', ['package_type' => ConfigHelper::evemooConfig('package.enquiry_package.key'), 'enquiry_id' => $enquiry->enquiry_id]) }}"><button class="btn btn-primary">{{ admin_trans($trans_path.'button.custom-enquiry') }}</button></a>
                                </div>
                            </div>
                            <div class="blog-single-sidebar-recent">
                                <h3 class="blog-sidebar-title uppercase">{{ admin_trans($trans_path.'content.respond.enquiry-by') }}</h3>
                                <p><strong>{{ admin_trans($trans_path.'content.respond.enquiry-user') }} </strong>{{ $enquiry->user->username }}</p>
                                <p><strong>{{ admin_trans($trans_path.'content.respond.enquiry-deadline') }} </strong>{{ ViewHelper::getHumanReadableDate($enquiry->deadline) }}</p>
                                <p><strong>{{ admin_trans($trans_path.'content.respond.enquiry-status') }} </strong>{!! ViewHelper::getEnquiryStatus($enquiry->status) !!}</p>
                            </div>
                            <div class="blog-single-sidebar-recent">
                                <h3 class="blog-sidebar-title uppercase">{{ admin_trans($trans_path.'content.respond.enquiry-suggestions') }}</h3>
                                    <hr>
                                @foreach($packages as $key => $package)
                                    <h4><strong>{{ admin_trans($trans_path.'content.respond.enquiry-suggestion.title') }} {{ ++$key }}</strong></h4>
                                   <p><strong>{{ admin_trans($trans_path.'content.respond.enquiry-suggestion.price') }} </strong>{{ ConfigHelper::defaultCurrency().$package->new_price }} {{ admin_trans($trans_path.'content.respond.enquiry-suggestion.price-per') }} {{ $package->package_types->slug }}</p>
                                    <p><strong>{{ admin_trans($trans_path.'content.respond.enquiry-suggestion.duration') }} </strong>{{ $package->delivery_in_min }} - {{ $package->delivery_in_max }} {{ admin_trans($trans_path.'content.respond.enquiry-suggestion.duration-days') }}</p>
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->

    </div>

@endsection

@section('script')


    @include('backend.common.discussion.discussion-js')


@endsection
