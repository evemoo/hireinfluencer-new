@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">
                        {{ admin_trans($trans_path.'content.common.enquiries') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br />

        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">

                    <div class="caption">
                        <i class="fa fa-list font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">
                            {{ admin_trans($trans_path.'content.list.description') }}
                        </span>
                    </div>


                    <div class="actions">

                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">

                        @include('backend.common.flash')

                        <table class="table table-hover table-light">
                            <thead class="flip-content">
                            <tr>
                                <th width="10%"> {{ admin_trans($trans_path.'content.common.enquiryID') }} </th>
                                <th>{{ admin_trans($trans_path.'content.common.title') }}</th>
                                <th>{{ admin_trans($trans_path.'content.common.service') }}</th>
                                <th>{{ admin_trans($trans_path.'content.common.deadline') }}</th>
                                <th>{{ admin_trans($trans_path.'content.common.message') }}</th>
                                <th>{{ admin_trans($trans_path.'content.common.status') }}</th>
                                <th>{{ admin_trans($trans_path.'content.common.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if($enquiries->count() > 0)
                                @foreach($enquiries as $enquiry)
                                    <tr>
                                        <td> {{ $enquiry->enquiry_id }} </td>
                                        <td> {{ $enquiry->subject }} </td>
                                        <td> {{ $enquiry->title }} </td>
                                        <td> {{ $enquiry->deadline }} </td>
                                        <td> {{ substr($enquiry->message, 0, 200) }}  </td>
                                        <td> {!! ViewHelper::getEnquiryStatus($enquiry->status) !!} </td>
                                        <td>
                                            <a href="{{ route('admin.enquiry.view', $enquiry->enquiry_id) }}"
                                               class="btn btn-outline btn-circle btn-sm blue">
                                                <i class="fa fa-pencil"></i>
                                                {{ admin_trans($trans_path.'content.respond.respond') }}
                                            </a>

                                            {{-- <a data-href="{{ route($base_route.'.delete', $enquiry->id) }}"
                                               class="btn btn-outline btn-circle btn-sm red  delete-record-warning">
                                                <i class="fa fa-trash"></i>
                                                {{ admin_trans($trans_path.'content.delete.list') }}
                                            </a> --}}

                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="6">{{ $enquiries->appends(request()->all())->render() }}</td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="6">{{ admin_trans($trans_path.'error.no-data-found') }}</td>
                                </tr>
                            @endif

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>

    </div>

@endsection

@section('script')


    <script>

        ( function () {

            $('.ui.dropdown').dropdown();

            $('.onchange_lang_redirection').change(function () {
                var lang =  $(this).val();

                location.href = '{{ URL::current() }}'+'?lang='+lang;
            })


            $(document).on('click', '.delete-record-warning', function (e) {
                e.preventDefault();

                var url = $(this).attr('data-href');

                swal({
                        title: "Are you sure?",
                        text: "Are you sure that you want to delete this record?",
                        type: "warning",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        confirmButtonText: "Yes, delete it!",
                        confirmButtonColor: "#ec6c62"
                    },
                    function (isConfirm) {

                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: url,
                                headers: {
                                    'X-CSRF-Token': '{!! csrf_token() !!}'
                                },
                                success: function (data) {
                                    swal({
                                        title: "Deleted!",
                                        text: "The user is successfully deleted!",
                                        type: "success",
                                        timer: 5000
                                    });

                                    window.location.replace("{{ route($base_route.'.index') }}");
                                },
                            });
                        }
                    });
            });

        })();


    </script>


@endsection
