@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.create.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.create.description') }}" name="description" />
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">
                        {{ admin_trans($trans_path.'content.common.social-media') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.add.create') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

    <!-- MAIN CONTENT -->
        <!-- widget div-->
        
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-title"> {{ admin_trans($trans_path.'content.common.social-media') }}
                    <small> > {{ admin_trans($trans_path.'content.add.create') }} </small>
                </h1>
            </div>

            <div class="col-lg-6">
                <div class="field pull-right" style="padding-top: 20px;">
                    <div class="ui dropdown selection lang_dropdown_change">
                        <input type="hidden" class="lang"  name="lang">
                        <div class="text">
                            {{ ConfigHelper::evemooConfig('lang.options.'.ConfigHelper::evemooConfig('lang.default').'.title') }}
                        </div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            @foreach(ConfigHelper::evemooConfig('lang.options') as $options)
                                <div class="item" {{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'active selected':'' }}
                                     data-value="{{ $options['code'] }}">{{ $options['title'] }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="portlet light bordered">

            {!! Form::open(['route' => 'admin.socialmedia.attributes.store', 'method' => 'POST', 'class' => 'smart-form social-media', 'enctype' => 'multipart/form-data']) !!}
                <div class="portlet-body">

	                @include($view_path.'.attributes.form')

		        </div>
            {!! Form::close() !!}

        </div>
        <!-- end widget content -->

    </div>

@endsection

@section('script')

    <script>

        (function () {

            $('.lang').change(function (e) {

                $('.hide-this-field').hide();

                //general
                $('#title_'+$('.lang').val()).show();
                $('#description_'+$('.lang').val()).show();

            });

            $('.save-social-media').click(function () {
                $('.social-media').submit();
            });

            $('.ui.dropdown').dropdown();

        })();

    </script>

@endsection