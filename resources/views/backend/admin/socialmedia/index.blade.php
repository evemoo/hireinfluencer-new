@extends('backend.master.layout')

@section('meta')
    <title> Social Media | {{ admin_trans($trans_path.'page.index.title') }}</title>
    <meta content="{{ admin_trans($trans_path.'page.index.description') }}" name="description" />
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">
                        {{ admin_trans($trans_path.'content.common.social-media') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br />

        <div class="row">

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">

                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">
                                {{ admin_trans($trans_path.'content.list.description') }}
                            </span>
                        </div>

                        <div class="actions">

                            <div class="ui dropdown selection">
                                <input type="hidden" class="lang onchange_lang_redirection"  name="lang">
                                <div class="text">
                                    @if(request('lang'))
                                        {{ ConfigHelper::evemooConfig('lang.options.'.request('lang').'.title') }}
                                    @else
                                        {{ ConfigHelper::evemooConfig('lang.options.'.ConfigHelper::evemooConfig('lang.default').'.title') }}
                                    @endif
                                </div>
                                <i class="dropdown icon"></i>
                                <div class="menu">
                                    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)
                                        <div class="item" {{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'active selected':'' }}
                                             data-value="{{ $options['code'] }}">{{ $options['title'] }}
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <a href="{{ route($base_route.'.create') }}" onclick="location.href = '{{ route($base_route.'.create') }}'">

                                    <button type="button"
                                            class="btn btn-primary mt-ladda-btn ladda-button btn-circle"
                                            data-style="expand-right">
                                        <span class="ladda-label">
                                            <i class="fa fa-plus-circle"></i>
                                            {{ admin_trans($trans_path.'button.add') }}
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </a>
                            </div>

                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            @include('backend.common.flash')

                            <table class="table table-hover table-light">
                                <thead class="flip-content">
                                <tr>
                                    <th>{{ admin_trans($trans_path.'content.list.name') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.list.slug') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.list.icon') }}</th>
                                    <th>{{ admin_trans($trans_path.'content.list.action') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($medias->count() > 0)

                                    @foreach($medias as $media)
                                        <tr>
                                            <td> 
                                                {{ $media->name }}
                                            </td>
                                            <td> {{ $media->slug }} </td>
                                            <td>
                                                <i class="{{ $media->icon }}">&nbsp;{{ $media->icon }}</i>
                                            </td>
                                            <td>
                                                <a href="#"
                                                      class="btn btn-outline btn-circle btn-sm blue">
                                                    <i class="fa fa-pencil"></i>
                                                    {{ admin_trans($trans_path.'content.list.edit') }}
                                                </a>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <a id="delete-item" data-id="{{ $media->id }}" data-href="{{ route($base_route.'.delete', $media->id) }}"
                                                   class="btn btn-outline btn-circle btn-sm red  delete-record-warning">
                                                    <i class="fa fa-trash"></i>
                                                    {{ admin_trans($trans_path.'content.list.delete') }}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="6">{{ admin_trans($trans_path.'error.no-data-found') }}</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>



                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

    </div>

@endsection


@section('script')
    <script>

        (function () {

            $('.onchange_lang_redirection').change(function () {
                var lang =  $(this).val();

                location.href = '{{ URL::current() }}'+'?lang='+lang;
            })

            $('.ui.dropdown').dropdown();

        })();

    </script>

    @include('backend.admin.common.delete-js')
@endsection

