<div class="ui form">

    @foreach(ConfigHelper::evemooConfig('lang.options') as $options)
        <div class="field hide-this-field"
            id="title_{{ $options['code'] }}"
            style="{{ $options['code'] == ConfigHelper::evemooConfig('lang.default')?'':"display:none" }}">
            <label>Title</label>
            <label class="input {{ $errors->has($options['code'].'.name') ? ' state-error' : '' }}">
                {!! Form::text($options['code'].'[name]', isset($trans_cat[$options['code']][0]->name)?$trans_cat[$options['code']][0]->name:null, ['id' => 'name', 'class' => 'name']) !!}
            </label>
            @if ($errors->has($options['code'].'.name'))
                <div class="note note-error">{{ $errors->first($options['code'].'.name') }}</div>
            @endif
        </div>
    @endforeach

    <div class="field">
        <label>Icon</label>
        <label class="select">
            {!! Form::text('default[icon]', null, []) !!} <i></i> 
        </label>
        @if ($errors->has('default.icon'))
            <div class="note note-error">{{ $errors->first('default.icon') }}</div>
        @endif
    </div>

    <div class="field">
        <label>Status</label>
        <label class="select">
            {!! Form::select('default[status]', ['1' => admin_trans($trans_path.'content.common.active'), '0' => admin_trans($trans_path.'content.common.inactive')], isset($cat->status)?$cat->status:1, []) !!} <i></i> 
        </label>
        @if ($errors->has('default.status'))
            <div class="note note-error">{{ $errors->first('default.status') }}</div>
        @endif
    </div>

    <footer>
		<button type="submit" class="btn btn-primary">Save</button>
		<a class="btn btn-default" href="{{ Request::server('HTTP_REFERER') }}">Back</a>
    </footer>

</div>