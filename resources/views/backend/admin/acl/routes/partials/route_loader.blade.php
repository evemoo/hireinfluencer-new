<li class="sparks-info">
    <a class="btn btn-warning" href="{!! route('admin.routes.load') !!}" title="{{ trans($trans_path.'general.action.load-routes') }}" >
        <i class="fa fa-refresh"></i> Load Route </a>
</li>