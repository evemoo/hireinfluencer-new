@extends('backend.master.layout')

@section('style-css') @endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">{{ admin_trans($trans_path.'content.common.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ admin_trans($trans_path.'content.common.route') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br/>

        <div class="row">

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">

                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">
                                {{ admin_trans($trans_path.'content.list.description') }}
                            </span>
                        </div>


                        <div class="actions">

                            <div class="btn-group btn-group-devided" data-toggle="buttons">

                                <a href="{{ route($base_route.'.load') }}"
                                   onclick="location.href = '{{ route($base_route.'.load') }}'">

                                    <button type="button"
                                            class="btn btn-success mt-ladda-btn ladda-button btn-circle"
                                            data-style="expand-right">
                                        <span class="ladda-label">
                                            <i class="fa fa-refresh"></i>
                                            {{ admin_trans($trans_path.'button.load_route') }}
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </a>

                                <a href="{{ route($base_route.'.create') }}"
                                   onclick="location.href = '{{ route($base_route.'.create') }}'">

                                    <button type="button"
                                            class="btn btn-primary mt-ladda-btn ladda-button btn-circle"
                                            data-style="expand-right">
                                        <span class="ladda-label">
                                            <i class="fa fa-plus-circle"></i>
                                            {{ admin_trans($trans_path.'button.add') }}
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </a>

                            </div>

                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            @include('backend.common.flash')



                            <table class="table table-hover table-light">
                                <thead class="flip-content">
                                <tr>
                                    <th>{{ admin_trans($trans_path.'columns.permission') }}</th>
                                    <th>{{ admin_trans($trans_path.'columns.method') }}</th>
                                    <th>{{ admin_trans($trans_path.'columns.path') }}</th>
                                    <th>{{ admin_trans($trans_path.'columns.name') }}</th>
                                    <th>{{ admin_trans($trans_path.'columns.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($routes->count() > 0)


                                    @foreach($routes as $route)
                                        <tr>
                                            <td align="center">{!! Form::checkbox('chkRoute[]', $route->id) !!}</td>
                                            <td>{!! Form::select( 'perms['. $route->id .']', $perms, (isset($route->permission)?$route->permission->id:''), [ 'style' => 'max-width:150px;', 'class' => 'select-perms'] ) !!}</td>
                                        {{--    <td>{!! link_to_route('admin.routes.show', $route->method, [$route->id], []) !!}</td>
                                            <td>{!! link_to_route('admin.routes.show', $route->path, [$route->id], []) !!}</td>--}}

                                            <td>
                                                <a href="{!! route('admin.routes.edit', $route->id) !!}"
                                                   title="{{ trans('general.button.edit') }}">
                                                    <i class="fa fa-pencil-square-o"></i></a>


                                                @if ( $route->isDeletableBy() )
                                                    <a href="{!! route('admin.routes.confirm-delete', $route->id) !!}"
                                                       data-toggle="modal"
                                                       data-target="#modal_dialog"
                                                       title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
                                                @else
                                                    <i class="fa fa-trash-o text-muted" title="{{ trans($trans_path. 'general.error.cant-delete-this-route') }}"></i>
                                                @endif


                                            </td>
                                        </tr>
                                    @endforeach

                                    <tr>
                                        <td colspan="6">{{ $routes->appends(request()->all())->render() }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="6">{{ admin_trans($trans_path.'error.no-data-found') }}</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

    </div>



@endsection

@section('script-js')

@endsection
