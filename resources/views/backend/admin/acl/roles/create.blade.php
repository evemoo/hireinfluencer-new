@extends('backend.master.layout')

@section('style-css')
    <style>
        .note-error {
            color:red;
        }
    </style>

    <!-- Select2 css -->
    {{--@include('admin.partials._head_extra_select2_css')--}}

@endsection

@section('content')


    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">
                        {{ admin_trans($trans_path.'content.common.roles') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li><span>{{ admin_trans($trans_path.'content.add.create') }}</span></li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-title"> {{ admin_trans($trans_path.'content.common.roles') }}
                    <small> > {{ admin_trans($trans_path.'content.add.create') }} </small>
                </h1>
            </div>

        </div>




        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
{{--
            @include('backend.admin.common.error_msg')--}}
            {!! Form::open(['route' => $base_route.'.store', 'method' => 'post', 'id'=> 'form_edit_role', 'class'=> 'smart-form']) !!}



            <div class="portlet-body">

                <div class="ui form">

                    @include($view_path.'.partials._role_form')

                        {!! Form::submit(admin_trans($trans_path.'button.add'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit']) !!}

                   {{-- <div class="ui primary button save-service" tabindex="0">{{ admin_trans($trans_path.'button.add') }}</div>--}}
                    <div class="ui error message"></div>
                </div>

            </div>

            {!! Form::close() !!}

        </div>

        <div class="clearfix"></div>


    </div>

@endsection

@section('script-js')

    <!-- extra script -->
    {{--@include($view_path.'.partials.scripts')--}}



   {{-- @include($view_path.'.partials._body_bottom_submit_role_edit_form_js')--}}

@endsection
