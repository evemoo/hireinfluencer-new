@extends('backend.master.layout')

@section('style-css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">{{ admin_trans($trans_path.'content.common.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ admin_trans($trans_path.'content.common.roles') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br/>

        <div class="row">

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">

                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">
                                {{ admin_trans($trans_path.'content.list.description') }}
                            </span>
                        </div>


                        <div class="actions">

                            <div class="btn-group btn-group-devided" data-toggle="buttons">

                                <a href="{{ route($base_route.'.create') }}"
                                   onclick="location.href = '{{ route($base_route.'.create') }}'">

                                    <button type="button"
                                            class="btn btn-primary mt-ladda-btn ladda-button btn-circle"
                                            data-style="expand-right">
                                        <span class="ladda-label">
                                            <i class="fa fa-plus-circle"></i>
                                            {{ admin_trans($trans_path.'button.add') }}
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </a>
                            </div>

                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            @include('backend.common.flash')

                            <table class="table table-hover table-light">
                                <thead class="flip-content">
                                <tr>
                                    <th>{{ admin_trans($trans_path . 'columns.name') }}</th>
                                    <th>{{ admin_trans($trans_path . 'columns.display_name') }}</th>
                                    <th>{{ admin_trans($trans_path . 'columns.description') }}</th>
                                    <th>{{ admin_trans($trans_path . 'columns.permissions') }}</th>
                                    <th>{{ admin_trans($trans_path . 'columns.users') }}</th>
                                    <th>{{ admin_trans($trans_path . 'columns.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($roles->count() > 0)


                                    @foreach($roles as $role)
                                        <tr>

                                            <td>{!! link_to_route('admin.roles.show', $role->name, [$role->id], []) !!}</td>
                                            <td>{!! link_to_route('admin.roles.show', $role->display_name, [$role->id], []) !!}</td>
                                            <td>{{ $role->description }}</td>
                                            <td>{{ $role->perms->count() }}</td>
                                            <td>{{ $role->users->count() }}</td>
                                            <td>
                                                @if ( $role->isEditable() )
                                                    <a href="{!! route('admin.roles.edit', $role->id) !!}"
                                                       title="{{ trans('general.button.edit') }}"><i
                                                                class="fa fa-pencil-square-o"></i></a>
                                                @else
                                                    <i class="fa fa-pencil-square-o text-muted"
                                                       title="{{ trans($trans_path . 'general.error.cant-edit-this-role') }}"></i>
                                                @endif

                                                @if ( $role->enabled )
                                                    <a
                                                       title="{{ trans('general.button.disable') }}"><i
                                                                class="fa fa-check-circle-o enabled"></i></a>
                                                @else
                                                    <a
                                                       title="{{ trans('general.button.enable') }}"><i
                                                                class="fa fa-ban disabled"></i></a>
                                                @endif

                                                @if ( $role->isDeletable() )
                                                    <a id="delete-item" href="javascript:void();"
                                                       data-id="{{$role->id}}"
                                                       title="{{ trans('general.button.delete') }}"><i
                                                                class="fa fa-trash-o deletable"></i></a>
                                                @else
                                                    <i class="fa fa-trash-o text-muted"
                                                       title="{{ trans($trans_path . 'general.error.cant-delete-this-role') }}"></i>
                                                @endif
                                            </td>

                                            {{-- <td>
                                                 <a href="{{ route($base_route.'.edit', $package->id) }}"
                                                    class="btn btn-outline btn-circle btn-sm blue">
                                                     <i class="fa fa-pencil"></i>
                                                     {{ admin_trans($trans_path.'content.list.edit') }}
                                                 </a>

                                                 <a data-href="{{ route($base_route.'.delete', $package->id) }}"
                                                    class="btn btn-outline btn-circle btn-sm red  delete-record-warning">
                                                     <i class="fa fa-trash"></i>
                                                     {{ admin_trans($trans_path.'content.list.delete') }}
                                                 </a>

                                             </td>--}}
                                        </tr>
                                    @endforeach

                                    <tr>
                                        <td colspan="6">{{ $roles->appends(request()->all())->render() }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="6">{{ admin_trans($trans_path.'error.no-data-found') }}</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

    </div>



@endsection

@section('script-js')

    {{-- @include('admin.common.script')--}}
    {{-- @include('backend.admin.includes.delete')--}}


@endsection
