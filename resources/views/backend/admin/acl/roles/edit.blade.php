@extends('backend.master.layout')

@section('style-css')


@endsection

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        {{ admin_trans($trans_path.'content.common.dashboard') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">
                        {{ admin_trans($trans_path.'content.common.roles') }}
                    </a>
                    <i class="fa fa-circle"></i>
                </li>
                <li><span>{{ admin_trans($trans_path.'content.add.create') }}</span></li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-title"> {{ admin_trans($trans_path.'content.common.roles') }}
                    <small> > {{ admin_trans($trans_path.'content.add.create') }} </small>
                </h1>
            </div>

        </div>




        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            {{--
                        @include('backend.admin.common.error_msg')--}}
            {!! Form::model( $role, ['route' => ['admin.roles.update', $role->id], 'method' => 'POST', 'id' => 'form_edit_role', 'class' => 'smart-form'] ) !!}



            <div class="portlet-body">

                <div class="ui form">

                    @include($view_path.'.partials._role_form')

                    {!! Form::submit( admin_trans($trans_path.'button.update'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit']) !!}


                    {{-- <div class="ui primary button save-service" tabindex="0">{{ admin_trans($trans_path.'button.add') }}</div>--}}
                    <div class="ui error message"></div>
                </div>

            </div>

            {!! Form::close() !!}

        </div>

        <div class="clearfix"></div>


    </div>

@endsection

@section('script')

    <script type="text/javascript">

        // DO NOT REMOVE : GLOBAL FUNCTIONS!

        $(document).ready(function() {
            // PAGE RELATED SCRIPTS

            /*$('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');*/
            /*$('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {*/
            $('.tree').find('li:has(ul)').attr('role', 'treeitem').find(' > span').on('click', function(e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(':visible')) {
                    children.hide('fast');
                    $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
                } else {
                    children.show('fast');
                    $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
                }
                e.stopPropagation();
            });

            $(".checkbox-inline").change(function() {
                // save state of parent
                c = $(this).is(':checked');
                $(this).parent().parent().find("input[type=checkbox]").each(function() {
                    // set state of siblings
                    $(this).prop('checked', c);
                    recursiveTravelChildToParent($(this));
                })
            });

            // Update parent checkbox based on children
            $("input[type=checkbox]:not('.parent_checkbox')").change(function() {
                recursiveTravelChildToParent($(this));
            });


        });


        function recursiveTravelChildToParent(node, action) {

            if (node.closest('ul').parent('li').find('.parent_checkbox').length > 0) {

                if (node.closest("ul").find("input[type=checkbox]:not('.parent_checkbox')").not(':checked').length < 1)
                    node.closest('ul').parent('li').find('.parent_checkbox').first().prop('checked', true);
                else
                    node.closest('ul').parent('li').find('.parent_checkbox').first().prop('checked', false);

                recursiveTravelChildToParent(node.closest('ul').parent('li').find('.parent_checkbox').first(), action);

            }
        }

        function recursiveTravelParentToChild(node) {

            if (node.find("input[type=checkbox]:not('.parent_checkbox')").not(':checked').length < 1) {

                node.find("input[type=checkbox]").prop('checked', true);

            } else {

                if (node.hasClass('parent_li')) {

                    node.find('ul[role="group"]').find('li[class="parent_li"]').each(function( index, elem ) {

                        recursiveTravelParentToChild($(this));

                    });

                }

            }

        }

    </script>

@endsection
