<?php $readonly = ($role->isEditable())? 'd' : 'readonly'; ?>
<?php $membershipFixed = ($role->canChangeMembership())? 'd' : 'disabled'; ?>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_details" data-toggle="tab" aria-expanded="true">{!! admin_trans($trans_path.'tabs.details') !!}</a></li>
        <li class=""><a href="#tab_options" data-toggle="tab" aria-expanded="false">{!! admin_trans($trans_path.'tabs.options') !!}</a></li>
        <li class=""><a href="#tab_perms" data-toggle="tab" aria-expanded="false">{!! admin_trans($trans_path.'tabs.perms') !!}</a></li>
       {{-- <li class=""><a href="#tab_users" data-toggle="tab" aria-expanded="false">{!! admin_trans($trans_path.'tabs.users') !!}</a></li>--}}
    </ul>
    <div class="tab-content">

        <div class="tab-pane active" id="tab_details">

            <fieldset>

                <section>
                    {!! Form::label('name', admin_trans($trans_path. 'columns.name') ) !!}
                    <label class="input">

                        {!! Form::text('name', null, ['class' => 'form-control', $readonly]) !!}

                    </label>
                    @if (count($errors) > 0 && $errors->has('country_id'))
                        <p class="help-block has-error">{{ $errors->first('country_id') }}</p>
                    @endif
                </section>

                <section>
                    {!! Form::label('display_name', admin_trans($trans_path. 'columns.display_name') ) !!}
                    <label class="input">

                        {!! Form::text('display_name', null, ['class' => 'form-control', $readonly]) !!}

                    </label>
                    @if (count($errors) > 0 && $errors->has('display_name'))
                        <p class="help-block has-error">{{ $errors->first('display_name') }}</p>
                    @endif
                </section>

                <section>
                    {!! Form::label('description', admin_trans($trans_path. 'columns.description') ) !!}
                    <label class="textarea">

                        {!! Form::text('description', null, ['class' => 'form-control', $readonly]) !!}

                    </label>
                    @if (count($errors) > 0 && $errors->has('description'))
                        <p class="help-block has-error">{{ $errors->first('description') }}</p>
                    @endif
                </section>

            </fieldset>

        </div><!-- /.tab-pane -->

        <div class="tab-pane" id="tab_options">

            <fieldset>
                <section>
                    <div class="form-group">
                        <div class="">
                            <label>
                                {!! '<input type="hidden" name="enabled" value="0">' !!}
                                <label class="checkbox">
                                    {!! Form::checkbox('enabled', 1, $role->enabled) !!}
                                    <i></i>{!! trans('general.status.enabled') !!}
                                </label>
                            </label>
                        </div>
                    </div>
                </section>
            </fieldset>
        </div><!-- /.tab-pane -->

        <div class="tab-pane" id="tab_perms">
            <fieldset>
                <section>
                    @include($view_path.'.partials.permission_tab_content')
                </section>
            </fieldset>
        </div><!-- /.tab-pane -->

     {{--   <div class="tab-pane" id="tab_users">

            <fieldset>
                <section>
                    <div class="form-group">
                        {!! Form::hidden('selected_users', null, [ 'id' => 'selected_users']) !!}
                        <div class="input-group select2-bootstrap-append">
                            {!! Form::select('user_search', [], null, ['class' => 'form-control select2', 'id' => 'user_search',  'style' => "width: 100%", $membershipFixed]) !!}
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="btn-add-user" type="button" {!! $membershipFixed !!}>
                                    <span class="fa fa-plus-square"></span>
                                </button>
                            </span>
                        </div>

                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover" id="tbl-users">
                                <tbody>
                                <tr>
                                    <th class="hidden" rowname="id">{!! trans(AppHelper::getTransPathFromViewPath('acl.users'). 'general.columns.id')  !!}</th>
                                    <th>{!! trans(AppHelper::getTransPathFromViewPath('admin.acl.users'). 'general.columns.name')  !!}</th>
                                    <th>{!! trans(AppHelper::getTransPathFromViewPath('admin.acl.users'). 'general.columns.username')  !!}</th>
                                    <th>{!! trans(AppHelper::getTransPathFromViewPath('admin.acl.users'). 'general.columns.enabled')  !!}</th>
                                    <th style="text-align: right">{!! trans(AppHelper::getTransPathFromViewPath('admin.acl.users'). 'general.columns.actions')  !!}</th>
                                </tr>
                                @foreach($role->users as $user)
                                    <tr>
                                        <td class="hidden" rowname="id">{!! $user->id !!}</td>
                                        <td>{!! link_to_route('admin.users.show', $user->full_name, [$user->id], []) !!}</td>
                                        <td>{!! link_to_route('admin.users.show', $user->username, [$user->id], []) !!}</td>
                                        <td>
                                            @if($user->enabled)
                                                <i class="fa fa-check text-green"></i>
                                            @else
                                                <i class="fa fa-close text-red"></i>
                                            @endif
                                        </td>
                                        <td style="text-align: right">
                                            <a class="btn-remove-user" href="#" title="{{ trans('general.button.remove-user') }}"><i class="fa fa-trash-o deletable"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->

                    </div>
                </section>
            </fieldset>
        </div><!-- /.tab-pane -->--}}

    </div><!-- /.tab-content -->
</div>




