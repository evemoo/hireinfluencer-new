@extends('backend.master.layout')

@section('content')

    <div class="page-content">

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">{{ admin_trans($trans_path.'content.common.dashboard') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ admin_trans($trans_path.'content.common.permission') }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ admin_trans($trans_path.'content.list.list') }}</span>
                </li>
            </ul>
            <div class="page-toolbar"></div>
        </div>

        <br/>

        <div class="row">

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">

                        <div class="caption">
                            <i class="fa fa-list font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">
                                {{ admin_trans($trans_path.'content.list.description') }}
                            </span>
                        </div>


                        <div class="actions">

                            <div class="btn-group btn-group-devided" data-toggle="buttons">

                                <a href="{{ route($base_route.'.generate') }}"
                                   onclick="location.href = '{{ route($base_route.'.generate') }}'">

                                    <button type="button"
                                            class="btn btn-success mt-ladda-btn ladda-button btn-circle"
                                            data-style="expand-right">
                                        <span class="ladda-label">
                                            <i class="fa fa-refresh"></i>
                                            {{ admin_trans($trans_path.'button.load_permission') }}
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </a>

                                <a href="{{ route($base_route.'.create') }}"
                                   onclick="location.href = '{{ route($base_route.'.create') }}'">

                                    <button type="button"
                                            class="btn btn-primary mt-ladda-btn ladda-button btn-circle"
                                            data-style="expand-right">
                                        <span class="ladda-label">
                                            <i class="fa fa-plus-circle"></i>
                                            {{ admin_trans($trans_path.'button.add') }}
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </a>

                            </div>

                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            @include('backend.common.flash')

                            <table class="table table-hover table-light">
                                <thead class="flip-content">
                                    <tr>
                                        <th>{{ admin_trans($trans_path. 'columns.display_name') }}</th>
                                        <th>{{ admin_trans($trans_path. 'columns.description') }}</th>
                                        <th>{{ admin_trans($trans_path. 'columns.routes') }}</th>
                                        <th>{{ admin_trans($trans_path. 'columns.roles') }}</th>
                                        <th>{{ admin_trans($trans_path. 'columns.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @if($perms->count() > 0)


                                    @foreach($perms as $perm)
                                        <tr>
                                            <td align="center">{!! Form::checkbox('chkPerm[]', $perm->id); !!}</td>
                                            <td>{!! link_to_route('admin.permissions.show', $perm->display_name, [$perm->id], []) !!}</td>
                                            <td>{!! link_to_route('admin.permissions.show', $perm->description, [$perm->id], []) !!}</td>
                                            <td>{{ $perm->routes->count() }}</td>
                                            <td>{{ $perm->roles->count() }}</td>
                                            <td>
                                                @if ( $perm->isEditable() )
                                                    <a href="{!! route('admin.permissions.edit', $perm->id) !!}" title="{{ trans('general.button.edit') }}"><i class="fa fa-pencil-square-o"></i></a>
                                                @else
                                                    <i class="fa fa-pencil-square-o text-muted" title="{{ trans($trans_path. 'general.error.cant-edit-this-permission') }}"></i>
                                                @endif

                                                @if ( $perm->enabled )
                                                    <a {{--href="{!! route('admin.permissions.disable', $perm->id) !!}"--}} title="{{ trans('general.button.disable') }}"><i class="fa fa-check-circle-o enabled"></i></a>
                                                @else
                                                    <a {{--href="{!! route('admin.permissions.enable', $perm->id) !!}"--}} title="{{ trans('general.button.enable') }}"><i class="fa fa-ban disabled"></i></a>
                                                @endif

                                                @if ( $perm->isDeletable() )
                                                    <a href="{!! route('admin.permissions.confirm-delete', $perm->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
                                                @else
                                                    <i class="fa fa-trash-o text-muted" title="{{ trans($trans_path. 'general.error.cant-delete-perm-in-use') }}"></i>
                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach

                                    <tr>
                                        <td colspan="6">{{ $perms->appends(request()->all())->render() }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="6">{{ admin_trans($trans_path.'error.no-data-found') }}</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

    </div>


    <div id="main" role="main">


    <!-- MAIN CONTENT -->
        <div id="content">

            <section id="widget-grid">
                <div class="row">

                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
                        {!!  Form::open( array('id' => 'frmDataList') )  !!}

                        <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-0"
                             data-widget-editbutton="false" role="widget" style="position: relative; opacity: 1; left: 0px; top: 0px;">

                            <!-- widget div-->
                            <div role="content">

                                <!-- widget content -->
                                <div class="widget-body">
                                    <div class="table-responsive">



                                    </div>
                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end widget div -->

                        </div>
                        {!! Form::close() !!}

                    </article>

                </div>
            </section>

        </div>

    </div>
@endsection


            <!-- Optional bottom section for modals etc... -->
@section('body_bottom')
    <script language="JavaScript">
        function toggleCheckbox() {
            checkboxes = document.getElementsByName('chkPerm[]');
            for(var i=0, n=checkboxes.length;i<n;i++) {
                checkboxes[i].checked = !checkboxes[i].checked;
            }
        }
    </script>
@endsection
