<li class="sparks-info">
    <a class="btn btn-primary" href="{{ route($base_route.'.generate') }}">
        <i class="fa fa-refresh"></i> Regenerate {{ str_singular($panel) }} </a>
</li>