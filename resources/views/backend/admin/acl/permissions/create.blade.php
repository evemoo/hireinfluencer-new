@extends('admin.layout.master')

@section('head_extra')
    <!-- Select2 css -->
    {{--@include('partials._head_extra_select2_css')--}}
@endsection

@section('content')


    <div id="main" role="main">

    @include('admin.common.primary_breadcrumb')

    <!-- MAIN CONTENT -->
        <div id="content">
            @include('admin.common.sub_breadcrumb')

            <section id="widget-grid">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
                        <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                            <!-- widget div-->
                            <div role="content">
                                @include('admin.common.error_msg')

                                <div class="widget-body no-padding">
                                    {!! Form::open( ['route' => $base_route.'.store', 'id' => 'form_edit_permission',  'class'=> 'smart-form'] ) !!}

                                    @include($view_path.'.partials._permission_form')

                                    <footer>
                                        {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit']) !!}
                                    </footer>

                                    {!! Form::close() !!}
                                </div>

                            </div>
                            <!-- end widget div -->
                        </div>
                    </article>
                </div>
            </section>
        </div>
        <!-- END MAIN CONTENT -->

    </div>




@endsection

@section('body_bottom')
    <!-- Select2 js -->
    {{--@include('partials._body_bottom_select2_js_route_search')
    @include('partials._body_bottom_select2_js_role_search')
    <!-- form submit -->
    @include('partials._body_bottom_submit_permission_edit_form_js')--}}
@endsection
