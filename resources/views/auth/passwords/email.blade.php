<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title>Admin Panel : Reset Password</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Admin Panel" name="description" />
    <meta content="" name="author" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/login.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class=" login">

<!-- BEGIN LOGIN -->
<div class="content">

    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" method="POST" action="{{ route('password.reset') }}" >
        {{ csrf_field() }}
        <h3 class="form-title font-green">Reset Password</h3>

        @if(Session::has('success'))

            <div class="note note-{{ 'success' }}">
                <h3>Successfully</h3>
                <p>{{ Session::get('success') }}</p>
            </div>

        @endif

        <div class="form-group  {{ $errors->has('email') ? ' has-error' : '' }}">

            <label class="control-label visible-ie8 visible-ie9">E-Mail Address</label>

            <input class="form-control form-control-solid placeholder-no-fix" type="email"
                   autocomplete="off" placeholder="Email" value="{{ old('email') }}" autofocus name="email" id="email" />

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-actions">
            <button type="submit" class="btn green uppercase">Send Reset Password.</button>

            <a href="{{ route('login') }}" class="forget-password">Login From Here</a>
        </div>


    </form>
    <!-- END LOGIN FORM -->

</div>
<div class="copyright"> 2014 © Metronic. Admin Dashboard Template. </div>

<script src="{{ asset('js/backend.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/login.min.js') }}" type="text/javascript"></script>

</body>

</html>

