<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title>Admin Panel</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Admin Panel" name="description" />
    <meta content="" name="author" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/login.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class=" login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="../assets/pages/img/logo-big.png" alt="" /> </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    @if(session()->has('warning'))
        <span class="help-block">{{ session()->get('warning') }}</span>
    @endif
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" method="POST" action="{{ route('login') }}" >
        {{ csrf_field() }}
        <h3 class="form-title font-green">Sign In</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter any username and password. </span>
        </div>
        <div class="form-group  {{ $errors->has('email') ? ' has-error' : '' }}">

            <label class="control-label visible-ie8 visible-ie9">Email</label>

            <input class="form-control form-control-solid placeholder-no-fix" type="email"
                   autocomplete="off" placeholder="Email" value="{{ old('email') }}" required autofocus name="email" id="email" />

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix"
                   type="password"
                   autocomplete="off"
                   placeholder="Password"
                   name="password" required />
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-actions">
            <button type="submit" class="btn green uppercase">Login</button>
            <label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" value="1" {{ old('remember') ? 'checked' : '' }}/>Remember
                <span></span>
            </label>
            <a href="{{ route('password.request') }}" id="forget-password" class="forget-password">Forgot Password?</a>
        </div>


    </form>
    <!-- END LOGIN FORM -->

    <p>Don't Have an Account? <a href="{{ route('register') }}">Register</a></p>

</div>
<div class="copyright"> {{ date('Y') }} © Metronic. Admin Dashboard Template. </div>

<script src="{{ asset('js/backend.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/login.min.js') }}" type="text/javascript"></script>

</body>

</html>

