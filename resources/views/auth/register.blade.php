<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title>Admin Panel</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Admin Panel" name="description" />
    <meta content="" name="author" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/login.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class=" login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="../assets/pages/img/logo-big.png" alt="" /> </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    @if(session()->has('warning'))
        <span class="help-block">{{ session()->get('warning') }}</span>
    @endif

    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" id="register" method="POST" action="{{ route('register') }}" >
        {{ csrf_field() }}
        <h3 class="form-title font-green">Register</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter any username and password. </span>
        </div>

        <div class="form-group  {{ $errors->has('first_name') ? ' has-error' : '' }}">

            <label class="control-label visible-ie8 visible-ie9">First Name</label>

            <input class="form-control form-control-solid placeholder-no-fix" type="name"
                   autocomplete="off" placeholder="First Name" value="{{ old('first_name') }}" required autofocus name="first_name" id="first_name" />

            @if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group  {{ $errors->has('last_name') ? ' has-error' : '' }}">

            <label class="control-label visible-ie8 visible-ie9">Last Name</label>

            <input class="form-control form-control-solid placeholder-no-fix" type="last_name"
                   autocomplete="off" placeholder="Last Name" value="{{ old('last_name') }}" required autofocus name="last_name" id="last_name" />

            @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group  {{ $errors->has('name') ? ' has-error' : '' }}">

            <label class="control-label visible-ie8 visible-ie9">UserName</label>

            <input class="form-control form-control-solid placeholder-no-fix" type="name"
                   autocomplete="off" placeholder="Username" value="{{ old('name') }}" required autofocus name="name" id="name" />

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group  {{ $errors->has('email') ? ' has-error' : '' }}">

            <label class="control-label visible-ie8 visible-ie9">Email</label>

            <input class="form-control form-control-solid placeholder-no-fix" type="email"
                   autocomplete="off" placeholder="Email" value="{{ old('email') }}" required autofocus name="email" id="email" />

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix"
                   type="password"
                   autocomplete="off"
                   placeholder="Password"
                   name="password" required />
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
            <input class="form-control form-control-solid placeholder-no-fix"
                   type="password"
                   autocomplete="off"
                   placeholder="Confirm Password"
                   name="password_confirmation" required />
            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-actions">
            <button type="submit" class="btn green uppercase">Register</button>
        </div>


    </form>
    <!-- END LOGIN FORM -->

    <p>Already Have an Account? <a href="{{ route('login') }}">Login</a></p>

</div>
<div class="copyright"> {{ date('Y') }} © Metronic. Admin Dashboard Template. </div>

<script src="{{ asset('assets/backend/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js" type="text/javascript"></script>
<script src="{{ asset('js/backend.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/login.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $('#register').validate();
</script>

</body>

</html>


