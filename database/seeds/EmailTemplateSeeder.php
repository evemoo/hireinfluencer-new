<?php

use Illuminate\Database\Seeder;

use App\Evemoo\Models\EmailTemplatePattern;
use App\Evemoo\Models\EmailTemplate;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

	    EmailTemplatePattern::truncate();

	    foreach ( config( 'evemoo.email_patterns' ) as $key => $value ) {

		    EmailTemplatePattern::create([
			    'pattern'    => $key,
			    'label'      => $value,
			    'created_at' => \Carbon\Carbon::now(),
			    'updated_at' => \Carbon\Carbon::now(),
		    ]);

	    }

	    EmailTemplate::truncate();

	    foreach ( config( 'evemoo.email_templates' ) as $key ) {
		    EmailTemplate::create( [
			    'title'      => $key['title'],
			    'slug'       => $key['slug'],
			    'content'    => $key['content'],
			    'created_at' => \Carbon\Carbon::now(),
			    'updated_at' => \Carbon\Carbon::now(),
		    ] );
	    }

	    \Illuminate\Support\Facades\DB::table('pattern_template')->truncate();

		$pattern_one =  EmailTemplatePattern::whereIn(
			'pattern', [
				'{USER_FULL_NAME}' ,
				'{APP_NAME}',
				'{WEB_LINK}'
			]
		)->pluck('id');

	    EmailTemplate::where('slug', 'email_welcome_template')
			 ->first()
			 ->patterns()
	         ->sync($pattern_one);

	    $pattern_two =  EmailTemplatePattern::whereIn(
		    'pattern', [
			    '{USER_NAME}' ,
			    '{ADS_LIST}',
			    '{WEB_LINK}'
		    ]
	    )->pluck('id');

	    EmailTemplate::where('slug', 'email_advertise_template')
	        ->first()
			->patterns()
	        ->sync($pattern_two);

	    $pattern_three =  EmailTemplatePattern::whereIn(
		    'pattern', [
			    '{USER_NAME}' ,
			    '{MESSAGE}',
			    '{WEB_LINK}'
		    ]
	    )->pluck('id');

	    EmailTemplate::where('slug', 'email_send_message')
		        ->first()
				->patterns()
	            ->sync($pattern_three);

	    $pattern_four =  EmailTemplatePattern::whereIn(
		    'pattern', [
			    '{USER_NAME}' ,
			    '{OFFERED_PRICE}',
			    '{WEB_LINK}'
		    ]
	    )->pluck('id');

	    EmailTemplate::where('slug', 'email_send_offer')
	        ->first()
			->patterns()
	        ->sync($pattern_four);

	    $pattern_five =  EmailTemplatePattern::whereIn(
		    'pattern', [
			    '{USER_NAME}' ,
			    '{VERIFICATION_TOKEN}',
			    '{WEB_LINK}'
		    ]
	    )->pluck('id');

	    EmailTemplate::where('slug', 'reset_user_password')
	        ->first()
			->patterns()
	        ->sync($pattern_five);

	    $pattern_six =  EmailTemplatePattern::whereIn(
		    'pattern', [
			    '{USER_FULL_NAME}' ,
			    '{USER_ACTIVATION_LINK}',
			    '{WEB_LINK}'
		    ]
	    )->pluck('id');

	    EmailTemplate::where('slug', 'send_user_activation_code')
	        ->first()
			->patterns()
	        ->sync($pattern_six);

	    $pattern_seven =  EmailTemplatePattern::whereIn(
		    'pattern', [
		    	'{APP_LOGO}',
			    '{USER_NAME}' ,
			    '{USER_EMAIL}',
			    '{USER_PHONE}',
			    '{USER_ADDRESS}',
			    '{MESSAGE}',
			    '{WEB_LINK}'
		    ]
	    )->pluck('id');

	    EmailTemplate::where('slug', 'contact_us')
	          ->first()
	          ->patterns()
	          ->sync($pattern_seven);

	    $pattern_eight =  EmailTemplatePattern::whereIn(
		    'pattern', [
			    '{HOTEL_NAME}',
			    '{USER_NAME}' ,
			    '{USER_EMAIL}',
			    '{USER_PHONE}',
			    '{NO_OF_ROOMS}',
			    '{NO_OF_PEOPLES}',
			    '{CHECK_IN_DATE}',
			    '{CHECK_OUT_DATE}',
			    '{MESSAGE}',
			    '{WEB_LINK}'
		    ]
	    )->pluck('id');

	    EmailTemplate::where('slug', 'hotel_booking_enquiry')
	          ->first()
	          ->patterns()
	          ->sync($pattern_eight);
    }

}
