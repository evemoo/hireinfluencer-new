<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserTableSeeder::class);
         $this->call(RoleTableSeeder::class);
         $this->call(UserRoleTableSeeder::class);
         $this->call(SettingTableSeeder::class);
         $this->call(EmailTemplateSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(InfluencerTableSeeder::class);
         $this->call(SocialMediaTableSeeder::class);
         $this->call(SocialMediaAttributeTableSeeder::class);
    }

}
