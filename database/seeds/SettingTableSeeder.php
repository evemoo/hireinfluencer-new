<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('system_configurations')->truncate();

        $data = [
            // general
            'logo' => 'images/logo.png',
            'domain' => 'http://www.evemoo.com',
            'company' => [
                'en' => 'Evemoo Pvt. Ltd.',
                'np' => '',
                'fra' => '',
            ],
            'email' => 'info@evemoo.com',
            'phone' => [
                'en' => '',
                'np' => '',
                'fra' => '',
            ],
            'mobile' => [
                'en' => '',
                'np' => '',
                'fra' => '',
            ],
            'fax' => [
                'en' => '',
                'np' => '',
                'fra' => '',
            ],
            'location' => [
                'en' => '',
                'np' => '',
                'fra' => '',
            ],
            'map' => '',
            'working_hours' => [
                'en' => '',
                'np' => '',
                'fra' => '',
            ],
            'social_links' => [
                'fb' => [
                    'title' => 'Facebook',
                    'link' => '',
                    'icon' => '<i class="fa fa-facebook-f"></i>',
                ],
                'twitter' => [
                    'title' => 'Twitter',
                    'link' => '',
                    'icon' => '<i class="fa fa-twitter"></i>',
                ],
                'google' => [
                    'title' => 'Google+',
                    'link' => '',
                    'icon' => '<i class="fa fa-google"></i>',
                ],
                'linkedin' => [
                    'title' => 'LinkedIn',
                    'link' => '',
                    'icon' => '<i class="fa fa-linkedin"></i>',
                ],
            ],

            // seo
            'seo_title' => [
                'en' => 'title',
                'np' => '',
                'fra' => '',
            ],
            'seo_keywords' => [
                'en' => 'key1,key2',
                'np' => '',
                'fra' => '',
            ],
            'seo_description' => [
                'en' => 'description',
                'np' => '',
                'fra' => '',
            ],
            'contact_page_seo_title' => [
                'en' => '',
                'np' => '',
                'fra' => '',
            ],

            // configurations
            'admin_type_users' => ['admins'],
            'normal_type_user' => ['customer'], // clients
            'role_to_assign_after_registration' => 'customer', // this role is to be assigned after user is registered in front end

            // page contents
            'register_form_title_1' => [
                'en' => 'Sign Up',
                'np' => '',
                'fra' => '',
            ],
            'register_form_title_2' => [
                'en' => 'Please enter your details below',
                'np' => '',
                'fra' => '',
            ],
            'registration_success_message' => [
                'en' => 'Thank You, please check you email to verify your account.',
                'np' => '',
                'fra' => '',
            ],
            'registration_fail_message' => [
                'en' => 'Sorry, something went wrong.',
                'np' => '',
                'fra' => '',
            ],
            'login_form_title_1' => [
                'en' => 'Login',
                'np' => '',
                'fra' => '',
            ],
            'login_form_title_2' => [
                'en' => 'Please enter your details below',
                'np' => '',
                'fra' => '',
            ],
            'login_fail_message' => [
                'en' => 'Sorry, something went wrong.',
                'np' => '',
                'fra' => '',
            ],
            'password_reset_request_form_title_1' => [
                'en' => 'Password Reset Request',
                'np' => '',
                'fra' => '',
            ],
            'password_reset_request_form_title_2' => [
                'en' => 'Please enter your email address to get password reset link.',
                'np' => '',
                'fra' => '',
            ],
            'password_reset_request_success_message' => [
                'en' => 'Thank You, please check you email for the reset link.',
                'np' => '',
                'fra' => '',
            ],
            'password_reset_request_fail_message' => [
                'en' => 'Sorry, something went wrong.',
                'np' => '',
                'fra' => '',
            ],
            'password_reset_form_title_1' => [
                'en' => 'Password Reset',
                'np' => '',
                'fra' => '',
            ],
            'password_reset_form_title_2' => [
                'en' => 'Please enter your new password.',
                'np' => '',
                'fra' => '',
            ],
            'password_reset_success_message' => [
                'en' => 'Thank You, you password is changed.',
                'np' => '',
                'fra' => '',
            ],
            'password_reset_fail_message' => [
                'en' => 'Sorry, something went wrong.',
                'np' => '',
                'fra' => '',
            ],
            'paypal_status'     => 1,
            'paypal_client_id'  => config('paypal.client_id'),
            'paypal_secret'     => config('paypal.secret'),
            'stripe_status'     => 1,
            'stripe_client_id'  => config('stripe.publishable_key'),
            'stripe_secret'     => config('stripe.secret_key'),
            'geo_block'         => [],
            'ip_block'          => [],
        ];

        foreach ($data as $key => $value) {

            $setting = new \App\Evemoo\Models\Setting();
            $setting->key = $key;

            if (is_array($value)) {
                $setting->value = json_encode($value);
            } else {
                $setting->value = $value;
            }

            $setting->save();

        }

    }
}
