<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'display_name'  => 'Admin',
            'name'   => 'admin',
            'description' => 'THis guy is awsome he can access any thing',
            'enabled'      => 1,
        ]);

        DB::table('roles')->insert([
            'display_name'  => 'Brand',
            'name'   => 'brand',
            'description' => 'THis guy is brand',
            'enabled'      => 1,
        ]);

        DB::table('roles')->insert([
            'display_name'  => 'Influencer',
            'name'   => 'influencer',
            'description' => 'THis guy is influencer',
            'enabled'      => 1,
        ]);

    }
}
