<?php

use Illuminate\Database\Seeder;

class InfluencerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('influencers')->insert([
            'user_id'               => 3,
            'influencer_username'   => 'influencer',
            'registration_date'     => \Carbon\Carbon::now(),
            'verified_date'         => \Carbon\Carbon::now(),
            'seen_by_admin'         => 1,
            'average_rating'        => 5.2,
            'total_reviews'         => 5,
            'date_of_birth'         => \Carbon\Carbon::now(),
            'status'                => 1,
        ]);

        foreach (config('evemoo.lang.options') as $key => $value) {
            DB::table('influencer_translations')->insert([
                'entry_id'      => 1,
                'code'          => $value['code'],
                'message'       => 'Message',
                'about_you'     => 'About Me',
                'gender'        => 'Male',
                'country'       => 'Nepal',
                'state'         => 'Bagmati',
            ]);
        }

    }
}
