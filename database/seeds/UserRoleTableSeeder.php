<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $root     = \App\User::where('email', 'root123@gmail.com')->first();
        $brand = \App\User::where('email', 'brand123@gmail.com')->first();
        $influencer = \App\User::where('email', 'influencer123@gmail.com')->first();
        $influencer2 = \App\User::where('email', 'influencer2056@gmail.com')->first();
        $admin    = \App\Evemoo\Models\Role::where('name', 'admin')->first();
        $brand_role = \App\Evemoo\Models\Role::where('name', 'brand')->first();
        $influencer_role = \App\Evemoo\Models\Role::where('name', 'influencer')->first();

        DB::table('role_user')->insert([
            'user_id' => $root->id,
            'role_id' => $admin->id,
        ]);

        DB::table('role_user')->insert([
            'user_id' => $brand->id,
            'role_id' => $brand_role->id,
        ]);

        DB::table('role_user')->insert([
            'user_id' => $influencer->id,
            'role_id' => $influencer_role->id,
        ]);

        DB::table('role_user')->insert([
            'user_id' => $influencer2->id,
            'role_id' => $influencer_role->id,
        ]);
        
    }
}
