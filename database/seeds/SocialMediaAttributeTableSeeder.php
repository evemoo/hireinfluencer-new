<?php

use Illuminate\Database\Seeder;

class SocialMediaAttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        foreach (config('evemoo.social_media_attributes') as $key => $media) {
            DB::table('social_media_attributes')->insert([
                'slug'    => $key,
            ]);

            foreach (config('evemoo.lang.options') as $keyL => $lang) {
                DB::table('social_media_attribute_translations')->insert([
                    'entry_id'=> $i,
                    'code'    => $keyL,
                    'name'    => $media,
                ]);
            }
            $i++;
        }

    }
}