<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'parent_id'     => 0,
            'slug'          => 'one',
            'status'        => 1,
        ]);

        foreach (config('evemoo.lang.options') as $key => $value) {
            DB::table('category_translations')->insert([
                'entry_id'     => 1,
                'code'         => $value['code'],
                'title'        => 'title one '.$value['title'],
                'description'  => 'description one '.$value['title'],
            ]);
        }

        DB::table('categories')->insert([
            'parent_id'     => 1,
            'slug'          => 'two',
            'status'        => 1,
        ]);

        foreach (config('evemoo.lang.options') as $key => $value) {
            DB::table('category_translations')->insert([
                'entry_id'     => 2,
                'code'         => $value['code'],
                'title'        => 'title two '.$value['title'],
                'description'  => 'description two '.$value['title'],
            ]);
        }

        DB::table('categories')->insert([
            'parent_id'     => 2,
            'slug'          => 'three',
            'status'        => 1,
        ]);

        foreach (config('evemoo.lang.options') as $key => $value) {
            DB::table('category_translations')->insert([
                'entry_id'     => 3,
                'code'         => $value['code'],
                'title'        => 'title three '.$value['title'],
                'description'  => 'description three '.$value['title'],
            ]);
        }

    }
}