<?php

use Illuminate\Database\Seeder;

class SocialMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        foreach (config('evemoo.social_medias') as $key => $media) {
            DB::table('social_media')->insert([
                'slug'    => $key,
                'icon'    => $media['icon'],
            ]);

            foreach (config('evemoo.lang.options') as $keyL => $lang) {
                DB::table('social_media_translations')->insert([
                    'entry_id'=> $i,
                    'code'    => $keyL,
                    'name'    => $media['title'],
                ]);
            }
            
            $i++;
        }

    }
}