<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username'   => 'root',
            'email'  => 'root123@gmail.com',
            'password' => bcrypt('root123'),
            'first_name' => 'Root',
            'middle_name' => 'Root',
            'last_name'   => 'Root',
            'social_medias' => \AppHelper::social_links(),
            'enabled' => 1
        ]);

        DB::table('users')->insert([
            'username'   => 'brand123',
            'email'  => 'brand123@gmail.com',
            'password' => bcrypt('brand123'),
            'first_name' => 'Brand',
            'middle_name' => 'Brand',
            'last_name'   => 'Brand',
            'social_medias' => \AppHelper::social_links(),
            'enabled' => 1
        ]);

        DB::table('users')->insert([
            'username'   => 'influencer123',
            'email'  => 'influencer123@gmail.com',
            'password' => bcrypt('influencer123'),
            'first_name' => 'Influencer1',
            'middle_name' => 'Influencer1',
            'last_name'   => 'Influencer1',
            'social_medias' => \AppHelper::social_links(),
            'enabled' => 1
        ]);

        DB::table('users')->insert([
            'username'   => 'influencer2056',
            'email'  => 'influencer2056@gmail.com',
            'password' => bcrypt('influencer123'),
            'first_name' => 'Influencer2',
            'middle_name' => 'Influencer2',
            'last_name'   => 'Influencer2',
            'social_medias' => \AppHelper::social_links(),
            'enabled' => 1
        ]);
    }
}
