<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('slot_id');
            $table->unsignedInteger('delivery_in_days');
            $table->unsignedInteger('audience_size');
            $table->unsignedInteger('no_of_posts');
            $table->boolean('permanent_post');
            $table->boolean('additional_link');
            $table->double('price', 10, 4);
            $table->boolean('default_package');
            $table->integer('rank');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
