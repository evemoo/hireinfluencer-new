<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencers', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->string('influencer_username', 100);
            $table->timestamp('registration_date');
            $table->timestamp('verified_date')->nullable();
            $table->boolean('seen_by_admin')->default(0);
            $table->double('average_rating', 3,2)->default(0);
            $table->integer('total_reviews')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->boolean('status')->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencers');
    }
}
