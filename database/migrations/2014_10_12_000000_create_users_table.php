<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 45)->unique();
            $table->string('email', 45)->nullable();
            $table->string('password', 100)->nullable();
            $table->string('first_name', 45)->nullable();
            $table->string('middle_name', 45)->nullable();
            $table->string('last_name', 45)->nullable();
            $table->string('gender', 15)->nullable();
            $table->date('birth_date')->nullable();
            $table->string('primary_contact', 45)->nullable();
            $table->string('secondary_contact', 45)->nullable();
            $table->string('profile_image', 255)->nullable();
            $table->string('verify_token', 255)->nullable();
            // $table->unsignedInteger('fund')->default(0);
            $table->boolean('enabled')->default(0);
            $table->boolean('status')->default(0);
            $table->text('social_medias')->nullable(); // added
            $table->dateTime('last_login_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
