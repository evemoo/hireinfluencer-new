<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencerTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_translations', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('entry_id');
            $table->string('code', 5);
            $table->text('message')->nullable();
            $table->text('about_you')->nullable();
            $table->string('gender', 45)->nullable();
            $table->string('country', 45)->nullable();
            $table->string('state', 45)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_translations');
    }
}
