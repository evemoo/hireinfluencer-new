<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlotGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slot_galleries', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('slot_id');
            $table->text('image')->nullable();
            $table->addColumn('tinyInteger', 'rank', ['length' => 4, 'default' => '0']);
            $table->boolean('status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slot_galleries');
    }
}
