<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencerSocialDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_social_datas', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('influencer_id');
            $table->unsignedInteger('social_media_id');
            $table->unsignedInteger('social_media_attribute_id');
            $table->string('name', 45);
            $table->unsignedInteger('value');
            $table->text('profile_link');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_social_datas');
    }
}
