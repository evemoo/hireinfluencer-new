<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageExtraWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_extra_works', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('package_id');
            $table->double('extra_price', 10, 4);
            $table->unsignedInteger('extra_delivery_days');
            $table->boolean('status');
            $table->unsignedInteger('rank');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_extra_works');
    }
}
