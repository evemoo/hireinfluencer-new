<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slots', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('influencer_social_data_id');
            $table->unsignedInteger('influencer_id');
            $table->text('banner')->nullable();
            $table->boolean('status');
            $table->addColumn('tinyInteger', 'can_deliver_within', ['lenght' => 4, 'default' => '0']);
            $table->double('average_rating', 3, 2)->nullable();
            $table->integer('total_reviews')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slots');
    }
}
